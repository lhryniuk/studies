ALTER TABLE uzytkownicy ADD PRIMARY KEY (id_uzytkownika);
ALTER TABLE uzytkownicy ADD UNIQUE (login);
ALTER TABLE uzytkownicy ALTER COLUMN login SET NOT NULL;
ALTER TABLE uzytkownicy ALTER COLUMN email SET NOT NULL;
ALTER TABLE uzytkownicy ALTER COLUMN id_rangi SET NOT NULL;
ALTER TABLE uzytkownicy ALTER COLUMN haslo SET NOT NULL;

ALTER TABLE rangi ADD PRIMARY KEY (id_rangi);
ALTER TABLE rangi ALTER COLUMN nazwa_rangi SET NOT NULL;
ALTER TABLE rangi ADD UNIQUE (nazwa_rangi);

ALTER TABLE uprawnienia ADD PRIMARY KEY (id_operacji,id_rangi);
ALTER TABLE uprawnienia ALTER COLUMN id_operacji SET NOT NULL;
ALTER TABLE uprawnienia ALTER COLUMN id_rangi SET NOT NULL;
ALTER TABLE uprawnienia ALTER COLUMN wartosc SET NOT NULL;

ALTER TABLE operacje ADD PRIMARY KEY (id_operacji);
ALTER TABLE operacje ADD UNIQUE (nazwa_operacji);
ALTER TABLE operacje ALTER COLUMN nazwa_operacji SET NOT NULL;

ALTER TABLE utwory ADD PRIMARY KEY (id_utworu);
ALTER TABLE utwory ALTER COLUMN id_uzytkownika SET NOT NULL;

ALTER TABLE gatunki ADD PRIMARY KEY (id_gatunku);
ALTER TABLE gatunki ADD UNIQUE (nazwa_gatunku);
ALTER TABLE gatunki ALTER COLUMN nazwa_gatunku SET NOT NULL;

ALTER TABLE artysci ADD PRIMARY KEY(id_artysty);
ALTER TABLE artysci ALTER COLUMN nazwa_artysty SET NOT NULL;
ALTER TABLE artysci ADD UNIQUE (nazwa_artysty);

ALTER TABLE albumy ADD PRIMARY KEY (id_albumu);
ALTER TABLE albumy ALTER COLUMN nazwa_albumu SET NOT NULL;
ALTER TABLE albumy ADD UNIQUE (nazwa_albumu);

ALTER TABLE komentarze ADD PRIMARY KEY (id_komentarza);
ALTER TABLE komentarze ALTER COLUMN id_uzytkownika SET NOT NULL;
ALTER TABLE komentarze ALTER COLUMN id_utworu SET NOT NULL;

ALTER TABLE wystawione_oceny ADD PRIMARY KEY (id_uzytkownika, id_komentarza);
ALTER TABLE wystawione_oceny ALTER COLUMN data SET NOT NULL;

ALTER TABLE interpretacje_utworow ADD UNIQUE (id_utworu);
