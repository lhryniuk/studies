CREATE TABLE uzytkownicy(
login VARCHAR (15),
haslo VARCHAR (20),
email VARCHAR (30),
id_rangi INTEGER DEFAULT(0),
id_uzytkownika SERIAL);
CREATE TABLE utwory(
id_uzytkownika INTEGER,
id_artysty INTEGER,
tekst TEXT,
tytul TEXT,
rok_produkcji INTEGER,
id_albumu INTEGER,
wyswietlenia INTEGER DEFAULT(0),
id_utworu SERIAL,
nr_utworu INTEGER,
id_gatunku INTEGER);
CREATE TABLE gatunki(
id_gatunku SERIAL,
nazwa_gatunku VARCHAR(40));
CREATE TABLE artysci(
id_artysty SERIAL,
nazwa_artysty VARCHAR(40));
CREATE TABLE albumy(
id_albumu SERIAL,
rok_wydania INTEGER,
nazwa_albumu TEXT);
CREATE TABLE komentarze(
ocena INTEGER DEFAULT(0),
tresc TEXT,
id_utworu INTEGER,
id_uzytkownika INTEGER,
id_komentarza SERIAL,
data DATE DEFAULT(CURRENT_TIMESTAMP));
CREATE TABLE rangi(
id_rangi SERIAL,
waga_glosu INTEGER DEFAULT (1),
nazwa_rangi VARCHAR(15));
CREATE TABLE uprawnienia(
id_rangi INTEGER,
id_operacji INTEGER,
wartosc BOOLEAN);
CREATE TABLE operacje(
id_operacji SERIAL,
nazwa_operacji VARCHAR (20));
CREATE TABLE wystawione_oceny(
id_uzytkownika INTEGER,
id_komentarza INTEGER,
wartosc INTEGER,
data DATE DEFAULT(CURRENT_TIMESTAMP));
CREATE TABLE wysylajacy_zapytanie (
id_uzytkownika INTEGER NOT NULL);
CREATE TABLE interpretacje_utworow (
id_komentarza INTEGER NOT NULL,
id_utworu INTEGER NOT NULL);
