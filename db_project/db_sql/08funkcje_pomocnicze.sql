CREATE OR REPLACE FUNCTION wstaw_utwor (tytul TEXT, tekst TEXT, wykonawca VARCHAR (40), album TEXT, rok INTEGER, id_ INTEGER, gatunek VARCHAR (40))
RETURNS VOID AS $$
DECLARE krotka RECORD;
BEGIN
	IF (wykonawca NOT IN (SELECT nazwa_artysty FROM artysci)) THEN
		INSERT INTO artysci(nazwa_artysty) VALUES
		(wykonawca);
	END IF;
	IF (album NOT IN (SELECT nazwa_albumu FROM albumy)) THEN
		INSERT INTO albumy(nazwa_albumu, rok_wydania) VALUES
		(album, rok);
	END IF;
	IF (gatunek NOT IN (SELECT nazwa_gatunku FROM gatunki)) THEN
		RAISE EXCEPTION 'wybierz gatunek z listy dostepnych';
	END IF;
	SELECT id_gatunku, id_artysty, id_albumu INTO krotka FROM
	albumy,artysci,gatunki
	WHERE 
	nazwa_artysty=wykonawca AND
	nazwa_albumu=album AND
	albumy.rok_wydania=rok AND
	nazwa_gatunku=gatunek;
	INSERT INTO utwory (tytul, tekst, id_uzytkownika, id_gatunku, id_albumu, id_artysty) VALUES
	(tytul, tekst, id_, krotka.id_gatunku, krotka.id_albumu, krotka.id_artysty);
END;
$$ LANGUAGE 'plpgsql';

--CREATE OR REPLACE FUNCTION pobierz_id (nick NAME)
--RETURNS INTEGER AS $$
--BEGIN
	--RETURN (SELECT id_uzytkownika FROM uzytkownicy WHERE uzytkownicy.login=nick);
--END;
--$$ LANGUAGE 'plpgsql';	
