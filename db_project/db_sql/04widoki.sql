CREATE VIEW komentarze_uzytkownika AS
SELECT * FROM uzytkownicy JOIN komentarze USING (id_uzytkownika);

CREATE VIEW utwory_uzytkownika AS
SELECT * FROM uzytkownicy JOIN utwory USING (id_uzytkownika);

CREATE VIEW informacje_o_utworze AS
SELECT * FROM 
utwory  JOIN gatunki USING (id_gatunku) JOIN artysci USING (id_artysty) JOIN albumy USING (id_albumu);

CREATE OR REPLACE VIEW informacje_o_uzytkowniku AS
SELECT tmp1.id_uzytkownika, tmp1.login, tmp1.email, tmp1.nazwa_rangi AS ranga, tmp1.ocena, tmp1.liczba_komentarzy, tmp2.liczba_dodanych_utworow  
FROM
(
	SELECT 
	u.id_uzytkownika, u.email, u.login, r.nazwa_rangi, r.waga_glosu, COALESCE(COUNT(k.id_komentarza),0) AS liczba_komentarzy, COALESCE(SUM(k.ocena),0) AS ocena
	FROM
	uzytkownicy as u JOIN rangi as r USING (id_rangi) LEFT JOIN komentarze as k USING (id_uzytkownika)
	GROUP BY id_uzytkownika, email, login, r.nazwa_rangi, r.waga_glosu
) 
AS tmp1 JOIN
(
	SELECT 
	id_uzytkownika, COALESCE(COUNT(id_utworu),0) AS  liczba_dodanych_utworow
	FROM
	uzytkownicy LEFT JOIN utwory USING (id_uzytkownika)
	GROUP BY id_uzytkownika
)
AS tmp2 USING (id_uzytkownika)
ORDER BY tmp1.login;

CREATE VIEW rankingi_tygodnia_oceny AS
SELECT w.login, w.ocena FROM
(SELECT login, date_part('year',w.data)::INTEGER AS rok, date_part('week',w.data)::INTEGER AS tydzien, SUM(w.wartosc) AS ocena
FROM uzytkownicy AS u JOIN komentarze AS k USING (id_uzytkownika) JOIN wystawione_oceny AS w USING(id_komentarza) 
GROUP BY login,rok, tydzien
ORDER BY rok,tydzien,ocena) w
WHERE rok = EXTRACT(YEAR FROM now()) AND tydzien = EXTRACT(WEEK FROM now());

CREATE VIEW rankingi_miesiaca_oceny AS
SELECT w.login, w.ocena FROM
(SELECT login, date_part('year',w.data)::INTEGER AS rok, date_part('month',w.data)::INTEGER AS miesiac, SUM(w.wartosc) AS ocena
FROM uzytkownicy AS u JOIN komentarze AS k USING (id_uzytkownika) JOIN wystawione_oceny AS w USING(id_komentarza) 
GROUP BY login,rok, miesiac
ORDER BY rok,miesiac,ocena) w
WHERE rok = EXTRACT(YEAR FROM now()) AND miesiac = EXTRACT(MONTH FROM now());

CREATE VIEW rankingi_roku_oceny AS
SELECT w.login, w.ocena FROM
(SELECT login, date_part('year',w.data)::INTEGER AS rok, SUM(w.wartosc) AS ocena
FROM uzytkownicy AS u JOIN komentarze AS k USING (id_uzytkownika) JOIN wystawione_oceny AS w USING(id_komentarza) 
GROUP BY login,rok
ORDER BY rok, ocena) w
WHERE rok = EXTRACT(YEAR FROM now())
