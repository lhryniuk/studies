CREATE OR REPLACE FUNCTION usuniecie_rangi ()
RETURNS TRIGGER AS $$
BEGIN
	IF (OLD.id_rangi>0) THEN
		UPDATE uzytkownicy
		SET uzytkownicy.id_rangi=uzytkownicy.id_rangi-1
		WHERE uzytkownicy.id_rangi=OLD.id_rangi;
	ELSE
		RAISE EXCEPTION 'NIE USUWAJ RANGI nowy, ban';
	END IF;
	RETURN OLD;
END;
$$ LANGUAGE 'plpgsql';

DROP TRIGGER usuwanie_rangi
	ON rangi CASCADE;
CREATE TRIGGER usuwanie_rangi
	BEFORE DELETE ON rangi
	FOR EACH ROW EXECUTE PROCEDURE
	usuniecie_rangi();
	
		
CREATE OR REPLACE FUNCTION zmien_ocene()
RETURNS TRIGGER AS $$
BEGIN
	UPDATE komentarze
	SET ocena=ocena+NEW.wartosc
	WHERE id_komentarza=NEW.id_komentarza;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';


DROP TRIGGER zmiana_oceny ON wystawione_oceny CASCADE;
CREATE TRIGGER zmiana_oceny
	AFTER INSERT ON wystawione_oceny
	FOR EACH ROW EXECUTE PROCEDURE
	zmien_ocene();
	
CREATE OR REPLACE FUNCTION sila_glosu()
RETURNS INTEGER AS $$
DECLARE wynik INTEGER;
BEGIN
	wynik:=(SELECT waga_glosu FROM uzytkownicy JOIN rangi USING (id_rangi) JOIN wysylajacy_zapytanie USING (id_uzytkownika));
	RETURN wynik;
END;
$$LANGUAGE 'plpgsql';

	
CREATE OR REPLACE FUNCTION przypisz_wartosc()
RETURNS TRIGGER AS $$
BEGIN
	NEW.wartosc:=sila_glosu();
	RETURN NEW;
END;
$$LANGUAGE 'plpgsql';

DROP TRIGGER awpisz_wartosc ON wystawione_oceny CASCADE;
CREATE TRIGGER awpisz_wartosc
		BEFORE INSERT ON wystawione_oceny
		FOR EACH ROW EXECUTE PROCEDURE
		przypisz_wartosc();
		
		
CREATE OR REPLACE FUNCTION domyslne_interpretacje()
RETURNS TRIGGER AS $$
BEGIN
	IF (NEW.id_utworu NOT IN (SELECT id_utworu FROM interpretacje_utworow)) THEN
		INSERT INTO interpretacje_utworow (id_utworu, id_komentarza) VALUES (NEW.id_utworu, NEW.id_komentarza);
	END IF;
	RETURN NEW;
END;
$$LANGUAGE 'plpgsql';

DROP TRIGGER ustaw_domyslna_interpretacje ON komentarze CASCADE;
CREATE TRIGGER ustaw_domyslna_interpretacje 
	AFTER INSERT ON komentarze
	FOR EACH ROW EXECUTE PROCEDURE
	domyslne_interpretacje();
	
