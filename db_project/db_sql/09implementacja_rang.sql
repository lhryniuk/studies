
CREATE OR REPLACE FUNCTION sprawdz_uprawnienia(operacja NAME)
RETURNS BOOLEAN AS $$
DECLARE czy BOOLEAN;
BEGIN
	czy:=(SELECT wartosc FROM (SELECT w.id_uzytkownika, operacje.id_operacji, wartosc, nazwa_operacji FROM informacje_o_uzytkowniku
                               JOIN rangi ON (ranga=nazwa_rangi)
                               JOIN uprawnienia USING (id_rangi)
                               JOIN operacje ON (operacje.id_operacji=uprawnienia.id_operacji)
                               JOIN wysylajacy_zapytanie AS w USING (id_uzytkownika)) AS foo WHERE foo.nazwa_operacji=operacja);
	RETURN czy;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION obsluga_rang()
RETURNS TRIGGER AS $$
BEGIN
	IF (sprawdz_uprawnienia(TG_NAME)) THEN
		RAISE NOTICE 'wykonano operacje- uprawnienia ok';
	ELSE
		RAISE EXCEPTION 'brak uprawnien';
	END IF;
	DELETE FROM wysylajacy_zapytanie;
	IF(TG_name!='usuniecie_komentarza') THEN
		RETURN NEW;
	ELSE RETURN OLD;
	END IF;
END;
$$ LANGUAGE 'plpgsql';

DROP TRIGGER dodanie_tekstu ON utwory CASCADE;
CREATE TRIGGER dodanie_tekstu BEFORE INSERT ON utwory
FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

-- DROP TRIGGER dodanie_autora ON utwory CASCADE;
-- CREATE TRIGGER dodanie_autora BEFORE INSERT ON artysci
-- FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

-- DROP TRIGGER dodanie_albumu ON albumy CASCADE;
-- CREATE TRIGGER dodanie_albumu BEFORE INSERT ON albumy
-- FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

DROP TRIGGER dodanie_komentarza ON komentarze CASCADE;
CREATE TRIGGER dodanie_komentarza BEFORE INSERT ON komentarze
FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

DROP TRIGGER edycja_tekstu ON utwory CASCADE;
CREATE TRIGGER edycja_tekstu BEFORE UPDATE OF tekst ON utwory
FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

DROP TRIGGER edycja_komentarza ON utwory CASCADE;
CREATE TRIGGER edycja_komentarza BEFORE UPDATE OF tresc ON komentarze
FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

DROP TRIGGER wybor_interpretacji ON utwory CASCADE;
CREATE TRIGGER wybor_interpretacji BEFORE UPDATE OF id_komentarza ON interpretacje_utworow
FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

DROP TRIGGER usuniecie_komentarza ON komentarze CASCADE;
CREATE TRIGGER usuniecie_komentarza BEFORE DELETE ON komentarze
FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

DROP TRIGGER ocenianie ON wystawione_oceny CASCADE;
CREATE TRIGGER ocenianie BEFORE INSERT ON wystawione_oceny
FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

DROP TRIGGER zmiana_rangi ON uzytkownicy CASCADE;
CREATE TRIGGER zmiana_rangi BEFORE UPDATE ON uzytkownicy
FOR EACH ROW EXECUTE PROCEDURE obsluga_rang();

