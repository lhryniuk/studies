ALTER TABLE uzytkownicy ADD CHECK (email ~ '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$');
ALTER TABLE uzytkownicy ADD CHECK (haslo ~ '(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([[:graph:]]{6,15})$');
