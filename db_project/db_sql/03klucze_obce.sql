ALTER TABLE uzytkownicy ADD FOREIGN KEY (id_rangi) REFERENCES rangi(id_rangi) 
 ON UPDATE CASCADE;

ALTER TABLE uprawnienia ADD FOREIGN KEY (id_rangi) REFERENCES rangi(id_rangi) 
 ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE uprawnienia ADD FOREIGN KEY (id_operacji) REFERENCES operacje(id_operacji) 
 ON UPDATE CASCADE;

ALTER TABLE utwory ADD FOREIGN KEY (id_uzytkownika) REFERENCES uzytkownicy(id_uzytkownika) 
 ON UPDATE CASCADE;

ALTER TABLE utwory ADD FOREIGN KEY (id_gatunku) REFERENCES gatunki(id_gatunku) 
 ON UPDATE CASCADE;

ALTER TABLE utwory ADD FOREIGN KEY (id_artysty) REFERENCES artysci(id_artysty) 
 ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE utwory ADD FOREIGN KEY (id_albumu) REFERENCES albumy(id_albumu) 
 ON DELETE CASCADE ON UPDATE CASCADE;
 
ALTER TABLE komentarze ADD FOREIGN KEY (id_utworu) REFERENCES utwory(id_utworu) 
 ON DELETE CASCADE ON UPDATE CASCADE;
 
ALTER TABLE komentarze ADD FOREIGN KEY (id_uzytkownika) REFERENCES uzytkownicy(id_uzytkownika) 
 ON DELETE CASCADE ON UPDATE CASCADE;
 
ALTER TABLE wystawione_oceny ADD FOREIGN KEY (id_uzytkownika) REFERENCES uzytkownicy(id_uzytkownika)
 ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE wystawione_oceny ADD FOREIGN KEY (id_komentarza) REFERENCES komentarze(id_komentarza)
 ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE wysylajacy_zapytanie ADD FOREIGN KEY (id_uzytkownika) REFERENCES uzytkownicy (id_uzytkownika);

ALTER TABLE interpretacje_utworow ADD FOREIGN KEY (id_utworu) REFERENCES utwory (id_utworu);

ALTER TABLE interpretacje_utworow ADD FOREIGN KEY (id_komentarza) REFERENCES komentarze (id_komentarza);
