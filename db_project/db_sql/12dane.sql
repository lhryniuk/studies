INSERT INTO uzytkownicy(login, haslo, email, id_rangi) VALUES
('ll', 'lDal23lcxa#!5', 'lukequaint@gmail.com', 1),
('test1', 'test1a', 'test1@test.com', 1),
('test2', 'test2a', 'test2@test.com', 2),
('test3', 'test3a', 'test3@test.com', 3);
('test4', 'test4a', 'test4@test.com', 0);

INSERT INTO gatunki(nazwa_gatunku) VALUES ('rock'), ('pop'), ('rap'), ('country'), ('r&b');

INSERT INTO wysylajacy_zapytanie VALUES(2);
SELECT wstaw_utwor('12 groszy', '12 groszy, tylko nie płacz proszę', 'Kazik', '12 groszy', 1997, 2, 'rap');

INSERT INTO wysylajacy_zapytanie VALUES(2);
SELECT wstaw_utwor('Seven Nation Army', 'And I''m talking to myself at night Because I can''t forget', 'The White Stripes', 'Elephant', 2002, 2, 'rock');

INSERT INTO wysylajacy_zapytanie VALUES(2);
SELECT wstaw_utwor('Waterloo', 'Waterloo, Waterloo', 'ABBA', 'Waterloo', 1973, 2, 'pop');

INSERT INTO wysylajacy_zapytanie VALUES(2);
INSERT INTO komentarze(tresc, id_utworu, id_uzytkownika) VALUES ('test', 1, 2);

INSERT INTO wysylajacy_zapytanie VALUES(2);
INSERT INTO komentarze(tresc, id_utworu, id_uzytkownika, data) VALUES ('test2', 1, 2, '2014-05-03');

INSERT INTO wysylajacy_zapytanie VALUES(3);
INSERT INTO komentarze(tresc, id_utworu, id_uzytkownika, data) VALUES ('test22', 1, 3, '2015-01-15');

INSERT INTO wysylajacy_zapytanie VALUES(4);
INSERT INTO komentarze(tresc, id_utworu, id_uzytkownika, data) VALUES ('test222', 1, 4, '2015-02-02');

INSERT INTO wysylajacy_zapytanie VALUES(3);
INSERT INTO wystawione_oceny(id_uzytkownika, id_komentarza, data) VALUES (3, 1, '2014-05-03');

INSERT INTO wysylajacy_zapytanie VALUES(4);
INSERT INTO wystawione_oceny(id_uzytkownika, id_komentarza, data) VALUES (4, 2, '2015-01-15');

INSERT INTO wysylajacy_zapytanie VALUES(4);
INSERT INTO wystawione_oceny(id_uzytkownika, id_komentarza, data) VALUES (4, 3, '2015-01-15');
INSERT INTO wysylajacy_zapytanie VALUES(3);
INSERT INTO wystawione_oceny(id_uzytkownika, id_komentarza, data) VALUES (3, 3, '2015-02-02');

INSERT INTO wysylajacy_zapytanie VALUES(3);
INSERT INTO wystawione_oceny(id_uzytkownika, id_komentarza, data) VALUES (2, 3, now());
