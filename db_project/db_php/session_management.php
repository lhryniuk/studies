<?php

include_once 'connect_to_db.php';

function check_login($login) {
  $db = connect_to_db();

  $get_users_query = "SELECT login FROM uzytkownicy";
  $result = pg_query($db, $get_users_query);

  while ($row = pg_fetch_array($result)) {
    if ($row[0] == $login) {
      disconnect_db($result, $db);
      return True;
    }
  }

  disconnect_db($result, $db);
  return False;
}

function check_passwords($password, $password_repeat) {
  return $password != $password_repeat;
}

function check_email($email) {
  return True;
}

function add_user_to_db($login, $email, $pass) {
  $db = connect_to_db();

  $push_user_query = "INSERT INTO uzytkownicy(login, haslo, email, id_rangi) VALUES ('$login', '$pass', '$email', 0);";
  $result = pg_query($db, $push_user_query);

  if (!$result)
  {
    die("Błąd bazy danych!" . pg_last_error());
  }

  return True;
}

function register_user() {
  $login = $_POST["inputLogin"];
  $email = $_POST["inputEmail"];
  $passw = $_POST["inputPassword"];
  $passr = $_POST["inputPasswordRepeat"];
  if (check_login($login) &&
      check_email($email) &&
      check_password($passw, $passr)) {
    add_user_to_db($login, $email, $passw);
    echo '<p class="lead">Możesz się teraz zalogować</p>';
  } else {
    echo '<p class="lead">Błąd</p>';
  }
}

function get_user_from_db($login, $pass) {
  $db = connect_to_db();
  $get_user_query = "SELECT login FROM uzytkownicy WHERE login = '$login' AND haslo = '$pass'";
  $result = pg_query($db, $get_user_query);
  if (!pg_num_rows($result)) {
    echo "Podałeś nieprawidłowy login lub hasło.<br />";
    disconnect_db($result, $db);
    return False;
  }
  disconnect_db($result, $db);
  return True;
}

function signin() {

  $activeLogin = $_POST["activeLogin"];
  $activePassword = $_POST["activePassword"];
  if (get_user_from_db($activeLogin, $activePassword)) {
    $_SESSION['login'] = $activeLogin;
    $_SESSION['passw'] = $activePassword;
    echo "<script>window.top.location='index.php'</script>";
    return True;
  }
  echo "Błąd przy logowaniu";
  return False;
}

?>
