<?php
include_once 'connect_to_db.php';

function register_user_action($db, $uid) {
  $user_info_remove_query = "DELETE FROM wysylajacy_zapytanie *";
  pg_query($db, $user_info_remove_query);
  $user_info_query = "INSERT INTO wysylajacy_zapytanie(id_uzytkownika) VALUES ($uid)";
  pg_query($db, $user_info_query);
}

function get_login_from_uid($uid) {
  $db = connect_to_db();
  $find_user_id_query = "SELECT login FROM uzytkownicy WHERE id_uzytkownika = '$uid'";
  $result = pg_query($db, $find_user_id_query);
  $user_row = pg_fetch_array($result, 0);
  $login = $user_row[0];
  return $login;
}

function get_uid_from_login($login) {
  $db = connect_to_db();
  $find_user_id_query = "SELECT id_uzytkownika FROM uzytkownicy WHERE login = '$login'";
  $result = pg_query($db, $find_user_id_query);
  $user_row = pg_fetch_array($result, 0);
  $uid = $user_row[0];
  return $uid;
}

function check_ban($db) {
  if (!empty($_GET['bnid'])) {
    $bnid = $_GET['bnid'];
    $uid = get_uid_from_login($_SESSION['login']);

    register_user_action($db, $uid);
    $ban_user_query = "UPDATE uzytkownicy SET id_rangi = -1 WHERE id_uzytkownika = $bnid";
    pg_query($db, $ban_user_query);
    echo "<script>window.top.location='index.php?p=users'</script>";
  }
}

function max_rank($db) {
  $find_max_rank_query = "SELECT max(id_rangi) FROM rangi";
  $result = pg_query($db, $find_max_rank_query);
  $user_row = pg_fetch_array($result, 0);
  $rid = $user_row[0];
  return $rid;
}

function check_adv($db) {
  if (!empty($_GET['avid'])) {
    $avid = $_GET['avid'];
    $uid = get_uid_from_login($_SESSION['login']);

    register_user_action($db, $uid);
    $adv_user_query = 'UPDATE uzytkownicy SET id_rangi = least(' . max_rank($db) . ", id_rangi + 1) WHERE id_uzytkownika = $avid";
    pg_query($db, $adv_user_query);
    echo "<script>window.top.location='index.php?p=users'</script>";
  }
}

function get_users() {
    $db = connect_to_db();
    $get_osoby_table = "SELECT * FROM informacje_o_uzytkowniku";
    $result = pg_query($db, $get_osoby_table);

    check_ban($db);
    check_adv($db);
    if (!$result)
    {
        die("Nie mogę wczytać listy użytkowników: " . pg_last_error());
    }

    echo '<table>';
    echo '<tr><th>login</th> <th>e-mail</th> <th>ranga</th> <th>ocena</th><th>liczba komentarzy</th> <th>liczba dodanych utworów</th><th>akcje</th></tr>';
    while ($row = pg_fetch_array($result)) {
        echo '<tr>';
        echo '<td>' . $row[1] . '</td>';
        echo '<td>' . $row[2] . '</td>';
        echo '<td>' . $row[3] . '</td>';
        echo '<td>' . (isset($row[4]) ? $row[4] : 0) . '</td>';
        echo '<td>' . $row[5] . '</td>';
        echo '<td>' . $row[6] . '</td>';
        echo '<td>' .
        '<a href="' . $_SERVER['PHP_SELF'] . '?p=users&bnid=' . "$row[0]" .
        '" ><button type="button" class="btn btn-xs btn-danger">Ban</button>

        </a>' .
        '<a href="' . $_SERVER['PHP_SELF'] . '?p=users&avid=' . "$row[0]" .
        '" ><button type="button" class="btn btn-xs btn-success">Awans</button></a>';
        echo '</tr>';
    }
    echo "<table>";
}

function get_rank($rank_name) {
  $db = connect_to_db();
  $get_osoby_table = "SELECT login, ocena FROM $rank_name ORDER BY ocena DESC";
  $result = pg_query($db, $get_osoby_table);

  if (!$result)
  {
    die("Nie mogę wczytać listy użytkowników z rankingu $rank_name: " . pg_last_error());
  }
  return $result;
}

function display_rank_table($rank) {
  while ($row = pg_fetch_array($rank)) {
    echo '<tr>';
    echo '<td>' . $row[0] . '</td>';
    echo '<td>' . $row[1] . '</td>';
    echo '</tr>';
  }
}

function display_rank($title, $rank_name) {
  echo '<h3>' . $title . '</h3>' . '<table style="margin: 30px">';
  echo '<tr><th>login</th> <th>ocena</th></tr>';
  display_rank_table(get_rank($rank_name));
  echo "<table>";
}

function display_ranks() {
  echo '<div style="width: 700px; margin: auto">';
  display_rank("Ranking tygodniowy", "rankingi_tygodnia_oceny");
  display_rank("Ranking miesięczny", "rankingi_miesiaca_oceny");
  display_rank("Ranking roczny", "rankingi_roku_oceny");
  echo '</div>';
}

?>
