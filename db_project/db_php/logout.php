<?php

function logout() {
  session_start();
  unset($_SESSION['login']);
  unset($_SESSION['passw']);
  $_SESSION = array();
  header("Location: ../index.php");
}

logout();

?>
