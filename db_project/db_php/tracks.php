<?php

include_once 'connect_to_db.php';
include_once 'get_users.php';

function get_genres() {
  $genres = '';
  $db = connect_to_db();
  $get_genres_table = "SELECT * FROM gatunki";
  $result = pg_query($db, $get_genres_table);

  if (!$result)
  {
    die("Nie mogę wczytać listy gatunków: " . pg_last_error());
  }


  while ($row = pg_fetch_array($result)) {
    $genres .= '<option value="' . "$row[0]" . '"' . ">$row[1]</option>";
  }
  return $genres;
}

function get_genre_name_from_id($id) {
  $db = connect_to_db();
  $get_genre_name_query = "SELECT nazwa_gatunku FROM gatunki WHERE id_gatunku = '$id'";
  $result = pg_query($db, $get_genre_name_query);
  $genre_row = pg_fetch_array($result, 0);
  $name = $genre_row[0];
  return $name;
}

function add_view($db, $tid) {
  $add_view_query = "UPDATE utwory SET wyswietlenia = wyswietlenia + 1 WHERE id_utworu = '$tid'";
  pg_query($db, $add_view_query);
}

function edit_track($edid) {
  $text = $_POST["text"];
  $login = $_SESSION['login'];
  $uid = get_uid_from_login($login);
  echo "edytuje!";
  $db = connect_to_db();
  register_user_action($db, $uid);
  $update_track_query = "
  UPDATE utwory SET
  tekst = '$text'
  WHERE id_utworu = '$edid'
  ";
  $result = pg_query($db, $update_track_query);

  echo '<div class="container">  <div class="starter-template" style="margin-top: 100px">';

  if (!$result) {
    echo "Nie można dodać utworu";
    echo "</div></div>";
    return False;
  }
  echo "</div></div>";
  echo "<script>window.top.location='index.php?t=$edid'</script>";

}

function add_track() {
  $title = $_POST["title"];
  $author = $_POST["author"];
  $album = $_POST["album"];
  $year = $_POST["year"];
  $text = $_POST["text"];
  $genre = get_genre_name_from_id($_POST["genre"]);
  $login = $_SESSION['login'];
  $uid = get_uid_from_login($login);

  $db = connect_to_db();
  register_user_action($db, $uid);
  $insert_track_query = "SELECT wstaw_utwor('$title', '$text', '$author', '$album', $year, $uid, '$genre')";
  $result2 = pg_query($db, $insert_track_query);

  echo '<div class="container">  <div class="starter-template" style="margin-top: 100px">';

  if (!$result2) {
    echo "Nie można dodać utworu";
  } else {
    echo "Utwór dodany!";
  }
  echo "</div></div>";
}

function add_comment($tid) {
  $content = $_POST['comment'];
  $uid = get_uid_from_login($_SESSION['login']);
  $add_comment_query = "INSERT INTO komentarze(tresc, id_utworu, id_uzytkownika) VALUES ('$content', $tid, $uid);";
  $db = connect_to_db();
  register_user_action($db, $uid);
  $result = pg_query($db, $add_comment_query);
  if (!$result) {
    echo "Nie udało się dodać komentarza.<br />";
  }
  echo "<script>window.top.location='index.php?t=$tid'</script>";
}

function change_comment_note($db, $uid, $cid, $note, $tid) {
  register_user_action($db, $uid);

  $comment_note_query = "INSERT INTO wystawione_oceny(id_uzytkownika, id_komentarza) VALUES ($uid, $cid)";
  pg_query($db, $comment_note_query);

  echo "<script>window.top.location='index.php?t=$tid'</script>";
}

function display_comments($db, $tid) {
  $get_comments_query = "SELECT * FROM komentarze WHERE id_utworu = $tid ORDER BY ocena DESC";
  $result = pg_query($db, $get_comments_query);
  $comments = '';

  while ($row = pg_fetch_array($result)) {
    $comments .= '<div class="comment">';
    $uid = get_uid_from_login($_SESSION['login']);
    $note = $row[0];
    $content = $row[1];
    $cid = $row[4];
    $login = get_login_from_uid($row[3]);
    $date = $row[5];
    $comments .= '<div class="comment_header">' . "[$note] ";
    $comments .= '<a class="comment_up" href="' . $_SERVER['PHP_SELF'] . '?t=' . "$tid&" . 'vote=1&uid=' . $uid . '&cid=' . $cid . '" >+</a>'. "</div>\n";
    $comments .= '<div style="float:right">' . " dodał: $login $date" . '</div>' . "\n";
    $comments .= '<div style="clear:both"><p>' . nl2br($content) . '</p></div>' . "\n";
    $comments .= '<div style="float:right">' .
                  '<a href="' . $_SERVER['PHP_SELF'] . '?t=' . "$tid" . '&rid=' . $cid .
                  '" ><button class="btn btn-xs btn-danger">Usuń</button></a></div>' . "\n";
    $comments .= '</div>' . "\n";
  }
  return $comments;
}

function check_remove($db, $tid) {
  if (!empty($_GET['rid'])) {
    $uid = get_uid_from_login($_SESSION['login']);
    $rid = $_GET['rid'];
    echo "$uid chce usunąć $rid";

    register_user_action($db, $uid);
    $rm_interpret_query = "DELETE FROM interpretacje_utworow WHERE id_komentarza = $rid";
    pg_query($db, $rm_interpret_query);

    register_user_action($db, $uid);
    $rm_comment_query = "DELETE FROM komentarze WHERE id_komentarza = $rid";
    pg_query($db, $rm_comment_query);
    echo "<script>window.top.location='index.php?t=$tid'</script>";
  }
}

function check_rating($db, $tid) {
  if (!empty($_GET['vote']) && !empty($_GET['uid']) && !empty($_GET['cid'])) {
    change_comment_note($db, $_GET['uid'], $_GET['cid'], intval($_GET['vote']), $tid);
  }
}

function display_track($tid) {

  if (isset($_POST['comok'])) {
    if (add_comment($tid)) {
      echo "Dodano komentarz";
    }
  }
  $db = connect_to_db();
  add_view($db, $tid);
  check_remove($db, $tid);
  check_rating($db, $tid);

  $get_track_query = "SELECT * FROM informacje_o_utworze WHERE id_utworu = $tid";
  $result = pg_query($db, $get_track_query);
  $row = pg_fetch_row($result, 0);
  $added_by_uid = $row[3];
  $added_by_login = get_login_from_uid($added_by_uid);
  $title = $row[5];
  $genre = $row[10];
  $artist = $row[11];
  $album = $row[13];
  $text = $row[4];
  $year = $row[12];
  $visits = $row[7];

  $comments = display_comments($db, $tid);

  $comment_form = '<div class="add_comment_form">
  <form class="form-horizontal" method="post" style="width: 300px">
  <fieldset>

  <!-- Form Name -->
  <legend>Dodaj komentarz</legend>

  <!-- Textarea -->
  <div class="form-group">
    <label class="col-md-8 control-label" for="comment"></label>
    <div class="col-md-8">
      <textarea class="form-control" id="comment" name="comment" style="width: 300px"></textarea>
    </div>
  </div>

  <!-- Button -->
  <div class="form-group">
    <label class="col-md-8 control-label" for="add"></label>
    <div class="col-md-8">
      <button id="comok" name="comok" class="btn btn-warning">Dodaj komentarz</button>
    </div>
  </div>

  </fieldset>
  </form>
  </div>';

  $data = '<div class="container" style="margin-top: 7%">' .
          '<div class="jumbotron" style="float: left">' .
          '<div class="song_box">' .
          '<div class="song_data">' .
          "<h4>$title</h4>
          <h5>$artist</h5>
          <h6>$album ($year)</h6>---<br />" .
          nl2br($text) .
          '</div>' .
          '<div style="width: 130px; margin-top: 3px; font-size: 11px; float: right">' .
          "dodał: $added_by_login <br />" .
          "wyświetleń: $visits <br />" .
          "gatunek: $genre <br /><br />" .
          '<a href="' . $_SERVER['PHP_SELF'] . '?p=edit_track&edid=' . "$tid" .
          '" ><button type="button" class="btn btn-sm btn-info">Edytuj tekst</button>
          </a>' .
          '</div>' .
          '</div>' .
          '<div style="width: 400px; float: right">' .
          "$comment_form" .
          "$comments" .
          '</div>
          </div>
          </div>';

  echo $data;
  return True;
}

function get_tracks_from_query($db, $query) {
  $result = pg_query($db, $query);
  $tracks = '';
  while ($row = pg_fetch_array($result)) {
    $tracks .= "<li><a href=index.php?t=$row[8]>";
    $tracks .= $row[5] . ' - ' . $row[11];
    $tracks .= "</a></li>";
  }
  return $tracks;
}

function get_tracks($pattern='') {
  $db = connect_to_db();
  if ($pattern == '') {
    $get_tracks_query = "SELECT * FROM informacje_o_utworze";
    $result = pg_query($db, $get_tracks_query);
    echo "<ul>";
    while ($row = pg_fetch_array($result)) {
      echo "<li><a href=index.php?t=$row[8]>";
      echo $row[5] . ' - ' . $row[11];
      echo "</a></li>";
    }
    echo "</ul>";
  } else {
    echo '<div class="container" style="margin-top: 7%; width: 1000px">
    <div class="jumbotron" style="min-height: 600px">
    <h3>Wyniki wyszukiwania</h3>';
    echo '<ul>';

    $tracks = '';
    $pattern = strtolower($pattern);
    $get_tracks_query = "SELECT * FROM informacje_o_utworze
                         WHERE lower(tytul) ~ '$pattern'
                         OR lower(nazwa_artysty) ~ '$pattern'
                         OR lower(tekst) ~ '$pattern'";
    $tracks .= get_tracks_from_query($db, $get_tracks_query);

    if (empty($tracks)) {
      echo "Nic nie znaleziono";
    } else {
      echo $tracks;
    }
    echo "</ul>";
    echo '</div>';
  }

  return True;
}

function search_results($pattern) {
  get_tracks($pattern);
}

?>
