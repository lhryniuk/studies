
<?php
include_once 'db_php/tracks.php';

if (!isset($_SESSION['login'])) {
  ?>
  <div class="container" style="width: 400px; margin: auto; align: center">
    <div class="starter-template" style="margin-top: 50%">
      <p class="lead"> Zaloguj się, aby dodać utwór </p>
    </div>
  </div>
  <?php
} else {
  if (!isset($_POST['addok'])) { ?>
    <div style="margin: auto">
      <div class="container">
        <div class="starter-template" style="margin-top: 100px">
          <form class="form-horizontal" method="post">
            <fieldset>

              <!-- Form Name -->
              <legend>Dodaj utwór</legend>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="title">Tytuł</label>
                <div class="col-md-4">
                  <input id="title" name="title" placeholder="podaj tytuł utworu" class="form-control input-md" type="text">

                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="author">Wykonawca</label>
                <div class="col-md-4">
                  <input id="author" name="author" placeholder="podaj wykonawcę" class="form-control input-md" type="text">

                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="album">Album</label>
                <div class="col-md-4">
                  <input id="album" name="album" placeholder="album" class="form-control input-md" type="text">

                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="year">Rok produkcji</label>
                <div class="col-md-2">
                  <input id="year" name="year" placeholder="rok" class="form-control input-md" type="text">

                </div>
              </div>

              <!-- Select Basic -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="genre">Gatunek</label>
                <div class="col-md-4">
                  <select id="genre" name="genre" class="form-control">
                    <?php
                      echo get_genres();
                    ?>

                  </select>
                </div>
              </div>

              <!-- Textarea -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="text">Tekst utworu</label>
                <div class="col-md-4">
                  <textarea class="form-control" id="text" name="text"></textarea>
                </div>
              </div>

              <!-- Button -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="add"></label>
                <div class="col-md-4">
                  <button id="addok" name="addok" class="btn btn-success">Dodaj utwór</button>
                </div>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
      <?php
    } else {
      include_once 'db_php/tracks.php';
      add_track();
    }
  }
  ?>
    </div>
