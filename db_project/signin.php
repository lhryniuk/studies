
<div style="width: 300px; margin: auto; margin-top: 20%">

  <?php
  include 'db_php/session_management.php';
  if (!isset($_POST['signinok'])) { ?>
    <form class="form-signin" method="post">
      <h2 class="form-signin-heading">Logowanie</h2>
      <label for="activeLogin" class="sr-only">login</label>
      <input type="login" name="activeLogin" id="activeLogin" class="form-control" placeholder="login" required autofocus>
      <label for="activePassword" class="sr-only">hasło</label>
      <input type="password" name="activePassword" id="activePassword" class="form-control" placeholder="hasło" required>
      <button class="btn btn-lg btn-primary btn-block" name="signinok" type="submit">Zaloguj</button>
    </form>
    <?php
  } else {
    signin();
  }
  ?>
</div>
