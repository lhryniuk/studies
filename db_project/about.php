<div class="jumbotron">
  <div class="container" style="margin-top: 100px; height: ">
    <h2>O stronie</h2>
    <p>
      Strona ta powstała jako projekt zaliczeniowy z przedmiotu bazy danych. Daje użytkownikom
      możliwość dzielenia się z innymi tekstami swoich ulubionych utwórów oraz ich komentowania.
      Zachęcamy, by komentarze były interpretacjami bądź pozwalały innym użytkownikom dowiedzieć
      się czegoś więcej o danym utworze (np. o szczegółach jego powstania).
    </p>
    <p>
      Oczywiście to użytkownicy stworzą tę stronę. Mamy nadzieję, że jakkolwiek zostanie ona
      wykorzystana, każdy z przyjemnością będzie spędzał tu czas.
    </p>
    <p>
      Autorzy <br />
      Łukasz Hryniuk <br />
      Jakub Kowalisko
    </p>
  </div>
</div>
