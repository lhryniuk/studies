<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../favicon.ico">

  <title>Projekt z baz danych</title>

  <!-- Bootstrap core CSS -->
  <link href="dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="lq_styles.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <?php
    $login = (isset($_SESSION['login']) ? $_SESSION['login'] : NULL);
    $passw = (isset($_SESSION['passw']) ? $_SESSION['passw'] : NULL) ;
  ?>
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="index.php">Strona główna</a></li>
          <li><a href="index.php?p=new_track">Dodaj utwór</a></li>
          <li><a href="index.php?p=users">Użytkownicy</a></li>
          <li><a href="index.php?p=ranks">Rankingi</a></li>
          <li><a href="index.php?p=about">O stronie</a></li>
        </ul>

          <?php
            if (!$login || !$passw) {?>
              <div style="margin-left: 50px; float: right; width: 30%; margin-top: 10px">
                <button type="button" onclick="location.href='index.php?p=registration'" class="btn btn btn-primary">Rejestracja</button>
                <button type="button" onclick="location.href='index.php?p=signin'" class="btn btn btn-success">Zaloguj się</button>
              </div>
            <?php
          } else {?>
            <div style="margin-left: 50px; float: right; width: 30%; margin-top: 10px">
            <?php
              echo '<span style="color: white; font-size: 15px; font-weight: bold; margin-right: 30px">' . $login . '</span>';
            ?>
            <button type="button" onclick="location.href='db_php/logout.php'" class="btn btn btn-danger">Wyloguj</button>
            </div>
          <?php
          }
          ?>
        </div>
        <div style="margin-left: 10px; float: left; width: 800px; height: 30px">
          <form style="display: inline" method="get">
            <input type="text" name="str" size="40">
            <button class="btn btn-xs btn-primary btn-success" type="submit">Wyszukaj</button>
          </form>
        </div>
      </div>
  </nav>

  <?php
  include_once 'db_php/tracks.php';
  if (!empty($_GET['p'])) {
    include($_GET['p'] . '.php');
  } elseif (!empty($_GET['t'])) {
    display_track($_GET['t']);
  } elseif (!empty($_GET['str'])) {
    search_results($_GET['str']);
  } else {?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container" style="margin-top: 100px">
        <?php
        if (isset($_SESSION['login'])) {
          echo "<h2>Witaj " . $_SESSION['login'] . " na naszej stronie!</h2>";
        }
        ?>
<div style="width: 400px; float: left">
        <p>
          Znajdź swój ulubiony utwór i dodaj interpretacje lub, jeśli go nie ma, dodaj sam utwór, korzystając z menu na górze strony.
        </p>
        <p>
          Możesz też przeglądać i oceniać istniejące interpretacje. Jeśli Twoja interpretacja spodoba się innym, zyskasz punkty,
          które przełożą się na Twoje uprawnienia na stronie.
        </p>
        <p>
          Mamy nadzieję, że korzystnie z niej będzie dla Ciebie miłym doświadczeniem i wkrótce znów nas odwiedzisz.
        </p>
        <h3>Utwory na naszej stronie:</h3>
        <?php
        get_tracks();
        ?>
</div>
<div style="clear: right; width: 600px; float: right">
<iframe width="600" height="450" src="https://www.youtube.com/embed/dQw4w9WgXcQ" frameborder="0" allowfullscreen></iframe>
</div>
      </div>
    </div>
    <?php
  }
  ?>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="../../dist/js/bootstrap.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
