
<div style="width: 300px; margin: auto; margin-top: 22%">

  <?php
    include 'db_php/session_management.php';
  if (!isset($_POST['registerok'])) { ?>
    <form class="form-signin" method="post">
    <h2 class="form-signin-heading">Rejestracja</h2>
    <label for="inputEmail" class="sr-only">login</label>
    <input type="login" name="inputLogin" id="inputLogin" class="form-control" placeholder="login" required autofocus>
    <label for="inputEmail" class="sr-only">adres e-mail</label>
    <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="adres e-mail" required autofocus>
    <label for="inputPassword" class="sr-only">hasło</label>
    <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="hasło" required>
    <input type="password" name="inputPasswordRepeat" id="inputPasswordRepeat" class="form-control" placeholder="powtórz hasło" required>
    <button class="btn btn-lg btn-primary btn-block" name="registerok" type="submit">Utwórz konto</button>
    </form>
  <?php
    } else {
      register_user();
    }
  ?>
</div>
