
<?php
include_once 'db_php/tracks.php';
$edid = $_GET['edid'];

if (!isset($_SESSION['login'])) {
  ?>
  <div class="container" style="width: 400px; margin: auto; align: center">
    <div class="starter-template" style="margin-top: 50%">
      <p class="lead"> Zaloguj się, aby dodać utwór </p>
    </div>
  </div>
  <?php
} else {
  if (!isset($_POST['editok'])) { ?>
    <div style="margin: auto">
      <div class="container">
        <div class="starter-template" style="margin-top: 100px">
          <form class="form-horizontal" method="post">
            <fieldset>

              <!-- Form Name -->
              <legend>Edytuj utwór</legend>

              <?php
                include_once 'db_php/connect_to_db.php';
                $db = connect_to_db();

                $get_track_query = "SELECT * FROM informacje_o_utworze WHERE id_utworu = $edid";
                $result = pg_query($db, $get_track_query);
                $row = pg_fetch_row($result, 0);
                $title = $row[5];
                $genre = $row[10];
                $artist = $row[11];
                $album = $row[13];
                $text = $row[4];
                $year = $row[12];
                $visits = $row[7];
              ?>


              <!-- Textarea -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="text">Tekst utworu</label>
                <div class="col-md-4">
                  <textarea class="form-control" id="text" name="text"><?php echo $text;?></textarea>
                </div>
              </div>

              <!-- Button -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="add"></label>
                <div class="col-md-4">
                  <button id="editok" name="editok" class="btn btn-info">Edytuj utwór</button>
                </div>
              </div>

            </fieldset>
          </form>
        </div>
      </div>
      <?php
    } else {
      include_once 'db_php/tracks.php';
      edit_track($edid);
    }
  }
  ?>
    </div>
