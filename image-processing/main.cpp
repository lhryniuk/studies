#include <QApplication>
#include <QColor>
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QImage>
#include <QPainter>
#include "qpainter.h"
#include <QPixmap>
#include <QString>
#include <QtCore/QCoreApplication>


#include "op/AllOps.hpp"
#include "op/Compose.hpp"
#include "op/Averages.hpp"
#include "op/DetectEdges.hpp"

#include "util/IO.hpp"
#include "util/Convert.hpp"

#include "armadillo/include/armadillo"

auto read_as_real_matrix = util::MonoQImageToMat{} * util::LoadImage{};

auto save_matrix_to_image(const std::string& output_file_path, arma::mat A)
{
    auto save = util::SaveImage{output_file_path} * util::MatToMonoQImage{};
    save(A);
}

auto save_matrix_to_image(const std::string& output_file_path, arma::cx_mat A)
{
    auto save_real = util::SaveImage{"real_" + output_file_path} * util::MatToMonoQImage{};
    auto save_imag = util::SaveImage{"imag_" + output_file_path} * util::MatToMonoQImage{};

    save_real(real(A));
    save_imag(imag(A));
}

auto transform_mono_image = [](
        const std::string& input_file_path,
        const std::string& output_file_path,
        auto ops)
{
    arma::mat A = read_as_real_matrix(input_file_path);
    auto result = ops(A);
    save_matrix_to_image(output_file_path, result);
};

int main(int argc, char *argv[])
{
}
