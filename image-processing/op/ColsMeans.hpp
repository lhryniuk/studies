#pragma once

#include "../armadillo/include/armadillo"


namespace op
{


struct ColsMeans
{
    arma::vec operator()(arma::mat A) const
    {
        arma::vec v(A.n_cols);

        for (auto i = 0u; i < A.n_cols; ++i) {
            v(i) = arma::mean(A.col(i));
        }

        return v;
    }
};


} // namespace op
