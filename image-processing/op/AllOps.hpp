#pragma once

#include "Averages.hpp"
#include "ColorsCdf.hpp"
#include "ColorsHistogram.hpp"
#include "Custom.hpp"
#include "FixUsingMask.hpp"
#include "Histogram.hpp"
#include "LogTransform.hpp"
#include "ScaleValues.hpp"
#include "Segment.hpp"
#include "SwapQuarters.hpp"
