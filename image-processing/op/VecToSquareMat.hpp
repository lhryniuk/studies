#pragma once

#include "ColorsHistogram.hpp"

#include "../armadillo/include/armadillo"

#include <algorithm>
#include <vector>


namespace op
{


struct VecToSquareMat
{
    arma::mat operator()(arma::vec v) const
    {
        const auto size = int(std::sqrt(v.n_rows));
        arma::mat A(size, size);

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                A(i, j) = v(i * size + j);
            }
        }

        return A;
    }
};


} // namespace op
