#pragma once

#include <utility>


namespace op
{

template<typename F, typename G>
auto compose(F&& f, G&& g)
{
    return [f=std::forward<F>(f),g=std::forward<G>(g)](auto x){return f(g(x));};
};

template<typename F, typename... GS>
auto compose(F&& f, GS&&... gs)
{
    return compose(std::forward<F>(f), compose(std::forward<GS>(gs)...));
}

template<typename F, typename G>
auto operator*(F&& f, G&& g)
{
    return compose(f, g);
}

} // namespace op

// FIXME: duplicate to enable ADL
namespace util
{

template<typename F, typename G>
auto compose(F&& f, G&& g)
{
    return [f=std::forward<F>(f),g=std::forward<G>(g)](auto x){return f(g(x));};
};

template<typename F, typename... GS>
auto compose(F&& f, GS&&... gs)
{
    return compose(std::forward<F>(f), compose(std::forward<GS>(gs)...));
}

template<typename F, typename G>
auto operator*(F&& f, G&& g)
{
    return compose(f, g);
}

} // namespace util
