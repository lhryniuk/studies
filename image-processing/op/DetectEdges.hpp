#pragma once

#include "armadillo/include/armadillo"

#include "ScaleValues.hpp"


namespace op
{


struct DetectEdges
{
    DetectEdges()
    {
        arma::mat sx(3, 3);
        sx(0, 0) = 1; sx(0, 1) = 2; sx(0, 2) = 1;
        sx(1, 0) = 0; sx(1, 1) = 0; sx(1, 2) = 0;
        sx(2, 0) = -1; sx(2, 1) = -2; sx(2, 2) = -1;

        arma::mat sy(3, 3);
        sy(0, 0) = -1; sy(0, 1) = 0; sy(0, 2) = 1;
        sy(1, 0) = -2; sy(1, 1) = 0; sy(1, 2) = 2;
        sy(2, 0) = -1; sy(2, 1) = 0; sy(2, 2) = 1;

        sx_ = sx;
        sy_ = sy;
    }
    arma::mat operator()(arma::mat A) const
    {
        arma::mat S1 = arma::conv2(A, sx_, "same");
        arma::mat S2 = arma::conv2(A, sy_, "same");

        arma::mat B = arma::mat(arma::sqrt(arma::square(S1) + arma::square(S2)));
        return op::ScaleValues<255>{}(B);
    }

    arma::mat sx_;
    arma::mat sy_;
};


} // namespace op
