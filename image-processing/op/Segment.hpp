#pragma once

#include "../armadillo/include/armadillo"

namespace op
{


struct Segment
{
    Segment(int threshold)
        : threshold_(threshold)
    {}
    arma::mat operator()(arma::mat A) const
    {
        const auto segment_color = [threshold=threshold_](auto& rgb_color)
            {rgb_color = rgb_color > threshold ? 255 : 0;};

        A.for_each(segment_color);
        return A;
    }
    arma::cx_mat operator()(arma::cx_mat A) const
    {
        return arma::cx_mat(
                this->operator()(arma::real(A)),
                this->operator()(arma::imag(A)));
    }
    const int threshold_;
};


} // namespace op
