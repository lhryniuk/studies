#pragma once

#include "../armadillo/include/armadillo"

namespace op
{


struct ApplyMask
{
    ApplyMask(arma::mat mask)
        : mask_(mask)
    {}
    arma::mat operator()(arma::mat A) const
    {
        return A % mask_;
    }
    arma::cx_mat operator()(arma::cx_mat A) const
    {
        return A % mask_;
    }
    const arma::mat mask_;
};


} // namespace op
