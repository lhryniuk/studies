#pragma once

#include "../armadillo/include/armadillo"


namespace op
{


struct Svd
{
    struct Eigens
    {
        arma::mat U; // eigen vectors (cols)
        arma::vec L; // eigen values
        arma::mat V;
    };

    Eigens operator()(arma::mat A) const
    {
        Eigens e;
        arma::svd(e.U, e.L, e.V, A);
        return e;
    }
};


} // namespace op
