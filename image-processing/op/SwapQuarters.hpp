#pragma once

#include "../armadillo/include/armadillo"

#include <cassert>


namespace op
{


struct SwapQuarters
{
    arma::mat operator()(arma::mat A) const
    {
        int center_x = (A.n_cols / 2);
        int center_y = (A.n_rows / 2);

        for (auto y = 0; y < center_y; ++y) {
            assert(static_cast<decltype(A.n_rows)>(y + center_y) < A.n_rows);
            A.swap_rows(y, y+center_y);
        }

        for (auto x = 0; x < center_x; ++x) {
            assert(static_cast<decltype(A.n_rows)>(x + center_x) < A.n_rows);
            A.swap_cols(x, x+center_x);
        }

        return A;
    }
    arma::cx_mat operator()(arma::cx_mat A) const
    {
        return arma::cx_mat(
                this->operator()(real(A)),
                this->operator()(imag(A)));
    }
};


} // namespace op
