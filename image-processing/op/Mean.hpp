#pragma once

#include "IndexForEach.hpp"

#include "../armadillo/include/armadillo"

#include <iostream>


namespace op
{


struct Mean
{
    double operator()(const arma::mat& A, unsigned row, unsigned col) const
    {
        auto count = 1;
        double sum = A(row, col);

        for (const auto& s : surr) {
            int nrow = row + s.first;
            int ncol = col + s.second;
            if (nrow >= 0 && nrow < static_cast<int>(A.n_rows)
             && ncol >= 0 && ncol < static_cast<int>(A.n_cols)) {
                sum += A(nrow, ncol);
                ++count;
            }
        }
        return sum /= static_cast<double>(count);
    }
    double operator()(arma::cx_mat, unsigned, unsigned) const
    {
        throw InvalidOperationException();
    }
    const std::vector<std::pair<int, int>> surr
    {
        {-1, -1},
        {-1,  0},
        {-1,  1},
        { 0, -1},
        { 0,  1},
        { 1, -1},
        { 1,  0},
        { 1,  1}
    };
};


} // namespace op
