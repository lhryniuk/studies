#pragma once

#include "../armadillo/include/armadillo"

#include <vector>


namespace op
{


struct ColorsHistogram
{
    std::vector<int> operator()(arma::mat A) const
    {
        std::vector<int> colorCounts(256, 0);
        A.for_each([&colorCounts](auto& v){++colorCounts[v];});
        return colorCounts;
    }
};


} // namespace op
