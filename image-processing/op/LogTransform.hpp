#pragma once

#include "ColorsCdf.hpp"
#include "InvalidOperationException.hpp"

#include "../armadillo/include/armadillo"

#include <iostream>


namespace op
{


arma::mat LogTransform(arma::cx_mat A)
{
    arma::mat B(A.n_rows, A.n_cols);
    arma::mat rA = arma::real(A);
    arma::mat iA = arma::imag(A);
    for (auto y = 0u; y < A.n_rows; ++y) {
        for (auto x = 0u; x < A.n_cols; ++x) {
            B(x, y) = std::log(1.0 + std::hypot(rA(x, y), iA(x, y)));
        }
    }
    return B;
}


} // namespace op
