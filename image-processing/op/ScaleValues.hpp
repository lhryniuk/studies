#pragma once

#include "../armadillo/include/armadillo"

#include <cassert>


namespace op
{


template<unsigned MaxValue=255>
struct ScaleValues
{
    enum { max_value = MaxValue };
    arma::mat operator()(arma::mat A) const
    {
        const auto max_value = static_cast<double>(MaxValue);
        double mini = A.min();
        double maxi = A.max();

        for (auto y = 0u; y < A.n_rows; ++y) {
            for (auto x = 0u; x < A.n_cols; ++x) {
                auto base_val = std::min(max_value, (A(y, x) - mini) * (max_value / (maxi - mini)));
                assert(base_val >= 0.0);
                A(y, x) = base_val;
            }
        }
        return A;
    }
    arma::cx_mat operator()(arma::cx_mat A) const
    {
        return arma::cx_mat(
                this->operator()(arma::real(A)),
                this->operator()(arma::imag(A)));
    }
};


} // namespace op
