#pragma once

#include <exception>
#include <stdexcept>

struct InvalidOperationException : public std::runtime_error
{
    InvalidOperationException()
        : std::runtime_error("Invalid operation error")
    {
    }
};
