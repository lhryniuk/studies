#pragma once

#include "IndexForEach.hpp"
#include "Mean.hpp"
#include "Median.hpp"

#include "../armadillo/include/armadillo"


namespace op
{


template<typename Type>
struct Averages
{
    arma::mat operator()(arma::mat A) const
    {
        IndexForEach index_for_each;

        return index_for_each(A, Type());
    }
    arma::cx_mat operator()(arma::cx_mat) const
    {
        throw InvalidOperationException();
    }
};

using Medians = Averages<Median>;
using Means = Averages<Mean>;


} // namespace op
