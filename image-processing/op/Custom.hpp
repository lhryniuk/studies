#pragma once

#include "../armadillo/include/armadillo"

#include <cassert>
#include <type_traits>


namespace op
{


template<typename Func>
struct Custom
{
    Custom(Func&& f)
        : f_{std::forward<Func>(f)}
    { }
    arma::mat operator()(arma::mat A) const
    {
        return f_(A);
    }
    arma::cx_mat operator()(arma::cx_mat A) const
    {
        return f_(A);
    }
    Func f_;
};

template<typename Func>
auto GetCustom(Func&& f) -> Custom<Func>
{
    return Custom<Func>{std::forward<Func>(f)};
}


} // namespace op
