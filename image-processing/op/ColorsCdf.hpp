#pragma once

#include "ColorsHistogram.hpp"

#include "../armadillo/include/armadillo"

#include <algorithm>
#include <vector>


namespace op
{


struct ColorsCdf
{
    std::vector<double> operator()(arma::mat A) const
    {
        const auto pixelsCount = A.n_elem;

        const auto colorCounts = ColorsHistogram()(A);

        std::vector<double> cdf(colorCounts.size());
        std::partial_sum(std::begin(colorCounts), std::end(colorCounts), std::begin(cdf));

        std::transform(std::begin(cdf), std::end(cdf), std::begin(cdf),
                [pixelsCount](auto v){return v / static_cast<double>(pixelsCount);});

        return cdf;
    }
};


} // namespace op
