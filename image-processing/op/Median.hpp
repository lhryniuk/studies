#pragma once

#include "IndexForEach.hpp"

#include "../armadillo/include/armadillo"


namespace op
{


struct Median
{
    double operator()(const arma::mat& A, unsigned row, unsigned col) const
    {
        std::vector<double> v;
        for (const auto& s : surr) {
            const int nrow = row + s.first;
            const int ncol = col + s.second;
            if (nrow >= 0 && nrow < static_cast<int>(A.n_rows)
             && ncol >= 0 && ncol < static_cast<int>(A.n_cols)) {
                v.push_back(A(nrow, ncol));
            }
        }
        return static_cast<double>(v[v.size() / 2]);
    }
    double operator()(arma::cx_mat, unsigned, unsigned) const
    {
        throw InvalidOperationException();
    }
    const std::vector<std::pair<int, int>> surr
    {
        {-1, -1},
        {-1,  0},
        {-1,  1},
        { 0, -1},
        { 0,  1},
        { 1, -1},
        { 1,  0},
        { 1,  1}
    };
};


} // namespace op
