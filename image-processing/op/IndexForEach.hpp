#pragma once

#include "InvalidOperationException.hpp"

#include "../armadillo/include/armadillo"


namespace op
{


struct IndexForEach
{
    template<typename Func>
    arma::mat operator()(arma::mat A, Func&& f)
    {
        arma::mat output(arma::size(A));

        for (auto row = 0u; row < A.n_rows; ++row) {
            for (auto col = 0u; col < A.n_cols; ++col) {
                output(row, col) = f(A, row, col);
            }
        }

        return output;
    }
    arma::cx_mat operator()(arma::cx_mat)
    {
        throw InvalidOperationException();
    }
};


} // namespace op
