#pragma once

#include <iostream>

#include "../armadillo/include/armadillo"

#include "ColsMeans.hpp"
#include "Svd.hpp"


namespace op
{


struct Pca
{
    arma::mat operator()(arma::mat A) const
    {
        using arma::operator*;
        const arma::vec d = ColsMeans{}(A);

        auto xx = A;
        for (auto i = 0u; i < A.n_cols; ++i) {
            arma::vec diff = d(i) * arma::vec(A.n_rows, arma::fill::ones);
            xx.col(i) -= diff;
        }

        arma::mat c = arma::cov(xx, xx);

        auto e = Svd{}(c);
        return xx * e.U;
    }
};


} // namespace op
