#pragma once

#include "ColorsCdf.hpp"
#include "InvalidOperationException.hpp"

#include "../armadillo/include/armadillo"

#include <iostream>


namespace op
{


struct Histogram
{
    arma::mat operator()(arma::mat A) const
    {
        const double max_channel_value = 255.0;

        arma::mat output(arma::size(A));

        const auto cdf = ColorsCdf()(A);

        for (auto i = 0u; i < A.n_rows; ++i) {
            for (auto j = 0u; j < A.n_cols; ++j) {
                output(i, j) = static_cast<int>(max_channel_value * cdf[A(i, j)]);
            }
        }

        return output;
    }
    arma::cx_mat operator()(arma::cx_mat) const
    {
        throw InvalidOperationException();
    }
};


} // namespace op
