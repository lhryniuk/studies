#pragma once

#include "ApplyMask.hpp"
#include "Compose.hpp"
#include "ScaleValues.hpp"
#include "SwapQuarters.hpp"

#include "../armadillo/include/armadillo"


struct FixUsingMask
{
    FixUsingMask(arma::mat mask)
        : mask_{mask}
    {
    }
    arma::cx_mat operator()(arma::mat img)
    {
        auto apply_mask =
            op::ScaleValues<255>{} *
            [](arma::cx_mat A) {return arma::ifft2(A);} *
            op::SwapQuarters{} *
            op::ApplyMask{mask_} *
            op::SwapQuarters{} *
            [](arma::mat A) {return arma::fft2(A);};

        return apply_mask(img);
    }

private:
    arma::mat mask_;
};

