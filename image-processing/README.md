# image-processing

Project created during the Image processing lecture, in spring 2017.

# design

The whole project is tightly coupled with
[Armadillo](http://arma.sourceforge.net/) and [Qt](https://www.qt.io/)
libraries, because they were required for our projects, and it probably won't
be changed.

The main part of it is the `op/` directory with a bunch of image/matrix
operations defined. The core idea is to allow to do a function composition with
`operator*` to make adding new methods faster and easier.