#pragma once

#include "../armadillo/include/armadillo"

#include <QImage>

#include <cassert>


namespace util
{

struct ColorQImageToMat
{
    arma::mat operator()(QImage img) const
    {
        arma::mat A(img.height() * img.height(), 3);

        for (auto y = 0; y < img.height(); ++y) {
            for (auto x = 0; x < img.width(); ++x) {
                auto color = QColor(img.pixel(x, y));
                A(y * img.width() + x, 0) = color.red();
                A(y * img.width() + x, 1) = color.green();
                A(y * img.width() + x, 2) = color.blue();
            }
        }

        return A;
    }
};

struct MonoQImageToMat
{
    arma::mat operator()(QImage img) const
    {
        assert(img.allGray());
        arma::mat A(img.height(), img.width());

        for (auto y = 0; y < img.height(); ++y) {
            for (auto x = 0; x < img.width(); ++x) {
                A(y, x) = QColor(img.pixel(x, y)).red();
            }
        }

        return A;
    }
};

struct MatToMonoQImage
{
    QImage operator()(arma::mat m) const
    {
        assert(not m.is_empty());
        QImage img(m.n_cols, m.n_rows, QImage::Format_RGB32);

        for (auto x = 0u; x < m.n_rows; ++x) {
            for (auto y = 0u; y < m.n_cols; ++y) {
                auto channel = static_cast<int>(m(x, y));
                assert(0 <= channel && channel <= 255.0);
                auto color = QColor(channel, channel, channel);
                img.setPixelColor(y, x, color);
            }
        }

        return img;
    }
};


} // namespace util
