#pragma once

#include <QDebug>
#include <QImage>

#include <cassert>
#include <string>

namespace util
{

struct LoadImage
{
    QImage operator()(std::string input_file_path) const
    {
        QImage img;
        QString filepath{input_file_path.c_str()};
        if (img.load(filepath)) {
            qDebug() << "OK. Wczytano obrazek " + filepath;
            qDebug() << " img_width =" << img.width() << " , img_height = " << img.height();
        } else {
            qDebug() << "BLAD: Nie wczytano obrazka " + filepath;
            exit(0);
        }
        return img;
    }
};

struct SaveImage
{
    SaveImage(std::string output_file_path)
        : output_file_path_{std::move(output_file_path)}
    {
        assert(not output_file_path_.empty());
    }
    void operator()(const QImage& img) const
    {
        qDebug() << "Zapisywanie do " << output_file_path_.c_str();
        img.save(QString(output_file_path_.c_str()));
    }
private:
    const std::string output_file_path_;
};



} // namespace util
