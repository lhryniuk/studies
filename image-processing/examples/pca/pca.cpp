#include <QApplication>
#include <QColor>
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QImage>
#include <QPainter>
#include "qpainter.h"
#include <QPixmap>
#include <QString>
#include <QtCore/QCoreApplication>


#include "op/AllOps.hpp"
#include "op/Compose.hpp"
#include "op/Pca.hpp"
#include "op/Svd.hpp"
#include "op/VecToSquareMat.hpp"

#include "util/IO.hpp"
#include "util/Convert.hpp"

#define BLIS_BLAS2BLIS_INT_TYPE_SIZE 32
#define BLIS_INT_TYPE_SIZE 64

#include "armadillo/include/armadillo"

constexpr int N = 100;

arma::mat generate_random_points(int rows, int dim, double max_value = 10.0)
{
    arma::mat A = max_value * arma::randu(rows, dim);

    // y = 2x + 1
    for (auto i = 0; i < rows; ++i) {
        A(i, 1) = 2.0 * A(i, 0) + arma::randu() + 1.0;
    }

    return A;
}

int main(int argc, char *argv[])
{
    if (argc >= 2) {
        arma::mat img = (util::ColorQImageToMat{} * util::LoadImage{})(argv[1]);
        arma::mat p = op::Pca{}(img);
        std::string grey_col = (argc == 3) ? argv[2] : "0";
        p = op::VecToSquareMat{}(p.col(std::stoi(grey_col)));
        auto i = util::MatToMonoQImage{}(op::ScaleValues<255>{}(p));
        util::SaveImage{"hello" + grey_col + ".jpg"}(i);
    } else {
        constexpr int seed = 17;
        arma::arma_rng::set_seed(seed);

        constexpr auto rows_count = 100;
        constexpr auto cols_count = 2;

        auto p = generate_random_points(rows_count, cols_count);
    }

}
