#include "op/Compose.hpp"
#include "op/DetectEdges.hpp"

#include "util/IO.hpp"
#include "util/Convert.hpp"

#include "armadillo/include/armadillo"

void usage()
{
    std::cout << "./detect-edges <input_image_path>" << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        usage();
        return -1;
    }

    const std::string input_file_path = argv[1];

    const auto load = util::MonoQImageToMat{} * util::LoadImage{};
    const auto get_edges = op::ScaleValues<255>{} * op::DetectEdges{};
    const auto save = util::SaveImage{"edges" + input_file_path + ".jpg"} * util::MatToMonoQImage{};

    const auto A = load(input_file_path);
    const auto edges = get_edges(A);
    save(edges);
}
