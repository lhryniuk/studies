# traceroute

It's a basic traceroute program, printing a route and average delay to a host
with given IPv4 address:

```shell
> sudo ./tc 8.8.8.8
1. 192.168.1.254 6.87ms
2. 62.87.220.4 8.12ms
3. 62.87.216.1 12.35ms
4. 83.238.252.60 17.48ms
5. 83.238.251.46 18.95ms
6. 213.195.141.49 16.16ms
7. 66.249.95.15 14.06ms
8. 209.85.241.99 42.72ms
9. 209.85.251.178 30.80ms
10. 209.85.255.39 35.08ms
11. 209.85.243.65 32.97ms
12. *
13. 8.8.8.8 32.99ms
```

Build it by executing `make` in main dir. Run with `sudo` :wink:.
