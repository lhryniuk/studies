#include <assert.h>
#include <stdio.h>
#include "util.h"

void print_bytes(uint8_t* buffer, int begin, int len)
{
    int mod = 8;
    for (int i = begin, t = 0; t < len; ++i, ++t) {
        printf("%02x ", buffer[i]);
        if ((i+1) % mod == 0) {
            printf("\n");
        }
    }
}

int get_packet_from_socket(int sd, uint8_t* buffer, ssize_t* packet_len, char* sender_ip_str, int sender_ip_str_len)
{
    struct sockaddr_in sender;
    socklen_t sender_len = sizeof(sender);
    *packet_len = recvfrom(
            sd,
            buffer,
            IP_MAXPACKET,
            MSG_DONTWAIT,
            (struct sockaddr*)&sender,
            &sender_len
        );
    if (*packet_len > 0) {
        inet_ntop(AF_INET, &(sender.sin_addr), sender_ip_str, sender_ip_str_len);
    }
    return *packet_len;
}

void print_packet(uint8_t* buffer, ssize_t packet_len)
{
    if (packet_len != -1) {
        printf("Received: %ld\n", packet_len);
    }
    printf("Packet content: \n");
    int mod = 8;
    for (int i = 0; i < packet_len; ++i) {
        printf("%02x ", buffer[i]);
        if ((i+1) % mod == 0) {
            printf("\n");
        }
    }
    printf("\n");
}

void print_icmphdr(struct icmphdr* hdr)
{
    printf(" -= icmphdr =-\n");
    printf("type: %d\n", hdr->type);
    printf("code: %d\n", ntohs(hdr->code));
    printf("id: %d\n", ntohs(hdr->un.echo.id));
    printf("seq: %d\n", ntohs(hdr->un.echo.sequence));
    printf("checksum: %d\n", hdr->checksum);
}

uint16_t compute_icmp_checksum(const void *buffer, int length)
{
    uint32_t sum;
    const uint16_t* ptr = buffer;
    assert(length % 2 == 0);
    for (sum = 0; length > 0; length -= 2) {
        sum += *ptr++;
    }
    sum = (sum >> 16) + (sum & 0xffff);
    return (uint16_t)(~(sum + (sum >> 16)));
}

void init_icmp_hdr_echo(struct icmphdr* hdr, int16_t id, int16_t seq)
{
    hdr->type = ICMP_ECHO;
    hdr->code = htons(0); // echo request code
    hdr->un.echo.id = htons(id);
    hdr->un.echo.sequence = htons(seq);
    hdr->checksum = 0;
    hdr->checksum = compute_icmp_checksum((uint16_t*)hdr, sizeof(hdr));
}
