#include "wymierna.h"
#include <cassert>
#include <iostream>
#include <sstream>

void test_Wymierna();
void test_gcd();

int main(void)
{
    test_gcd();
    test_Wymierna();
    std::cout << "Testy ok!" << std::endl;
    return 0;
}


/*
 * testy
 */
void test_gcd()
{
    assert(Wymierna::gcd(5, 3) == 1);
    assert(Wymierna::gcd(15, 5) == 5);
    assert(Wymierna::gcd(1, 0) == 1);
    assert(Wymierna::gcd(0, 1) == 1);
    assert(Wymierna::gcd(-35, 7) == 7);
    assert(Wymierna::gcd(21, -63) == 21);
    assert(Wymierna::gcd(1, 1) == 1);
}

void test_Wymierna(void)
{
    Wymierna w1(2, -4);
    assert(w1.Licznik() == -1);
    assert(w1.Mianownik() == 2);

    Wymierna w2;
    assert(w2.Licznik() == 0);
    assert(w2.Mianownik() == 1);

    Wymierna w3(2);
    assert(w3.Licznik() == 2);
    assert(w3.Mianownik() == 1);

    w1 += w3;
    assert(w1.Licznik() == 3);
    assert(w1.Mianownik() == 2);

    w1 += w3 += Wymierna(4, 8);
    assert(w3.Licznik() == 5);
    assert(w3.Mianownik() == 2);
    assert(w1.Licznik() == 4);
    assert(w1.Mianownik() == 1);

    w1 -= Wymierna(4, 8);
    assert(w1.Licznik() == 7);
    assert(w1.Mianownik() == 2);

    w1 = Wymierna(9, 4);
    w1 *= Wymierna(16, 15); // 9/4 * 16/15 = 3/1 * 4/5 = 12/5 = 144/60
    assert(w1.Licznik() == 12);
    assert(w1.Mianownik() == 5);

    w1 /= Wymierna(16, 15);
    assert(w1.Licznik() == 9);
    assert(w1.Mianownik() == 4);

    w1 = 3;
    assert(w1.Licznik() == 3);
    assert(w1.Mianownik() == 1);

    assert(w1 == 3);

    std::istringstream we("-6/8 10/-14");
    we >> w1 >> w2;
    assert(we);
    assert(w1 == Wymierna(-3, 4));
    assert(w2 == Wymierna(-5, 7));

    std::istringstream we2("-6 /8");
    we >> w1;
    assert(!we);
}
