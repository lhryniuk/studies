#include "wymierna.h"

Wymierna::Wymierna(long long licznik, long long mianownik)
{
    assert(mianownik != 0);
    if (mianownik < 0) {
        mianownik *= -1;
        licznik *= -1;
    }
    licznik_ = licznik;
    mianownik_ = mianownik;
    Skroc();
}

long long Wymierna::Licznik(void) const
{
    return licznik_;
}

long long Wymierna::Mianownik(void) const
{
    return mianownik_;
}

long long Wymierna::gcd(long long a, long long b)
{
    a = abs(a); // by uproscic wywolania
    b = abs(b);
    while (b)
        std::swap(a %= b, b);
    return a;
}

bool Wymierna::operator==(const Wymierna &q) const
{
    return (((licznik_) == q.Licznik()) && (mianownik_ == q.Mianownik()));
}


/* a/b + c/d = (a * (d / gcd(b,d)) + c * (b / gcd(b,d))) / lcm(b, d)
 * 3 gcd, 3 /, 3 *
 * wiekszy zakres liczb
 * lub
 * >> a/b + c/d = (a * d + c * b) / (b * d) = e / f = (e / gcd(e,f)) : (f / gcd(e,f)
 * 2 gcd, 2 /, 3 *
 * ograniczone przez mnozenie
 * (przy parach, ktorych mianowniki nie sa wzglednie pierwsze)
 * */
Wymierna &Wymierna::operator+=(const Wymierna &q)
{
    if (mianownik_ != q.Mianownik()) {
        licznik_ = licznik_ * q.Mianownik() + mianownik_ * q.Licznik();
        mianownik_ = mianownik_ * q.Mianownik();
    } else {
        licznik_ += q.Licznik();
    }
    Skroc();
    return *this;
}

Wymierna &Wymierna::operator-=(const Wymierna &q)
{
    if (mianownik_ != q.Mianownik()) {
        licznik_ = licznik_ * q.Mianownik() - mianownik_ * q.Licznik();
        mianownik_ = mianownik_ * q.Mianownik();
    } else {
        licznik_ -= q.Licznik();
    }
    Skroc();
    return *this;
}

Wymierna &Wymierna::operator*=(const Wymierna &q)
{
    licznik_ *= q.Licznik();
    mianownik_ *= q.Mianownik();
    Skroc();
    return *this;
}

Wymierna &Wymierna::operator/=(const Wymierna &q)
{
    assert(q.Licznik() != 0);
    licznik_ *= q.Mianownik();
    mianownik_ *= q.Licznik();
    Skroc();
    return *this;
}

void Wymierna::Skroc(void)
{
    long long gcd_lm = Wymierna::gcd(licznik_, mianownik_);
    licznik_ /= gcd_lm;
    mianownik_ /= gcd_lm;
}

std::istream &operator>>(std::istream &we, Wymierna &liczba)
{
    long long licznik = 0;
    long long mianownik = 0;
    we >> licznik;
    if (we && we.peek() == '/') {
        we.get();
        if (we && (we.peek() == '-' || isdigit(we.peek()))) {
            we >> mianownik;
            if (we && mianownik != 0) {
                liczba = Wymierna(licznik, mianownik);
                return we;
            }
        }
    }
    if (we)
        we.setstate(std::ios_base::failbit);
    return we;
}
