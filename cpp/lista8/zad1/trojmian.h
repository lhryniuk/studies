#ifndef TROJMIAN_H
#define TROJMIAN_H

#include <iostream>

const double kEpsilon = 1e-2;

class Trojmian
{
    public:
    Trojmian(double x0 = 0.0, double x1 = 0.0, double x2 = 0.0);
    double Wspolczynnik(int stopien) const;
    Trojmian &operator+=(const Trojmian &t);
    double operator()(const double x) const;
    static bool MniejWiecej(const Trojmian &t1, const Trojmian &t2);

    private:
    double x0_;
    double x1_;
    double x2_;
};

std::istream &operator>>(std::istream &we, Trojmian &t);

#endif
