#include "trojmian.h"

Trojmian::Trojmian(double x0, double x1, double x2) : x0_(x0), x1_(x1), x2_(x2)
{
}

/* static */
bool Trojmian::MniejWiecej(const Trojmian &t1, const Trojmian &t2)
{
    return ((((t1.Wspolczynnik(0) - kEpsilon) <= t2.Wspolczynnik(0)) &&
             (t1.Wspolczynnik(0) + kEpsilon) >= t2.Wspolczynnik(0)) &&
            (((t1.Wspolczynnik(1) - kEpsilon) <= t2.Wspolczynnik(1)) &&
             (t1.Wspolczynnik(1) + kEpsilon) >= t2.Wspolczynnik(1)) &&
            (((t1.Wspolczynnik(2) - kEpsilon) <= t2.Wspolczynnik(2)) &&
             (t1.Wspolczynnik(2) + kEpsilon) >= t2.Wspolczynnik(2)));
}

/* schemat Hornera:
 * x2_ * x^2 + x1_ * x + x0_ = x(x2_* x + x1_) + x0_ */
double Trojmian::operator()(const double x) const
{
    return (x * (x2_ * x + x1_) + x0_);
}

Trojmian &Trojmian::operator+=(const Trojmian &t)
{
    x0_ += t.Wspolczynnik(0);
    x1_ += t.Wspolczynnik(1);
    x2_ += t.Wspolczynnik(2);
    return *this;
}

double Trojmian::Wspolczynnik(int stopien) const
{
    switch (stopien) {
    case 0:
        return x0_;
    case 1:
        return x1_;
    case 2:
        return x2_;
    default:
        return 0.0;
    }
}

std::istream &operator>>(std::istream &we, Trojmian &t)
{
    double x0 = 0.0;
    double x1 = 0.0;
    double x2 = 0.0;
    while (we && we.peek() == ' ')
        we.get();
    if (we && we.peek() == '[') {
        we.get();
        we >> x0;
        if (we && we.peek() == ',') {
            we.get();
            if (we && (isdigit(we.peek()) || (we.peek() == '-'))) {
                we >> x1;
                if (we && we.peek() == ',') {
                    we.get();
                    if (we && (isdigit(we.peek()) || we.peek() == '-')) {
                        we >> x2;
                        if (we && we.peek() == ']') {
                            we.get();
                            t = Trojmian(x0, x1, x2);
                            return we;
                        }
                    }
                }
            }
        }
    }
    if (we)
        we.setstate(std::ios_base::failbit);
    return we;
}
