#include <algorithm>
#include <cassert>
#include <deque>
#include <iostream>

void Odniesienie(std::deque<int>::iterator p, std::deque<int>::iterator k)
{
    int mini = *std::min_element(p, k);
    std::transform(p, k, p, [&](int i) { return (i - mini); });
}

void test_Odniesienie()
{
    std::deque<int> d{ -4, 5, 7, 2, 8, -3, -5, 9, 9 };
    std::deque<int> dd{ 1, 10, 12, 7, 13, 2, 0, 14, 9 };
    Odniesienie(std::begin(d), std::end(d) - 1);
    assert(d == dd);
    std::cerr << "test_Odniesienie ok!\n";
}

int main(void)
{
    test_Odniesienie();
    return 0;
}
