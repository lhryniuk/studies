#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

bool porownaj(std::string a, std::string b)
{
    if (a.find('[') == std::string::npos) {
        return false;
    } else if (b.find('[') == std::string::npos) {
        return true;
    } else {
        return std::lexicographical_compare(begin(a) + a.find('['), begin(a) + a.find(']'),
                                            begin(b) + b.find('['), begin(b) + b.find(']'));
    }
}

void SortujPoHaslach(std::vector<std::string>::iterator p, std::vector<std::string>::iterator k)
{
    stable_sort(p, k, porownaj);
}

void test_SortujPoHaslach(void);

int main(void)
{
    test_SortujPoHaslach();
    return 0;
}

/* ********
 * * test *
 * ******** */
void test_SortujPoHaslach(void)
{
    std::vector<std::string> d{
        "O 13 uruchomienie aplikacji",
        "O 14 proba logowania haslem [madrehaslo] zakonczona powodzeniem",
        "O 15 proba logowania haslem [glupiehaslo] zakonczona niepowodzeniem",
        "O 15.30 restart aplikacji",
        "Uzytkownik zmienil [madrehaslo] na [innehaslo] o 16",
        "O 17 odmowa zmiany hasla na [aaaaaa] z powodu niedostateczego skomplikowania",
        "O 18 chyba bledna proba zmiany hasla na puste []"
    };

    std::vector<std::string> ds{
        "O 18 chyba bledna proba zmiany hasla na puste []",
        "O 17 odmowa zmiany hasla na [aaaaaa] z powodu niedostateczego skomplikowania",
        "O 15 proba logowania haslem [glupiehaslo] zakonczona niepowodzeniem",
        "O 14 proba logowania haslem [madrehaslo] zakonczona powodzeniem",
        "Uzytkownik zmienil [madrehaslo] na [innehaslo] o 16",
        "O 13 uruchomienie aplikacji",
        "O 15.30 restart aplikacji"
    };
    SortujPoHaslach(std::begin(d), std::end(d));
    assert(d == ds);
    std::cerr << "test_SortujPoHaslach ok!\n";
}
