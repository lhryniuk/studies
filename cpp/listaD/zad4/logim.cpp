#include <algorithm>
#include <cassert>
#include <iostream>
#include <set>
#include <string>
#include <vector>

struct cmp {
    bool operator()(const std::string &a, const std::string &b)
    {
        return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end());
    }
};

/* zal.: po kazdym [ nastepuje ] */
std::vector<std::string> Hasla(std::vector<std::string>::iterator p, std::vector<std::string>::iterator k)
{
    std::set<std::string, cmp> S;
    /* przechodzi po vectorze stringow i wyciaga z nich hasla
     * przy uzyciu funkcji wybierz */
    auto przeszukaj = [&](std::string &str) {
        /* wyszukuje hasla i wrzuca do S */
        auto wybierz = [&](char &c) {
            if (c == '[') {
                int index = &c - &(*str.begin());
                S.insert(str.substr(index + 1, str.find(']', index) - index - 1));
            }
        };
        for_each(str.begin(), str.end(), wybierz);
    };

    for_each(p, k, przeszukaj);
    std::vector<std::string> hasla(S.size());
    std::copy(S.begin(), S.end(), hasla.begin());
    return hasla;
}

void test_Hasla(void);

int main(void)
{
    test_Hasla();
    return 0;
}

/* ********
 * * test *
 * ******** */
void test_Hasla(void)
{
    std::vector<std::string> d{
        "O 13 uruchomienie aplikacji",
        "O 14 proba logowania haslem [madrehaslo] zakonczona powodzeniem",
        "O 15 proba logowania haslem [glupiehaslo] zakonczona niepowodzeniem",
        "O 15.30 restart aplikacji",
        "Uzytkownik zmienil [madrehaslo] na [innehaslo] o 16",
        "O 17 odmowa zmiany hasla na [aaaaaa] z powodu niedostateczego skomplikowania",
        "O 18 chyba bledna proba zmiany hasla na puste []"
    };
    std::vector<std::string> hasla = Hasla(d.begin(), d.end());
    std::vector<std::string> gotowe{ "", "aaaaaa", "glupiehaslo", "innehaslo", "madrehaslo" };
    assert(hasla == gotowe);
    std::cout << "test_SortujPoHaslach ok!\n";
}
