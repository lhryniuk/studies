#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <numeric>
#include <vector>


double SredniaPozytywnych(std::vector<double> v)
{
    using namespace std::placeholders;
    auto poz = std::bind(std::greater<>(), _1, 1);
    std::transform(std::begin(v), std::end(v), std::begin(v), [&poz](auto k){return k * poz(k);});
    return std::accumulate(std::begin(v), std::end(v), 0) / double(std::count_if(std::begin(v), std::end(v), poz));
}

void test_SredniaPozytywnych()
{
    std::vector<double> oceny{ 1, 3, 1, 2, 4, 5, 1 };
    auto k = SredniaPozytywnych(oceny);
    std::cout << k << "\n";
    assert(SredniaPozytywnych(oceny) == 3.5);
    std::cerr << "test_SredniaPozytywnych ok!\n";
}

int main()
{
    test_SredniaPozytywnych();
}
