#include <cassert>
#include <cmath>
#include <iostream>
#include <string>


constexpr double EPS = 1e-3;

constexpr bool equal(double a, double b)
{
    if (a - EPS <= b && a + EPS >= b)
        return true;
    return false;
}

auto account_round(double price)
{
    long long tmp_price = (long long)(price * 1000.0);
    if (tmp_price % 10 >= 5) {
        tmp_price += 10;
    }
    tmp_price /= 10;
    price = double(tmp_price) / 100.0;
    return price;
}

void test_account_round(void);

class Produkt
{
    public:
    Produkt(std::string nazwa, double cena, double marza = 0.0)
    {
        assert(!nazwa.empty());
        assert(marza >= 0);
        assert(cena > 0);
        nazwa_ = nazwa;
        cena_ = cena;
        marza_ = marza;
    }
    std::string Nazwa() const
    {
        return nazwa_;
    }
    double CenaZakupu() const
    {
        return cena_;
    }
    double MarzaWymagana() const
    {
        return marza_;
    }
    double CenaSprzedazy() const
    {
        return account_round(cena_ * (1.0 + marza_));
    }
    double MarzaOsiagana() const
    {
        return (CenaSprzedazy() - cena_) / cena_;
    }

    private:
    std::string nazwa_;
    double cena_;
    double marza_;
};

void test_Produkt()
{
    Produkt a("mleko", 3.15, 0.13);
    assert(equal(a.CenaZakupu(), 3.15));
    assert(equal(a.CenaSprzedazy(), 3.56));
    assert(equal(a.MarzaOsiagana(), 0.13));
    Produkt b("woda", 1.136, 0.11);
    assert(equal(b.CenaZakupu(), 1.136));
    assert(equal(b.CenaSprzedazy(), 1.26));
    assert(equal(b.MarzaOsiagana(), 0.11));
}

int main(void)
{
    test_Produkt();
    test_account_round();
    Produkt a("abc", 8.125, 4.125);
    return 0;
}

void test_account_round(void)
{
    assert(account_round(4.125) == 4.13);
    assert(account_round(3.14731) == 3.15);
    assert(account_round(1.522) == 1.52);
    assert(account_round(9.1273) == 9.13);
}
