#include <cassert>
#include <iostream>
#include <string>

std::string uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZĄĆĘŁŃÓŚŹŻ";
std::string lowers = "abcdefghijklmnopqrstuvwxyząćęłńóśźż";

// counts leading bits (for UTF-8)
constexpr auto bits(int byte)
{
    int nbits = 0;
    int mask = 128;
    while ((mask > 0) && (byte & mask)) {
        ++nbits;
        mask >>= 1;
    }
    return std::max(1, nbits);
}

// ***

// dlugosc tekstu w znakach
auto dlugosc(std::string tekst)
{
    int litery = 0;
    for (unsigned i = 0; i < tekst.size(); ++i) {
        ++litery;
        i += std::max(bits(tekst[i]) - 1, 0);
    }
    return litery;
}

void test_dlugosc();

//
inline bool sensowna_nazwa(std::string nazwa)
{
    assert(dlugosc(nazwa) >= 2);
    if (uppers.find(nazwa.substr(0, bits(nazwa[0]))) == std::string::npos)
        return false;
    for (unsigned i = bits(nazwa[0]); i < nazwa.size(); ++i) {
        if (lowers.find(nazwa.substr(i, bits(nazwa[i]))) == std::string::npos)
            return false;
        i += bits(nazwa[0]) - 1;
    }
    return true;
}

void test_sensowna_nazwa();

class Osoba
{
    public:
    Osoba(std::string imieI, std::string imieII, std::string nazwisko)
    : imieI_(imieI), imieII_(imieII), nazwisko_(nazwisko)
    {
        assert(!imieI.empty() && sensowna_nazwa(imieI));
        assert(imieII.empty() || sensowna_nazwa(imieII));
        inicjaly_ = imieI.substr(0, std::max(1, bits(imieI[0]))) + ".";
        if (!imieII.empty()) {
            inicjaly_ += " " + imieII.substr(0, std::max(1, bits(imieII[0]))) + ".";
        }
    }
    int IleImion() const
    {
        return ((imieII_.empty()) ? 1 : 2);
    }
    std::string Imie() const
    {
        return imieI_;
    }
    std::string Imiona() const
    {
        return ((imieII_.empty()) ? imieI_ : (imieI_ + " " + imieII_));
    }
    std::string Nazwisko() const
    {
        return nazwisko_;
    }
    std::string ImionaINazwisko() const
    {
        return ((imieII_.empty()) ? (imieI_ + " " + nazwisko_) : (imieI_ + " " + imieII_ + " " + nazwisko_));
    }
    std::string InicjalyINazwisko() const
    {
        return inicjaly_ + " " + nazwisko_;
    }

    private:
    std::string imieI_;
    std::string imieII_;
    std::string nazwisko_;
    std::string inicjaly_;
};

void test_Osoba(void);

void test();

int main(void)
{
    test();
    return 0;
}

void test_dlugosc()
{
    assert(dlugosc("Łukasz") == 6);
    assert(dlugosc("Kasia") == 5);
    assert(dlugosc("żążyć") == 5);
    assert(dlugosc("żółw") == 4);
    assert(dlugosc("ąćęłńóśźż") == 9);
    assert(dlugosc("ĄĆĘŁŃÓŚŹŻ") == 9);
}

void test_sensowna_nazwa()
{
    assert(sensowna_nazwa("Łukasz"));
    assert(sensowna_nazwa("Ćwir"));
    assert(sensowna_nazwa("Gość"));
    assert(sensowna_nazwa("Źdźbło"));
    assert(!sensowna_nazwa("lukasz"));
    assert(!sensowna_nazwa("$:adaśko?#"));
    assert(!sensowna_nazwa("źdźbło"));
}

void test_Osoba(void)
{

    Osoba jkw("Jan", "Karol", "Wscieklica");
    assert(jkw.IleImion() == 2);
    assert(jkw.Imie() == "Jan");
    assert(jkw.Imiona() == "Jan Karol");
    assert(jkw.Nazwisko() == "Wscieklica");
    assert(jkw.ImionaINazwisko() == "Jan Karol Wscieklica");
    assert(jkw.InicjalyINazwisko() == "J. K. Wscieklica");

    const Osoba aa("Adam", "", "Abacki");
    assert(aa.IleImion() == 1);
    assert(aa.Imie() == "Adam");
    assert(aa.Imiona() == "Adam");
    assert(aa.Nazwisko() == "Abacki");
    assert(aa.ImionaINazwisko() == "Adam Abacki");
    assert(aa.InicjalyINazwisko() == "A. Abacki");
}

void test()
{
    test_sensowna_nazwa();
    test_dlugosc();
    test_Osoba();
}
