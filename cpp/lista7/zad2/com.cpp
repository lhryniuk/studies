#include <cassert>
#include <cmath>
#include <iostream>
#include <sstream>

constexpr double EPS = 1e-6;

constexpr auto equal(double a, double b)
{
    if (a - EPS <= b && a + EPS >= b)
        return true;
    return false;
}

class Zespolona
{
    public:
    explicit Zespolona(double real = 0.0, double imag = 0.0)
    : real_(real), imag_(imag), abs_(sqrt(real * real + imag * imag))
    {
        if (equal(real, 0.0) && equal(imag, 0.0)) {
            arg_ = 0.0;
        } else {
            arg_ = asin(imag / sqrt(real * real + imag * imag));
        }
    }
    double Re() const
    {
        return real_;
    }
    double Im() const
    {
        return imag_;
    }
    double Abs()
    {
        return abs_;
    }
    double Arg()
    {
        return arg_;
    }
    std::string Tekst()
    {
        std::ostringstream out;
        out << real_ << std::showpos << imag_ << "i";
        return out.str();
    }

    private:
    double real_;
    double imag_;
    double abs_;
    double arg_;
};

void test_Zespolona()
{
    Zespolona a;
    assert(equal(a.Im(), 0.0));
    assert(equal(a.Re(), 0.0));
    assert(equal(a.Abs(), 0.0));
    Zespolona b(3.2, 1.2);
    assert(equal(b.Im(), 1.2));
    assert(equal(b.Re(), 3.2));
    assert(equal(b.Abs(), 3.4176015));
}

int main(void)
{
    test_Zespolona();
    // Zespolona a(3.14, 0.0);
    // std::cout << a.Tekst();
    return 0;
}
