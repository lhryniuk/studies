#include "macierz.h"

Macierz5::Macierz5(std::vector<Wektor5> macierz)
{
    macierz_ = macierz;
}

double Macierz5::operator()(int n, int m) const
{
    assert(n >= 1 && n <= kRozmiar);
    assert(m >= 1 && m <= kRozmiar);
    return macierz_[n - 1][m - 1];
}

Macierz5 &Macierz5::operator+=(Macierz5 B)
{
    for (auto i = 0; i < kRozmiar; ++i) {
        for (auto j = 0; j < kRozmiar; ++j) {
            macierz_[i+1][j+1] += B(i + 1, j + 1);
        }
    }
    return *this;
}

Macierz5 &Macierz5::operator*=(Macierz5 B)
{
    std::vector<Wektor5> wynik(kRozmiar);
    for (int i = 0; i < kRozmiar; ++i) {
        for (int j = 0; j < kRozmiar; ++j) {
            wynik[i][j] = Wiersz(i) * B.Kolumna(j);
        }
    }
    macierz_ = wynik;
    return *this;
}

Macierz5 operator+(Macierz5 A, Macierz5 B)
{
    return (A += B);
}

Macierz5 operator*(Macierz5 A, Macierz5 B)
{
    return (A *= B);
}

Wektor5 operator*(Macierz5 A, Wektor5 X)
{
    std::vector<double> wynik;
    for (auto i = 0; i < Wektor5::kRozmiar; ++i) {
        wynik.push_back(A[i] * X);
    }
    return Wektor5(wynik);
}

Wektor5 Macierz5::operator[](int w) const
{
    return Wiersz(w);
}

Wektor5 Macierz5::Wiersz(int w) const
{
    assert(w >= 0 && w < kRozmiar);
    return Wektor5(macierz_[w]);
}

Wektor5 Macierz5::Kolumna(int k) const
{
    assert(k >= 0 && k < kRozmiar);
    std::vector<double> kolumna;
    for (int i = 0; i < kRozmiar; ++i) {
        kolumna.push_back(macierz_[i][k]);
    }
    return Wektor5(kolumna);
}
