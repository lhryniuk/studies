#include "wektor.h"

Wektor5::Wektor5(std::vector<double> wektor)
{
    wektor.resize(kRozmiar, 0.0);
    wektor_ = wektor;
}

double Wektor5::operator[](int i) const
{
    assert(i >= 1 && i <= kRozmiar);
    return wektor_[i - 1];
}

double& Wektor5::operator[](int i)
{
    assert(i >= 1 && i <= kRozmiar);
    return wektor_[i - 1];
}

Wektor5 &Wektor5::operator+=(Wektor5 Y)
{
    for (int i = 0; i < kRozmiar; ++i) {
        wektor_[i] += Y[i];
    }
    return *this;
}

Wektor5 &Wektor5::operator*=(double a)
{
    for (int i = 0; i < kRozmiar; ++i) {
        wektor_[i] *= a;
    }
    return *this;
}

double operator*(Wektor5 X, Wektor5 Y)
{
    double wynik = 0.0;
    for (int i = 0; i < Wektor5::kRozmiar; ++i) {
        wynik += X[i] * Y[i];
    }
    return wynik;
}

Wektor5 operator*(double a, Wektor5 X)
{
    return (X *= a);
}

Wektor5 operator*(Wektor5 X, double a)
{
    return (X *= a);
}

double Wektor5::Dlugosc() const
{
    double dlugosc = 0.0;
    for (int i = 0; i < kRozmiar; ++i) {
        dlugosc += wektor_[i];
    }
    return sqrt(dlugosc);
}
