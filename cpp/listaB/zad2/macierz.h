#ifndef MACIERZ_H
#define MACIERZ_H

#include <vector>
#include "wektor.h"

class Macierz5
{
    public:
    static const int kRozmiar = Wektor5::kRozmiar;
    explicit Macierz5(std::vector<Wektor5> macierz = std::vector<Wektor5>());
    double operator()(int n, int m) const;
    Macierz5 &operator+=(Macierz5 B);
    Macierz5 &operator*=(Macierz5 B);
    Wektor5 operator[](int w) const;
    Wektor5 Wiersz(int w) const;
    Wektor5 Kolumna(int k) const;

    private:
    std::vector<Wektor5> macierz_;
};

Macierz5 operator+(Macierz5 A, Macierz5 B);
Macierz5 operator*(Macierz5 A, Macierz5 B);
Wektor5 operator*(Macierz5 A, Wektor5 X);


#endif
