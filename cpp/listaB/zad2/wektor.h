#ifndef WEKTOR_H
#define WEKTOR_H

#include <cassert>
#include <cmath>
#include <vector>

class Wektor5
{
    public:
    static const int kRozmiar = 5;
    Wektor5(std::vector<double> wektor = std::vector<double>(kRozmiar, 0.0));
    double operator[](int i) const;
    double& operator[](int i);
    Wektor5 &operator+=(Wektor5 Y);
    Wektor5 &operator*=(double a);
    double Dlugosc() const;

    private:
    std::vector<double> wektor_;
};

double operator*(Wektor5 X, Wektor5 Y); /* iloczyn skalarny */
Wektor5 operator*(double a, Wektor5 X);
Wektor5 operator*(Wektor5 X, double a);

#endif
