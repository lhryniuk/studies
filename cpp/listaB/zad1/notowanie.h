#ifndef NOTOWANIE_H_
#define NOTOWANIE_H_

#include "data.h"

class Notowanie
{
    public:
    Notowanie(long long data, double cena);
    double DataN() const;
    double Cena() const;

    private:
    Data data_;
    double cena_;
};

#endif
