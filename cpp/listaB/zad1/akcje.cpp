#include "data.h"
#include "notowanie.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

const std::string kursplik = "TPSA.mst";
const std::string trendplik = "trend.txt";

void wylicz_trend(std::vector<Notowanie> notowania, double &a, double &b)
{
    double XX = 0.0;
    double XY = 0.0;
    double X = 0.0;
    double Y = 0.0;
    double N = notowania.size();
    for (unsigned i = 0; i < notowania.size(); ++i) {
        double x = notowania[i].DataN();
        double y = notowania[i].Cena();
        X += x;
        Y += y;
        XX += x * x;
        XY += x * y;
    }
    /* pochodne czastkowe:
     * da = x^2 * a + x * b - x * y
     * db = x * a + b - y
     *
     * ze wzorow Cramera */
    double W = XX * N - X * X;
    double Wa = N * XY - X * Y;
    double Wb = XX * Y - Y * X;
    if (W != 0) {
        a = Wa / W;
        b = Wb / W;
    } else {
        a = 0.0;
        b = 0.0;
    }
}

std::vector<Notowanie> wczytaj_dane()
{
    std::ifstream in(kursplik.c_str());
    if (in) {
        std::cerr << "wczytuje!\n";
        std::string tmpstr;
        std::vector<Notowanie> dane;
        long long data = 0;
        char c = ' ';
        double tmp = 0.0;
        double close = 0.0;
        getline(in, tmpstr);
        while (in && !in.eof()) {
            getline(in, tmpstr);
            if (in.eof())
                break;
            tmpstr = tmpstr.substr(5);
            std::istringstream instr(tmpstr);
            instr >> data >> c >> tmp >> c >> tmp >> c >> tmp >> c >> close >> c >> tmp;
            if (in) {
                dane.push_back(Notowanie(data, close));
            }
        }
        in.close();
        std::cerr << "zwracam dane\n";
        return dane;
    } else {
        std::cerr << "nie udalo sie otworzyc pliku do wczytania danych!\n";
        return std::vector<Notowanie>();
    }
}

void zapisz_dane(std::vector<Notowanie> notowania, double a, double b)
{
    std::ofstream out(trendplik.c_str());
    out.setf(std::ios::fixed);
    out.precision(4);
    if (out) {
        for (unsigned i = 0; i < notowania.size(); ++i) {
            out << notowania[i].DataN() << ';' << notowania[i].Cena() << ';'
                << notowania[i].DataN() * a + b << '\n';
        }
    } else {
        std::cerr << "nie udalo sie otworzyc pliku do zapisu!\n";
    }
}

int main(void)
{
    std::vector<Notowanie> notowania;
    notowania = wczytaj_dane();
    double a = 0.0;
    double b = 0.0;
    if (notowania.empty()) {
        std::cerr << "nie wczytano danych!\n";
    } else {
        wylicz_trend(notowania, a, b);
        std::cout << a << ' ' << b << std::endl;
        zapisz_dane(notowania, a, b);
    }
    return 0;
}
