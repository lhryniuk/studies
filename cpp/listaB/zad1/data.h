#ifndef DATA_H_
#define DATA_H_

#include <cassert>
#include <iostream>
#include <vector>

class Data
{
    public:
    explicit Data(long long data);
    double Ulamkowo() const;

    private:
    /* do liczenia liczby dni przy konwersji na ulamek
     * oraz sprawdzania sensownosci daty (by uniknac ifow) */
    static const std::vector<int> kLiczbaDniN;
    static const std::vector<int> kLiczbaDniP;
    void Sensowna(int rok, int miesiac, int dzien);
    bool Przestepny(int rok);
    double KonwersjaNaUlamek();
    std::vector<int> kLiczbaDni;
    int dzien_;
    int miesiac_;
    int rok_;
    double ulamek_;
    bool przestepny_;
};

#endif
