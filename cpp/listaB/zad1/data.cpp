#include "data.h"

/* inicjalizacja statycznych stalych */
const std::vector<int> Data::kLiczbaDniN{ 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
const std::vector<int> Data::kLiczbaDniP{ 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

Data::Data(long long data)
{
    int dzien = data % 100;
    data /= 100;
    int miesiac = data % 100;
    data /= 100;
    int rok = data;
    Sensowna(rok, miesiac, dzien);
    przestepny_ = Przestepny(rok);
    dzien_ = dzien;
    miesiac_ = miesiac;
    rok_ = rok;
    ulamek_ = KonwersjaNaUlamek();
}

double Data::Ulamkowo() const
{
    return ulamek_;
}

void Data::Sensowna(int rok, int miesiac, int dzien)
{
    assert(rok > 0);
    assert((1 <= miesiac && miesiac <= 12));
    assert(dzien > 0);
    std::vector<int> kLiczbaDni = (przestepny_ ? kLiczbaDniP : kLiczbaDniN);
    assert(dzien <= kLiczbaDni[miesiac]);
}

bool Data::Przestepny(int rok)
{
    return (!(rok % 400) || (!(rok % 4) && (rok % 100)));
}

double Data::KonwersjaNaUlamek()
{
    int liczba_dni = 0;
    int mianownik = 0;
    std::vector<int> kLiczbaDni = (przestepny_ ? kLiczbaDniP : kLiczbaDniN);
    mianownik = (przestepny_ ? 366 : 365);
    for (int i = 1; i < miesiac_; ++i) {
        liczba_dni += kLiczbaDni[i];
    }
    liczba_dni += dzien_;
    return (double(rok_) + double(liczba_dni) / double(mianownik));
}
