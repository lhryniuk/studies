#include <iostream>

struct Zespolona {
    Zespolona(double Real = 0.0, double Imag = 0.0) : Re_(Real), Im_(Imag)
    {
    }
    double Re_;
    double Im_;
};

std::ostream &operator<<(std::ostream &out, Zespolona number)
{
    out << number.Re_ << std::showpos << number.Im_ << 'i';
    return out;
}

std::istream &operator>>(std::istream &in, Zespolona &number)
{
    auto i = char(0);
    auto real = 0.0;
    auto imag = 0.0;
    in >> real >> imag >> i;
    if (in) {
        if (i == 'i') {
            number = Zespolona(real, imag);
        } else {
            in.setstate(std::ios_base::failbit);
        }
    }
    return in;
}

int main()
{
    Zespolona a;
    // read and give back number
    std::cin >> a;
    if (std::cin) {
        std::cout << a << '\n';
    } else {
        std::cout << "std::cin zepsuty\n";
    }
}
