#include <cassert>
#include <iostream>
#include <string>

std::string dot2comma(std::string text)
{
    auto limit = text.length() - 1;
    for (auto i = 1u; i < limit; ++i) {
        if (text[i] == '.' && (isdigit(text[i - 1]) && isdigit(text[i + 1]))) {
            text[i] = ',';
        }
    }
    return text;
}

void test_dot2comma()
{
    assert(dot2comma("Liczba pi ma wartosc 3.14.") == "Liczba pi ma wartosc 3,14.");
    assert(dot2comma("Liczba pi ma wartosc 3.14. Liczba Eulera "
                     "e ma natomiast wartosc 2.71, obie w przyblizeniu...") ==
           "Liczba pi ma wartosc 3,14. Liczba Eulera e "
           "ma natomiast wartosc 2,71, obie w przyblizeniu...");
    assert(dot2comma("3.14") == "3,14");
    assert(dot2comma("1.12 5.14 6.33 74564.13213141") == "1,12 5,14 6,33 74564,13213141");
}

int main()
{
    test_dot2comma();
}
