#include <cassert>
#include <iostream>
#include <string>

/*
 * counts leading bits
 * 128 =
 * 10000000(2)
 * 128 >>= 1
 * 01000000(2) = 64
 * ...
 */
constexpr auto first_bits(int byte)
{
    int bits = 0;
    int mask = 128;
    while ((mask > 0) && (byte & mask)) {
        ++bits;
        mask >>= 1;
    }
    return bits;
}

//  bits -  first bit - min in hex
//   1   -  0xxxxxxx  -     00
//   2   -  110xxxxx  -     C0
//   3   -  1110xxxx  -     E0
//   4   -  11110xxx  -     F0
//   5   -  111110xx  -     F8
//   6   -  1111110x  -     FC

std::string inversed(std::string text)
{
    std::string inversed_text;
    for (unsigned i = 0; i < text.size(); ++i) {
        int bits = first_bits(text[i]);
        inversed_text = text.substr(i, std::max(bits, 1)) + inversed_text;
        i += std::max(0, bits - 1);
    }
    return inversed_text;
}

void test_inversed()
{
    assert(inversed("gżegżółka") == "akłóżgeżg");
    assert(inversed("żółw") == "włóż");
    assert(inversed("ąćęłńóśźż") == "żźśóńłęćą");
}

int main(void)
{
    std::cout << inversed("żółw") << '\n';
    std::cout << "\xEA\x99\xA6  \xEA\x99\xAD \xE2\x87\x89  \xEA\x99\xAC" << '\n';
    std::cout << inversed("\xEA\x99\xA6  \xEA\x99\xAD \xE2\x87\x89  \xEA\x99\xAC") << '\n';
    std::cout << "\xE0\xBA\xA2  \xD7\x90 \xD5\x89  \xD8\xAC" << '\n';
    std::cout << inversed("\xE0\xBA\xA2  \xD7\x90 \xD5\x89  \xD8\xAC") << '\n';
    test_inversed();
    return 0;
}
