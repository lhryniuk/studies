#include "check.h"

std::string remove_comments(std::string text)
{
    if (text.empty())
        return text;
    int end = text.size() - 1; // last character index
    auto i = 0; // while-index
    if (text[0] == '(') {
        i = 0;
        while (i < end && text[i++] != ')')
            ; // !trick!
        if (text[i - 1] == ')') {
            return text.substr(i);
        }
    } else if (text[end] == ')') {
        i = end;
        while (i >= 0 && text[i--] != '(')
            ; // !trick!
        if (text[i + 1] == '(') {
            return text.substr(0, i + 1);
        }
    }
    return text;
}

void split_address(std::string address, std::string &local_part, std::string &domain_part)
{
    unsigned i = 0;
    while (i < address.size() && address[i] != '@') {
        local_part += address[i];
        ++i;
    }
    // here, after first ++i, variable i should be
    // one character after '@'
    while (++i < address.size()) {
        domain_part += address[i];
    }
}

bool validate_ip(std::string ip)
{
    int dots = 0;
    for (unsigned i = 0; i < ip.size(); ++i) {
        if (!(isdigit(ip[i]) || (ip[i] == '.'))) {
            return false;
        }
        if (ip[i] == '.')
            ++dots;
    }
    if (dots != 3)
        return false;
    std::istringstream in(ip);
    char dot = '0';
    int octet = 0;
    for (int i = 0; i < 4; ++i) {
        in >> octet;
        if (i < 3)
            in >> dot;
        if (!in || octet > 255)
            return false;
    }
    return true;
}

bool validate_local(std::string local)
{
    if (local.size() > 64) // maximum size
        return false;

    local = remove_comments(local); // for easier validating
    bool quote = false;
    for (unsigned i = 0; i < local.size(); ++i) {
        // quote-state change
        if (local[i] == '\"') {
            quote = !quote;
            // part in quotes should be dot separated or be single element
            if ((quote && ((i > 0) && (local[i - 1] != '.'))) ||
                (!quote && ((i < local.size() - 1) && (local[i + 1] != '.'))))
                return false;
        }

        // special characters used with quotes and backslash
        if (!quote) {
            if (local[i] == '(' || local[i] == ')' || local[i] == ',' || local[i] == ':' ||
                local[i] == ';' || local[i] == '<' || local[i] == '>' || local[i] == '@' ||
                local[i] == '[' || local[i] == '\\' || local[i] == ']')
                return false;
        } else if (local[i] == '\\') {
            if (((i + 1 < local.size()) && ((local[i + 1] == '\\') || (local[i + 1] == '\"')))) {
                ++i;
            } else {
                return false;
            }
        }
    }
    return true;
}

inline bool validate_label(std::string label) // tested with validate_domain()
{
    if (label.empty())
        return false;
    if (label[0] == '-' || label[label.size() - 1] == '-')
        return false;
    for (unsigned i = 0; i < label.size(); ++i) {
        if (!((isdigit(label[i])) || (isalpha(tolower(label[i]))) || (label[i] == '-')))
            return false;
    }
    return true;
}

bool validate_domain(std::string domain)
{
    // maximum size
    if (domain.size() > 253)
        return false;
    domain = remove_comments(domain);
    int domain_size = domain.size();
    // if domain is ip address and there is no [ ]
    if (validate_ip(domain))
        return false;
    if (domain[0] == '[' && domain[domain_size - 1] == ']') {
        return validate_ip(domain.substr(1, domain_size - 2));
    } else {
        for (int i = 0; i < domain_size; ++i) {
            std::string label;
            int k = i;
            while ((i < domain_size) && (domain[i] != '.'))
                ++i;
            label = domain.substr(k, i - k);
            bool result = validate_label(label);
            if (!result)
                return false;
        }
        if (domain[domain_size - 1] == '.')
            return false;
    }
    return true;
}

std::string consistent(std::string text, unsigned &begin)
{
    assert(begin < text.size());
    std::string element;
    bool quote = false;
    while ((begin < text.size()) && text[begin] == ' ')
        ++begin;
    while ((begin < text.size()) && !(!quote && text[begin] == ' ')) {
        if (text[begin] == '\"') {
            quote = !quote;
        }
        element += text[begin];
        ++begin;
    }
    return element;
}

std::string find_address(std::string text)
{
    std::string address;
    std::string local, domain;
    for (unsigned i = 0; i < text.size(); ++i) {
        local.clear();
        domain.clear();
        // possibly address
        address = consistent(text, i);
        // maximum size and one @
        if ((ats_number(address) != 1) || (address.size() > 254))
            continue;
        split_address(address, local, domain);
        if (validate_local(local) && validate_domain(domain)) {
            return address;
        }
    }
    return std::string();
}
