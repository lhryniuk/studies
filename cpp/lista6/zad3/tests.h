#ifndef TESTS_H
#define TESTS_H

#include "check.h"
#include <cassert>

void test_ats_number();
void test_remove_comments();
void test_validate_ip();
void test_validate_local();
void test_validate_domain();
void test_consistent();
void test_find_address();
void test();

#endif
