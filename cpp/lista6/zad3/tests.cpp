#include "tests.h"

void test_ats_number()
{
    assert(ats_number("\"@@@@\"@qwer@rty\"@\"") == 2);
    assert(ats_number("\"-@-@-@-@-\"") == 0);
    assert(ats_number("simple@domain.com") == 1);
}

void test_remove_comments()
{
    assert(remove_comments("(...)asd") == "asd");
    assert(remove_comments("asd(...)") == "asd");
    assert(remove_comments("(...asd") == "(...asd");
    assert(remove_comments("asd...)") == "asd...)");
    assert(remove_comments("a") == "a");
    assert(remove_comments("") == "");
}

void test_validate_ip()
{
    assert(validate_ip("192.168.1.1"));
    assert(validate_ip("1.1.1.1"));
    assert(validate_ip("123.51.41.156"));
    assert(!validate_ip("12..31.413"));
    assert(!validate_ip("23.23.23.333"));
    assert(!validate_ip("123.-3.31.23"));
    assert(!validate_ip("13.ad.12.13"));
}

void test_validate_local()
{
    assert(validate_local("name.lastname"));
    assert(validate_local("\"Fred Bloggs\""));
    assert(validate_local("\"Abc@def\""));
    assert(validate_local("\"much.more unusual\""));
    assert(!validate_local("\"\"\"\""));
    assert(!validate_local("a\"b(c)d,e:f;g<h>i[j\\k]l"));
    assert(!validate_local("just\"not\"right"));
    assert(!validate_local("still\"not\\allowed"));
}

void test_validate_domain()
{
    assert(validate_domain("home.com"));
    assert(validate_domain("bar.com"));
    assert(!validate_domain("bar.com."));
    assert(!validate_domain("-bla.com"));
    assert(!validate_domain("-ba-"));
    assert(!validate_domain("[123.1413.131.da1]"));
    assert(!validate_domain("[123.123.123.333]"));
}

void test_consistent()
{
    unsigned i = 0;
    assert(consistent("\"    dab@da.cas\".dadaw", i) == "\"    dab@da.cas\".dadaw");
    i = 2;
    assert(consistent("   dada.sda   ", i) == "dada.sda");
    i = 2;
    assert(consistent("da dad da", i) == "dad");
}

void test_find_address()
{
    assert(find_address("name.lastname@domain.com aflasd dasda dada") ==
           "name.lastname@domain.com");
    assert(find_address("a@bar.com.   adas qeqd aaa@[123.123.123.123]a "
                        "aaa@[123.123.123.123]") == "aaa@[123.123.123.123]");
    assert(find_address("test@...........com foobar@192.168.0.1 aaa@.com") == "");
    assert(find_address("qwerty dasdaf dkqwada vcxoaps a@com") == "a@com");
}

void test()
{
    test_ats_number();
    test_remove_comments();
    test_validate_ip();
    test_validate_local();
    test_validate_domain();
    test_consistent();
    test_find_address();
}
