#include "check.h"
#include "tests.h"
#include <cassert>
#include <iostream>
#include <sstream>
#include <string>

int main(void)
{
    test();
    std::string text("dasda lukequaint@gmail.com dadas");
    std::cout << find_address(text) << '\n';
    std::cout << find_address("dasda afkafj faklfjae fqkjkfa") << '\n';
    std::cout << find_address("dasda afkaf adas@[192.168.1.13]") << '\n';
    std::cout << find_address("dasda afkaf \"hello{{{{\".adas@[192.168.1.13]") << '\n';
    return 0;
}
