#ifndef CHECK_H
#define CHECK_H

#include <cassert>
#include <sstream>
#include <string>

// counts @
inline int ats_number(std::string text)
{
    int ats = 0;
    bool quote = false; // text in quotes?
    for (unsigned i = 0; i < text.size(); ++i) {
        if (text[i] == '\"')
            quote = !quote;
        if (!quote && text[i] == '@')
            ++ats;
    }
    return ats;
}

/*
 * comments can be placed in ( )
 * at the begining/end of each part
 */
std::string remove_comments(std::string text);

/*
 * give local and domain part of address
 * for separated validating
 */
void split_address(std::string address, std::string &local_part, std::string &domain_part);

/*
 * ip can be given in [ ] at domain part
 */
bool validate_ip(std::string ip);

/*
 * checks local part
 * rules below
 */
bool validate_local(std::string local);

/*
 * checks domain part:
 *  - labels are connected with dots
 *  - domain.size() < 253
 *  - 0 < label.size() < 64
 *  - only 0-9, a-z (case-insensitive) and hyphens in labels
 */
inline bool validate_label(std::string label); // tested with validate_domain()

bool validate_domain(std::string domain);

/*
 * finds consistent part of text
 * which can be address
 * omits leading spaces
 */
std::string consistent(std::string text, unsigned &begin);

std::string find_address(std::string text);

#endif
