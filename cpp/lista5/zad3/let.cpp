#include <cassert>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

/*
 * |
 * zero     -   zlotych (3)
 * jeden    -   zloty   (1)
 * dwa      -   zlote   (2)
 * trzy     -   zlote
 * cztery   -   zlote
 * piec     -   zlotych (3)
 * szesc    -   zlotych
 * siedem   -   zlotych
 * osiem    -   zlotych
 * dziewiec -   zlotych
 *
 */

/*
 *  sprawdzanie odmiany rzeczownika
 *  wg tabeli wyzej
 */
auto form(int n) -> int
{
    assert(n >= 0);
    if (n == 1)
        return 1;
    n %= 100;
    if (n > 10 && n < 20)
        return 3;
    n %= 10;
    if (n > 1 && n < 5)
        return 2;
    return 3;
}

auto test_form() -> void
{
    assert(form(0) == 3);
    assert(form(1) == 1);
    assert(form(122) == 2);
    assert(form(43) == 2);
    assert(form(145) == 3);
}

/*
 *  zamiana liczby na polski liczebnik
 */

auto one(int n) -> std::string
{
    assert(n > 0 && n < 10);
    if (n == 1)
        return "jeden";
    if (n == 2)
        return "dwa";
    if (n == 3)
        return "trzy";
    if (n == 4)
        return "cztery";
    if (n == 5)
        return "piec";
    if (n == 6)
        return "szesc";
    if (n == 7)
        return "siedem";
    if (n == 8)
        return "osiem";
    if (n == 9)
        return "dziewiec";
    return std::string();
}

auto test_one() -> void
{
    assert(one(1) == "jeden");
    assert(one(5) == "piec");
}

auto ten(int n) -> std::string
{
    if (n < 10) {
        return one(n);
    } else if (n < 20) {
        if (n == 10)
            return "dziesiec";
        if (n == 11)
            return "jedenascie";
        if (n < 14)
            return one(n % 10) + "nascie";
        if (n == 14)
            return "czternascie";
        if (n == 15)
            return "pietnascie";
        if (n == 16)
            return "szesnascie";
        if (n < 19)
            return one(n % 10) + "nascie";
        return "dziewietnascie";
    } else {
        std::string ten_str;
        int dec = n / 10;
        if (dec > 4)
            ten_str = one(n / 10) + "dziesiat";
        if (dec == 2)
            ten_str = "dwadziescia";
        if (dec == 3)
            ten_str = "trzydziesci";
        if (dec == 4)
            ten_str = "czterdziesci";
        if (n % 10 != 0) {
            ten_str += " " + one(n % 10);
        }
        return ten_str;
    }
    return std::string();
}

auto test_ten() -> void
{
    assert(ten(31) == "trzydziesci jeden");
    assert(ten(10) == "dziesiec");
    assert(ten(14) == "czternascie");
    assert(ten(99) == "dziewiecdziesiat dziewiec");
    return;
}

auto hundred(int number) -> std::string
{
    assert(number >= 0);
    std::string number_string;
    int hundreds_digit = number / 100;
    if (hundreds_digit) {
        if (hundreds_digit > 4)
            number_string = one(hundreds_digit) + "set";
        if (hundreds_digit == 1)
            number_string = "sto";
        if (hundreds_digit == 2)
            number_string = "dwiescie";
        if (hundreds_digit == 3)
            number_string = "trzysta";
        if (hundreds_digit == 4)
            number_string = "czterysta";
        if (number % 100)
            number_string += " ";
    }
    if (number % 100) {
        number_string += ten(number % 100);
    }
    return number_string;
}

auto test_hundred() -> void
{
    assert(hundred(310) == "trzysta dziesiec");
    assert(hundred(212) == "dwiescie dwanascie");
    assert(hundred(349) == "trzysta czterdziesci dziewiec");
}

auto split(int number) -> std::string
{
    if (!number)
        return "zero";
    std::string full_number_string;
    if (number < 0) {
        number = -number;
        full_number_string = "minus ";
    }
    int thousands = number / 1000;
    int hundreds = number % 1000;
    if (thousands) {
        full_number_string += hundred(thousands) + " ";
        int form_number = form(thousands % 100);
        if (form_number == 1)
            full_number_string = "tysiac";
        if (form_number == 2)
            full_number_string += "tysiace";
        if (form_number == 3)
            full_number_string += "tysiecy";
        if (hundreds) {
            full_number_string += " ";
        }
    }
    full_number_string += hundred(hundreds);
    return full_number_string;
}

auto test_split() -> void
{
    assert(split(0) == "zero");
    assert(split(1000) == "tysiac");
    assert(split(13134) == "trzynascie tysiecy sto trzydziesci cztery");
    assert(split(-92) == "minus dziewiecdziesiat dwa");
    assert(split(-32512) == "minus trzydziesci dwa tysiace piecset dwanascie");
    return;
}

auto test_number() -> void
{
    test_form();
    test_one();
    test_ten();
    test_hundred();
    test_split();
}

/*
 *  zwracaja wlasciwe formy slow zloty i grosz
 */
auto zlote(int n) -> std::string
{
    int form_n = form(n);
    if (form_n == 1)
        return "zloty";
    if (form_n == 2)
        return "zlote";
    return "zlotych";
}

auto grosze(int n) -> std::string
{
    int form_n = form(n);
    if (form_n == 1)
        return "grosz";
    if (form_n == 2)
        return "grosze";
    return "groszy";
}

/*
 *  sprawdza przestepnosc roku
 */
auto leap_year(int year) -> bool
{
    assert(year >= 1600);
    return (!(year % 400) || (!(year % 4) && (year % 100)));
}

auto test_leap_year() -> void
{
    assert(!leap_year(1700));
    assert(leap_year(1884));
    assert(leap_year(1752));
    assert(!leap_year(1911));
    assert(leap_year(2000));
}

auto day_of_week(int day, int month, int year) -> int
{
    /*
     * liczymy dni 0..6
     * aby ulatwic operacje modulo
     * nastepnie na koncu dodajemy 1
     */
    int week_day = 5;
    int start_year = 1600;
    int start_month = 1;


    while (start_year != year) {
        if (leap_year(start_year)) {
            week_day += 2;
        } else {
            week_day += 1;
        }
        ++start_year;
    }
    week_day %= 7;

    while (start_month != month) {
        if (start_month == 2 && leap_year(year)) {
            week_day += 1;
        } else if (((start_month < 8) && (start_month % 2 != 0)) ||
                   ((start_month > 7) && (start_month % 2 == 0))) {
            week_day += 3;
        } else if (start_month != 2) {
            week_day += 2;
        }
        ++start_month;
    }

    week_day += day - 1;
    week_day %= 7;
    return week_day + 1;
}

auto test_day_of_week() -> void
{
    assert(day_of_week(1, 1, 1600) == 6);
    assert(day_of_week(1, 1, 1803) == 6);
    assert(day_of_week(5, 10, 1803) == 3);
    assert(day_of_week(1, 4, 2013) == 1);
    assert(day_of_week(11, 4, 1903) == 6);
    assert(day_of_week(12, 8, 1999) == 4);
}

/*
 *  zamiana daty na zapis slowny
 */
auto day_string(int day) -> std::string
{
    assert(day >= 1 && day <= 7);
    if (day == 1)
        return "poniedzialek";
    if (day == 2)
        return "wtorek";
    if (day == 3)
        return "sroda";
    if (day == 4)
        return "czwartek";
    if (day == 5)
        return "piatek";
    if (day == 6)
        return "sobota";
    if (day == 7)
        return "niedziela";
    return std::string();
}

auto month_string(int month) -> std::string
{
    assert(month >= 1 && month <= 12);
    if (month == 1)
        return "stycznia";
    if (month == 2)
        return "lutego";
    if (month == 3)
        return "marca";
    if (month == 4)
        return "kwietnia";
    if (month == 5)
        return "maja";
    if (month == 6)
        return "czerwca";
    if (month == 7)
        return "lipca";
    if (month == 8)
        return "sierpnia";
    if (month == 9)
        return "wrzesnia";
    if (month == 10)
        return "pazdziernika";
    if (month == 11)
        return "listopada";
    if (month == 12)
        return "grudnia";
    return std::string();
}

auto date_string(int day, int month, int year) -> std::string
{
    assert(year > 0);
    assert(day >= 1 && day <= 31);
    assert(month >= 1 && month <= 12);
    std::ostringstream out;
    out << day_string(day_of_week(day, month, year)) + ", ";
    out << day << ' ' << month_string(month) << ' ' << year;
    return out.str();
}

auto test_date() -> void
{
    test_leap_year();
    test_day_of_week();
}

auto letter(int gender, double value, std::string first_name, std::string surname, std::string address, int day, int month, int year)
    -> std::string
{
    std::ostringstream out;
    std::string title, live, must;
    if (gender) {
        title = "Pan";
        live = "zamieszkaly";
        must = "zobowiazany";
    } else {
        title = "Pani";
        live = "zamieszkala";
        must = "zobowiazana";
    }
    int zl_value = int(value);
    int gr_value = int(100.0 * (value - int(value)));
    out << "\t\t\t" << std::setw(30) << "Wroclaw"
        << "\n\t\t\t" << std::setw(30) << date_string(day, month, year) << "\n\n"
        << title << " " << first_name << " " << surname << ",\n"
        << live << " " << address << ", " << must
        << " jest do niezwlocznej splaty dlugu w wysokosci\n"
        << zl_value << " zl " << gr_value << " gr (slownie: " << split(zl_value) << " "
        << zlote(zl_value) << " " << split(gr_value) << " " << grosze(gr_value) << ").";
    return out.str();
}

int main(void)
{
    test_number();
    std::cout << letter(0, 21326.31, "Foo", "Bar", "/dev/null", 4, 4, 2013) << '\n';
    return 0;
}
