#include <cassert>
#include <iostream>
#include <sstream>
#include <string>

/*
 *                  %7
 *  styczen          3
 *  luty            0/1
 *  marzec           3
 *  kwiecien         2
 *  maj              3
 *  czerwiec         2
 *  lipiec           3
 *  sierpien         3
 *  wrzesien         2
 *  pazdziernik      3
 *  listopad         2
 *  grudzien         3
 *
 *  day no. 0 - 01.01.1600 - sobota
 */

constexpr auto leap_year(int year)
{
    assert(year >= 1600);
    return (!(year % 400) || (!(year % 4) && (year % 100)));
}

auto test_leap_year() -> void
{
    assert(!leap_year(1700));
    assert(leap_year(1884));
    assert(leap_year(1752));
    assert(!leap_year(1911));
    assert(leap_year(2000));
}

auto day_of_week(int day, int month, int year) -> int
{
    /*
     * liczymy dni 0..6
     * aby ulatwic operacje modulo
     * nastepnie na koncu dodajemy 1
     */
    int week_day = 5;
    int start_year = 1600;
    int start_month = 1;


    while (start_year != year) {
        if (leap_year(start_year)) {
            week_day += 2;
        } else {
            week_day += 1;
        }
        ++start_year;
    }
    week_day %= 7;

    while (start_month != month) {
        if (start_month == 2 && leap_year(year)) {
            week_day += 1;
        } else if (((start_month < 8) && (start_month % 2 != 0)) ||
                   ((start_month > 7) && (start_month % 2 == 0))) {
            week_day += 3;
        } else if (start_month != 2) {
            week_day += 2;
        }
        ++start_month;
    }

    week_day += day - 1;
    week_day %= 7;
    return week_day + 1;
}

auto test_day_of_week() -> void
{
    assert(day_of_week(1, 1, 1600) == 6);
    assert(day_of_week(1, 1, 1803) == 6);
    assert(day_of_week(5, 10, 1803) == 3);
    assert(day_of_week(1, 4, 2013) == 1);
    assert(day_of_week(11, 4, 1903) == 6);
    assert(day_of_week(12, 8, 1999) == 4);
}

auto day_string(int day) -> std::string
{
    assert(day >= 1 && day <= 7);
    if (day == 1)
        return "poniedzialek";
    if (day == 2)
        return "wtorek";
    if (day == 3)
        return "sroda";
    if (day == 4)
        return "czwartek";
    if (day == 5)
        return "piatek";
    if (day == 6)
        return "sobota";
    if (day == 7)
        return "niedziela";
    return std::string();
}

auto month_string(int month) -> std::string
{
    assert(month >= 1 && month <= 12);
    if (month == 1)
        return "stycznia";
    if (month == 2)
        return "lutego";
    if (month == 3)
        return "marca";
    if (month == 4)
        return "kwietnia";
    if (month == 5)
        return "maja";
    if (month == 6)
        return "czerwca";
    if (month == 7)
        return "lipca";
    if (month == 8)
        return "sierpnia";
    if (month == 9)
        return "wrzesnia";
    if (month == 10)
        return "pazdziernika";
    if (month == 11)
        return "listopada";
    if (month == 12)
        return "grudnia";
    return std::string();
}

auto date_string(int day, int month, int year) -> std::string
{
    assert(year > 0);
    assert(day >= 1 && day <= 31);
    assert(month >= 1 && month <= 12);
    std::ostringstream out;
    out << day_string(day_of_week(day, month, year)) + ", ";
    out << day << ' ' << month_string(month) << ' ' << year;
    return out.str();
}

auto test() -> void
{
    test_leap_year();
    test_day_of_week();
}

int main(void)
{
    test();
    int day{ 0 }, month{ 0 }, year{ 0 };
    while (true) {
        std::cin >> day >> month >> year;
        std::cout << date_string(day, month, year) << '\n';
    }
    return 0;
}
