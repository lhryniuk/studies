#include <cassert>
#include <iostream>
#include <string>

/*
 * |                         split()                              |
 * |               hundred()              | elements()  |
 * |            ten()           |
 * | one() |                                   form()      form()
 * zero     -   *    -    *     -    *    - elemetow    -    *
 * jeden    -   *    -    *     -    *    - element     - tysiąc
 * dwa      - nascie - dziescia -    *    - elementy    - tysiace
 * trzy     - nascie - dziesci  -    *    - elementy    - tysiace
 * cztery   -   *    -    *     -    *    - elementy    - tysiace
 * piec     -   *    - dziesiat -   set   - elementow   - tysiecy
 * szesc    -   *    - dziesiat -   set   - elementow   - tysiecy
 * siedem   - nascie - dziesiat -   set   - elementow   - tysiecy
 * osiem    - nascie - dziesiat -   set   - elementow   - tysiecy
 * dziewiec -   *    - dziesiat -   set   - elementow   - tysiecy
 *
 */

auto form(int n) -> int
{
    assert(n >= 0);
    if (n == 1)
        return 1;
    n %= 100;
    if (n > 10 && n < 20)
        return 3;
    n %= 10;
    if (n > 1 && n < 5)
        return 2;
    return 3;
}

auto test_form() -> void
{
    assert(form(0) == 3);
    assert(form(1) == 1);
    assert(form(122) == 2);
    assert(form(43) == 2);
    assert(form(145) == 3);
}

auto one(int n) -> std::string
{
    assert(n > 0 && n < 10);
    if (n == 1)
        return "jeden";
    if (n == 2)
        return "dwa";
    if (n == 3)
        return "trzy";
    if (n == 4)
        return "cztery";
    if (n == 5)
        return "pięć";
    if (n == 6)
        return "sześć";
    if (n == 7)
        return "siedem";
    if (n == 8)
        return "osiem";
    if (n == 9)
        return "dziewięć";
    return std::string();
}

auto test_one() -> void
{
    assert(one(1) == "jeden");
    assert(one(5) == "pięć");
}

auto ten(int n) -> std::string
{
    if (n < 10) {
        return one(n);
    } else if (n < 20) {
        if (n == 10)
            return "dziesięć";
        if (n == 11)
            return "jedenaście";
        if (n < 14)
            return one(n % 10) + "naście";
        if (n == 14)
            return "czternaście";
        if (n == 15)
            return "piętnaście";
        if (n == 16)
            return "szesnaście";
        if (n < 19)
            return one(n % 10) + "naście";
        return "dziewiętnaście";
    } else {
        std::string ten_str;
        int dec = n / 10;
        if (dec > 4)
            ten_str = one(n / 10) + "dziesiąt";
        if (dec == 2)
            ten_str = "dwadzieścia";
        if (dec == 3)
            ten_str = "trzydzieści";
        if (dec == 4)
            ten_str = "czterdzieści";
        if (n % 10 != 0) {
            ten_str += " " + one(n % 10);
        }
        return ten_str;
    }
    return std::string();
}

auto test_ten() -> void
{
    assert(ten(31) == "trzydzieści jeden");
    assert(ten(10) == "dziesięć");
    assert(ten(14) == "czternaście");
    assert(ten(99) == "dziewięćdziesiąt dziewięć");
    return;
}

auto hundred(int number) -> std::string
{
    assert(number >= 0);
    std::string number_string;
    int hundreds_digit = number / 100;
    if (hundreds_digit) {
        if (hundreds_digit > 4)
            number_string = one(hundreds_digit) + "set";
        if (hundreds_digit == 1)
            number_string = "sto";
        if (hundreds_digit == 2)
            number_string = "dwieście";
        if (hundreds_digit == 3)
            number_string = "trzysta";
        if (hundreds_digit == 4)
            number_string = "czterysta";
        if (number % 100)
            number_string += " ";
    }
    if (number % 100) {
        number_string += ten(number % 100);
    }
    return number_string;
}

auto test_hundred() -> void
{
    assert(hundred(310) == "trzysta dziesięć");
    assert(hundred(212) == "dwieście dwanaście");
    assert(hundred(349) == "trzysta czterdzieści dziewięć");
}

auto split(int number) -> std::string
{
    if (!number)
        return "zero";
    std::string full_number_string;
    if (number < 0) {
        number = -number;
        full_number_string = "minus ";
    }
    int thousands = number / 1000;
    int hundreds = number % 1000;
    if (thousands) {
        full_number_string += hundred(thousands) + " ";
        int form_number = form(thousands % 100);
        if (form_number == 1)
            full_number_string = "tysiąc";
        if (form_number == 2)
            full_number_string += "tysiące";
        if (form_number == 3)
            full_number_string += "tysięcy";
        if (hundreds) {
            full_number_string += " ";
        }
    }
    full_number_string += hundred(hundreds);
    return full_number_string;
}

auto test_split() -> void
{
    assert(split(0) == "zero");
    assert(split(1000) == "tysiąc");
    assert(split(13134) == "trzynaście tysięcy sto trzydzieści cztery");
    assert(split(-92) == "minus dziewięćdziesiąt dwa");
    assert(split(-32512) == "minus trzydzieści dwa tysiące pięćset dwanaście");
    return;
}

auto elements(int n) -> std::string
{
    if (n < 0)
        n = -n;
    int form_number = form(n);
    if (form_number == 1)
        return "element";
    if (form_number == 2)
        return "elementy";
    if (form_number == 3)
        return "elementów";
    return std::string();
}

auto test_elements() -> void
{
    return;
}

auto test() -> void
{
    test_form();
    test_one();
    test_ten();
    test_hundred();
    test_split();
    test_elements();
}

int main(void)
{
    test();
    int n{ 0 };
    std::cin >> n;
    std::cout << split(n) + " " + elements(n) << '\n';
    return 0;
}
