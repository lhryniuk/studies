#ifndef SERIA_H_
#define SERIA_H_

#include "pomiar.h"
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

class SeriaPomiarowa
{
    public:
    explicit SeriaPomiarowa(std::vector<Pomiar> pomiary = std::vector<Pomiar>());
    Pomiar operator[](int i) const;
    double Predkosc(int i) const;
    double Przyspieszenie(int i) const;
    long long LiczbaPomiarow() const;

    private:
    std::vector<Pomiar> pomiary_;
    long long liczba_pomiarow_;
};

std::ostream &operator<<(std::ostream &out, const SeriaPomiarowa &seria);
std::istream &operator>>(std::istream &in, SeriaPomiarowa &seria);
bool operator==(const SeriaPomiarowa &a, const SeriaPomiarowa &b);

void test_SeriaPomiarowa(void);

#endif
