#ifndef POMIAR_H_
#define POMIAR_H_

#include <cassert>
#include <iostream>

class Pomiar
{
    public:
    Pomiar(double t = 0.0, double polozenie = 0.0);
    double Y(void) const;
    double T(void) const;

    private:
    double t_;
    double polozenie_;
};

std::istream &operator>>(std::istream &in, Pomiar &p);

#endif
