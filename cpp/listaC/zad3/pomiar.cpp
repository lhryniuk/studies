#include "pomiar.h"

Pomiar::Pomiar(double t, double polozenie) : t_(t), polozenie_(polozenie)
{
}

double Pomiar::Y(void) const
{
    return polozenie_;
}

double Pomiar::T(void) const
{
    return t_;
}

std::istream &operator>>(std::istream &in, Pomiar &p)
{
    bool ws_flag = in.flags() | std::ios::skipws;
    char bs = ' ';
    char be = ' ';
    char sep = ' ';
    double polozenie = 0.0;
    double t = 0.0;
    if (in.eof()) {
        return in;
    }
    in >> bs >> std::skipws >> t >> sep >> polozenie >> be >> std::ws;
    if (in) {
        if (bs == '(' && sep == ';' && be == ')') {
            p = Pomiar(t, polozenie);
        } else if (in && in.eof()) {
            return in;
        } else {
            in.setstate(std::ios_base::failbit);
            return in;
        }
    }
    if (!ws_flag) {
        in >> std::noskipws;
    }
    return in;
}
