#include "seria.h"

SeriaPomiarowa::SeriaPomiarowa(std::vector<Pomiar> pomiary)
: pomiary_(pomiary), liczba_pomiarow_(pomiary.size())
{
}

Pomiar SeriaPomiarowa::operator[](int i) const
{
    return pomiary_[i];
}

double SeriaPomiarowa::Predkosc(int i) const
{
    assert(i > 0 && i < liczba_pomiarow_);
    return ((pomiary_[i + 1].Y() - pomiary_[i - 1].Y()) / (pomiary_[i + 1].T() - pomiary_[i - 1].T()));
}

double SeriaPomiarowa::Przyspieszenie(int i) const
{
    assert(i > 0 && i < liczba_pomiarow_);
    return (2.0 * (((pomiary_[i].T() - pomiary_[i - 1].T()) * (pomiary_[i + 1].Y() - pomiary_[i].Y()) -
                    (pomiary_[i + 1].T() - pomiary_[i].Y()) * (pomiary_[i].Y() - pomiary_[i - 1].Y())) /
                   (pomiary_[i + 1].T() - pomiary_[i - 1].T())));
}

long long SeriaPomiarowa::LiczbaPomiarow() const
{
    return liczba_pomiarow_;
}

std::ostream &operator<<(std::ostream &out, const SeriaPomiarowa &seria)
{
    for (int i = 0; i < seria.LiczbaPomiarow(); ++i) {
        out << seria[i].T() << ';' << seria[i].Y();
        if (0 < i && i < seria.LiczbaPomiarow() - 1) {
            out << ';' << seria.Predkosc(i) << ';' << seria.Przyspieszenie(i);
        }
        out << '\n';
    }
    return out;
}

std::istream &operator>>(std::istream &in, SeriaPomiarowa &seria)
{
    std::vector<Pomiar> pomiary;
    Pomiar tmp;
    while (in >> tmp) {
        std::cerr << "wczytanie\n";
        pomiary.push_back(tmp);
        if (in.eof())
            break;
    }
    if (in) {
        seria = SeriaPomiarowa(pomiary);
    }
    return in;
}

bool eq(double a, double b)
{
    const double eps = 10e-2;
    /* |a - b| <= eps
     * -eps <= a - b <= eps
     *  a - eps <= b && b <= a + eps*/
    if (a - eps <= b && b <= a + eps)
        return true;
    return false;
}

bool operator==(const SeriaPomiarowa &a, const SeriaPomiarowa &b)
{
    if (a.LiczbaPomiarow() != b.LiczbaPomiarow())
        return false;
    for (unsigned i = 0; i < a.LiczbaPomiarow(); ++i) {
        if (!eq(a[i].T(), b[i].T()) || !eq(a[i].Y(), b[i].Y()))
            return false;
    }
    return true;
}

void test_SeriaPomiarowa()
{
    std::istringstream in("  (1.32; 1.44 )    (   2.31; 2.5)\n"
                          "  (3.14   ;  2.72)   (\n4.15; 6.1 )\n\n\n");
    SeriaPomiarowa a;
    in >> a;
    SeriaPomiarowa b{ std::vector<Pomiar>{ { 1.32, 1.44 }, { 2.31, 2.5 }, { 3.14, 2.72 }, { 4.15, 6.1 } } };
    assert(a == b);
    std::ofstream out("dane.txt");
    out << a;
    std::cerr << "test_SeriaPomiarowa ok!\n";
}
