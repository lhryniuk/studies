#include <cassert>
#include <algorithm>
#include <iostream>
#include <vector>

void Szescian(std::vector<int>::iterator poczatek, std::vector<int>::iterator koniec, std::vector<int>::iterator cel)
{
    std::transform(poczatek, koniec, cel, [](auto i){return i * i * i;});
}

void test_Szescian(void);

int main(void)
{
    test_Szescian();
    return 0;
}

void test_Szescian(void)
{
    std::vector<int> d{ 1, 2, 3, 4 };
    std::vector<int> w(5, 17);
    Szescian(std::begin(d), std::end(d), std::begin(w));
    assert((w == std::vector<int>{ 1, 8, 27, 64, 17 }));

    Szescian(std::begin(d) + 2, std::end(d), std::begin(d));
    assert((d == std::vector<int>{ 27, 64, 3, 4 }));

    std::vector<int> y(5, 17);
    Szescian(std::begin(d), std::begin(d), std::begin(y));
    assert((y == std::vector<int>(5, 17)));

    std::cerr << "test_Szescian ok!\n";
}
