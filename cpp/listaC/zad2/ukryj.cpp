#include <cassert>
#include <iostream>
#include <string>

void UkryjZnaki(std::string::iterator poczatek, std::string::iterator koniec)
{
    for (std::string::iterator it = poczatek; it != koniec; ++it) {
        *it = '*';
    }
}

std::string UkryjHasla(std::string napis)
{
    unsigned k = 0;
    unsigned pk = 0;
    std::string::iterator it;
    /* czary-mary */
    while ((it = std::begin(napis) + (((k = napis.find('[', pk)) == std::string::npos) ? napis.size() : k)) !=
           std::end(napis)) {
        for (std::string::iterator is = it + 1; *is != ']'; ++is) {
            *is = '*';
        }
        pk = k + 1;
    }
    return napis;
}

void test_Ukryj(void);

int main(void)
{
    test_Ukryj();
    return 0;
}

void test_Ukryj(void)
{
    std::string a("qwertyuiop");
    UkryjZnaki(std::begin(a) + 3, std::begin(a) + 6);
    assert(a == "qwe***uiop");

    std::string b("uzytkownik test zalogowal sie haslem [glupiehaslo]");
    std::string c = UkryjHasla(b);
    assert(c == "uzytkownik test zalogowal sie haslem [***********]");

    std::string d("tu nie ma hasla");
    assert(UkryjHasla(d) == "tu nie ma hasla");
    UkryjZnaki(std::begin(d), std::begin(d));
    assert(d == "tu nie ma hasla");

    std::cerr << "test_Ukryj ok!\n";
}
