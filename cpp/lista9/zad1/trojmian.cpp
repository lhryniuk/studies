#include "trojmian.h"
#include <cmath>
#include <limits>

Trojmian::Trojmian(double x0, double x1, double x2) : x0_(x0), x1_(x1), x2_(x2)
{
}

double Trojmian::Pierwiastki(double &r1, double &r2)
{
    if (x2_ != 0) {
        double delta = x1_ * x1_ - 4 * x2_ * x0_;
        if (delta < 0) {
            r1 = 0.0 / 0.0;
            r2 = 0.0 / 0.0;
            return 0.0;
        } else if (delta == 0) {
            r1 = r2 = -x1_ / (2 * x0_);
            return 1.0;
        } else {
            if (x1_ > 0) {
                r1 = ((2 * x0_) / (-x1_ - sqrt(delta)));
                r2 = ((-x1_ - sqrt(delta)) / (2 * x2_));
            } else {
                r1 = ((-x1_ + sqrt(delta)) / (2 * x2_));
                r2 = ((2 * x0_) / (-x1_ + sqrt(delta)));
            }
            return 2.0;
        }
    } else if (x1_ != 0) {
        r1 = r2 = -x0_ / x1_;
        return 1.0;
    } else if (x0_ != 0) {
        r1 = r2 = 0.0 / 0.0;
        return 0.0;
    } else {
        r1 = r2 = 0.0 / 0.0;
        return 1.0 / 0.0;
    }
}

double Trojmian::Wspolczynnik(int stopien) const
{
    switch (stopien) {
    case 0:
        return x0_;
    case 1:
        return x1_;
    case 2:
        return x2_;
    default:
        return 0.0;
    }
}

/* static */
bool Trojmian::MniejWiecej(const Trojmian &t1, const Trojmian &t2)
{
    return ((((t1.Wspolczynnik(0) - kEpsilon) <= t2.Wspolczynnik(0)) &&
             (t1.Wspolczynnik(0) + kEpsilon) >= t2.Wspolczynnik(0)) &&
            (((t1.Wspolczynnik(1) - kEpsilon) <= t2.Wspolczynnik(1)) &&
             (t1.Wspolczynnik(1) + kEpsilon) >= t2.Wspolczynnik(1)) &&
            (((t1.Wspolczynnik(2) - kEpsilon) <= t2.Wspolczynnik(2)) &&
             (t1.Wspolczynnik(2) + kEpsilon) >= t2.Wspolczynnik(2)));
}

/* schemat Hornera:
 * x2_ * x^2 + x1_ * x + x0_ = x(x2_* x + x1_) + x0_ */
double Trojmian::operator()(const double x) const
{
    return (x * (x2_ * x + x1_) + x0_);
}

Trojmian &Trojmian::operator+=(Trojmian t)
{
    x0_ += t.Wspolczynnik(0);
    x1_ += t.Wspolczynnik(1);
    x2_ += t.Wspolczynnik(2);
    return *this;
}

Trojmian &Trojmian::operator-=(const Trojmian t)
{
    x0_ -= t.Wspolczynnik(0);
    x1_ -= t.Wspolczynnik(1);
    x2_ -= t.Wspolczynnik(2);
    return *this;
}

Trojmian operator-(Trojmian t)
{
    return Trojmian(-t.Wspolczynnik(0), -t.Wspolczynnik(1), -t.Wspolczynnik(2));
}

Trojmian operator+(Trojmian t1, Trojmian t2)
{
    return t1 += t2;
}

Trojmian operator-(Trojmian t1, Trojmian t2)
{
    return t1 -= t2;
}

std::istream &operator>>(std::istream &we, Trojmian &t)
{
    char p1, p2, c1, c2;
    double x0 = 0.0;
    double x1 = 0.0;
    double x2 = 0.0;
    we >> p1 >> std::noskipws >> x0 >> c1 >> x1 >> c2 >> x2 >> p2 >> std::skipws;
    if (we) {
        if (p1 == '[' && p2 == ']' && c1 == ',' && c2 == ',') {
            t = Trojmian(x0, x1, x2);
        } else {
            we.setstate(std::ios_base::failbit);
        }
    }
    return we;
}
