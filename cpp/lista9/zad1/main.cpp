#include "trojmian.h"
#include <cassert>
#include <cmath>
#include <iostream>
#include <sstream>

void test_Trojmian(void);

int main(void)
{
    test_Trojmian();
    return 0;
}


void test_Trojmian(void)
{
    Trojmian t1(1.0, 2.0, 3.0);
    assert(t1.Wspolczynnik(0) == 1.0);
    assert(t1.Wspolczynnik(1) == 2.0);
    assert(t1.Wspolczynnik(2) == 3.0);
    assert(t1(0) == 1.0);
    assert(t1(1) == 6.0);

    t1 += Trojmian(2.0, 3.0, 4.0);
    assert(t1.Wspolczynnik(0) == 3.0);
    assert(t1.Wspolczynnik(1) == 5.0);
    assert(t1.Wspolczynnik(2) == 7.0);
    assert(t1(0) == 3.0);
    assert(t1(1) == 15.0);
    assert(Trojmian::MniejWiecej(t1, Trojmian(3.0, 5.0, 7.0)));
    assert(!(Trojmian::MniejWiecej(t1, Trojmian(3.5, 5.5, 7.5))));

    Trojmian t2;
    t2 = 5.0;
    assert(t2.Wspolczynnik(0) == 5.0);
    assert(t2.Wspolczynnik(1) == 0.0);
    assert(t2.Wspolczynnik(2) == 0.0);

    std::istringstream we("[1.0,2.0,3.0] [3.5,4.5,-5.5]");
    we >> t1 >> t2;
    assert(we);
    assert(Trojmian::MniejWiecej(t1, Trojmian(1.0, 2.0, 3.0)));
    assert(Trojmian::MniejWiecej(t2, Trojmian(3.5, 4.5, -5.5)));

    Trojmian t3(2.3, 1.3, 6.0);
    assert(Trojmian::MniejWiecej((t1 + t3).Wspolczynnik(2), 9.0));
    assert(Trojmian::MniejWiecej((t1 - t3).Wspolczynnik(1), 0.7));
    assert((-t3).Wspolczynnik(0) == -2.3);
    t3 -= Trojmian(1.0, 3.0, 1.0);
    assert(Trojmian::MniejWiecej(t3.Wspolczynnik(1), -1.7));

    double x1 = 0.0;
    double x2 = 0.0;
    assert(t1.Pierwiastki(x1, x2) == 0);
    assert(std::isnan(x1));
    assert(t2.Pierwiastki(x1, x2) == 2);
    Trojmian t4(0.0, 0.0, 0.0);
    assert(std::isinf(t4.Pierwiastki(x1, x2)));
    assert(std::isnan(x1));
    std::cerr << "test_Trojmian ok\n";
}
