#ifndef WYMIERNA_H
#define WYMIERNA_H

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>

class Wymierna
{
    public:
    Wymierna(long long licznik = 0, long long mianownik = 1);
    long long Licznik(void) const;
    long long Mianownik(void) const;
    Wymierna &operator++();
    Wymierna &operator--();
    Wymierna operator++(int);
    Wymierna operator--(int);
    Wymierna &operator+=(Wymierna a);
    Wymierna &operator-=(Wymierna a);
    Wymierna &operator*=(Wymierna a);
    Wymierna &operator/=(Wymierna a);
    static long long gcd(long long a, long long b); // great common divisor
    static long long lcm(long long a, long long b); // least common multiple*/

    private:
    void Skroc(void);
    long long licznik_;
    long long mianownik_;
};

std::istream &operator>>(std::istream &we, Wymierna &liczba);
Wymierna operator+(Wymierna w);
Wymierna operator-(Wymierna w);
bool operator==(Wymierna w1, Wymierna w2);
bool operator!=(Wymierna w1, Wymierna w2);
bool operator>(Wymierna w1, Wymierna w2);
bool operator<(Wymierna w1, Wymierna w2);
bool operator>=(Wymierna w1, Wymierna w2);
bool operator<=(Wymierna w1, Wymierna w2);
Wymierna operator+(Wymierna w1, Wymierna w2);
Wymierna operator-(Wymierna w1, Wymierna w2);
Wymierna operator*(Wymierna w1, Wymierna w2);
Wymierna operator/(Wymierna w1, Wymierna w2);

#endif
