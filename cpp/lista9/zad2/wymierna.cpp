#include "wymierna.h"

Wymierna::Wymierna(long long licznik, long long mianownik)
{
    assert(mianownik != 0);
    if (mianownik < 0) {
        mianownik *= -1;
        licznik *= -1;
    }
    licznik_ = licznik;
    mianownik_ = mianownik;
    Skroc();
}

long long Wymierna::Licznik(void) const
{
    return licznik_;
}

long long Wymierna::Mianownik(void) const
{
    return mianownik_;
}

long long Wymierna::gcd(long long a, long long b)
{
    a = abs(a); // by uproscic wywolania
    b = abs(b);
    while (b)
        std::swap(a %= b, b);
    return a;
}

long long Wymierna::lcm(long long a, long long b)
{
    return ((a * b) / gcd(a, b));
}

Wymierna &Wymierna::operator++()
{
    licznik_ += mianownik_;
    return *this;
}

Wymierna Wymierna::operator++(int)
{
    Wymierna kopia = (*this);
    licznik_ += mianownik_;
    return kopia;
}

Wymierna &Wymierna::operator--()
{
    licznik_ -= mianownik_;
    return *this;
}

Wymierna Wymierna::operator--(int)
{
    Wymierna kopia = (*this);
    licznik_ -= mianownik_;
    return kopia;
}

Wymierna &Wymierna::operator+=(Wymierna q)
{
    if (mianownik_ != q.Mianownik()) {
        licznik_ = licznik_ * q.Mianownik() + mianownik_ * q.Licznik();
        mianownik_ = mianownik_ * q.Mianownik();
    } else {
        licznik_ += q.Licznik();
    }
    Skroc();
    return *this;
}

Wymierna &Wymierna::operator-=(Wymierna q)
{
    if (mianownik_ != q.Mianownik()) {
        licznik_ = licznik_ * q.Mianownik() - mianownik_ * q.Licznik();
        mianownik_ = mianownik_ * q.Mianownik();
    } else {
        licznik_ -= q.Licznik();
    }
    Skroc();
    return *this;
}

Wymierna &Wymierna::operator*=(Wymierna q)
{
    licznik_ *= q.Licznik();
    mianownik_ *= q.Mianownik();
    Skroc();
    return *this;
}

Wymierna &Wymierna::operator/=(Wymierna q)
{
    assert(q.Licznik() != 0);
    licznik_ *= q.Mianownik();
    mianownik_ *= q.Licznik();
    Skroc();
    return *this;
}

void Wymierna::Skroc(void)
{
    long long gcd_lm = Wymierna::gcd(licznik_, mianownik_);
    licznik_ /= gcd_lm;
    mianownik_ /= gcd_lm;
}

Wymierna operator+(Wymierna w)
{
    return w;
}

Wymierna operator-(Wymierna w)
{
    return Wymierna(-w.Licznik(), w.Mianownik());
}

bool operator==(Wymierna w1, Wymierna w2)
{
    return ((w1.Licznik() == w2.Licznik()) && (w1.Mianownik() == w2.Mianownik()));
}

bool operator!=(Wymierna w1, Wymierna w2)
{
    return !(w1 == w2);
}

bool operator>(Wymierna w1, Wymierna w2)
{
    if (w1.Licznik() >= 0 && w2.Licznik() < 0) {
        return true;
    } else if (w1.Licznik() < 0 && w2.Licznik() >= 0) {
        return false;
    } else {
        if (w1.Licznik() * w2.Mianownik() > w1.Mianownik() * w2.Licznik()) {
            return true;
        }
    }
    return false;
}

bool operator<(Wymierna w1, Wymierna w2)
{
    if (w2.Licznik() >= 0 && w1.Licznik() < 0) {
        return true;
    } else if (w2.Licznik() < 0 && w1.Licznik() >= 0) {
        return false;
    } else {
        if (w2.Licznik() * w1.Mianownik() > w2.Mianownik() * w1.Licznik()) {
            return true;
        }
    }
    return false;
}

bool operator>=(Wymierna w1, Wymierna w2)
{
    return ((w1 > w2) || (w1 == w2));
}

bool operator<=(Wymierna w1, Wymierna w2)
{
    return ((w1 < w2) || (w1 == w2));
}

Wymierna operator+(Wymierna w1, Wymierna w2)
{
    return w1 += w2;
}

Wymierna operator-(Wymierna w1, Wymierna w2)
{
    return w1 -= w2;
}

Wymierna operator*(Wymierna w1, Wymierna w2)
{
    return w1 *= w2;
}

Wymierna operator/(Wymierna w1, Wymierna w2)
{
    return w1 /= w2;
}

std::istream &operator>>(std::istream &we, Wymierna &liczba)
{
    long long licznik = 0;
    long long mianownik = 0;
    we >> licznik;
    if (we && we.peek() == '/') {
        we.get();
        if (we && (we.peek() == '-' || isdigit(we.peek()))) {
            we >> mianownik;
            if (we && mianownik != 0) {
                liczba = Wymierna(licznik, mianownik);
                return we;
            }
        }
    }
    if (we)
        we.setstate(std::ios_base::failbit);
    return we;
}
