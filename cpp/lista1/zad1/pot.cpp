#include <iostream>

constexpr long long power(long long base, long long exponent)
{
    long long w = 1;
    while (exponent != 0) {
        w *= base;
        exponent -= 1;
    }
    return w;
}

int main()
{
    std::cout << power(3, 3) << "\n";
}
