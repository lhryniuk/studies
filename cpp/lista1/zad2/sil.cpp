#include <iostream>

constexpr long long fact(long long n)
{
    long long w = 1;
    while (n > 0) {
        w *= n;
        n -= 2;
    }
    return w;
}

int main()
{
    std::cout << fact(7) << "\n";
}
