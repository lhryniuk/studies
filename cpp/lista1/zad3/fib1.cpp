#include <iostream>

constexpr long long fib(int n)
{
    long long fa = 0;
    long long fb = 1;
    long long fc = 1;
    do {
        fa = fb;
        fb = fc;
        fc = fa + fb;
        n--;
    } while (n >= 0);
    return fa;
}

int main()
{
    std::cout << fib(2) << "\n";
}
