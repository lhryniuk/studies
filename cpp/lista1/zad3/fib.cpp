#include <iostream>

constexpr long long fib0(int n)
{
    long long fa = 0;
    long long fb = 1;
    for (int i = 1; i <= n; ++i) {
        long long fc = fa + fb;
        fa = fb;
        fb = fc;
    }
    return fa;
}

int main(void)
{
    std::cout << fib0(5) << "\n";
}
