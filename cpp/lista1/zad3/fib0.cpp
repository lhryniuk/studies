#include <iostream>

constexpr long long fib0(int n)
{
    long long fa = 0;
    long long fb = 1;
    while (n >= 1) {
        long long fc = fa + fb;
        fa = fb;
        fb = fc;
        n--;
    }
    return fa;
}

int main()
{
    std::cout << fib0(40) << "\n";
}
