#include <cmath>
#include <iostream>

// #define NDEBUG
#include <cassert>

constexpr auto deg2rad(double deg)
{
    return (deg / 180.0) * M_PI;
}

/*
 *   |\
 *   |x\ c
 * a |  \
 *   |z y\
 *   ------
 *     b
 */

auto triangle(double side, double angle)
{
    assert(angle > 0.0 && angle < 90.0);
    assert(side > 0.0);

    std::cout << "\n  |\\\n"
                 "  |x\\ c\n"
                 "  |  \\\n"
                 "a |   \\\n"
                 "  |z  y\\\n"
                 "  ------\n"
                 "     b\n\n";
    std::cout << "boki:\na: " << side << "\nb: " << side * (1.0 / tan(deg2rad(angle)))
              << "\nc: " << side * (1.0 / sin(deg2rad(angle))) << "\n\n";

    std::cout << "katy:\nx: " << 90.0 - angle << "\ny: " << angle << "\nz: " << 90.0 << "\n";
}

int main()
{
    double side{ 0.0 };
    double angle{ 0.0 };
    std::cout << "podaj dlugosc boku: ";
    std::cin >> side;
    while (std::cin.fail()) {
        std::cin.clear();
        std::cin.ignore(1000, '\n');

        std::cout << "podaj dlugosc boku jeszcze raz: ";
        std::cin >> side;
    }
    std::cout << "podaj miare kata: ";
    std::cin >> angle;
    while (std::cin.fail()) {
        std::cin.clear();
        std::cin.ignore(1000, '\n');

        std::cout << "podaj kat jeszcze raz: ";
        std::cin >> angle;
    }
    triangle(side, angle);
    return 0;
}
