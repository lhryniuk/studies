#include <iomanip>
#include <iostream>

auto table() -> void
{
    int ver{ -1 };
    while (++ver <= 10) {
        int hor{ -1 };
        while (++hor <= 10) {
            int num{ ver * hor };
            if (num) {
                std::cout << std::setw(4) << num;
            } else {
                if (hor) {
                    std::cout << std::setw(4) << hor;
                } else if (ver) {
                    std::cout << std::setw(4) << ver;
                } else {
                    std::cout << std::setw(4) << " ";
                }
            }
        }
        std::cout << '\n';
    }
}

int main(void)
{
    table();
    return 0;
}
