#include <cmath>
#include <iostream>

//#define NDEBUG
#include <cassert>

constexpr auto exp_digit(unsigned int number, int position, int base = 10)
{
    int pos = 0;
    while (pos++ < position) {
        number /= base;
    }
    return number % base;
}

/*
 * warning: signed numbers
 */

auto digit(int number) -> void
{
    assert(number > 0);
    // double fnum = number;
    int digit_number = floor(log10(double(number))) + 1;
    while (digit_number-- > 0) {
        std::cout << exp_digit(number, digit_number);
        if (digit_number > 0)
            std::cout << ", ";
    }
}

int main(void)
{
    int number{ 0 };
    int answer{ 0 };
    do {
        std::cout << "podaj liczbe naturalna: ";
        std::cin >> number;

        while (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(1000, '\n');

            std::cout << "podaj liczbe (naturalna) jeszcze raz: ";
            std::cin >> number;
        }

        digit(number);
        std::cout << "\nchcesz wprowadzic jeszcze jedna liczbe (1/0)?: ";
        std::cin >> answer;

        while ((answer != 0 && answer != 1) || std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(1000, '\n');

            std::cout << "wprowadz 0 lub 1: ";
            std::cin >> answer;
        }
        std::cout << '\n';
    } while (answer == 1);
    return 0;
}
