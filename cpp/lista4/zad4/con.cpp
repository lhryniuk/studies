#include <cmath>
#include <iostream>

// #define NDEBUG
#include <cassert>

/*
 * area = pi * r * r + pi * r * l
 */

auto cone_area(double height, double diameter)
{
    assert(height > 0.0 && diameter > 0.0);
    double r = diameter / 2.0;
    return (r * r + r * sqrt(r * r + height * height)) * M_PI;
}

auto test() -> void
{
    std::cout << "h = " << 3.0 << " d = " << 8.0 << " = " << cone_area(3.0, 8.0) << '\n';
    std::cout << "h = " << 1.0 << " d = " << 2.0 << " = " << cone_area(1.0, 2.0) << '\n';
}

int main()
{
    test();
    double d{ 0.0 };
    double h{ 0.0 };
    std::cout << "podaj wysokosc stozka: ";
    std::cin >> h;
    std::cout << "podaj srednice podstawy: ";
    std::cin >> d;
    std::cout << cone_area(h, d) << '\n';
}
