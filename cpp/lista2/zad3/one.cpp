#include <algorithm>
#include <bitset>
#include <iostream>

auto one(unsigned long number)
{
    auto str = std::bitset<sizeof(number)>(number).to_string();
    std::reverse(std::begin(str), std::end(str));
    return str.find_last_of('1') + 1;
}

int main(void)
{
    std::cout << one(100) << std::endl;
}
