#include <iostream>

/*
 * good results for n <= 30 (about)
 * for better - bignum implementation (on strings)
 */

/*
 * for prime factorization
 *
 * 1. prime = check_this (actual number)
 *    k = 0
 * 2. prime++
 *    k = 1 (check all of divisors)
 *    a)
 *      k++
 *    b) if k < prime && prime % k != 0
 *      return to a)
 * 3. if k < prime && prime % k == 0
 *    return to 2.
 * 4. return prime
 */

constexpr auto next_prime(int check_this)
{
    auto prime = check_this;
    auto k = 0;
    do {
        prime++;
        k = 1;
        do {
            k++;
        } while (k <= check_this && prime % k != 0);
    } while (k <= check_this && prime % k == 0);

    return prime;
}

constexpr auto power(int base, int exponent)
{
    unsigned long long result = 1;
    do {
        result *= base;
        exponent--;
    } while (exponent > 0);
    return result;
}

/*
 * Check how many times in (start) * ... * (end) appears divisor
 *
 */

constexpr auto divide(int start, int end, int divisor)
{
    auto exponent = 0;
    do {
        int k = start;
        k *= divisor;
        do {
            k /= divisor;
            exponent++;
        } while (k % divisor == 0);
        exponent--;
        start++;
    } while (start <= end);
    return exponent;
}

/*
 * Catalan number:
 *      (2n)!         (n + 2)(n + 3)...(2n)
 *   -----------   =  --------------------
 *  (n + 1)! * n!               n!
 *
 * Prime factorization
 *
 * */

constexpr auto catalan(int n)
{
    unsigned long long cat_num = 1;
    auto limit = 2 * n;
    auto i = 2;
    do {
        int exponent = divide(n + 2, 2 * n, i) - divide(1, n, i);
        cat_num *= power(i, exponent + 1);
        cat_num /= i;
        i = next_prime(i);
    } while (i <= limit);
    return cat_num;
}

int main()
{
    std::cout << catalan(1) << "\n";
}
