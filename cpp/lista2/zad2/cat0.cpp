#include <array>
#include <iostream>

std::array<int, 200> tab;

void divide(int n, int step)
{
    for (int i = 2; i <= n; ++i) {
        while (n % i == 0) {
            n /= i;
            tab[i] += step;
        }
    }
}

auto catalan(int n)
{
    for (auto i = n + 2; i <= 2 * n; ++i) {
        divide(i, 1);
    }
    for (auto i = 2; i <= n; ++i) {
        divide(i, -1);
    }
    unsigned long long cat_num = 1;
    for (auto i = 2; i < 200; ++i) {
        while (tab[i] > 0) {
            tab[i]--;
            cat_num *= i;
        }
    }
    return cat_num;
}

int main()
{
    std::cout << catalan(30) << "\n";
}
