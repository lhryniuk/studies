#include <iostream>

/*
 * From ISO148842:2011(e) 5.6-4:
 * The binary / operator yields the quotient, and the binary % operator
 * yields the remainder from the division of the first expression
 * by the second. If the second operand of / or % is zero the behavior
 * is undefined. For integral operands the / operator yields
 * the algebraic quotient with any fractional part discarded;
 * if the quotient a/b is representable in the type of the result,
 * (a/b)*b + a%b is equal to a.
 *
 * -3, 2:
 *  (-3/2)*2 + (-3) % 2 == -3
 *  -2 + (-3) % 2 == -3
 *
 * -3, -2:
 *  (-3/-2)*(-2) + (-3)%(-2) == -3
 *  -2 + (-3)%(-2) == -3
 *
 *  3/-2:
 *  (3/-2)*-2 + 3 % -2 == 3
 *  2 + 3 % -2 == 3
 * ###
 * r is negative for negative a
 * and positive for positive a
 * */

int main()
{
    auto n = -7;
    auto d = 2;
    std::cout << "n/d=" << n / d << "n%d" << n % d << "\n";
    n = 7;
    d = -2;
    std::cout << "n/d=" << n / d << "n%d" << n % d << "\n";
    std::cout << -7 / 2 << "\n";
    std::cout << 7 / (-2) << "\n";
    std::cout << 7 / 2 << "\n";
    std::cout << -7 / (-2) << "\n";
}
