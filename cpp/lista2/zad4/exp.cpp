#include <iostream>

constexpr auto exp(unsigned int number, int base, int position)
{
    auto r = 0;

    do {
        r = number % base;
        number /= base;
        position--;
    } while (position >= 0);

    return r;
}

int main(void)
{
    std::cout << exp(254, 16, 4) << "\n";
}
