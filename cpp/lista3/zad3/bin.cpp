#include <iostream>

constexpr auto m(long long n)
{
    long long p = 0;
    long long s = n;
    long long m = (p + s) / 2;
    long long k = 0;
    while (!(((k = m * m) <= n) && ((m + 1) * (m + 1) > n))) {
        if (k > n) {
            s = m - 1;
        } else {
            p = m + 1;
        }
        m = (p + s) / 2;
    }
    return m;
}

int main(void)
{
    std::cout << m(5) << std::endl;
    return 0;
}
