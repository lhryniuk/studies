#include <algorithm>
#include <iostream>
#include <functional>

constexpr auto max_ofour(double a, double b, double c, double d)
{
    return std::max({a,b,c,d}, std::greater<>());
}

int main(void)
{
    std::cout << max_ofour(3.1231e-40, 4.313e20, 3.3231313e35, 10e-20) << "\n";
}
