#include <iostream>

constexpr bool triangle(long long a, long long b, long long c)
{
    return ((a > c - b) && (c > b - a) && (b > a - c));
}

int main()
{
    long long a = 2;
    long long b = 3;
    long long c = 5;
    if (triangle(a, b, c))
        std::cout << "t" << std::endl;
    else
        std::cout << "n" << std::endl;
    return 0;
}
