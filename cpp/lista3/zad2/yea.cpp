#include <iostream>

// since 4 X 1582
// 4 X -> 15 X
// leap year:
//      n % 4 == 0 && n % 100 != 0
//      n % 400 == 0

constexpr bool leap_year(int year)
{
    if (year > 1582) {
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
            return true;
        }
    } else {
        if (year % 4 == 0) {
            return true;
        }
    }
    return false;
}

int main()
{
    auto y = 1700;
    if (leap_year(y)) {
        std::cout << "przestepny" << std::endl;
    } else {
        std::cout << "nieprzestepny" << std::endl;
    }
}
