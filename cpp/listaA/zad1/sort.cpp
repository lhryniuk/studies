#include <cassert>
#include <iostream>
#include <vector>

void merge(std::vector<std::string> &v, int p, int r, int s)
{
    int size = s - p + 1;
    std::vector<std::string> tmpv(size);
    int i = p;
    int j = r + 1;
    int k = 0;
    while (i <= r && j <= s) {
        if (v[i] < v[j]) {
            tmpv[k++] = v[i++];
        } else {
            tmpv[k++] = v[j++];
        }
    }
    while (i <= r) {
        tmpv[k++] = v[i++];
    }
    while (j <= s) {
        tmpv[k++] = v[j++];
    }
    for (int i = 0; i < size; ++i) {
        v[i + p] = tmpv[i];
    }
}
void test_merge();

void merge_sort(std::vector<std::string> &v, int p, int s)
{
    if (p >= s)
        return;
    int r = (p + s) / 2;
    merge_sort(v, p, r);
    merge_sort(v, r + 1, s);
    merge(v, p, r, s);
}

std::vector<std::string> sort(std::vector<std::string> v)
{
    std::vector<std::string> vs = v;
    merge_sort(vs, 0, v.size() - 1);
    return vs;
}
void test_sort();

void test();

int main(void)
{
    test();
    std::cout << "test ok!" << std::endl;
    return 0;
}

void test_merge()
{
    std::vector<std::string> slowa{ "a", "c", "e", "g", "b", "d", "f", "i" };
    merge(slowa, 0, 3, 7);
    assert(slowa[0] == "a");
    assert(slowa[7] == "i");
    assert(slowa[2] == "c");
    assert(slowa[3] == "d");
}

void test_sort()
{
    std::vector<std::string> slowa{ "dda", "ba", "aa", "acc", "ab", "abc", "Ads", "Ha", "EE" };
    std::vector<std::string> slowa_posortowane_a{ "Ads", "EE",  "Ha", "aa", "ab",
                                                  "abc", "acc", "ba", "dda" };
    std::vector<std::string> slowa_posortowane_b = sort(slowa);
    assert(slowa_posortowane_a == slowa_posortowane_b);
}

void test()
{
    test_merge();
    test_sort();
}
