#include <cassert>
#include <iostream>
#include <vector>

std::vector<unsigned long long> sito(unsigned long long N)
{
    std::vector<bool> czy_pierwsza(N + 1, true); // nasze sito
    std::vector<unsigned long long> P; // lista liczb pierwszych
    czy_pierwsza[0] = czy_pierwsza[1] = false;
    /* przechodzimy jedynie do sqrt(N), poniewaz jezeli
     * jakas liczba ma wiekszy dzielnik, to zostala juz
     * skreslona */
    for (unsigned long long i = 2; i /* *i */ <= N; ++i) {
        if (czy_pierwsza[i]) {
            /* wrzucamy liczbe do vectora liczb pierwszych */
            P.push_back(i);
            /*  wykreslamy wielokrotnosci od i * i, bo poprzednie
             *  wielokrotnosci zostaly juz skreslone */
            for (unsigned long long j = i; i * j <= N; ++j) {
                czy_pierwsza[i * j] = false;
            }
        }
    }
    return P;
}

void test_sito();

int main()
{
    test_sito();
    std::cout << "test ok!\n";
}

void test_sito()
{
    std::vector<unsigned long long> P = sito(20000);
    assert(P[0] == 2);
    assert(P[16] == 59);
    assert(P[1991] == 17327);
    assert(P[1695] == 14461);
    assert(P[1081] == 8689);
    assert(P[1381] == 11467);
    std::vector<unsigned long long> P0 = sito(20);
    std::vector<unsigned long long> P1{ 2, 3, 5, 7, 11, 13, 17, 19 };
    assert(P0 == P1);
}
