#include "wymierna.h"
#include <cassert>
#include <iostream>
#include <vector>

template <typename T> std::vector<T> generuj_sup(const std::vector<T> &v0, const std::vector<T> &v1)
{
    int size = std::min(v0.size(), v1.size());
    std::vector<T> v(size);
    for (int i = 0; i < size; ++i) {
        v[i] = std::max(v0[i], v1[i]);
    }
    return v;
}
void test_generuj_sup();

int main(void)
{
    test_generuj_sup();
    std::cout << "test ok!\n";
    return 0;
}

void test_generuj_sup()
{
    std::vector<Wymierna> v0{ Wymierna(4, 5),  Wymierna(13, 11), Wymierna(3, 7),
                              Wymierna(10, 4), Wymierna(9, 13),  Wymierna(15, 4),
                              Wymierna(7, 8),  Wymierna(3, 41),  Wymierna(20, 11) };

    std::vector<Wymierna> v1{ Wymierna(4, 5),  Wymierna(18, 9), Wymierna(16, 18), Wymierna(4, 17),
                              Wymierna(11, 5), Wymierna(4, 6),  Wymierna(7, 19),  Wymierna(15, 2),
                              Wymierna(7, 10), Wymierna(15, 9), Wymierna(8, 13),  Wymierna(15, 28) };
    std::vector<Wymierna> v = generuj_sup(v0, v1);
    assert(v[0].Licznik() == 4 && v[0].Mianownik() == 5);
    assert(v[1].Licznik() == 2 && v[1].Mianownik() == 1);
    assert(v[2].Licznik() == 8 && v[2].Mianownik() == 9);
    assert(v[3].Licznik() == 5 && v[3].Mianownik() == 2);
    assert(v[4].Licznik() == 11 && v[4].Mianownik() == 5);
    assert(v[5].Licznik() == 15 && v[5].Mianownik() == 4);
    assert(v[6].Licznik() == 7 && v[6].Mianownik() == 8);
    assert(v[7].Licznik() == 15 && v[7].Mianownik() == 2);
}
