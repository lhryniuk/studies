import matplotlib.pyplot as plt

class Grapher:
    """Grapher class

    Manages plots of different parameters of given population like:
     - fitness function,
     - best/mean/worst results,
     - best individuals...
    """

    # That's a tricky solution for continuous updating of the plot,
    # without plt.pause, matplotlib just freezes.
    DELAY = 0.0000001

    def __init__(self, plotting_queue):
        """Creates and initializes a plot window"""
        self.plotting_queue = plotting_queue
        self.xs = []
        self.bests = []
        self.means = []
        plt.ion()
        plt.show()

    def start(self):
        while True:
            points = self.plotting_queue.get()
            self.xs.append(points['iteration'])
            self.bests.append(points['best score'])
            self.means.append(points['mean score'])
            plt.plot(self.xs, self.bests, 'ro')
            plt.plot(self.xs, self.means, 'bo')
            plt.show()
            plt.pause(Grapher.DELAY)
            self.plotting_queue.task_done()
