import numpy as np
import time

class Obstacle:
    """Obstacle class

    Represents moving polygon, which shouldn't be touched by robot arm"""

    def __init__(self, pointlist, s_t=None, addition=True):
        assert pointlist is not None and len(pointlist) >= 2
        self.addition = addition
        self.init_pointlist = np.asarray(pointlist)
        self.pointlist = np.asarray(pointlist)
        self.nbr_of_points = len(self.pointlist)
        self.s_t = s_t if s_t is not None else lambda t: np.array([0.0, 0.0])
        self.curr_time = time.time()
        self.all_dt = 0

    @staticmethod
    def transect(a, b, c, d):
        """Check if [a,b] transects [c,d]"""
        v, x, y = np.array([d, a, b]) - c
        u, w, z = np.array([b, c, d]) - a
        return np.sign(np.cross(u, w)) != np.sign(np.cross(u, z)) and\
               np.sign(np.cross(v, x)) != np.sign(np.cross(v, y))

    def collide(self, arm):
        ans = False
        for i in range(self.nbr_of_points):
            a = self.pointlist[i]
            b = self.pointlist[(i+1)%self.nbr_of_points]
            for j in range(len(arm)-1):
                c, d = arm[j:j+2]
                if Obstacle.transect(a, b, c, d):
                    ans = True
        tmp_time = time.time()
        dt = tmp_time - self.curr_time
        self.all_dt += dt
        if self.addition:
            self.pointlist = self.pointlist + self.s_t(dt)
        else:
            self.pointlist = self.init_pointlist + self.s_t(self.all_dt)
        self.curr_time = tmp_time
        return ans
