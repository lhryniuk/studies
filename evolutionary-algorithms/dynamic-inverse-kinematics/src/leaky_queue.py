import queue

class LeakyQueue(queue.Queue):

    def put(self, item):
        if self.full():
            self.get()
        super().put(item)
