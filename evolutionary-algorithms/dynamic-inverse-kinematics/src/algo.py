import numpy as np
import random
import time

from src.obstacle import *


ALGO_INF = 1000


class Algo:
    """Algo class

    Main logic: produces solutions of inverse kinematics problem with
    evolutionary algorithm"""

    NBR_OF_PARENTS = 2
    SIGMA = 0.005

    def __init__(self, population_size, nbr_of_arm_segments, goal,
                 objects_queue, plotting_queue, obstacles=None,
                 arms_sd=1, arms_mean=0):
        self.obstacles = obstacles
        self.objects_queue = objects_queue
        self.plotting_queue = plotting_queue
        self.nbr_of_arm_segments = nbr_of_arm_segments
        self.population_size = population_size
        self.goal = goal
        self.segment_lengths = np.random.randn(self.nbr_of_arm_segments) * arms_sd + arms_mean

    def start(self):
        self.es(self.distance_to_goal, self.nbr_of_arm_segments,
                self.population_size, 2 * self.population_size,
                Algo.NBR_OF_PARENTS, Algo.SIGMA,
                1.0/np.sqrt(4.0), 1.0/np.sqrt(2.0*np.sqrt(4.0)))

    def es(self, objective_function, chromosome_length, population_size,
           number_of_offspring, number_of_parents, sigma, tau, tau_0, log_frequency=1):
        t = 0

        best_objective_value = 0.00
        best_chromosome = np.zeros((1, chromosome_length))

        # generating an initial population
        current_population = np.random.randn(population_size, chromosome_length)
        current_sigmas = sigma * np.ones((population_size, chromosome_length))

        # evaluating the objective function on the current population
        objective_values = objective_function(current_population)

        while True:
            # selecting the parent indices by the roulette wheel method
            fitness_values = (objective_values - objective_values.min()) / (objective_values - objective_values.min()).sum()
            fitness_values_cumulative = fitness_values.cumsum()
            parent_indices = np.zeros((number_of_offspring, number_of_parents))
            for i in range(number_of_offspring):
                for j in range(number_of_parents):
                    roulette = np.random.random(1)
                    parent_indices[i, j] = 0
                    while fitness_values_cumulative[parent_indices[i, j]] < roulette:
                        parent_indices[i, j] = parent_indices[i, j] + 1
            parent_indices = parent_indices.astype(np.int)

            # creating the children population by Global Intermediere Recombination
            children_population = np.zeros((number_of_offspring, chromosome_length))
            children_sigmas = np.zeros((number_of_offspring, chromosome_length))
            for i in range(number_of_offspring):
                children_population[i, :] = current_population[parent_indices[i, :], :].mean(axis=0)
                children_sigmas[i, :] = current_sigmas[parent_indices[i, :], :].mean(axis=0)

            # mutating the children population by adding random gaussian noise
            children_sigmas = children_sigmas * np.exp(tau *
                    np.random.randn(number_of_offspring, chromosome_length) +\
                    tau_0 * np.random.randn(number_of_offspring, 1))
            children_population = children_population +\
                    children_sigmas * np.random.randn(number_of_offspring, chromosome_length)

            # evaluating the objective function on the children population
            children_objective_values = objective_function(children_population)

            # replacing the current population by (Mu + Lambda) Replacement
            objective_values = np.hstack([objective_values, children_objective_values])
            current_population = np.vstack([current_population, children_population])
            current_population = np.array([np.random.randn(chromosome_length)
                    if objective_values[i] == -ALGO_INF else current_population[i]
                    for i in range(len(current_population))
                ])

            objective_values = objective_function(current_population)
            current_sigmas = np.vstack([current_sigmas, children_sigmas])

            I = np.argsort(objective_values)[::-1]
            current_population = current_population[I[:population_size], :]
            current_sigmas = current_sigmas[I[:population_size]]
            objective_values = objective_values[I[:population_size]]

            # recording some statistics
            if best_objective_value < objective_values[0]:
                best_objective_value = objective_values[0]
                best_chromosome = current_population[0, :]

            if self.objects_queue is not None and np.mod(t, log_frequency) == 0:
                self.objects_queue.put({'arms': [self.angles_to_arm(angles) for angles in current_population]})

            if self.plotting_queue is not None and np.mod(t, log_frequency) == 0:
                self.plotting_queue.put({'iteration': t,
                                         'best score': objective_values.max(),
                                         'mean score': objective_values.mean()})
            t += 1

    def distance_of_angles(self, angles):
        arm = self.angles_to_arm(angles)
        for i in range(len(arm)-1):
            for j in range(len(arm)-1):
                if abs(i - j) > 1 and Obstacle.transect(arm[i], arm[i+1], arm[j], arm[j+1]):
                    return -ALGO_INF
        if any([o.collide(arm) for o in self.obstacles]):
            return -ALGO_INF
        return -np.linalg.norm(self.angles_to_arm(angles)[-1] - self.goal)

    def distance_to_goal(self, population):
        return np.array(list(map(self.distance_of_angles, population)))


    def angles_to_arm(self, angles):
        cs_angles = np.cumsum(angles)
        rot_mat = lambda x: np.array([[np.cos(x), -np.sin(x)],
                                      [np.sin(x), np.cos(x)]])
        scl_mat = lambda x: np.array([[x, 0.0], [0.0, x]])

        rot_matrixes = np.array(list(map(rot_mat, cs_angles)))
        scl_matrixes = np.array(list(map(scl_mat, self.segment_lengths)))

        transform_matrixes = np.array([np.matmul(a, b)
                                for a, b in zip(rot_matrixes, scl_matrixes)])

        return np.vstack([np.array([[0, 0]]),
            np.cumsum(np.matmul(transform_matrixes, np.array([1.0, 0.0])), axis=0)
            ])
