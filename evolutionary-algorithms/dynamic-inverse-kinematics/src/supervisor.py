import random
import threading

import numpy as np

import src.algo as algo
import src.painter as painter
import src.grapher as grapher
import src.leaky_queue as leaky_queue


class Supervisor:
    """Supervisor class

    Manages the whole project, spawning threads for plotting, drawing and
    running algorithm.
    """

    def __init__(self, population_size, nbr_of_arm_segments, obstacles,
                 goal=None,
                 create_plot=True, draw_objects=True):
        self.create_plot = create_plot
        self.draw_objects = draw_objects

        self.goal = np.asarray(goal) if goal is not None else\
                    (random.randint(0, painter.Painter.WIDTH) - painter.Painter.WIDTH // 2,
                     random.randint(0, painter.Painter.HEIGHT) - painter.Painter.HEIGHT // 2)
        self.objects_queue = leaky_queue.LeakyQueue(maxsize=1)
        self.plotting_queue = None
        if self.create_plot:
            self.plotting_queue = leaky_queue.LeakyQueue(maxsize=1)
            self.grapher = grapher.Grapher(self.plotting_queue)
        if self.draw_objects:
            self.painter = painter.Painter(self.objects_queue, self.goal, obstacles)
            self.drawing_thread = threading.Thread(None, self.painter.start)
        self.algo = algo.Algo(population_size, nbr_of_arm_segments, self.goal,
                              self.objects_queue, self.plotting_queue,
                              obstacles, 10, 70)
        self.algo_thread = threading.Thread(None, self.algo.start)

    def start(self):
        if self.draw_objects:
            self.drawing_thread.start()
        self.algo_thread.start()
        if self.create_plot:
            self.grapher.start()
