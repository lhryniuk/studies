import sys

import numpy as np
import pygame
import pygame.locals


class Painter:
    """Painter class

    Draws robot arm, obstacles and goal.
    """
    WIDTH = 800
    HEIGHT = 600
    GREEN = (0, 255, 0)
    RED = (255, 0, 0)
    BLUE = (0, 0, 255)
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)
    GOAL_RADIUS = 3

    def __init__(self, objects_queue, goal, obstacles):
        self.goal = goal
        self.objects_queue = objects_queue
        self.obstacles = obstacles
        pygame.init()
        self.coord_begin = np.array([Painter.WIDTH // 2, Painter.HEIGHT // 2])
        self.screen = pygame.display.set_mode((Painter.WIDTH, Painter.HEIGHT))

    def start(self):
        """Start drawing loop"""
        while True:
            for event in pygame.event.get():
                if event.type == pygame.locals.QUIT:
                    pygame.quit()
                    sys.exit()
            self.screen.fill(Painter.WHITE);
            self.draw_obstacles()
            self.draw_goal()
            self.draw_arms(self.objects_queue.get()['arms'])
            pygame.display.update()

    def draw_obstacles(self):
        for obstacle in self.obstacles:
            pygame.draw.polygon(self.screen, Painter.RED, obstacle.pointlist + np.asarray(self.coord_begin))

    def draw_goal(self):
        pygame.draw.circle(self.screen, Painter.GREEN,
                            (self.goal[0] + self.coord_begin[0],
                            self.goal[1] + self.coord_begin[1]),
                            Painter.GOAL_RADIUS)

    def draw_arm(self, arm, best=False):
        col = Painter.BLACK if not best else Painter.BLUE
        start = np.asarray(arm[0])
        for end in arm[1:]:
            pygame.draw.line(self.screen, col,
                self.coord_begin + start,
                self.coord_begin + np.asarray(end))
            start = end

    def draw_arms(self, arms):
        self.draw_arm(arms[0], True)
        for arm in arms[1:]:
            self.draw_arm(arm)
