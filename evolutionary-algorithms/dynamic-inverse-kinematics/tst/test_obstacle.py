import numpy as np

from src.obstacle import Obstacle

def test_obstacle_transect():
    obstacle = Obstacle()
    a = np.array([0, 0])
    b = np.array([10, 10])
    c = np.array([0, 10])
    d = np.array([4, 1])
    e = np.array([3, 5])
    f = np.array([3, 1])
    g = np.array([6, 5])
    h = np.array([1, 3])
    i = np.array([3, 6])
    assert obstacle.transect(a, b, d, e)
    assert obstacle.transect(d, e, f, g)
    assert obstacle.transect(f, h, a, g)
    assert not obstacle.transect(a, b, f, g)
    assert not obstacle.transect(g, b, c, e)


def test_obstace_collide():
    obstacle1 = Obstacle([(1, 1), (10, 10), (0, 10)])
    arm1 = np.array([[0, 0], [10, 0], [10, 5], [4, 5]])
    arm2 = np.array([[0, 0], [10, 0], [10, 5], [8, 5]])
    arm3 = np.array([[0, 0], [0, 10]])
    arm4 = np.array([[0, 0], [0, 9]])
    assert obstacle1.collide(arm1)
    assert obstacle1.collide(arm3)
    assert not obstacle1.collide(arm2)
    assert not obstacle1.collide(arm4)

    obstacle2 = Obstacle([(3, 3), (5, 3), (5, 5), (3, 5)])
    arm5 = np.array([[0, 0], [7, 7]])
    arm6 = np.array([[0, 0], [7, 0], [7, 7]])
    arm7 = np.array([[0, 0], [0, 5], [3, 7], [7, 7]])
    assert obstacle2.collide(arm5)
    assert not obstacle2.collide(arm6)
    assert not obstacle2.collide(arm7)
