# dynamic_inverse_kinematics

Project created during **Evolutionary algorithms** course. It uses [Evolution
strategy](https://en.wikipedia.org/wiki/Evolution_strategy) algorithm to find
the best position of an arm to reach given goal and avoid moving obstacles.

Image below shows an example execution. Red squares are obstacles, green dot is
the goal and these lines are generated arms, with the best one painted blue.

![Image with example](https://github.com/hryniuk/studies/blob/master/dynamic-inverse-kinematics/doc/example.png?raw=true)

## Running

Install dependencies:

```shell
$ pip install -r requirements.txt
```

and run `dik.py`:
```shell
python dik.py
```

## Code

Project consists of three main classes:
 * `Algo` that calculates (with **NumPy**) the best possible solutions
 * `Painter`, which draws (using **PyGame**) current placement of robot arm, goal
   and obstacles
 * `Grapher`, which creates a plot (using **matplotlib**) with computed results
   (best and mean).

Objects of all these three classes are created in the fourth class,
`Supervisor`, which manages them and runs code of each in a separate thread.
`Algo`, `Painter` and `Grapher` communicate (`Painter <- Algo -> Grapher`)
using `LeakyQueue`, which is just a variant of `queue.Queue` dropping
unprocessed data.
