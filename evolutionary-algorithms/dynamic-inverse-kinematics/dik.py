import numpy as np

import src.supervisor
from src.obstacle import Obstacle


def test1():
    obstacles = [Obstacle([[-100, -80], [30, -50], [-75, 50]],
                 lambda t: np.array([1.5 * t, -7.5 * t])),
                 Obstacle([[100, 100], [50, 50], [100, 0]],
                 lambda t: np.array([-1.5 * t, -7.5 * t])),
                 Obstacle([[0, 200], [50, 200], [50, 150], [0, 150]],
                 lambda t: np.array([-6.0 * t, -8.0 * t]))]
    goal = (-100, -100)
    return (obstacles, goal)

def test2():
    obstacles = [
            Obstacle([[50, 200], [100, 200], [100, 150], [50, 150]],
                     lambda t: np.array([0.0, -6.0 * t])),
            Obstacle([[150, 200], [200, 200], [200, 150], [150, 150]],
                     lambda t: np.array([0.0, -16.0 * t])),
            Obstacle([[250, 200], [300, 200], [300, 150], [250, 150]],
                     lambda t: np.array([0.0, -8.0 * t])),
            Obstacle([[350, 200], [400, 200], [400, 150], [350, 150]],
                     lambda t: np.array([0.0, -12.0 * t]))
            ]
    goal = (225, 100)
    return (obstacles, goal)

def test3():
    obstacles = [Obstacle([[50, 50], [100, 50], [100, 0], [50, 0]],
                          lambda t: np.array([0.0, 150 * np.sin(t / 6.0)]),
                          False),
                 Obstacle([[250, 50], [300, 50], [300, 0], [250, 0]],
                          lambda t: np.array([0.0, 150 * np.sin(t / 10.0)]),
                          False),
                 Obstacle([[50, 80], [100, 80], [100, 30], [50, 30]],
                          lambda t: np.array([150 * np.sin(t / 8.0), 0.0]),
                          False)]
    goal = (225, 100)
    return (obstacles, goal)

def test4():
    obstacles = [Obstacle([[-25, 25], [25, 25], [25, -25], [-25, -25]],
                          lambda t: np.array([150 * np.cos(t / 3.0),
                                              150 * np.sin(t / 3.0)]),
                          False),
                 Obstacle([[-25, 25], [25, 25], [25, -25], [-25, -25]],
                          lambda t: np.array([250 * np.cos(t / 5.0),
                                              250 * np.sin(t / 5.0)]),
                          False)]
    goal = (0, 200)
    return (obstacles, goal)


if __name__ == '__main__':
    create_plot = True
    draw_objects = True
    population_size = 20
    nbr_of_arm_segments = 8

    obstacles, goal = test4()
    supervisor = src.supervisor.Supervisor(
            population_size,
            nbr_of_arm_segments,
            obstacles, goal, create_plot, draw_objects)
    supervisor.start()
