from copy import deepcopy
import matplotlib.pyplot as plt
import numpy as np
import sys

D = 40
M = 10000
LOOPS = 10000
K = 0.1
GS = 100
PHI = 0.7
DELAY = 0.001
PLOT = False


class Individual:
    def __init__(self, d, x=None, s=None):
        self.d = d
        self.fitness = -1
        if x is None:
            self.x = np.array([0.0 for _ in range(d)])
        else:
            self.x = np.array(x)
        if s is None:
            self.s = np.array([0.0 for _ in range(d)])
        else:
            self.s = np.array(s)

class Population:
    def __init__(self, n, d):
        self.inds = [Individual(d) for _ in range(n)]

def random_population(n, d):
    return [Individual(d, np.random.randn(d), np.random.randn(d)) for _ in range(n)]

def quad(x):
    return x[0]**2 + 2 * x[0] + 1

def quad1(x):
    return x[0]**2 + 3 * x[0] - 4

def booth(x):
    return (x[0] + 2*x[1] - 7)**2.0  + (2*x[0] + x[1] - 5)**2

def griewank(x):
    iss = np.array(list(range(1, len(x) + 1)))
    return 1 + np.sum((1/40000) * x * x) - np.prod(np.cos(x / iss))

def rastrigin(x):
    n = len(x)
    A = 10
    return A * n + sum(x * x - A * np.cos(2 * np.pi * x))

def zakharov(x):
    iss = np.array(list(range(1, len(x) + 1)))
    s = 0.5 * iss * x
    return sum(x * x + np.power(s, 2.0) + np.power(s, 4.0))

def sum_of_squares(x):
    iss = np.array(list(range(1, len(x) + 1)))
    return sum(iss * x * x)

def sphere(x):
    return sum(x * x)

def population_evaluation(p, f):
    values = np.array([f(ind.x) for ind in p])
    max_v = max(values)
    assert max_v != 0
    scaled_v = max_v - values
    sum_v = sum(scaled_v)
    if sum_v == 0.0:
        sum_v = 1.0
    assert sum_v != 0
    ev = scaled_v / sum_v
    for i in range(len(p)):
        p[i].fitness = ev[i]
    return p

def termination_condition():
    if termination_condition.loops > 0:
        termination_condition.loops -= 1
        return False
    return True
termination_condition.loops = LOOPS


# roulette wheel
def parent_selection(p, l):
    sys.stdout.flush()
    sum_e = 1.0
    new_p = []
    for _ in range(l):
        x = np.random.uniform()
        curr = 0.0
        j = 0
        while curr <= x and j < len(p):
            curr += p[j].fitness
            j += 1
        new_p.append(p[j-1])
    return new_p


def mutation(p, t, t0):
    sys.stdout.flush()
    d = p[0].d
    e0 = np.random.normal(0, t0**2)
    ms = np.array([np.random.normal(0, t**2) + e0 for _ in range(d)])
    exp_ms = np.exp(ms)
    for i, ind in enumerate(p):
        item = p[i]
        item.s *= exp_ms
        ei = [np.random.normal(0, max(0.2, item.s[j])) for j in range(len(item.s))]
        item.x += ei
        p[i] = item
    return p

def get_best_one(p):
    j = 0
    e = p[j].fitness
    for i, ind in enumerate(p):
        if ind.fitness > e:
            e = ind.fitness
            j = i
    return p[j]

def tournament_selection(p, p_c, m, k=GS):
    all_p = list(p)[:]
    all_p.extend(list(p_c))
    all_p = np.array(all_p)
    assert all_p is not None
    new_p = list()
    for i in range(m):
        ins = all_p[np.random.randint(0, len(all_p), k)]
        best = get_best_one(ins)
        new_p.append(deepcopy(get_best_one(ins)))
    return new_p

def get_best(p, p_c, m):
    all_p = list(p)[:]
    all_p.extend(list(p_c))
    assert all_p is not None
    all_p.sort(key = lambda i: i.fitness)
    return all_p[-m:]

def es_mu_plus_lambda(m, l, d, f, tau, tau0):
    p = random_population(m, d)
    p = population_evaluation(deepcopy(p), f)
    xs = []
    ys = []
    while not termination_condition():
        b = get_best_one(p)
        if PLOT:
            xs.append(LOOPS - termination_condition.loops)
            ys.append(f(b.x))
        print("{} {}".format(f(b.x), list(b.x)))
        # print("{}".format(f(b.x)))
        sys.stdout.flush()
        if PLOT:
            plt.plot(xs, ys, 'b:')
            plt.show()
            plt.pause(DELAY)
        p_c = tournament_selection(p, list(), l)
        p_c = mutation(p_c, tau, tau0)
        p_c = population_evaluation(p_c, f)
        p = tournament_selection(p, p_c, m)
        p = population_evaluation(p, f)
    return get_best_one(p)

def es_mu_lambda(m, l, d, f, tau, tau0):
    p = random_population(m, d)
    p = population_evaluation(deepcopy(p), f)
    xs = []
    ys = []
    while not termination_condition():
        b = get_best_one(p)
        if PLOT:
            xs.append(LOOPS - termination_condition.loops)
            ys.append(f(b.x))
        print("{} {}".format(f(b.x), list(b.x)))
        # print("{}".format(f(b.x)))
        if PLOT:
            plt.plot(xs, ys, 'b:')
            plt.show()
            plt.pause(DELAY)
        sys.stdout.flush()
        p_c = tournament_selection(p, list(), l)
        p_c = mutation(p_c, tau, tau0)
        p_c = population_evaluation(p_c, f)
        p = tournament_selection(p_c, list(), m)
        p = population_evaluation(p, f)
    return get_best_one(p)

def run_benchmark(es, name, f, d, m, phi=PHI):
    print("e_mi+lambda")
    print("LOOPS = {}".format(LOOPS))
    print(name)
    tau = K/np.sqrt(2.0 * d)
    tau0 = K/np.sqrt(2.0 * np.sqrt(d))
    l = int(phi * m)
    print("tau = {}\ntau0 = {}\n l = {}".format(tau, tau0, l))
    r = es(m, l, d, f, tau, tau0)
    print(f(r.x))

if __name__ == '__main__':
    if PLOT:
        plt.ion()
    # plt.axis([0, LOOPS, 0, 5.0])
    d = D
    m = M
    run_benchmark(es_mu_plus_lambda, "rastrigin", rastrigin, d, m)
    run_benchmark(es_mu_lambda, "griewank", griewank, d, m)
    run_benchmark(es_mu_plus_lambda, "zakharov", zakharov, d, m)
    run_benchmark(es_mu_lambda, "sum_of_squares", sum_of_squares, d, m)
    run_benchmark(es_mu_lambda, "sphere", sphere, d, m)

