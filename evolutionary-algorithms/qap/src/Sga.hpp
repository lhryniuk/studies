#ifndef SGA_HPP
#define SGA_HPP

#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>
#include <future>
#include <limits>
#include <map>
#include <random>
#include <thread>
#include <vector>

#include "Individual.hpp"
#include "TspGraph.hpp"


template <
    typename TerminationT,
    typename ParentSelectionT,
    typename CrossoverT,
    typename MutationT,
    typename GraphT,
    typename ProcessResultsT
    >
class Sga
{
public:
    Sga(TerminationT t, GraphT graph, unsigned n,
        unsigned m, double phi_c, double phi_m, ProcessResultsT process_results) :
        termination_condition_(t), parent_selection_(n), crossover_(graph.V(), phi_c),
        mutation_(graph.V(), phi_m, false),
        graph_(graph),
        process_results_(process_results),
        n_(n), d_(graph.V()),
        m_(m), phi_c_(phi_c), phi_m_(phi_m),
        rd_{}, g_(rd_()), dist_(0, n),
        current_best_{std::numeric_limits<int>::max()}
    {
        assert(graph.V() > 0);
        assert(n_ > 0);
        assert(m_ > 0);
        assert(m_ % 2 == 0);
    }
    void operator()()
    {
        population_ = RandomPopulation(n_);
        PopulationEvaluation();
        std::uniform_real_distribution<> real_dist(0, 1.0);
        auto to_kill = n_ / 5;
        using std::swap;
        while (!termination_condition_()) {
            if (real_dist(g_) < 0.005) {
                auto rand_pop = RandomPopulation(to_kill);
                auto np = parent_selection_(population_, n_ - to_kill);
                for (auto&& k : rand_pop) {
                    np.push_back(std::move(k));
                }
                population_ = std::move(np);
            }
            auto p_p = parent_selection_(population_, m_);
            auto p_c = crossover_(std::move(p_p));
            p_c = mutation_(std::move(p_c));

            TPopulation new_population;
            for (int i = 0; i < n_ - m_; ++i) {
                new_population.push_back(population_[i]);
            }
            for (auto i = 0u; i < m_; ++i) {
                auto best = p_c[i];
                auto best_cost = graph_.Cost(best.permutation_);
                auto i1 = p_c[i];
                for (auto j = 1u; j < d_; ++j) {
                    auto i2 = p_c[i];
                    swap(i2.permutation_[j-1], i2.permutation_[j]);
                    auto new_cost = graph_.Cost(i2.permutation_);
                    if (new_cost < best_cost) {
                        best = i2;
                        best_cost = new_cost;
                    }
                }
                new_population.push_back(std::move(best));
            }
            std::shuffle(std::begin(new_population), std::end(new_population), g_);
            population_ = std::move(new_population);
            assert(population_.size() == n_);
            PopulationEvaluation();
        }
    }

private:
    TPopulation RandomPopulation(unsigned n)
    {
        TPermutation basic_perm(d_);
        std::iota(std::begin(basic_perm), std::end(basic_perm), 1);
        TPopulation population;
        for (auto i = 0u; i < n; ++i) {
            std::shuffle(std::begin(basic_perm), std::end(basic_perm), g_);
            // print_permutation(basic_perm);
            // std::cout << "\n";
            population.emplace_back(basic_perm);
        }
        return population;
    }
    void PopulationEvaluation()
    {
        auto best = std::numeric_limits<long long>::max();
        auto worst = std::numeric_limits<long long>::min();
        double sum = 0.0;
        assert(population_.size() == n_);
        for (auto&& ind : population_) {
            ind.cost_ =
                graph_.Cost(ind.permutation_);
            best = std::min(best, ind.cost_);
            worst = std::max(worst, ind.cost_);
            sum += ind.cost_;
        }
        double max_cost = worst;
        double f_sum = 0.0;
        for (auto&& ind : population_) {
            f_sum += ind.cost_ - max_cost;
        }
        for (auto&& ind : population_) {
            ind.fitness_ = (ind.cost_ - max_cost) / f_sum;
            // std::cout << "cost = " << ind.cost_ << " fitness = " << ind.fitness_ << "\n";
        }

        double mean = sum / static_cast<double>(n_);
        std::async(std::launch::async, process_results_, best, mean, worst);
    }

    const int K = 5;
    TerminationT termination_condition_;
    ParentSelectionT parent_selection_;
    CrossoverT crossover_;
    MutationT mutation_;
    GraphT graph_;
    TPopulation population_;
    ProcessResultsT process_results_;
    unsigned n_;
    unsigned d_;
    unsigned m_;
    double phi_c_;
    double phi_m_;
    std::random_device rd_;
    std::mt19937 g_;
    std::uniform_int_distribution<> dist_;
    int current_best_;
};

#endif /* SGA_HPP */
