#ifndef TSPLIB_IMPORTER_HPP
#define TSPLIB_IMPORTER_HPP

#include <memory>
#include <vector>
#include "TsplibData.hpp"

class TsplibImporter
{
public:
    TsplibImporter(const std::string& filepath);
    TspGraph GetGraph() const;
    void ShowGraph() const;

private:
    std::vector<std::string> ReadDataFromFile(std::string filepath=std::string());
    std::string filepath_;
    std::unique_ptr<TsplibData> data_;
};


#endif // TSPLIB_IMPORTER_HPP
