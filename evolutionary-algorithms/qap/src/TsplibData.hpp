#ifndef TSPLIB_DATA_HPP
#define TSPLIB_DATA_HPP

#include <map>
#include <string>
#include <vector>

#include "TspGraph.hpp"

using KeyValueT = std::map<std::string, std::string>;

class TsplibData
{
public:
    TsplibData(std::vector<std::string> data_lines);
    void ShowGraph() const;
    TspGraph GetGraph() const;

private:
    void SaveSpecEntryFromLine(std::string line);
    std::vector<double> ReadValuesFromLine(const std::string& line) const;
    void ProcessSection(const std::string& section_name, std::vector<std::vector<double>> v);
    KeyValueT specification_;
    TspGraph graph_;
};

#endif /* TSPLIB_DATA_HPP */
