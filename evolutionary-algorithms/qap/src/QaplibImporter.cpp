#include <fstream>
#include <iostream>
#include <string>
#include <streambuf>

#include "QaplibImporter.hpp"

QaplibImporter::QaplibImporter(std::string filepath) :
    filepath_{filepath}
{ }

QapGraph QaplibImporter::GetGraph() const
{
    return graph_;
}

void QaplibImporter::ReadDataFromFile(std::string filepath)
{
    if (filepath.empty()) {
        filepath = filepath_;
    } else {
        filepath_ = filepath;
    }

    auto v = -1;
    std::ifstream input(filepath_);
    std::cerr << "Reading number of vertices:";
    input >> v;
    std::cerr << v << "\n";
    graph_ = QapGraph(v);

    std::cerr << "Reading distances...";
    for (auto i = 0; i < v; ++i) {
        for (auto j = 0; j < v; ++j) {
            int dist{};
            input >> dist;
            graph_.AddWeightFromAToB(i+1, j+1, dist);
        } 
    }
    std::cerr << "done!\n";

    std::cerr << "Reading flows... ";
    for (auto i = 0; i < v; ++i) {
        for (auto j = 0; j < v; ++j) {
            int flow{};
            input >> flow;
            graph_.AddFlowFromAToB(i+1, j+1, flow);
        } 
    }
    std::cerr << "done!\n";
}

