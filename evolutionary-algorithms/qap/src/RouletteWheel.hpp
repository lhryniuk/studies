#ifndef ROULETTE_WHEEL_HPP
#define ROULETTE_WHEEL_HPP

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <random>

#include "Types.hpp"

class RouletteWheel
{
public:
    explicit RouletteWheel(unsigned n);
    TPopulation operator()(const TPopulation& population, unsigned m);
private:
    std::random_device rd_;
    std::mt19937 g_;
};

#endif /* ROULETTE_WHEEL_HPP */
