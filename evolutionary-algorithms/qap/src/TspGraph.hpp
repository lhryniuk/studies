#ifndef TSP_GRAPH_HPP
#define TSP_GRAPH_HPP

#include <string>
#include <vector>

#include "Types.hpp"
#include "WeightedGraph.hpp"

/* TspGraph represents Complted Weighted Graph with N vertices
 * for Travelling Salesman Problem
 * */
class TspGraph : public WeightedGraph
{
public:
    explicit TspGraph(unsigned v=0);
    virtual ~TspGraph() = default;
};

#endif // TSP_GRAPH_HPP
