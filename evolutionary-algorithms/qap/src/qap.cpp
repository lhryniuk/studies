#include <cassert>
#include <fstream>
#include <functional>
#include <future>
#include <iostream>
#include <sstream>
#include <thread>

#include "TsplibImporter.hpp"
#include "QaplibImporter.hpp"
#include "TournamentSelection.hpp"
#include "RouletteWheel.hpp"
#include "Pmx.hpp"
#include "Sga.hpp"
#include "Transposition.hpp"
#include "Utils.hpp"


// constexpr unsigned n_ = 1000;
// constexpr unsigned m_ =  600;
// constexpr double theta_c_ = 0.85;
// constexpr double theta_m_ = 0.02;
// constexpr unsigned kgroup_size_ = 10;


std::string output_filename(std::string filepath, unsigned n, unsigned m,
        double theta_c, double theta_m, unsigned k, int loops)
{
    std::stringstream ss;
    ss << "n" << n << "m" << m << "tc" << theta_c << "tm" << theta_m << "k" << k << "l" << loops << filepath;
    std::string filename = ss.str();
    for (auto&& c : filename) {
        if (c == '.') {
            c = '_';
        }
    }
    std::cout << filename << "\n";
    return filename;
}

TaskT setup_qap(
        std::string main_dir, std::string filename, unsigned n, unsigned m,
        double theta_c, double theta_m, unsigned k, int loops)
{
    std::cerr << "STARTING QAP TASK\n";
    std::cerr << " *** " << filename << " ***\n";
    auto filepath = main_dir + filename;
    std::string output_file = output_filename(filename, n, m, theta_c, theta_m, k, loops);
    return [filepath, n, m, theta_c, theta_m, k, output_file, loops]()
    {
        QaplibImporter imp(filepath);
        imp.ReadDataFromFile();
        auto graph = imp.GetGraph();
        assert(graph.V() > 0);
        int l = loops;
        auto t = [&l]()->bool{return !(l--);};
        std::ofstream output("./" + output_file);
        auto process_results = [&output](long long best, double mean, long long worst)
            { output << best << " " << std::llround(mean) << " " << worst << "\n"; };
        Sga<decltype(t), RouletteWheel, Pmx, Transposition, QapGraph, decltype(process_results)>
            sga(t, graph, n, m, theta_c, theta_m, process_results);
        sga();
    };
}

TaskT setup_tsp(
        std::string main_dir, std::string filename, unsigned n, unsigned m,
        double theta_c, double theta_m, unsigned k, int loops)
{
    std::cerr << "STARTING TSP TASK\n";
    std::cerr << " *** " << filename << " ***\n";
    auto filepath = main_dir + filename;
    std::string output_file = output_filename(filename, n, m, theta_c, theta_m, k, loops);
    return [filepath, n, m, theta_c, theta_m, k, output_file, loops]()
    {
        TsplibImporter imp(filepath);
        auto graph = imp.GetGraph();
        int l = loops;
        auto t = [&l]()->bool{return (l--) < 0;};
        std::ofstream output("./" + output_file);
        auto process_results = [&output](long long best, double mean, long long worst)
            { output << best << " " << std::llround(mean) << " " << worst << "\n"; };
        Sga<decltype(t), RouletteWheel, Pmx, Transposition, TspGraph, decltype(process_results)>
            sga(t, graph, n, m, theta_c, theta_m, process_results);
        sga();
    };
}

int main(int argc, char** argv)
{
    assert(argc > 1);
    std::string main_dir = argv[1];
    ThreadPool tp(4);
    // TSP
    // tp.submit(setup_tsp(main_dir, "bayg29.tsp", 5000, 1000,
    //                     0.90, 0.005, 5, 100000));
    // tp.submit(setup_tsp(main_dir, "bays29.tsp", 1000, 650,
    //                     0.7, 0.01, 10, 10000));
    // tp.submit(setup_tsp(main_dir, "berlin52.tsp", 5000, 4000,
    //                     0.9, 0.02, 15,  50000));
    // tp.submit(setup_tsp(main_dir, "kroA100.tsp", 1000, 400,
    //                     0.98, 0.003, 20, 100000));
    // tp.submit(setup_tsp(main_dir, "kroA150.tsp", 30000, 7000,
    //                     0.9, 0.01, 25, 100000));
    // tp.submit(setup_tsp(main_dir, "kroA200.tsp", 10000, 7000,
    //                     0.9, 0.01, 30, 100000));
    // QAP
    // tp.submit(setup_qap(main_dir, "nug12.dat", 200, 180,
    //                     0.9, 0.25, 5, 10000));
    // tp.submit(setup_qap(main_dir, "nug14.dat", 300, 180,
    //                     0.7, 0.02, 6, 10000));
    // tp.submit(setup_qap(main_dir, "nug15.dat", 400, 240,
    //                     0.8, 0.01, 7, 10000));
    // tp.submit(setup_qap(main_dir, "nug16a.dat", 500, 300,
    //                     0.9, 0.2, 8, 10000));
    // tp.submit(setup_qap(main_dir, "nug17.dat", 600, 400,
    //                     0.9, 0.01, 8, 50000));
    // tp.submit(setup_qap(main_dir, "nug18.dat", 700, 400,
    //                     0.9, 0.02, 4, 50000));
    // tp.submit(setup_qap(main_dir, "nug20.dat", 800, 550,
    //                     0.5, 0.1, 7,  50000));
    // tp.submit(setup_qap(main_dir, "nug21.dat", 900, 700,
    //                     0.9, 0.2, 6, 50000));
    // tp.submit(setup_qap(main_dir, "nug22.dat", 1000, 700,
    //                     0.9, 0.02, 9, 50000));
    // tp.submit(setup_qap(main_dir, "nug24.dat", 1000, 600,
    //                     0.85, 0.03, 4, 50000));
    // tp.submit(setup_qap(main_dir, "nug25.dat", 1200, 900,
    //                     0.9, 0.02, 25, 80000));
    // tp.submit(setup_qap(main_dir, "nug27.dat", 1500, 1000,
    //                     0.9, 0.02, 10, 100000));
    // tp.submit(setup_qap(main_dir, "nug28.dat", 1700, 1500,
    //                     0.9, 0.2, 15, 100000));
    // tp.submit(setup_qap(main_dir, "nug30.dat", 5000, 4000,
    //                     0.98, 0.001, 10, 100000));
    tp.submit(setup_qap(main_dir, "tai50a.dat", 1000, 100,
                        0.99, 0.002, 10, 300000));
    // tp.submit(setup_qap(main_dir, "tai60a.dat", 4000, 2700,
    //                     0.8, 0.05, 25, 400000));
    // tp.submit(setup_qap(main_dir, "tai80a.dat", 5000, 3000,
    //                     0.9, 0.02, 30, 500000));
    return 0;
}
