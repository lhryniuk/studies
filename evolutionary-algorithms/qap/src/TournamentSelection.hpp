#ifndef TOURNAMENT_SELECTION_HPP
#define TOURNAMENT_SELECTION_HPP

#include <cassert>
#include <iostream>
#include <iterator>
#include <random>

#include "Types.hpp"

class TournamentSelection
{
public:
    TournamentSelection(unsigned k, unsigned n) :
        k_{k}, rd_{}, g_(rd_()), dist_(0, n-1)
    {}
    TPopulation operator()(const TPopulation& population, unsigned m)
    {
        TPopulation new_population;
        while (new_population.size() < m) {
            new_population.push_back(SelectBest(TournamentGroup(population)));
        }
        return new_population;
    }
private:
    TPopulation TournamentGroup(const TPopulation& population)
    {
        TPopulation tournament_group;
        while (tournament_group.size() < k_) {
            // FIXME: this distribution is not uniform for n != km
            // due to modulo operation
            tournament_group.push_back(population[dist_(g_) % population.size()]);
        }
        return tournament_group;
    }
    Individual SelectBest(TPopulation&& population)
    {
        assert(population.size() == k_);
        Individual best = population.front();
        for (const auto& ind : population) {
            if (ind.fitness_ < best.fitness_) {
                best = ind;
            }
        }
        return best;
    }
    unsigned k_;
    std::random_device rd_;
    std::mt19937 g_;
    std::uniform_int_distribution<> dist_;
};

#endif /* TOURNAMENT_SELECTION_HPP */
