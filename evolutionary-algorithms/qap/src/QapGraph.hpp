#ifndef QAP_GRAPH_HPP
#define QAP_GRAPH_HPP

#include <string>
#include <vector>

#include "Types.hpp"
#include "WeightedGraph.hpp"

/* QapGraph represents Complete Weighted Graph with N vertices
 * for Quadratic Assignment Problem
 * */
class QapGraph : public WeightedGraph
{
public:
    explicit QapGraph(unsigned v=0);
    virtual void AddFlowFromAToB(unsigned a, unsigned b, double flow);
    virtual int FlowFromAToB(unsigned a, unsigned b) const;
    virtual int Cost(const TPermutation& perm) const;
    virtual ~QapGraph() = default;

private:
    std::vector<std::vector<int>> flows_;
};

#endif // QAP_GRAPH_HPP
