#include "RouletteWheel.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <random>

#include "Types.hpp"

RouletteWheel::RouletteWheel(unsigned n) :
    rd_{}, g_(rd_())
{}

TPopulation RouletteWheel::operator()(const TPopulation& population, unsigned m)
{
    auto max_f = std::accumulate(std::begin(population), std::end(population), 0.0,
                    [](double f, const auto& ind)
                    {
                        return f + ind.fitness_; 
                    });
    auto urd = std::uniform_real_distribution<>(0, max_f);
    TPopulation new_population;
    for (auto i = 0u; i < m; ++i) {
        auto x = urd(g_); 
        double curr = 0.0;
        unsigned j = 0u;
        for (;curr <= x && j < population.size(); ++j) {
            curr += population[j].fitness_; 
        }
        // std::cout << "j = " << j << "\n";
        new_population.push_back(population[j-1]);
    }
    return new_population;
}
