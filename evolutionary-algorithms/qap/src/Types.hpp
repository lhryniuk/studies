#ifndef TYPES_HPP
#define TYPES_HPP

#include <map>
#include <vector>

#include "Individual.hpp"

using TPopulation = std::vector<Individual>;
using TEvaluation = std::map<int, int>;

#endif /* TYPES_HPP */
