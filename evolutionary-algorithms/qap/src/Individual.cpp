#include <iostream>
#include <iterator>

#include "Individual.hpp"

Individual::Individual(TPermutation v) :
    permutation_{v}, fitness_{}
{}

Individual::Individual(std::initializer_list<int> l) :
    permutation_{l}
{}

void print_permutation(const TPermutation& perm)
{
    std::copy(std::begin(perm), std::end(perm), std::ostream_iterator<int>(std::cout, " "));
}

bool cmp_perm(const Individual& a, const Individual& b)
{
    return a.fitness_ >= b.fitness_;
}
