#include "TsplibData.hpp"

#include <cassert>
#include <cctype>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <regex>


TsplibData::TsplibData(std::vector<std::string> data_lines)
{
    bool specification_part = true;
    std::string current_section;
    std::vector<std::vector<double>> current_section_values;

    for (auto&& line : data_lines) {
        if (specification_part && (line.find(':') != std::string::npos)) {
            SaveSpecEntryFromLine(line);
        } else {
            if (specification_part) {
                unsigned v = std::atoi(specification_["DIMENSION"].c_str());
                graph_ = TspGraph(v);
            }
            specification_part = false;

            if (std::isalpha(line.front())){
                if (!current_section.empty()) {
                    ProcessSection(current_section, std::move(current_section_values));
                    current_section_values = decltype(current_section_values){};
                }
                current_section = line;
                std::cerr << "Reading section " << current_section << "...\n";
            } else {
                current_section_values.push_back(ReadValuesFromLine(line));
            }
        }
    }
}

void TsplibData::SaveSpecEntryFromLine(std::string line)
{
    std::regex r("((\\S+)\\s*:\\s*(\\S+))");
    std::smatch m;

    std::regex_search(line, m, r);
    assert(m.size() >= 4);
    std::cerr << "Saving " << m[2] << " = " << m[3] << "\n";
    specification_[m[2]] = m[3];
}

std::vector<double> TsplibData::ReadValuesFromLine(const std::string& line) const
{
    std::istringstream input{line};
    std::vector<double> v;
    double a;
    while (input >> a) {
        v.push_back(a);
    }
    return v;
}

void TsplibData::ProcessSection(const std::string& section_name, std::vector<std::vector<double>> section_data)
{
    auto v = graph_.V();
    if (section_name == "EDGE_WEIGHT_SECTION") {
        int row_shift{}; // shift for upper triangular matrix
        int diagonal_shift{}; // shift for data with diagonal data

        if (specification_["EDGE_WEIGHT_FORMAT"] == "LOWER_ROW"
         || specification_["EDGE_WEIGHT_FORMAT"] == "FULL_MATRIX") {
            row_shift = 0;
            diagonal_shift = 0;
        } else if (specification_["EDGE_WEIGHT_FORMAT"] == "UPPER_ROW") {
            row_shift = 1;
        } else {
            std::cerr << "Edge weight format " << specification_["EDGE_WEIGHT_FORMAT"]
                      << " not implemented!\n";
            std::exit(-1);
        }
        for (auto i = 0u; i < section_data.size(); ++i) {
            for (auto j = 0u; j < section_data[i].size(); ++j) {
                int x = i+1;
                int y = j+1 + (i+1) * row_shift;
                graph_.AddWeightFromAToB(x, y, section_data[i][j]);
                graph_.AddWeightFromAToB(y, x, section_data[i][j]);
            }
        }
    } else if (section_name == "NODE_COORD_SECTION") {
        if (specification_["EDGE_WEIGHT_TYPE"] == "EUC_2D") {
            std::vector<std::pair<double, double>> coordinates(v + 1);
            for (auto i = 0u; i < section_data.size(); ++i) {
                auto index = static_cast<int>(section_data[i][0]);
                coordinates[index].first = section_data[i][1];
                coordinates[index].second = section_data[i][2];
            }
            // calculating distance
            auto euc2d_dist = [](std::pair<double, double> a, std::pair<double, double> b)
            {
                double xd = std::fabs(a.first - b.first);
                double yd = std::fabs(a.second - b.second);
                return std::lround(std::sqrt(xd * xd + yd * yd));
            };
            for (auto i = 1u; i <= v; ++i) {
                for (auto j = 1u; j <= v; ++j) {
                    auto dist = euc2d_dist(coordinates[i], coordinates[j]);
                    graph_.AddWeightFromAToB(i, j, dist);
                    graph_.AddWeightFromAToB(j, i, dist);
                }
            }
        }
    } else {
        std::cerr << "Section " << section_name << " parsing not implemented\n";
        // std::exit(-1);
    }
}

void TsplibData::ShowGraph() const
{
    auto v = graph_.V();
    for (auto i = 1u; i <= v; ++i) {
        for (auto j = 1u; j <= v; ++j) {
            std::cout << std::setw(3) << graph_.WeightFromAToB(i, j) << ' ';
        }
        std::cout << "\n";
    }
}

TspGraph TsplibData::GetGraph() const
{
    return graph_;
}
