#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <set>

#include "Pmx.hpp"

Pmx::Pmx(unsigned d, double phi_c) :
    d_{d}, phi_c_{phi_c}, rd_{}, g_(rd_()), dist_int_(0, d-1), dist_real_(0, 1.0)
{ }

TPopulation Pmx::operator()(TPopulation population)
{
    TPopulation offsprings;
    std::shuffle(std::begin(population), std::end(population), g_);
    for (auto i = 0u; i < population.size(); i += 2) {
        if (dist_real_(g_) < phi_c_) {
            offsprings.push_back(GetOffspring(population[i], population[i+1]));
        } else {
            offsprings.push_back(population[i]);
        }
        if (dist_real_(g_) < phi_c_) {
            offsprings.push_back(GetOffspring(population[i+1], population[i]));
        } else {
            offsprings.push_back(population[i+1]);
        }
    }
    return offsprings;
}

Individual Pmx::GetOffspring(const Individual& a, const Individual& b)
{
    assert(a.permutation_.size() > 0);
    assert(b.permutation_.size() > 0);
    std::vector<bool> is_copied(a.permutation_.size(), false);
    Individual offspring(TPermutation(a.permutation_.size(), -1));
    int i = dist_int_(g_) % (d_ >> 1);
    int j = std::max(1u, (dist_int_(g_) % (d_ - i)));
    int end = i + j;
    for (auto k = i; k < end; ++k) {
        // std::cout << offspring.permutation_.size() << "\n";
        assert(offspring.permutation_.size() >= k);
        assert(a.permutation_.size() >= k);
        offspring.permutation_[k] = a.permutation_[k];
        is_copied[a.permutation_[k]] = true;
    }
    auto v_index = [](const Individual& t, int value)
    {
        return find(std::begin(t.permutation_),
                    std::end(t.permutation_), value) - std::begin(t.permutation_);
    };
    auto in_slice = [i, end](int index){return (i <= index) && (index < end);};
    int vi = i;
    while (vi < end) {
        int va = b.permutation_[vi];
        int vb = va;
        if (!is_copied[vb]) {
            int vj = v_index(b, a.permutation_[vi]);
            while (in_slice(vj)) {
                vb = b.permutation_[vj];
                vj = v_index(b, a.permutation_[vj]);
            }
            vb = b.permutation_[vj];
            is_copied[vb] = true;
            offspring.permutation_[vj] = va;
        }
        ++vi;
    }
    for (auto i = 0u; i < b.permutation_.size(); ++i) {
        if (offspring.permutation_[i] == -1) {
            offspring.permutation_[i] = b.permutation_[i];
        }
    }
    // std::cout << "o = ";
    // print_permutation(offspring.permutation_);
    // std::cout << "\n";
    std::set<int> s;
    for (auto i = 0u; i < offspring.permutation_.size(); ++i) {
        assert(s.find(offspring.permutation_[i]) == s.end());
        s.insert(offspring.permutation_[i]);
    }
    // std::cout << "#### PMX END ####\n";
    return offspring;
}
