#ifndef PMX_HPP
#define PMX_HPP

#include <random>

#include "Types.hpp"

class Pmx
{
public:
    Pmx(unsigned d, double phi_c);
    TPopulation operator()(TPopulation population);

private:
    Individual GetOffspring(const Individual& a, const Individual& b);
    unsigned d_;
    double phi_c_;
    std::random_device rd_;
    std::mt19937 g_;
    std::uniform_int_distribution<> dist_int_;
    std::uniform_real_distribution<> dist_real_;
};

#endif /* PMX_HPP */
