#ifndef INDIVIDUAL_HPP
#define INDIVIDUAL_HPP

#include <initializer_list>
#include <vector>

using TPermutation = std::vector<int>;

struct Individual
{
    Individual(TPermutation v);
    Individual(std::initializer_list<int> l);
    TPermutation permutation_;
    long long cost_;
    double fitness_;
};

void print_permutation(const TPermutation& perm);

bool cmp_perm(const Individual& a, const Individual& b);

#endif /* INDIVIDUAL_HPP */
