#ifndef TRANSPOSITION_HPP
#define TRANSPOSITION_HPP

#include <initializer_list>
#include <random>
#include <vector>

#include "Types.hpp"


class Transposition
{
public:
    Transposition(unsigned d, double phi_m, bool each=true);
    TPopulation operator()(TPopulation population);

private:
    unsigned d_;
    bool each_;
    double phi_m_;
    std::random_device rd_;
    std::mt19937 g_;
    std::uniform_int_distribution<> dist_int_;
    std::uniform_real_distribution<> dist_real_;
};

#endif /* TRANSPOSITION_HPP */
