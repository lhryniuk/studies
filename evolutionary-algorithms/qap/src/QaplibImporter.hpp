#ifndef QAPLIB_IMPORTER_HPP
#define QAPLIB_IMPORTER_HPP

#include <memory>
#include <vector>

#include "QapGraph.hpp"

class QaplibImporter
{
public:
    QaplibImporter(std::string filepath);
    QapGraph GetGraph() const;
    void ShowGraph() const;
    void ReadDataFromFile(std::string filepath=std::string());

private:
    std::string filepath_;
    QapGraph graph_;
};


#endif // QAPLIB_IMPORTER_HPP
