#ifndef UTILS_HPP
#define UTILS_HPP

#include <mutex>
#include <future>
#include <queue>
#include <thread>

using TaskT = std::function<void()>;

class ScopedThread
{
    std::thread thd;
public:
    template<typename... Args>
        ScopedThread(Args&&... args) : thd(std::forward<Args>(args)...)
    { }
    ScopedThread(const ScopedThread&) = delete;
    ScopedThread& operator=(const ScopedThread&) = delete;
    ScopedThread(ScopedThread&&) = default;
    ScopedThread& operator=(ScopedThread&&) = default;
    ~ScopedThread()
    {
        if(thd.joinable()) thd.join();
    }
};


template<typename T>
class ThreadSafeQueue
{
public:
    ThreadSafeQueue(size_t limit) : limit(limit) {}
    void push(T item)
    {
        std::unique_lock<std::mutex> ul(mtx);
        while(q.size() >= limit) {
            cond_push.wait(ul);
        }
        q.push(item);
        cond_pop.notify_one();
    }
    T pop()
    {
        std::unique_lock<std::mutex> ul(mtx);
        while(q.empty())
            cond_pop.wait(ul);
        T res = q.front();
        q.pop();
        cond_push.notify_one();
        return res;
    }

private:
    std::queue<T> q;
    std::mutex mtx;
    std::condition_variable cond_pop;
    std::condition_variable cond_push;
    size_t limit{0};
};


class ThreadPool
{
public:
    ThreadPool(int size)
    {
        for(int i = 0 ; i < size ; ++i) {
            workers.emplace_back([this]
            {
                for(;;) {
                    auto task = q.pop();
                    if(!task) return;
                    task();
                }
            });
        }
    }
    ~ThreadPool()
    {
        for(unsigned int i = 0 ; i < workers.size() ; ++i) {
            q.push(TaskT());
        }
    }
    void submit(TaskT task)
    {
        if (task) {
            q.push(task);
        }
    }
    template<typename F>
    auto async(F f) -> std::future<decltype(f())>
    {
        using res_t = decltype(f());
        auto pt = std::make_shared<std::packaged_task<res_t()>>(f);
        std::future<res_t> res = pt->get_future();
        q.push([pt] { (*pt)(); });
        return res;
    }

private:
    ThreadSafeQueue<TaskT> q{1000};
    std::vector<ScopedThread> workers;
};

#endif /* UTILS_HPP */
