#include <algorithm>
#include <cassert>
#include <iostream>

#include "Transposition.hpp"

Transposition::Transposition(unsigned d, double phi_m, bool each) :
    d_{d}, each_{each}, phi_m_{phi_m}, rd_{}, g_(rd_()), dist_int_(0, d-1), dist_real_(0, 1.0)
{ }


TPopulation Transposition::operator()(TPopulation population)
{
    assert(population.size() > 0);
    assert(population[0].permutation_.size() == d_);
    (void)d_;
    using std::swap;
    if (each_) {
        for (auto i = 0u; i < population.size(); ++i) {
            unsigned limit = population[i].permutation_.size() >> 1;
            for (auto j = 0u; j < limit; ++j) {
                if (dist_real_(g_) < phi_m_) {
                    int k = dist_int_(g_);
                    swap(population[i].permutation_[j], population[i].permutation_[k]);
                }
            }
        }
    } else {
        for (auto i = 0u; i < population.size(); ++i) {
            if (dist_real_(g_) < phi_m_) {
                swap(population[i].permutation_[dist_int_(g_)], population[i].permutation_[dist_int_(g_)]);
            }
        }
    }
    return population;
}
