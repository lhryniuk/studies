#include <cassert>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "WeightedGraph.hpp"

WeightedGraph::WeightedGraph(unsigned v) :
    v_{v}, graph_{std::vector<std::vector<int>>{}}
{
    graph_.resize(v_+1);
    for (auto&& node : graph_) {
        node.resize(v_+1, 0);
    }
}

void WeightedGraph::AddWeightFromAToB(unsigned a, unsigned b, int weight)
{
    assert(0 <= a && a <= v_);
    assert(0 <= b && b <= v_);
    graph_[a][b] = weight;
}

int WeightedGraph::WeightFromAToB(unsigned a, unsigned b) const
{
    assert(0 <= a && a <= v_);
    assert(0 <= b && b <= v_);
    return graph_[a][b];
}

unsigned WeightedGraph::V() const
{
    return v_;
}

std::string WeightedGraph::Show() const
{
    std::ostringstream outstr;
    for (auto i = 1u; i <= v_; ++i) {
        for (auto j = 1u; j <= v_; ++j) {
            outstr << std::setw(3) << WeightFromAToB(i, j) << ' ';
        }
        outstr << "\n";
    }
    return outstr.str();
}

int WeightedGraph::Cost(const TPermutation& perm) const
{
    assert(perm.size() > 0);
    auto l = 0;
    for (auto i = 1u; i < perm.size(); ++i) {
        l += graph_[perm[i-1]][perm[i]];
    }
    l += graph_[perm[perm.size()-1]][perm[0]];
    return l;
}
