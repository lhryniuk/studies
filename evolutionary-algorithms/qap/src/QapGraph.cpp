#include <cassert>
#include <vector>

#include "QapGraph.hpp"

QapGraph::QapGraph(unsigned v) :
    WeightedGraph(v), flows_(v+1, std::vector<int>(v+1, 0))
{ }

void QapGraph::AddFlowFromAToB(unsigned a, unsigned b, double flow)
{
    flows_[a][b] = flow;
}

int QapGraph::FlowFromAToB(unsigned a, unsigned b) const
{
    return flows_[a][b];
}

int QapGraph::Cost(const TPermutation& perm) const
{
    int cost{};
    for (auto i = 1u; i <= v_; ++i) {
        for (auto j = 1u; j <= v_; ++j) {
            cost += flows_[i][j] * graph_[perm[i-1]][perm[j-1]];
        }
    }
    return cost;
}
