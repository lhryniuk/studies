#ifndef WEIGHTED_GRAPH_HPP
#define WEIGHTED_GRAPH_HPP

#include <string>
#include <vector>

#include "Types.hpp"

class WeightedGraph
{
public:
    explicit WeightedGraph(unsigned v=0);
    virtual void AddWeightFromAToB(unsigned a, unsigned b, int weight);
    virtual int WeightFromAToB(unsigned a, unsigned b) const;
    virtual unsigned V() const;
    virtual std::string Show() const;
    virtual int Cost(const TPermutation& perm) const;
    virtual ~WeightedGraph() = default;

protected:
    unsigned v_;
    std::vector<std::vector<int>> graph_;
};

#endif // WEIGHTED_GRAPH_HPP
