#include <cassert>
#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>

#include "TsplibImporter.hpp"

TsplibImporter::TsplibImporter(const std::string& filepath) :
    filepath_{filepath},
    data_{std::make_unique<TsplibData>(ReadDataFromFile())}
{ }

TspGraph TsplibImporter::GetGraph() const
{
    auto graph = data_->GetGraph();
    assert(graph.V() > 0);
    return data_->GetGraph();
}

void TsplibImporter::ShowGraph() const
{
    data_->ShowGraph();
}

std::vector<std::string> TsplibImporter::ReadDataFromFile(std::string filepath)
{
    if (filepath.empty()) {
        filepath = filepath_;
    } else {
        filepath_ = filepath;
    }
    std::vector<std::string> lines;

    std::cerr << "Reading file " << filepath_ << "\n";
    std::string str;
    std::ifstream input(filepath_);
    assert(input.is_open());
    while(std::getline(input, str)) {
        lines.push_back(str);
    }
    return lines;
}

