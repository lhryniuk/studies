#ifndef UTIL_H
#define UTIL_H

#include <iomanip>
#include <vector>

template<typename T>
auto print_vector(const std::vector<T>& v)
{
    for (const auto& i : v) {
        std::cout << std::fixed << std::setprecision(2) << i << ' ';
    }
    std::cout << std::setprecision(10) << "\n";
}

template<typename T>
auto mean(const std::vector<T>& i)
{
    auto sum = std::accumulate(std::begin(i), std::end(i), T{});
    return double(sum) / double(i.size());
}

template<typename T>
std::pair<T, int> get_best_value(const std::vector<T>& v)
{
    auto best_index = -1;
    T best_value = T{};
    for (auto i = 0u; i < v.size(); ++i) {
        if (v[i] > best_value) {
            best_value = v[i];
            best_index = i;
        }
    }
    return {best_value, best_index};
}

#endif /* UTIL_H */
