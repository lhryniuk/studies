#ifndef PBIL_H
#define PBIL_H

#include <cassert>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <vector>

#include "util.h"


std::vector<double> initial_probability_vector(int size);
double random_value();
int binary_random(double p);
auto random_individual(const std::vector<double>& p, unsigned d);
std::vector<std::vector<int>>  random_population(const std::vector<double>& p,
                       int n,
                       int d);

auto onemax(const std::vector<int>& i);

template<typename T>
auto population_evaluation(const std::vector<std::vector<int>>& p,
                           T&& f)
{
    assert(!p.empty());
    std::vector<double> evaluation;
    for (auto&& i : p) {
        evaluation.push_back(f(i));
    }
    return evaluation;
}

template<typename T1>
void print_best(const std::vector<std::vector<int>>& pop, T1 best_for_now, int best_index)
{

    std::cout << "new best: " << best_for_now << '\n';
    // print_vector(pop[best_index]);
}

template <
    int loops=-1,
    int debug=1,
    typename T1,
    typename T2=std::function<void(const std::vector<std::vector<int>>&, int, int)>>
void pbil(T1&& f,
          int n,
          int d,
          double p1,
          double p2,
          double p3,
          T2&& process_best=print_best<int>)
{
    if (debug) {
        std::cerr << "Starting PBIL algorithm with parameters:\n"
                << "number of loops = " << loops
                << "\nn = " << n << "\nd = " << d << "\np1 = " << p1
                << "\np2 = " << p2 << "\np3 = " << p3 << "\n";
    }
    auto p = initial_probability_vector(d);
    auto pop = random_population(p, n, d);
    auto eval = population_evaluation(pop, f);
    auto bests = get_best_value(eval);
    auto best_index = bests.second;
    auto best_value = bests.first;
    auto best_for_now = best_value;
    process_best(pop, best_for_now, best_index);

    int loop = 0;
    while (loops < 0 || (loop < loops)) {
        for (auto i = 0; i < d; ++i) {
            p[i] = p[i] * (1.0 - p1) + pop[best_index][i] * p1;
        }
        for (auto k = 0; k < d; ++k) {
            auto x = random_value();
            if (x < p2) {
                p[k] = p[k] * (1.0-p3) + binary_random(0.5) * p3;
            }
        }
        pop = random_population(p, n, d);
        eval = population_evaluation(pop, f);

        auto bests = get_best_value(eval);
        best_value = bests.first;
        best_index = bests.second;
        if (best_value > best_for_now) {
            best_for_now = std::max(best_value, best_for_now);
            process_best(pop, best_for_now, best_index);
        }
        ++loop;
    }
}


#endif /* PBIL_H */
