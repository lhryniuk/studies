#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <vector>

#include "pbil.h"

// population size
const int N = 1000;
// size of individual
const int D = 1000;
// parameter for k-deceptive onemax
const int K = 2;
// learning coefficient
const double P1 = 0.2;
// mutation probability
const double P2 = 0.05;
// disturbance coefficient
const double P3 = 0.02;

auto onemax(const std::vector<int>& v)
{
    return std::accumulate(std::begin(v), std::end(v), 0);
}

auto deceptive_onemax(const std::vector<int>& v)
{
    auto a =  std::accumulate(std::begin(v), std::end(v), 0);
    return (a ? a : 2 * v.size());
}

template<int k=K>
auto kdeceptive_onemax(const std::vector<int>& v) -> int
{
    int r = 0;
    for (auto i = 0u; i < v.size(); i += k) {
        std::vector<int> w(std::begin(v) + i, std::begin(v) + i + k);
        r += deceptive_onemax(w);
    }
    return r;
}


int main(int argc, char** argv)
{
    // double p1{};
    // double p2{};
    // double p3{};
    // if (argc == 4) {
    //     p1 = std::stod(argv[1]);
    //     p2 = std::stod(argv[2]);
    //     p3 = std::stod(argv[3]);
    // } else {
    //     p1 = P1;
    //     p2 = P2;
    //     p3 = P3;
    // }
    std::ofstream omfile("onemax_results.txt");
    std::ofstream kdomfile("kdecept_onemax_results.txt");
    for (double p1 = 0.00; p1 <= 1.0; p1 += 0.05) {
        for (double p2 = 0.00; p2 <= 1.0; p2 += 0.05) {
            for (double p3 = 0.00; p3 <= 1.0; p3 += 0.05) {
                int best_overall = -1;
                auto process = [&best_overall](const std::vector<std::vector<int>>&,
                                int best_value, auto)
                {
                    best_overall = std::max(best_overall, best_value);
                };
                std::vector<int> results;
                for (int i = 0; i < 5; ++i) {
                    best_overall = -1;
                    pbil<50, 0>(onemax, N, D, p1, p2, p3, process);
                    results.push_back(best_overall);
                }
                omfile << p1 << ' ' << p2 << ' ' << p3 << ' ' << mean(results) << std::endl;
            }
        }
    }
    for (double p1 = 0.01; p1 <= 1.0; p1 += 0.05) {
        for (double p2 = 0.01; p2 <= 1.0; p2 += 0.05) {
            for (double p3 = 0.01; p3 <= 1.0; p3 += 0.05) {
                int best_overall = -1;
                auto process = [&best_overall](const std::vector<std::vector<int>>&,
                                int best_value, auto)
                {
                    best_overall = std::max(best_overall, best_value);
                };
                std::vector<int> results;
                for (int i = 0; i < 5; ++i) {
                    best_overall = -1;
                    pbil<200, 0>(kdeceptive_onemax<5>, N, D, p1, p2, p3, process);
                    results.push_back(best_overall);
                }
                kdomfile << p1 << ' ' << p2 << ' ' << p3 << ' ' << mean(results) << std::endl;
            }
        }
    }
    // auto process = [](const std::vector<std::vector<int>>&,
    //                 int best_value, auto)
    // {
    //     std::cout << "best_value: " << best_value << "\n";
    // };
    // pbil<100, 0>(kdeceptive_onemax<5>, N, D, p1, p2, p3, process);
}
