
K = 5

def onemax(v):
    return sum(v)

def deceptive_onemax(v):
    if all(map(lambda x: x == 0, v)):
        return 2 * D
    return sum(v)

def kdeceptive_onemax(D, k, v):
    if k == None:
        k = K
    assert k > 0
    assert D % k == 0
    l = D // k
    s = 0
    for i in range(l):
        if all(map(lambda x: x == 0, v[i*k:(i+1)*k])):
            s += 2 * k
        else:
            s += sum(v[i*k:(i+1)*k])
    return s
