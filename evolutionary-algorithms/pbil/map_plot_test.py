import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors as colors
import math


width_orig = 106
height_orig = 148
width_red = 85
height_red = 110
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(0, width_orig)
ax.set_xlim(0, height_orig)
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

# colors = ['white', 'lime', 'tomato', 'mediumblue']
colors = [ 'mediumspringgreen', 'black', 'darkgreen', 'mediumseagreen',
        'lightskyblue', 'darkred', 'rosybrown', 'darkkhaki', 'tomato', 'ghostwhite',
        'burlywood', 'wheat']

def add_rectange(x, y, c='lime'):
    pos = (x, y)
    ax.add_patch(patches.Rectangle(pos, 1.0, 1.0, color=c))


def draw_map(mapv, width, height):
    for y, row in enumerate(mapv):
        for x, square in enumerate(row):
            # print(max(mapv[y]))
            add_rectange(y, width - x - 1, colors[mapv[y][x]])

def create_map_from_oned_vector(v, width, height):
    map_rect = []
    for i in range(height):
        map_rect.append([])
        for j in range(width):
            map_rect[i].append(v[i*width+j])
    return map_rect

def read_first_line_as_vector(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()[0]
        parts = [int(float(k)) for k in lines.split()]
        return parts
    return None


def read_map(filename):
    parts = read_first_line_as_vector(filename)
    return create_map_from_oned_vector(parts)


def draw_map_from_file(filename):
    m = read_map('ImageExpert.txt')
    draw_map(m)
    # plt.show()
    plt.savefig(filename)


def read_rules(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
        rules = [[int(float(k)) for k in x.split()] for x in lines]
        # print(rules)
        return rules
    return None


def read_subset_file(filename):
    with open(filename, 'r') as f:
        subset = [int(k) for k in f.readlines()[0].split()]
        return subset


def create_vector_base_on_subset(rules, subset, width, height):
    points = []
    for p in range(width * height):
        votes = [0 for i in range(4)]
        for i, rule in enumerate(rules):
            if subset[i] == 1:
                votes[rule[p]] += 1
        j = -1
        m = -1
        for k, n in enumerate(votes):
            if votes[k] > m:
                m = n
                j = k
        points.append(j)
    return points


def draw_map_for_rules_and_subset(rules, subset, filename):
    v = create_vector_base_on_subset(rules, subset)
    m = create_map_from_oned_vector(v)
    draw_map(m)
    # plt.show()
    plt.savefig(filename)

def generate_positons_vector(image):
    inte = [3, 7, 9]
    pos = {3: dict(), 7: dict(), 9: dict()}
    nbr_of = {3: 0, 7: 0, 9: 0}
    for i, p in enumerate(image):
        if p in inte:
            pos[p][i] = nbr_of[p]
            nbr_of[p] += 1
    return pos

if __name__ == '__main__':
    image = read_first_line_as_vector('ImageExpert.txt')
    image_reduced = read_first_line_as_vector('ImageExpertReduced.txt')
    starts = {
            1:image_reduced.index(1),
            2:image_reduced.index(2),
            3:image_reduced.index(3)
            }
    pos = generate_positons_vector(image)
    exchange = {1: 3, 2: 7, 3: 9}
    exchange_back = {3: 1, 7: 2, 9: 3}
    rules = read_rules('ClassificationRules.txt')
    subset1 = read_subset_file('6909p1_15p2_8p3_4.rul')
    subset2 = read_subset_file('8706p1_15p2_8p3_4.rul')
    subset3 = read_subset_file('9235p1_10p2_5p3_2.rul')
    v = create_vector_base_on_subset(rules, subset2, width_red, height_red)

    inte = [3, 7, 9]

    for i, p in enumerate(image):
        if p in inte:
            image[i] = exchange[v[starts[exchange_back[p]] + pos[p][i]]]
    draw_map(create_map_from_oned_vector(image, width_orig, height_orig),
            width_orig, height_orig)
    plt.savefig('map8706.png')

    # rules = read_rules('ClassificationRules.txt')
    # subset1 = read_subset_file('6909p1_15p2_8p3_4.rul')
    # v = create_vector_base_on_subset(rules, subset1, width_red, height_red)

    # print(len(list(filter(lambda x: x == 9, image))))
    # print(len(list(filter(lambda x: x == 2, image_reduced))))
    # print(image_reduced)
    # rules = read_rules('ClassificationRules.txt')
    # subset1 = read_subset_file('6909p1_15p2_8p3_4.rul')
    # v = create_vector_base_on_subset(rules, subset1, width_red, height_red)
    # print(v)
    # print(image)
    # subset2 = read_subset_file('8706p1_15p2_8p3_4.rul')
    # subset3 = read_subset_file('9235p1_10p2_5p3_2.rul')
    # draw_map_for_rules_and_subset(rules, subset1, 'r6909.png')
    # print(colors)
    # draw_map_from_file('expert.png')
