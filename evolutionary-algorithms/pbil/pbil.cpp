#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <iomanip>
#include <random>
#include <sstream>
#include <utility>
#include <vector>

#include <unistd.h>

#include "pbil.h"

// population size
const int N = 1000;
// size of individual
const int D = 266;
// parameter for k-deceptive onemax
const int K = 2;
// learning coefficient
const double P1 = 0.2;
// mutation probability
const double P2 = 0.05;
// disturbance coefficient
const double P3 = 0.02;

const double delay = 0.001;

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dis(0, 1);

double random_value()
{
    return dis(gen);
}


std::vector<double> initial_probability_vector(int size)
{
    assert (size > 0);
    return std::vector<double>(size, 0.5);
}

int binary_random(double p)
{
    assert(1.0 >= p && p >= 0.0);
    auto x = dis(gen);
    if (x < p) {
        return 1;
    } else {
        return 0;
    }
}

auto random_individual(const std::vector<double>& p, unsigned d)
{
    assert(p.size() == d);
    std::vector<int> v;
    for (auto i = 0u; i < d; ++i) {
        v.push_back(binary_random(p[i]));
    }
    return v;
}

std::vector<std::vector<int>> random_population(const std::vector<double>& p,
                       int n,
                       int d)
{
    assert(d > 0);
    assert(n > 0);
    std::vector<std::vector<int>> pop;
    for (int i = 0; i < n; ++i) {
        pop.push_back(random_individual(p, d));
    }
    return pop;
}
