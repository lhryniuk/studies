#!/usr/bin/env python

import matplotlib.patches as mpatches
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
import random
import statistics as st
import time
import functools

from onemax import *

PLOT = True

# population size
N = 1000
# size of individual
D = 200
K = 5
# learning coefficient
P1 = 0.2
# mutation probability
P2 = 0.1
# disturbance coefficient
P3 = 0.02
LOOPS = 10000

delay = 0.001

def initial_probability_vector(d):
    assert d > 0
    return [0.5] * d

def binary_random(p):
    assert 0.0 <= p and p <= 1.0
    return 1 if random.uniform(0, 1) < p else 0


def random_individual(p, d):
    return [binary_random(p[i]) for i in range(d)]


def random_population(p, n, d):
    return [random_individual(p[:], d) for i in range(n)]


def population_evaluation(p, f):
    evaluation = [f(i) for i in p]
    return st.mean(evaluation), st.median(evaluation),\
           evaluation.index(max(evaluation)), evaluation.index(min(evaluation))


def results_plot(fname, f, n=N, d=D, p1=P1, p2=P2, p3=P3, t=None, plot=PLOT):
    assert n > 0
    assert d > 0
    assert 0.0 <= p1 and p1 <= 1.0
    assert 0.0 <= p2 and p2 <= 1.0
    assert 0.0 <= p3 and p3 <= 1.0

    p = initial_probability_vector(d)
    pop = random_population(p[:], n, d)
    mean, median, best_index, worst_index = population_evaluation(pop[:], f)

    xs = []
    bests = []
    medians = []
    worsts = []
    for i in range(LOOPS):
        xs.append(i)
        x = pop[best_index]
        for k in range(d):
            p[k] = p[k] * (1-p1) + x[k] * p1

        for k in range(d):
            if random.uniform(0, 1) < p2:
                p[k] = p[k] * (1 - p3) + binary_random(0.5) * p3
        pop = random_population(p, n, d)
        mean, median, best_index, worst_index = population_evaluation(pop, f)
        best_f = f(pop[best_index])
        worst_f = f(pop[worst_index])
        bests.append(best_f)
        medians.append(median)
        worsts.append(worst_f)
    if plot:
        plt.figure(figsize=(16, 9))
        plt.plot(xs, bests, 'g-')
        plt.plot(xs, medians, 'b-')
        plt.plot(xs, worsts, 'r-')
        plt.title('{}\nN = {} D = {} p1 = {} p2 = {} p3 = {}'.format(fname,
                                                        n, d, p1, p2, p3))
        plt.savefig('pbil_{}_p1_{}_p2_{}_p3_{}_bmw.png'.format(fname.lower(), p1, p2, p3))


def probability_plot_pbil(fname, f, n=N, d=D, p1=P1, p2=P2, p3=P3, t=None, plot=PLOT):
    assert n > 0
    assert d > 0
    assert 0.0 <= p1 and p1 <= 1.0
    assert 0.0 <= p2 and p2 <= 1.0
    assert 0.0 <= p3 and p3 <= 1.0

    p = initial_probability_vector(d)
    pop = random_population(p, n, d)
    mean, median, best_index, worst_index = population_evaluation(pop, f)

    xs = []
    bests = []
    medians = []
    worsts = []
    # i = 0
    colors_list = list(colors.cnames.keys())
    probability_vector_values = [[] for i in range(d)]
    for i in range(LOOPS):
        xs.append(i)
        x = pop[best_index]
        for k in range(d):
            p[k] = p[k] * (1-p1) + x[k] * p1

        for k in range(d):
            if random.uniform(0, 1) < p2:
                p[k] = p[k] * (1 - p3) + binary_random(0.5) * p3
            probability_vector_values[k].append(p[k])
        pop = random_population(p, n, d)
        # print([int(k * 100.0) for k in p])
        # for k in range(d):
        #     plt.plot(xs, probability_vector_values[k], colors_list[k % len(colors_list)])
        # plt.show()
        # plt.pause(delay)
    if plot:
        plt.figure(figsize=(16, 9))
        for k in range(d):
            plt.plot(xs, probability_vector_values[k], colors_list[k % len(colors_list)])
        plt.title('{}\nN = {} D = {} p1 = {} p2 = {} p3 = {}'.format(fname,
                                                        n, d, p1, p2, p3))
        plt.savefig('pbil_{}_p1_{}_p2_{}_p3_{}.png'.format(fname.lower(), p1, p2, p3))


def kdec_plot(k):
    kdec = functools.partial(kdeceptive_onemax, D, k)
    results_plot('{}-Deceptive_OneMax'.format(k), kdec, N, D, 0.2, 0.05, 0.02)

def kdec_probability_plot(k):
    kdec = functools.partial(kdeceptive_onemax, D, k)
    probability_plot_pbil('{}-Deceptive_OneMax'.format(k), kdec, N, D, 0.2, 0.05, 0.02)


if __name__ == '__main__':
    # if PLOT:
    #     plt.ion()
    #     green_patch = mpatches.Patch(color='green', label='best')
    #     blue_patch = mpatches.Patch(color='blue', label='median')
    #     red_patch = mpatches.Patch(color='red', label='worst')
    #     plt.legend(handles=[green_patch, blue_patch, red_patch])
        # plt.show()
        # plt.axis([0, 500, 0, 1])
        # plt.show()
    #results_plot('OneMax', onemax, N, D, 0.2, 0.05, 0.02)
    probability_plot_pbil('OneMax', onemax, N, D, 0.2, 0.05, 0.02)
    # kdec_probability_plot(10)
    # kdec_plot(5)
    # kdec_plot(10)
    # kdec_plot(20)
    # L = D + 10
    # p = [
    #         [0.4, 0.2, 0.05],
    #         [0.3, 0.1, 0.1],
    #         [0.2, 0.05, 0.02],
    #         [0.1, 0.05, 0.05]
    # pbil('Deceptive OneMax', onemax, N, D, 0.2 0.05, 0.02)
    # pbil('5-Deceptive OneMax', , N, D, 0.2 0.05, 0.02)
    #     ]
    # for i in range(len(p)):
    #     pbil('OneMax', onemax, N, D, p[i][0], p[i][1], p[i][2])
    # for i in range(len(p)):
    #     pbil('Deceptive OneMax', deceptive_onemax, N, D, p[i][0], p[i][1], p[i][2])
    # ks = [5, 10, 20]
    # for k in ks:
    #     for i in range(len(p)):
    #         pbil('{}-deceptive OneMax'.format(k), deceptive_onemax, N, D, p[i][0], p[i][1], p[i][2])
