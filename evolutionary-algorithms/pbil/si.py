
import functools
import time

import pbil

nbr_of_points = 9350
nbr_of_rules = 266
image_filename = 'ImageRawReduced.txt'
image_size = (3, nbr_of_points)
perfect_filename = 'ImageExpertReduced.txt'
perfect_size = (1, nbr_of_points)
rules_filename = 'ClassificationRules.txt'
rules_size = (nbr_of_rules, nbr_of_points)


def load_vectors_from_file(size, filename):
    assert len(size) == 2
    for s in size:
        assert s > 0
    with open(filename, 'r') as f:
        lines = f.readlines()
        nonempty = lambda x: len(x) > 0
        vectors = list(map(lambda y: [float(i) for i in list(filter(nonempty, y))],
                           map(lambda x: x.split(' '), lines)))
        assert len(vectors) == size[0]
        assert len(vectors[0]) == size[1]
        return vectors
    return None

def voting_winner(l):
    assert len(l) == 4
    max_votes = -1
    winner = -1
    for i, votes in enumerate(l):
        if (votes > max_votes):
            winner = i
            max_votes = votes
    return winner


def rate_set(rules, perfect, subset):
    assert len(rules) == nbr_of_rules
    assert len(perfect) == nbr_of_points
    assert len(subset) == nbr_of_rules
    a = [[0] * 4] * nbr_of_points
    for k, rule in enumerate(rules):
        if subset[k]:
            for i in range(nbr_of_points):
                # print ("vote on point {} for {}".format(i, rule[i]))
                a[i][int(rule[i])] += 1
    nbr_of_matches = 0
    for i in range(nbr_of_points):
        vw = voting_winner(a[i][:])
        # print ("vw = {}, perfect[i] = {}".format(vw, perfect[i]))
        if vw == perfect[i]:
            nbr_of_matches += 1
    print(nbr_of_matches)
    return nbr_of_matches / nbr_of_points


if __name__ == '__main__':
    image = load_vectors_from_file(image_size, image_filename)
    perfect = load_vectors_from_file(perfect_size, perfect_filename)[0]
    rules = load_vectors_from_file(rules_size, rules_filename)

    fitness = functools.partial(rate_set, rules, perfect)
    pbil.pbil(fitness, 100, nbr_of_rules)
