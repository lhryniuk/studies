#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <iomanip>
#include <random>
#include <sstream>
#include <utility>
#include <vector>

#include <unistd.h>

#include "pbil.h"
#include "util.h"

const std::string image_filename = "ImageRawReduced.txt";
const std::string expert_filename = "ImageExpertReduced.txt";
const std::string rules_filename = "ClassificationRules.txt";

// population size
const int N = 1000;
// size of individual
const int D = 266;
// parameter for k-deceptive onemax
const int K = 2;
// learning coefficient
const double P1 = 0.15;
// mutation probability
const double P2 = 0.08;
// disturbance coefficient
const double P3 = 0.04;

const double delay = 0.001;

auto read_lines_from_file(std::string filename)
{
    std::ifstream ifile(filename);
    if (ifile) {
        std::string tmp;
        std::vector<std::string> v;
        while (std::getline(ifile, tmp)) {
            v.push_back(tmp);
        }
        return v;
    }
    return std::vector<std::string>{};
}

auto process_vector_strings(std::vector<std::string> w)
{
    auto get_vector_from_string =
        [](const auto& s)
        {
            std::istringstream istr{s};
            std::vector<double> v;
            double x;
            while (istr >> x) {
                v.push_back(x);
            }
            return v;
        };
    std::vector<std::vector<double>> v;
    for (const auto& s : w) {
        v.push_back(get_vector_from_string(s));
    }
    return v;
}

const int nbr_of_rules = 266;
const int nbr_of_points = 9350;

int rate(const std::vector<std::vector<int>>& rules,
            const std::vector<double>& expert,
            const std::vector<int>& subset)
{
    int matches = 0;
    for (auto i = 0; i < nbr_of_points; ++i) {
        std::vector<int> votes(4, 0);
        int chose = -1;
        int chose_votes = -1;
        for (auto j = 0; j < nbr_of_rules; ++j) {
            if (subset[j]) {
                auto vote_for = rules[i][j];
                ++votes[vote_for];
                if (votes[vote_for] > chose_votes) {
                    chose_votes = votes[vote_for];
                    chose = vote_for;
                }
            }
        }
        if (chose == expert[i]) {
            ++matches;
        }
    }
    return matches;
}

auto transpose_rules(const std::vector<std::vector<double>>& rules)
{
    std::vector<std::vector<int>> v(nbr_of_points);
    for (int i = 0; i < nbr_of_points; ++i) {
        for (int j = 0; j < nbr_of_rules; ++j) {
            v[i].push_back(rules[j][i]);
        }
    }
    return v;
}

void save_best_vector(const std::string& filename_suffix,
                      const std::vector<std::vector<int>>& v,
                      int best_value,
                      int best_index)
{
    std::cout << "best for now: " << best_value << "\n";
    std::ofstream ofile(std::to_string(best_value) + filename_suffix + ".rul");
    for (int i = 0; i < D; ++i) {
        ofile << v[best_index][i] << ' ';
    }
    ofile << "\n";
}

int main(int argc, char** argv)
{
    // auto image = process_vector_strings(read_lines_from_file(image_filename));
    auto rules = transpose_rules(process_vector_strings(read_lines_from_file(rules_filename)));
    auto expert = process_vector_strings(read_lines_from_file(expert_filename)).front();

    auto fitness =
        [&expert,&rules](const std::vector<int>& subset) -> double
        {
            return rate(rules, expert, subset);
        };
    double p1{};
    double p2{};
    double p3{};
    if (argc == 4) {
        p1 = std::stod(argv[1]);
        p2 = std::stod(argv[2]);
        p3 = std::stod(argv[3]);
    } else {
        p1 = P1;
        p2 = P2;
        p3 = P3;
    }
    std::string suffix = "p1_" + std::to_string(int(p1 * 100))
                        + "p2_" + std::to_string(int(p2 * 100))
                        + "p3_" + std::to_string(int(p3 * 100));
    auto process = [&suffix](auto&&... param)
    {
        save_best_vector(suffix, param...);
    };
    pbil(fitness, N, D, p1, p2, p3, process);
}
