
import matplotlib.pyplot as plt
import sys
import os

POINTS=9350.0
D = 266


def accuracy_of_file(filename):
    return (float(filename[0:4]) / POINTS) * 100.0


def get_files_list(root, percent):
    file_paths = []
    for dirname, dirs, files in os.walk(root):
        for filename in files:
            if filename.endswith('.rul')\
               and accuracy_of_file(filename) > percent:
                file_paths.append(os.path.join(dirname, filename))
    return file_paths


def read_vector_from_file(filepath):
    v = []
    conv = lambda x: [int(a) for a in x.split()]
    with open(filepath, 'r') as file:
        for line in file.readlines():
            v.extend(conv(line))
    return v


def plot_vectors_with_accuracy_over(percent, filename, sort=True):
    vectors = list(filter(lambda x: len(x) > 0, [read_vector_from_file(file)
               for file in get_files_list('second_task_results', percent)]))
    nbr_of_vectors = len(vectors)
    print("nbr_of_vectors = {}".format(nbr_of_vectors))
    for v in vectors:
        assert(len(v) == D)

    count_ones = lambda vs, i: len(list(filter(lambda v: v[i] == 1, vs)))
    xs = list(range(D))
    fraction_of_ones = [count_ones(vectors, i)/nbr_of_vectors for i in range(D)]
    if sort:
        plt.bar(xs, sorted(fraction_of_ones), color='g')
    else:
        plt.bar(xs, fraction_of_ones, color='g')
    plt.savefig(filename)


if __name__ == '__main__':
    plot_vectors_with_accuracy_over(98.7, 'over_987_sorted_14.png', True)
