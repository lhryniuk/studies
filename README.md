# studies
Projects created during my studies at University of Wrocław

#### `cpp` and `cpp2`

Object-oriented programming course in two parts, code in **C++**.

#### `db_project`

Project done during Databases course using **PHP** and **PostgreSQL**.

#### `dynamic-inverse-kinematics`

Final project of Evolutionary algorithms course in **Python**.

#### `image-processing`

Different projects done during Image processing course, including
edge detection and PCA used for compression and image desaturation; **C++**.

#### `lhc`

Unfinished compiler from Compilation techniques course; **C**.

#### `traceroute`

Computer networks course project.
