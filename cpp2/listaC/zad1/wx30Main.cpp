/***************************************************************
 * Name:      wx30Main.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2014-01-05
 * Copyright:  ()
 * License:
 **************************************************************/

//#define wxUSE_MENUS
//#define wxUSE_STATUSBAR

#include <algorithm>

#ifdef WX_PRECOMP
    #include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include <wx/textfile.h>
#include "wx30Main.h"

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__WXMAC__)
        wxbuild << _T("-Mac");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

BEGIN_EVENT_TABLE(wx30Frame, wxFrame)
    EVT_MENU(idMenuQuit, wx30Frame::OnQuit)
    EVT_MENU(idMenuAbout, wx30Frame::OnAbout)
    EVT_BUTTON(idButtonACC, wx30Frame::OnACC)
END_EVENT_TABLE()

wx30Frame::wx30Frame(wxFrame *frame, const wxString& title)
    : wxFrame(frame, -1, title), timer_(this,idTimer),
      reftime_(wxDateTime::Now()), ts_(0, 0, k_time_to_lock)
{
#if wxUSE_MENUS
    // create a menu bar
    wxMenuBar* mbar = new wxMenuBar();
    wxMenu* fileMenu = new wxMenu(_T(""));
    fileMenu->Append(idMenuSave, _("&Save\tCtrl-s"), _("Save list"));
    fileMenu->Append(idMenuQuit, _("&Quit\tAlt-F4"), _("Quit the application"));
    mbar->Append(fileMenu, _("&File"));
    Bind(wxEVT_MENU,&wx30Frame::OnSave,this,idMenuSave);

    wxMenu* helpMenu = new wxMenu(_T(""));
    helpMenu->Append(idMenuAbout, _("&About\tF1"), _("Show info about this application"));
    mbar->Append(helpMenu, _("&Help"));

    SetMenuBar(mbar);
#endif // wxUSE_MENUS

#if wxUSE_STATUSBAR
    // create a status bar with some information about the used wxWidgets version
    CreateStatusBar(2);
    timer_.Start(1000);
    Bind(wxEVT_TIMER,[this](wxTimerEvent&)
    {
        // niesymetryczne operatory + i -, wxDateTime lub wxTimeSpan w wyniku
        wxTimeSpan time_left = ts_ - (wxDateTime::Now() - reftime_);
        if (time_left >= wxTimeSpan()) {
            SetStatusText(time_left.Format(), 0);
        } else {
            SetStatusText(wxTimeSpan(0).Format());
        }
        CheckLockInterface(time_left <= wxTimeSpan(0));
    },idTimer);

    SetStatusText(wxbuildinfo(short_f), 1);

    wxPanel* panel = new wxPanel(this);

    // text field with people list
    text_ = new wxTextCtrl(panel,-1,"",wxDefaultPosition,wxSize(200,100),
                           wxTE_MULTILINE | wxTE_READONLY);

    // labels texts
    name_label_ = new wxStaticText(panel, -1, "Imię", wxDefaultPosition,
                                      wxSize(100, 20));
    surname_label_ = new wxStaticText(panel, -1, "Nazwisko", wxDefaultPosition,
                                      wxSize(100, 20));

    // input text fields
    name_ = new wxTextCtrl(panel,-1,"",wxDefaultPosition,wxSize(100,20));
    surname_ = new wxTextCtrl(panel,-1,"",wxDefaultPosition,wxSize(100,20));

    wxButton* buttonACC = new wxButton(panel,idButtonACC,"Wpisz");
    buttonACC->Bind(wxEVT_BUTTON,&wx30Frame::OnACC,this);

    wxBoxSizer* vsizer = new wxBoxSizer(wxVERTICAL);
    vsizer->Add(text_,wxSizerFlags(1).Expand());

    wxBoxSizer* labels_hsizer = new wxBoxSizer(wxHORIZONTAL);
    labels_hsizer->Add(name_label_, wxSizerFlags(1));
    labels_hsizer->Add(surname_label_, wxSizerFlags(1));

    wxBoxSizer* input_hsizer = new wxBoxSizer(wxHORIZONTAL);
    input_hsizer->Add(name_, wxSizerFlags(1));
    input_hsizer->Add(surname_, wxSizerFlags(1));

    vsizer->Add(labels_hsizer, wxSizerFlags(0).Expand());
    vsizer->Add(input_hsizer, wxSizerFlags(0).Expand());
    vsizer->Add(buttonACC, wxSizerFlags(0).Expand());

    panel->SetSizer(vsizer);
    vsizer->SetSizeHints(this);

#endif // wxUSE_STATUSBAR
}


wx30Frame::~wx30Frame()
{
}

bool wx30Frame::GetPassword(void)
{
    wxPasswordEntryDialog password_dialog(this, "Podaj hasło do odblokowania: ",
                          "Wymagane hasło");
    password_dialog.ShowModal();
    return (k_pass == password_dialog.GetValue());
}

void wx30Frame::OnClose(wxCloseEvent &event)
{
    Destroy();
}

void wx30Frame::OnQuit(wxCommandEvent &event)
{
    Destroy();
}

void wx30Frame::OnSave(wxCommandEvent& event)
{
    if (locked_) {
      if (!GetPassword()) {
        wxMessageBox("Hasło nieprawidłowe!", "Błąd!");
        return;
      }
    }
    reftime_ = wxDateTime::Now();
    wxFileDialog saveFileDialog(this,"Save txt file", "", "",
        "txt Files (*.txt)|*.txt",wxFD_SAVE);

    if(saveFileDialog.ShowModal() == wxID_CANCEL)
        return;
    wxString filename = saveFileDialog.GetPath();

    wxTextFile output_file(filename);
    if (!output_file.Exists())
        output_file.Create();
    output_file.Open(wxConvAuto(wxFONTENCODING_UTF8));
    output_file.AddLine(text_->GetValue());
    output_file.Write();
    wxMessageBox(filename, "Dane zostały zapisane do pliku!");
}

void wx30Frame::OnAbout(wxCommandEvent &event)
{
    if (locked_) {
      if (!GetPassword()) {
        wxMessageBox("Hasło nieprawidłowe!", "Błąd!");
        return;
      }
    }
    reftime_ = wxDateTime::Now();
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

// remove whitespace around text and multiply spaces
wxString process_input_string(wxString a)
{
    std::string k(a);
    while (!k.empty() && (k.front() == ' '))
        k.erase(k.begin());
    while (!k.empty() && (k.back() == ' '))
        k.erase(k.size() - 1);
    int pos = 0;
    while ((pos = k.find("  ")) != std::string::npos)
        k.replace(pos, 2, " ");
    return wxString(k);
}

void wx30Frame::OnACC(wxCommandEvent& event)
{
    if (locked_) {
      if (!GetPassword()) {
        wxMessageBox("Hasło nieprawidłowe!", "Błąd!");
        return;
      }
    }
  reftime_ = wxDateTime::Now();
  wxString name = name_->GetValue();
  wxString surname = surname_->GetValue();
  if (name == wxString()) {
    wxMessageBox("Imię nie może być puste!", "Błąd!");
    return;
  }
  if (surname == wxString()) {
    wxMessageBox("Nazwisko nie może być puste!", "Błąd!");
    return;
  }
  Osoba input_person = Osoba(name, surname);
  people_list_.insert(input_person);
  //wxMessageBox(input_person.Imie() + " " + input_person.Nazwisko(),"Dodana osoba");
  UpdateText();
}

void wx30Frame::UpdateText(void)
{
  std::string people;
  auto add_person = [&](Osoba a)
    {
        people += a.Imie() + ' ' + a.Nazwisko() + '\n';
    };
  std::for_each(std::begin(people_list_), std::end(people_list_), add_person);
  text_->SetValue(people);
  name_->Clear();
  surname_->Clear();
}

void wx30Frame::CheckLockInterface(bool lock)
{
    locked_ = lock;
    if (lock) {
        text_->SetBackgroundColour("#EEEEEE");
    } else {
        text_->SetBackgroundColour(text_->GetDefaultAttributes().colBg);
    }
}
