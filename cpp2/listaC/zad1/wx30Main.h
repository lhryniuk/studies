/***************************************************************
 * Name:      wx30Main.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2014-01-05
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef wx30MAIN_H
#define wx30MAIN_H

#include <set>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/datetime.h>
    #include <wx/convauto.h>
#endif

#include "wx30App.h"
#include "Osoba.h"

const int k_time_to_lock= 15;
const wxString k_pass = "123456";

struct osoba_cmp
{
    bool operator()(Osoba a, Osoba b)
    {
        if (a.Imie() < b.Imie())
            return true;
        else if (a.Imie() == b.Imie())
            return a.Nazwisko() < b.Nazwisko();
        return false;
    }
};

class wx30Frame: public wxFrame
{
    public:
        wx30Frame(wxFrame *frame, const wxString& title);
        ~wx30Frame();
    private:
        enum
        {
            idMenuQuit = 1000,
            idMenuSave,
            idMenuAbout,
            idButtonACC,
            idTimer
        };
        void OnClose(wxCloseEvent& event);
        void OnQuit(wxCommandEvent& event);
        void OnSave(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnHello(wxCommandEvent& event);
        void OnACC(wxCommandEvent& event);
        void CheckLockInterface(bool lock);
        bool GetPassword(void);
        void UpdateText(void);
        wxTextCtrl* text_;
        wxStaticText* name_label_;
        wxStaticText* surname_label_;
        wxTextCtrl* name_;
        wxTextCtrl* surname_;
        wxTimer timer_;
        wxDateTime reftime_;
        wxTimeSpan ts_;
        bool locked_;
        std::multiset<Osoba, osoba_cmp> people_list_;
        DECLARE_EVENT_TABLE()
};


#endif // wx30MAIN_H
