#ifndef OSOBA_H_
#define OSOBA_H_

#include <string>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

class Osoba
{
  public:
    Osoba(wxString imie="", wxString nazwisko="");
    virtual ~Osoba(void) = default;
    wxString Imie(void) const;
    wxString Nazwisko(void) const;

  private:
    wxString imie_;
    wxString nazwisko_;
};

#endif /* OSOBA_H_ */
