#include "Osoba.h"

Osoba::Osoba(wxString imie, wxString nazwisko)
  : imie_(imie), nazwisko_(nazwisko)
{ }

wxString Osoba::Imie(void) const
{
  return imie_;
}

wxString Osoba::Nazwisko(void) const
{
  return nazwisko_;
}
