#include <algorithm>
#include <iostream>
#include <vector>

class Person
{
  public:
    Person(std::string imie, std::string nazwisko)
      : imie_(imie), nazwisko_(nazwisko) 
    {
      std::cerr << "Tworze " << imie_ << " " << nazwisko_ << '\n';  
      persons_.push_back(this);
    }
    Person(const Person& p)
      : imie_(p.imie_), nazwisko_(p.nazwisko_)
    {
      persons_.push_back(this);
      std::cerr << "Kopiuje " << imie_ << " " << nazwisko_ << '\n';  
    }
    ~Person(void)
    {
      std::cerr << "Usuwam " << imie_ << " " << nazwisko_ << '\n';  
      Person::persons_.erase(std::find(std::begin(Person::persons_), 
                std::end(Person::persons_), this));
    }
    auto Imie(void) const -> std::string
    {
      return imie_;
    }
    auto Nazwisko(void) const -> std::string
    {
      return nazwisko_;
    }
    static auto OutputPersons(std::ostream& out) -> void
    {
      out << "lista osob:\n";
      for (auto p : Person::persons_)
      {
        std::cout << p->imie_ << ' ' << p->nazwisko_ << '\n';
      }
    }
  private:
    static std::vector<Person*> persons_;
    std::string imie_;
    std::string nazwisko_;
};

std::vector<Person*> Person::persons_;

class Worker
{
  public:
    Worker(const Person& person, std::string position)
      : person_(person), position_(position) {}
  private:
    Person person_;
    std::string position_;
};

void Fun2(void)
{
  Person s("Dariusz", "Dabacki");
  Person t("Eugeniusz", "Ebacki");
  {
    Person u("Franciszek", "Fabacki");
  }
  Worker w(Person("Grzegorz","Gabacki"),"guard");
  Person::OutputPersons(std::cout);
}

void Fun(void)
{
  Person* pq = new Person("Bartosz", "Babacki");
  Person r("Czeslaw", "Cabacki");
  Fun2();
  delete pq;
}

int main(void)
{
  Person p("Adam", "Abacki");
  Fun();
  return 0;
}
