#include <algorithm>
#include <cassert>
#include <iostream>
#include <utility>
#include <vector>

class Person
{
  public:
    Person(std::string imie, std::string nazwisko)
      : imie_(imie), nazwisko_(nazwisko) { }
    auto Imie(void) const -> std::string
    {
      return imie_;
    }
    auto Nazwisko(void) const -> std::string
    {
      return nazwisko_;
    }

  private:
    std::string imie_;
    std::string nazwisko_;
};

class Kolektyw
{
  public:
    explicit Kolektyw(Person* p)
      : p_(p)
    {
      assert(p_);
      std::cerr << "Tworzenie\n"; 
      auto ind = znajdz(p_);
      if (ind < std::end(Kolektyw::ref_count_)) {
        ind->second++;
      } else {
        Kolektyw::ref_count_.push_back(std::make_pair(p, 1));
      }
    }
    Kolektyw(Kolektyw& k)
      : p_(k.p_)
    {
      std::cerr << "Kopiowanie\n";
      auto ind = znajdz(p_);
      if (ind < std::end(Kolektyw::ref_count_)) {
        ind->second++;
      } else {
        Kolektyw::ref_count_.push_back(std::make_pair(p_, 1));
      }
    }
    Kolektyw(Kolektyw&& k) = delete;
    auto operator=(Kolektyw& k) -> Kolektyw&
    {
      std::cerr << "Przypisanie\n";
      p_ = k.p_;
      auto ind = znajdz(p_);
      if (ind < std::end(Kolektyw::ref_count_)) {
        ind->second++;
      } else {
        Kolektyw::ref_count_.push_back(std::make_pair(p_, 1));
      }
      return *this;
    }
    ~Kolektyw(void)
    {
      auto ind = znajdz(p_);
      ind->second--;
      if (ind->second == 0) {
        delete ind->first;
        Kolektyw::ref_count_.erase(ind);
      }
    }
    auto LiczbaReferencji(void) -> int
    {
      auto ind = znajdz(p_);
      return ind->second;
    }

  private:
    static std::vector<std::pair<Person*, int> > ref_count_;
    auto znajdz(Person* p) -> std::vector<std::pair<Person*, int>>::iterator
    {
      auto cond = [&](std::pair<Person*, int> a){ return a.first == p; };
      return std::find_if(std::begin(Kolektyw::ref_count_),
                          std::end(Kolektyw::ref_count_),
                          cond);
    }
    Person* p_;
};

std::vector<std::pair<Person*, int> > Kolektyw::ref_count_;

auto test_Kolektyw1(void) -> void
{
  Kolektyw sp1(new Person("Adam", "Nowak"));
  Kolektyw sp2(new Person("Zdzis", "Krzys"));
  assert(sp1.LiczbaReferencji() == 1);
  assert(sp2.LiczbaReferencji() == 1);
  Kolektyw sp3(sp2); // Kopiowanie
  Kolektyw sp4(new Person("Jan", "Kowalski"));
  Kolektyw sp5(sp4); // Kopiowanie
  Kolektyw sp6 = sp4; // Kopiowanie 
  //Kolektyw sp7(Kolektyw(sp6)); // UWAGA! TUTAJ JEST DEKLARACJA FUNKCJI ZWRACAJACEJ
                               // KOLEKTYW I PRZYJMUJACEJ ARGUMENT O NAZWIE
                               // SP6 TYPU KOLEKTYW 
  assert(sp1.LiczbaReferencji() == 1);
  assert(sp2.LiczbaReferencji() == 2);
  assert(sp3.LiczbaReferencji() == 2);
  //std::cerr << sp4.LiczbaReferencji() << '\n';
  assert(sp4.LiczbaReferencji() == 3);
  assert(sp5.LiczbaReferencji() == 3);
  assert(sp6.LiczbaReferencji() == 3);
  //assert(sp7.LiczbaReferencji() == 3);
}

int main(void)
{
  test_Kolektyw1(); std::cerr << "test_Kolektyw ok!\n";
  return 0;
}
