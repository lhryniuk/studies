#ifndef INTRUSIVE_POINTER_
#define INTRUSIVE_POINTER_

#include <cmath>
#include <vector>


template<class T>
class IntrusivePointer
{
 public:
  IntrusivePointer(void) {}
  explicit IntrusivePointer(T* ptr) : ptr_(ptr) { ptr_->AddRef(); }
  IntrusivePointer(const IntrusivePointer<T>& a) : ptr_(a.ptr_) { ptr_->AddRef(); }
  IntrusivePointer(IntrusivePointer<T>&& a) noexcept = default;
  ~IntrusivePointer(void) { ptr_->DelRef(); }
  auto operator=(const IntrusivePointer<T>& a) -> IntrusivePointer<T>& { IntrusivePointer<T> b(a); swap(b); return *this;}
  auto operator=(IntrusivePointer<T>&& a) noexcept -> IntrusivePointer<T>& = default;
  auto operator*(void) -> T& { return *ptr_; }
  auto operator->(void) -> T* { return &(operator*()); }
  operator bool(void) const { return ptr_ != nullptr; }
  auto swap(IntrusivePointer<T>& a) noexcept -> void
  {
    using std::swap;
    swap(ptr_, a.ptr_);
  }
  
 private:
  T* ptr_=nullptr;
};

class Counter
{
 public:
  auto AddRef(void) const -> void
  {
    ++ref_;
  }
  auto DelRef(void) const -> void
  {
    --ref_;
  }
  auto Ref(void) const -> int
  {
    return ref_;
  }

 private:
  mutable int ref_;
};

#endif /* INTRUSIVE_POINTER_ */
