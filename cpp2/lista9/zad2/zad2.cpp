#include <cassert>
#include <iostream>
#include "IntrusivePointer.h"

class Grid : public Counter
{
  int x_, y_;
  public:
  Grid(int x=0, int y=0) : x_(x), y_(y) {}
  int X(void) const {return x_;}
  int Y(void) const {return y_;}
};

bool operator==(Grid g1, Grid g2)
{
  return (g1.X() == g2.X()) && (g1.Y() == g2.Y());
}

std::ostream& operator<<(std::ostream& wy, Grid g)
{
  return wy << '(' << g.X() << ',' << g.Y() << ')';
}

auto test_IntrPtr(void) -> void
{
  IntrusivePointer<Grid> ip1(new Grid(3, 1));
  assert(ip1->Ref() == 1);
  IntrusivePointer<Grid> ip2(ip1);
  assert(ip1->Ref() == 2);
  IntrusivePointer<const Grid> ip3(new Grid(2, 1));
  IntrusivePointer<const Grid> ip4(ip3);
  IntrusivePointer<const Grid> ip5(ip4);
  IntrusivePointer<const Grid> ip6 = ip4;
  assert(ip3->Ref() == 4);
}

int main(void)
{
  test_IntrPtr(); std::cerr << "test_IntrPtr ok!\n";
  return 0;
}

