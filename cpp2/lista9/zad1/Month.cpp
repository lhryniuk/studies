#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <string>

#define P(x) { x, #x }

enum Month { January=1, February, March, April, May, June, July,
             August, September, October, November, December };

struct MonthConvert
{
  MonthConvert(void)
    : month2str(std::map<Month,std::string>{ P(January), P(February),
                                             P(March), P(April), P(May),
                                             P(June), P(July), P(August),
                                             P(September), P(October),
                                             P(November), P(December) })
  {
    auto add_pair = [&](std::pair<Month, std::string> x)
                    { str2month.insert(std::make_pair(x.second, x.first)); };
    for_each(std::begin(month2str), std::end(month2str), add_pair);
  }
  auto In(std::string a) const -> bool
  {
    auto value = [=](std::pair<Month, std::string> x) { return x.second == a; };
    return (find_if(std::begin(month2str), std::end(month2str), value)
            != std::end(month2str));
  }
  std::map<Month, std::string> month2str;
  std::map<std::string, Month> str2month;
} month_convert;

std::ostream& operator<<(std::ostream& out, Month a)
{
  out << month_convert.month2str[a];
  return out;
}

std::istream& operator>>(std::istream& in, Month& a)
{
  std::string month;
  in >> month;
  if (in) {
    if (month_convert.In(month)) {
      a = month_convert.str2month[month];
    } else {
      if (in)
        in.setstate(std::ios::failbit);
    }
  }
  return in;
}

template<typename F>
void ForEachMonth(F f)
{
  for (int i = 1; i <= 12; ++i) {
    f(static_cast<Month>(i));
  }
}

template<typename F>
void ForEachMonthReverse(F f)
{
  for (int i = 12; i >= 1; --i) {
    f(static_cast<Month>(i));
  }
}

auto testMonth(void) -> void
{
  Month a = April;
  Month b = January;
  std::cout << a << '\n';
  std::istringstream in0("January");
  in0 >> b;
  if (!std::cin) {
    std::cerr << "Error!\n";
  } else {
    std::cout << b << '\n';
  }
  std::ostringstream out;
  ForEachMonthReverse([&](Month m)
      {
      out << m << "\n";
      });
  std::istringstream in(out.str());
  std::vector<Month> v;
  std::copy(std::istream_iterator<Month>(in),std::istream_iterator<Month>(),std::back_inserter(v));
  assert(v.size()==12);
  int i=12;
  ForEachMonth([&v,&i](Month m)
      {
      assert(m == v[--i]);
      });
}

int main(void)
{
  testMonth(); std::cerr << "testMonth ok!\n";
  return 0;
}
