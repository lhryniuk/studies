#ifndef MYC_VECTOR_H_
#define MYC_VECTOR_H_

#include <algorithm>
#include <cassert>
#include <memory>

#define DEB

namespace myc
{

  template<typename T>
  class Vector
  {
    public:
      explicit Vector(int n=0)
        : ptr_(nullptr), size_(n)
      {
        if (n)
          ptr_.reset(new T[n]);
      }
      Vector(const Vector& v)
        : ptr_(nullptr), size_(0)
      {
        if (v.size()) {
          ptr_.reset(new T[v.size()]);
          size_ = v.size_;
          std::copy(v.ptr_.get(), v.ptr_.get() + size_, ptr_.get());
        }
      }
      Vector(Vector&&) noexcept = default;
      Vector& operator=(const Vector& v)
      {
        Vector k(v);
        swap(k);
        return *this;
      }
      Vector& operator=(Vector&&) noexcept = default;
      T& operator[](int i)
      {
        return ptr_[i];
      }
      const T& operator[](int i) const
      {
        return ptr_[i];
      }
      void swap(Vector& v) noexcept
      {
        using std::swap;
        swap(ptr_, v.ptr_);
        swap(size_, v.size_);
      }
      int size(void) const
      {
        return size_;
      }
      bool empty(void) const
      {
        return (size_ == 0);
      }
      void push_back(T&& a)
      {
        std::unique_ptr<T[]> tmp(new T[size_ + 1]);
        std::move(ptr_.get(), ptr_.get() + size_, tmp.get());
        tmp[size_] = std::move(a);
        ++size_;
        ptr_ = std::move(tmp);
      }
      void push_back(const T& a)
      {
        push_back(T(a));
        /*std::unique_ptr<T[]> tmp(new T[size_ + 1]);*/
        /*std::move(ptr_.get(), ptr_.get() + size_, tmp.get());*/
        /*tmp[size_] = a;*/
        /*++size_;*/
        /*ptr_ = std::move(tmp);*/
      }
      void push_front(T&& a)
      {
        std::unique_ptr<T[]> tmp(new T[size_ + 1]);
        std::move(ptr_.get(), ptr_.get() + size_, tmp.get() + 1);
        tmp[0] = std::move(a);
        ++size_;
        ptr_ = std::move(tmp);
      }
      void push_front(const T& a)
      {
        push_front(T(a));
        /*std::unique_ptr<T[]> tmp(new T[size_ + 1]);*/
        /*std::move(ptr_.get(), ptr_.get() + size_, tmp.get() + 1);*/
        /*tmp[0] = a;*/
        /*++size_;*/
        /*ptr_ = std::move(tmp);*/
      }
      void pop_back(void)
      {
        --size_;
        std::unique_ptr<T[]> tmp(new T[size_]);
        std::move(ptr_.get(), ptr_.get() + size_, tmp.get());
        ptr_ = std::move(tmp);
      }
      void pop_front(void)
      {
        --size_;
        std::unique_ptr<T[]> tmp(new T[size_]);
        std::move(ptr_.get() + 1, ptr_.get() + size_ + 1, tmp.get());
        ptr_ = std::move(tmp);
      }
      T& back(void)
      {
        return ptr_[size_-1];
      }
      const T& back(void) const
      {
        return ptr_[size_-1];
      }
      T& front(void)
      {
        return ptr_[0];
      }
      const T& front(void) const
      {
        return ptr_[0];
      }

    private:
      std::unique_ptr<T[]> ptr_;
      int size_;
  };

  template<typename T>
  void swap(Vector<T>& v1, Vector<T>& v2)
  {
    v1.swap(v2);
  }

}

#endif /* MYC_VECTOR_H_ */
