#include <iostream>

class Podatnik
{
  public:
    Podatnik(std::string nip)
      : nip_(nip) {}
    virtual ~Podatnik(void) = 0;
    auto Nip(void) const -> std::string
    {
      return nip_;
    }
    virtual auto pokaz(void) const -> void
    {
      std::cout << "NIP: " << nip_ << '\n';
    }
  
  private:
    std::string nip_;
};

class Osoba
{
  public:
    Osoba(std::string imie, std::string nazwisko)
      : imie_(imie), nazwisko_(nazwisko) {}
    auto Imie(void) const -> std::string
    {
      return imie_;
    }
    auto Nazwisko(void) const -> std::string
    {
      return nazwisko_;
    }

  private:
    std::string imie_;
    std::string nazwisko_;

};

Podatnik::~Podatnik(void) = default;

class OsobaFizyczna : public Podatnik, public Osoba
{
  public:
    OsobaFizyczna(std::string nip, std::string imie, std::string nazwisko)
      : Podatnik(nip), Osoba(imie, nazwisko) {}
    auto pokaz(void) const -> void override
    {
      std::cout << " Imie: " << Imie() << " Nazwisko: " << Nazwisko() << '\n';
    }
};

class Dzialalnosc : public Podatnik
{
  public:  
    Dzialalnosc(std::string nip, std::string regon)
      : Podatnik(nip), regon_(regon) {}
    auto Regon(void) const -> std::string
    {
      return regon_;
    }
    auto pokaz(void) const -> void override
    {
      std::cout << "NIP: " << Nip() << " REGON: " << Regon() << '\n';
    }
  private:
    std::string regon_;
};

class Firma : public Dzialalnosc
{
  public:
    Firma(std::string nip, std::string regon)
      : Dzialalnosc(nip, regon) {}
    auto pokaz(void) const -> void override
    {
      std::cout << "NIP: " << Nip() << " REGON: " << Regon() << '\n';
    }
};

class DzialalnoscJednoosobowa : public Osoba, public Dzialalnosc
{
  public:
    DzialalnoscJednoosobowa(std::string nip, std::string imie, 
                            std::string nazwisko, std::string regon)
      : Osoba(imie, nazwisko), Dzialalnosc(nip, regon) {}
    auto pokaz(void) const -> void override
    {
      std::cout << "NIP: " << Nip() << " Imie: " << Imie() << " Nazwisko: "
                << Nazwisko() << " REGON: " << Regon() << '\n';
    }
};

int main(void)
{
  DzialalnoscJednoosobowa a("123", "Jan", "Kowalski", "123456785");
  a.pokaz();
  return 0;
}
