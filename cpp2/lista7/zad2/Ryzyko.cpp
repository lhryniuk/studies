#include "Ryzyko.h"
#include "Wypadek.h"
#include "Pozar.h"
#include "SzkodaNaturalna.h"

Ryzyko::Ryzyko(double wartosc_pojazdu, int wiek_wlasciciela, std::string marka)
  : wartosc_pojazdu_(wartosc_pojazdu),
    wiek_wlasciciela_(wiek_wlasciciela), 
    marka_(marka)
{ }

auto Ryzyko::DodajPodryzyko(const std::string& nazwa) -> void
{
  if (nazwa == "wypadek") {
    podryzyka_.push_back(std::unique_ptr<Podryzyko>
                         (new Wypadek(wiek_wlasciciela_)));    
  } else if (nazwa == "pozar") {
    podryzyka_.push_back(std::unique_ptr<Podryzyko> (new Pozar()));    
  } else {
    podryzyka_.push_back(std::unique_ptr<Podryzyko> (new SzkodaNaturalna()));    
  }
  ogolna_stawka_ += podryzyka_.back()->Stawka();
}

auto Ryzyko::WartoscPojazdu(void) const -> double
{
  return wartosc_pojazdu_;
}

auto Ryzyko::WiekWlasciciela(void) const -> double
{
  return wiek_wlasciciela_;
}

auto Ryzyko::Marka(void) const -> const std::string&
{
  return marka_;
}

auto Ryzyko::Stawka(void) const -> double
{
  return ogolna_stawka_;
}
