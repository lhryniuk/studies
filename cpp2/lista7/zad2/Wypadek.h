#ifndef WYPADEK_H_
#define WYPADEK_H_

#include "Podryzyko.h"

class Wypadek : public Podryzyko
{
  public:
    explicit Wypadek(int wiek_wlasciciela)
      : Podryzyko(0.0)
    {
      if (wiek_wlasciciela < 26) {
        stawka_ = 0.2;
      } else {
        stawka_ = 0.1;
      }
    }
};

#endif /* WYPADEK_H_ */
