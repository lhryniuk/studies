#ifndef SZKODYNATURALNE_H_
#define SZKODYNATURALNE_H_

#include "Podryzyko.h"

class SzkodaNaturalna : public Podryzyko
{
  public:
    SzkodaNaturalna(void)
      : Podryzyko(0.02)
    { }
};

#endif /* SZKODYNATURALNE_H_ */
