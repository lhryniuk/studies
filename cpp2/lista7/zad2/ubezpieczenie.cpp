#include <iostream>
#include "Ryzyko.h"

void test_Ryzyko(void)
{
  Ryzyko pol1(30000.0, 25, "alfa romeo");
  pol1.DodajPodryzyko("szkoda_naturalna");
  pol1.DodajPodryzyko("wypadek");
  pol1.DodajPodryzyko("pozar");
  std::cout << "Ogolna stawka: " << pol1.Stawka() << " od wartosci pojazdu\n";
}

int main(void)
{
  test_Ryzyko();
  return 0;
}
