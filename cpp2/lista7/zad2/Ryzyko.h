#ifndef RYZYKO_H_
#define RYZYKO_H_

#include <vector>
#include <memory>
#include "Podryzyko.h"

class Ryzyko
{
  public:
    Ryzyko(double wartosc_pojazdu, int wiek_wlasciciela, std::string marka);
    auto DodajPodryzyko(const std::string& nazwa) -> void;
    auto PrzeliczStawke(void) -> void;
    auto WartoscPojazdu(void) const -> double;
    auto WiekWlasciciela(void) const -> double;
    auto Marka(void) const -> const std::string&; 
    auto Stawka(void) const -> double;

  private:
    double wartosc_pojazdu_;
    int wiek_wlasciciela_;
    std::string marka_;
    double ogolna_stawka_;
    std::vector<std::unique_ptr<Podryzyko>> podryzyka_;
};

#endif /* RYZYKO_H_ */
