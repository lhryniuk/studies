#ifndef PODRYZYKO_H_
#define PODRYZYKO_H_

class Podryzyko
{
  public:
    Podryzyko(double stawka=0.0);
    auto Stawka(void) const -> double;
    virtual ~Podryzyko(void) = 0;

  protected:
    double stawka_ = 0.0;
};

#endif /* PODRYZYKO_H_ */
