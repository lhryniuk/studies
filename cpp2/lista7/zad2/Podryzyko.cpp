#include "Podryzyko.h"

Podryzyko::Podryzyko(double stawka)
  : stawka_(stawka)
{ }

auto Podryzyko::Stawka(void) const -> double
{
  return stawka_;
}

Podryzyko::~Podryzyko(void) = default;
