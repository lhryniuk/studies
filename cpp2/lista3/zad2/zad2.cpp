#include <cassert>
#include <iostream>
#include "Tab.h"
#include "Grid.h"

/* poprawic testy */
void test_Tab(void)
{
  Tab<int> it(5);
  it[0]=10;
  it[4]=20;
  assert(it.size()==5);
  assert(it[0]==10);
  assert(it[4]==20);

  Tab<int> iu(10);
  iu[0]=17;
  iu[4]=34;
  assert(iu.size()==10);
  assert(iu[0]==17);
  assert(iu[4]==34);

  iu.swap(it);
  assert(iu.size()==5);
  assert(iu[0]==10);
  assert(it.size()==10);
  assert(it[0]==17);

  Tab<Grid> is(4);
  assert(is.size()==4);
  is[0] = Grid(5, 4);
  is[1] = Grid(6, -6);
  is[2] = Grid(1, -3);
  assert(is[0].X() == 5);
  assert(is[2].Y() == -3);
  assert(is[1] == Grid(6, -6));

  Tab<Grid> iz(is);
  Tab<Grid> ic(4);
  assert(iz.size() == 4);
  assert(iz[3] == Grid(0, 0));
  assert(iz[1] == Grid(6, -6));
  
  ic.swap(is);
  ic = iz;
  assert(ic[2].Y() == -3);
  assert(is[0] == Grid(0, 0));

  std::cerr << "Test Tab ok\n";
}

int main(void)
{
  test_Tab();
  return 0;
}
