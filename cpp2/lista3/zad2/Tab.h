#ifndef INTTAB_H_
#define INTTAB_H_

#include <algorithm>

template<typename T>
class Tab
{
  public:
    Tab(int size=0)
      : size_(size), tab_ptr_(nullptr)
    {
      /* moze cos lepszego? */
      if (size > 0)
        tab_ptr_ = new T[size];
    }

    Tab(const Tab& a)
      : size_(a.size_)
    {
      assert(size >= 0);
      if (a.tab_ptr_) {
        tab_ptr_ = new T[a.size_];
        std::copy(a.tab_ptr_, a.tab_ptr_ + a.size_, tab_ptr_);
      }
    }
    
    ~Tab(void)
    {
      delete[] tab_ptr_;
    }
    
    void swap(Tab& a)
    {
      using std::swap;
      swap(tab_ptr_, a.tab_ptr_);
      swap(size_, a.size_);
    }

    Tab& operator=(Tab a)
    {
      swap(a);
      return *this;
    }

    int size(void) const
    {
      return size_;
    }

    T& operator[](int a) const
    {
      assert(0 <= a && a < size_);
      return tab_ptr_[a];
    }

    T& operator[](int a)
    {
      assert(0 <= a && a < size_);
      return tab_ptr_[a];
    }

  private:
    int size_;
    T* tab_ptr_;
};

template<typename T>
void swap(Tab<T>& a, Tab<T>& b)
{
  a.swap(b);
}

#endif /* INITAB_H_ */
