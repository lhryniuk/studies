#include <algorithm>
#include <iostream>
#include <cassert>
#include "IntTab.h"

IntTab::IntTab(int size)
  : size_(size), tab_ptr_(nullptr)
{
  /* moze cos lepszego? */
  if (size > 0)
    tab_ptr_ = new int[size];
}

IntTab::IntTab(const IntTab& a)
  : size_(a.size_), tab_ptr_(nullptr)
{
  if (a.tab_ptr_) {
    tab_ptr_ = new int[a.size_];
    std::copy(a.tab_ptr_, a.tab_ptr_ + a.size_, tab_ptr_);
  }
}

IntTab& IntTab::operator=(IntTab a)
{
  swap(a);
  return *this;
}

IntTab::~IntTab(void)
{
  delete[] tab_ptr_;
}

void IntTab::swap(IntTab& a) noexcept
{
  using std::swap;
  swap(tab_ptr_, a.tab_ptr_);
  swap(size_, a.size_);
}

int IntTab::size(void) const
{
  return size_;
}

int IntTab::operator[](int a) const
{
  assert(0 <= a && a < size_);
  return tab_ptr_[a];
}

int& IntTab::operator[](int a)
{
  assert(0 <= a && a < size_);
  return tab_ptr_[a];
}

void swap(IntTab& a, IntTab& b)
{
  a.swap(b);
}
