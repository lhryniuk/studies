#include <cassert>
#include <iostream>
#include "IntTab.h"

void test_IntTab(void)
{
  IntTab it(5);
  it[0]=10;
  it[4]=20;
  assert(it.size()==5);
  assert(it[0]==10);
  assert(it[4]==20);
  
  const IntTab ii(it);
  assert(ii[0] == 10);

  IntTab iu(10);
  iu[0]=17;
  iu[4]=34;
  assert(iu.size()==10);
  assert(iu[0]==17);
  assert(iu[4]==34);

  iu.swap(it);
  assert(iu.size()==5);
  assert(iu[0]==10);
  assert(it.size()==10);
  assert(it[0]==17);

  IntTab is(iu);
  assert(is.size()==5);
  assert(is[0]==10);

  IntTab iw;
  iw = it;
  assert(iw.size()==10);
  assert(iw[0]==17);

  it[0]=1;
  iu[0]=2;
  is[0]=3;
  iw[0]=4;

  assert(it[0]==1);
  assert(iu[0]==2);
  assert(is[0]==3);
  assert(iw[0]==4);

  std::cerr << "Test IntTab ok\n";
}

int main(void)
{
  test_IntTab();
  return 0;
}
