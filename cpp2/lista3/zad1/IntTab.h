#ifndef INTTAB_H_
#define INTTAB_H_

#include <algorithm>
#include "IntTab.h"

class IntTab
{
  public:
    explicit IntTab(int size=0);
    IntTab(const IntTab& a);
    ~IntTab(void);
    IntTab operator=(const IntTab& a);
    void swap(IntTab& a);
    int size(void) const;
    const int& operator[](int a) const;
    int& operator[](int a);

  private:
    int size_;
    int* tab_ptr_;
};

void swap(IntTab& a, IntTab& b);

#endif /* INITAB_H_ */
