#ifndef RYZYKO_H_
#define RYZYKO_H_

#include <string>
#include <map>
#include <vector>
#include <set>

/* dla skrocenia nazw i ogarniecia wszystkiego */
typedef std::map<std::string, double> stawki_t;
typedef std::vector<std::pair<std::string, stawki_t> > cechy_t;
typedef std::map<std::string, cechy_t> podryzyka_t;

const std::string g_nazwa_pliku_podryzyk = "podryzyka.txt";

/* klasa Ryzyko
 * Reprezentuje pojedyncza polise.
 * 
 * ### Przed uzyciem nalezy wczytac podryzyka z pomoca ###
 * ### statycznej metody WczytajPodryzyka!             ###
 *
 * Wszystkie mozliwe podryzyka sa przechowywane w pliku
 * g_nazwa_pliku_podryzyk
 * i wcztywane stamtad. Aby dodac podryzyko do polisy,
 * nalezy uzyc metody DodajPodryzyko, ktora korzystajac
 * z nazwy i wczesniej wczytanej struktury, wyliczy stawke */
class Ryzyko
{
  public:
    static void WczytajPodryzyka(
        const std::string& nazwa_pliku=g_nazwa_pliku_podryzyk);
    static void WypiszPodryzyka(void);
    static podryzyka_t g_podryzyka_;
    Ryzyko(double wartosc_pojazdu,
           int wiek_wlasciciela=0,
           std::string marka=std::string());
    /* dodaje podryzyko wybierajac ze struktury g_podryzyka_  */
    void DodajPodryzyko(const std::string& nazwa);
    /* wylicza skladke podliczajac uwzgledniajac stawki
     * za wszystkie dodane podryzyka */
    void PrzeliczStawkeISkladke(void);
    /* wylicza wartosc konkretnego podryzyka */
    double WartoscPodryzyka(const std::string& nazwa) const;
    /* zwraca ogolna stawke od wartosci pojazdu */
    double Stawka(void) const;
    /* zwraca skladke polisy */
    double Skladka(void) const;

  private:
    /* struktura g_podryzyka:
     * nazwa
     *   +---cecha
     *   |     +---wartosc:stawka
     *   |     +---wartosc:stawka
     *   +---cecha
     *   |     +---wartosc:stawka
     *   |     +---wartosc:stawka
     *   |     +---wartosc:stawka
     *   +---cecha
     *        +---wartosc:stawka 
     * np.:
     * g_podryzyka["kradziez"].first = "marka"
     * g_podryzyka["kradziez"].second["daewoo"] = 0.04;
     *
     * */
    std::map<std::string, double> podryzyka_; // miejsce na strukture z zad.3.
    double wartosc_pojazdu_;
    int wiek_wlasciciela_;
    std::string marka_;
    double skladka_; /* wartosc_pojazdu_ * ogolna_stawka_ */
    double ogolna_stawka_;
};

#endif /* RYZYKO_H_ */
