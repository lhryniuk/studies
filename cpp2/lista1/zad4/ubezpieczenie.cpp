#include <iostream>
#include "ryzyko.h"

int main(void)
{
  Ryzyko::WczytajPodryzyka();
  Ryzyko::WypiszPodryzyka();
  Ryzyko polisa(30000.0, 26, "alfa romeo");
  polisa.DodajPodryzyko("wypadek");
  polisa.DodajPodryzyko("kradziez");
  polisa.DodajPodryzyko("pozar");
  std::cout << "Ogolna stawka: " << polisa.Stawka() << " od wartosci pojazdu\n";
  std::cout << "Skladka: " << polisa.Skladka() << "\n";
  return 0;
}
