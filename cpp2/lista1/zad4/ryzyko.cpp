#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include "ryzyko.h"

//#define DG

podryzyka_t Ryzyko::g_podryzyka_;

double str2double(std::string str)
{
  double a = 0.0;
  std::stringstream ss;
  ss << str;
  ss >> a;
  return a;
}

void wstaw_ceche(cechy_t& opisy_cech,
                 std::string cecha, 
                 stawki_t& wartosc_stawka)
{
  if (!wartosc_stawka.empty()) {
    opisy_cech.push_back(std::make_pair(cecha, wartosc_stawka));
    wartosc_stawka.clear();
  }
}

void Ryzyko::WczytajPodryzyka(const std::string& nazwa_pliku)
{
  std::ifstream wejscie(nazwa_pliku);
  std::string linia;
  std::string nazwa;
  std::string cecha;
  std::string wartosc;
  double stawka;
  stawki_t wartosc_stawka;
  cechy_t opisy_cech;
  podryzyka_t podryzyka;
  while (std::getline(wejscie, linia)) {
    if (linia.substr(0, 2) == "N:") {
      nazwa = linia.substr(2);
#ifdef DG
      std::cout << "Wczytuje podryzyko o nazwie " << nazwa << "\n";
#endif
    } else if (linia.substr(0, 2) == "C:") {
      wstaw_ceche(opisy_cech, cecha, wartosc_stawka);
      cecha = linia.substr(2);
#ifdef DG
      std::cout << "Wczytuje ceche " << cecha << "\n";
#endif
    } else if (!linia.empty()) {
      long long sep_pos = linia.rfind(" ");
      wartosc = linia.substr(0, sep_pos);
      std::istringstream str_linia_(linia.substr(sep_pos + 1));
      str_linia_ >> stawka;
      wartosc_stawka[wartosc] = stawka;
#ifdef DG
      std::cout << "Wczytalem " << wartosc << " - " << stawka << "\n";
#endif
    } else {
      wstaw_ceche(opisy_cech, cecha, wartosc_stawka);
      podryzyka[nazwa] = opisy_cech;
      opisy_cech.clear();
    }
  }
  wstaw_ceche(opisy_cech, cecha, wartosc_stawka);
  podryzyka[nazwa] = opisy_cech;
  opisy_cech.clear();
  Ryzyko::g_podryzyka_ = podryzyka;
}

void Ryzyko::WypiszPodryzyka(void)
{
  /* wypisywanie calej struktury g_podryzyka_ */
  for (auto it = std::begin(Ryzyko::g_podryzyka_);
            it != std::end(Ryzyko::g_podryzyka_);
            ++it) {
#ifdef DG
    std::cout << "nazwa podryzyka: " << it->first << "\n";
#endif
    for (auto para : it->second) {
#ifdef DG
      std::cout << "nazwa cechy: " << para.first << "\n"; 
#endif
      for (auto itm = std::begin(para.second);
                itm != std::end(para.second);
                ++itm) {
#ifdef DG
        std::cout << "wartosc cechy: " << itm->first << " "
                  << "stawka: " << itm->second << "\n";
#endif
      }
    }
#ifdef DG
    std::cout << '\n';
#endif
  }
}

Ryzyko::Ryzyko(double wartosc_pojazdu,
               int wiek_wlasciciela,
               std::string marka)
  : wartosc_pojazdu_(wartosc_pojazdu), 
    wiek_wlasciciela_(wiek_wlasciciela),
    marka_(marka)
{
  // co tu jeszcze dodac...?
}

void Ryzyko::DodajPodryzyko(const std::string& nazwa)
{
  podryzyka_[nazwa] = WartoscPodryzyka(nazwa); 
  PrzeliczStawkeISkladke();
}

void Ryzyko::PrzeliczStawkeISkladke(void)
{
  ogolna_stawka_ = 0.0;
  for (auto it = std::begin(podryzyka_);
            it != std::end(podryzyka_);
            ++it) {
    ogolna_stawka_ += it->second;
  }
  skladka_ = wartosc_pojazdu_ * ogolna_stawka_;
}

double Ryzyko::WartoscPodryzyka(const std::string& nazwa) const
{
  if (Ryzyko::g_podryzyka_.find(nazwa) == Ryzyko::g_podryzyka_.end()) {
    std::cerr << "Blad! Ryzyka " << nazwa << " nie ma w systemie!\n";
    std::exit(1);
  } else {
    double wartosc_ryzyka = 0.0;
    for (auto cecha : Ryzyko::g_podryzyka_[nazwa]) {
      if (cecha.first == "wartosc") {
        wartosc_ryzyka += cecha.second["-"];
#ifdef DG
        std::cout << "Dodalem stawke " << cecha.second["-"] << "\n";
#endif
      } else if (cecha.first == "wiek") {
        if (wiek_wlasciciela_ < 26) { 
          wartosc_ryzyka += cecha.second["26"];
#ifdef DG
        std::cout << "Dodalem stawke " << cecha.second["26"] << "\n";
#endif
        } else {
          wartosc_ryzyka += cecha.second["999"];
#ifdef DG
        std::cout << "Dodalem stawke " << cecha.second["999"] << "\n";
#endif
        }
      } else if (cecha.first == "marka") {
        wartosc_ryzyka += cecha.second[marka_];
#ifdef DG
        std::cout << "Dodalem stawke " << cecha.second[marka_] << "\n";
#endif
      }
    }
    return wartosc_ryzyka;
  }
  return 0.0;
}

double Ryzyko::Stawka(void) const
{
  return ogolna_stawka_;
}

double Ryzyko::Skladka(void) const
{
  return skladka_;
}
