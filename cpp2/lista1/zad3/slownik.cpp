#include <iostream>
#include <algorithm>
#include <limits>
#include <cassert>
#include <cmath>
#include <vector>

class Slownik
{
 public:
  double& operator[](const std::string a)
  {
    auto it = find_if(std::begin(pary_), std::end(pary_),
        [=](const std::pair<std::string, double> p) {return p.first == a;});
    if (it == std::end(pary_)) {
      //pary_.push_back(std::make_pair(a, std::numeric_limits<double>::quiet_NaN));
      pary_.push_back(std::make_pair(a, std::numeric_limits<double>::quiet_NaN()));
      return (pary_.back().second);
    } else {
      return ((*it).second);
    }
  }
 private:
  std::vector<std::pair<std::string, double> > pary_;

};

const double eps = 10e-4;

void test(void)
{
  Slownik sl;
  assert (std::isnan(sl["a"]));
  sl["a"] = 4;
  sl["b"] = 3;
  sl["tomek"] = 5.31;
  assert(sl["b"] - eps <= 3 && 3 <= sl["b"] + eps);
  assert(sl["a"] - eps <= 4 && 4 <= sl["a"] + eps);
  assert((sl["tomek"] - eps <= 5.31) && (5.31 <= sl["tomek"] + eps));
}

int main(void)
{
  test(); std::cout << "test ok!\n";
  return 0;
}
