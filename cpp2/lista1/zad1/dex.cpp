#include <iostream>
#include <sstream>
#include <cassert>

auto change(int n, int base) -> std::string
{
  std::string number_string, minus;
  if (n == 0) return "0";
  if (n < 0) {
    minus = "-";    
    n = -n;
  }
  while (n) {
    int digit = n % base;
    if (digit < 10) {
      number_string = char('0' + digit) + number_string;
    } else {
      number_string = char('A' + digit - 10) + number_string;
    }
    n /= base;
  }
  return minus + number_string;
}

auto test_change() -> void
{
  assert(change(35, 36) == "Z");
  assert(change(255, 16) == "FF");
  assert(change(0, 10) == "0");
  assert(change(-15, 2) == "-1111");
}

int main(void)
{
  test_change();
  return 0;
}
