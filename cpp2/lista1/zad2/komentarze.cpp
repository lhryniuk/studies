#include "czysciciel.h"

/* komentarz blokowy */

/* komentarz blokowy
 *
 *
 *
 * na kilka
 * linii
 * */

// komentarz liniowy

int main(int argc, char** argv)
{
	Czysciciel czysc;
	if (argc > 1) {
		for (int i = 1; i < argc; ++i) {
			czysc.WczytajKod(std::string(argv[i]));
			czysc.UsunKomentarze();
			czysc.WypiszKod();
		}
	} else {
		while (true) {
			czysc.WczytajKod();
			czysc.UsunKomentarze();
			czysc.WypiszKod();
		}
	}
	return 0;
}

