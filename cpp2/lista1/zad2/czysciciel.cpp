#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include "czysciciel.h"

void Czysciciel::UsunKomentarze(void)
{
  std::vector<Czysciciel::Komentarz> komentarze = ZnajdzKomentarze();
  struct cmp_
  {
    bool operator()(const Czysciciel::Komentarz& a,
        const Czysciciel::Komentarz& b)
    { return a.poczatek_ > b.poczatek_; }
  } cmp;
  std::sort(std::begin(komentarze), std::end(komentarze), cmp);
  for (const auto& komentarz : komentarze) {
    kod_.erase(komentarz.poczatek_, komentarz.dlugosc_);
    /* usuwa zbedne znaki nowej linii */
    //kod_.erase(kod_.find('\n', komentarz.poczatek_), 1);
  }
}

void Czysciciel::WypiszKod(bool do_pliku,
    const std::string& nazwa_pliku) const
{
  bool blad = false;
  if (do_pliku) {
    std::string plik_wyjsciowy;
    if (nazwa_pliku.empty()) {
      std::string::size_type kropka = plik_wejsciowy_.rfind(".");
      plik_wyjsciowy = plik_wejsciowy_.substr(0, plik_wejsciowy_.rfind("."))
        + "_bez_komentarzy." 
        + plik_wejsciowy_.substr(kropka + 1);
    } else {
      plik_wyjsciowy = nazwa_pliku;
    }
    std::ofstream wyjscie(plik_wyjsciowy);
    if (wyjscie) {
      wyjscie << kod_;
    } else {
      SygnalizujBlad("Nie udalo sie otworzyc pliku wyjsciowego, "
          "wypisuje kod na standardowe wyjscie.", false);
      blad = true;
    }
  }
  if (!do_pliku || blad) {
    std::cout << kod_ << '\n';
  }
}

void Czysciciel::SygnalizujBlad(const std::string& komunikat, 
    int kod,
    bool zakoncz) const
{
  std::cerr << komunikat << '\n';
  if (zakoncz)
    exit(kod);
}

void Czysciciel::SprawdzNazwePliku(const std::string& nazwa) const
{
  std::string::size_type kropka = nazwa.rfind(".");
  if (kropka == std::string::npos
      || (nazwa.substr(kropka + 1) != "c"
        && nazwa.substr(kropka + 1) != "cpp"
        && nazwa.substr(kropka + 1) != "h")) {
    SygnalizujBlad("Przyjmuje tylko pliki z rozszerzeniem .c, .cpp lub .h");
  }
}

std::string Czysciciel::WczytajPlik(const std::string& nazwa_pliku) const
{
  SprawdzNazwePliku(nazwa_pliku);
  std::string kod;
  std::ifstream wejscie(nazwa_pliku);
  if (wejscie) {
    std::string linia;
    while (getline(wejscie, linia)) {
      kod += linia + '\n';
    }
  } else {
    SygnalizujBlad("Nie udalo sie otworzyc pliku " + nazwa_pliku + ". "
        "Sprawdz, czy plik istnieje");
  }
  return kod;
}

void Czysciciel::WczytajKod(const std::string& plik_wejsciowy)
{
  std::string linia;
  std::string nazwa_pliku;
  if (plik_wejsciowy.empty()) {
    std::cout << "Podaj nazwę pliku (nacisnij enter, by wprowadzic "
      "kod z klawiatury): \n";
    getline(std::cin, nazwa_pliku);
    if (nazwa_pliku.empty()) {
      std::cout << "\nPodaj kod (aby zakonczyc wpisywanie wcisnij ctrl-d - "
        "na Linuksie - lub ctrl-z - na Windowsie):\n";
      while (getline(std::cin, linia)) {
        kod_ += linia + '\n';
      }
    } else {
      plik_wejsciowy_ = nazwa_pliku;
      kod_ = WczytajPlik(nazwa_pliku);
    }
  } else {
    plik_wejsciowy_ = plik_wejsciowy;
    kod_ = WczytajPlik(plik_wejsciowy);
  }
}

/* do konwersji numeru linii */
std::string int2str(int a)
{
  std::string str;
  std::stringstream ss;
  ss << a;
  return ss.str();
}

int licz_linie(const std::string& kod, int znak)
{
  return count_if(std::begin(kod), 
      std::begin(kod) + znak, 
      [](char k){return k == '\n';});
}

std::vector<Czysciciel::Komentarz> Czysciciel::ZnajdzKomentarze(void)
{
  std::vector<Czysciciel::Komentarz> komentarze;
  long long poczatek = 0;
  long long tmp = 0;
  long long koniec = 0;
  std::set<int> zagniezdzone;
  /* wyszukuje otwierajace i zamykajace znaczniki komentarzy blokowych */
  while (((poczatek = kod_.find("/*", poczatek)) != std::string::npos)
      && ((koniec = kod_.find("*/", koniec)) != std::string::npos)) {
    if (poczatek > koniec)
      SygnalizujBlad("Brak w pliku " + plik_wejsciowy_ 
          + " otwierajacego znacznika '/*' dla zamykajacego '*/' w linii " 
          + int2str(licz_linie(kod_, koniec) + 1) + "!");
    else if (((tmp = kod_.find("/*", poczatek + 1)) != std::string::npos) && (tmp < koniec)) {
      SygnalizujBlad("Zagniezdzony komentarz w pliku " + plik_wejsciowy_
          + "w linii " + int2str(licz_linie(kod_, tmp) + 1) + "!");
    } else {
      /* pozbywa sie zagniezdzonych komentarzy liniowych */
      for (int i = poczatek + 1; i < koniec; ++i) {
        if (kod_.substr(i, 2) == "//") {
          zagniezdzone.insert(i);
        }
      }
      komentarze.push_back(Czysciciel::Komentarz(poczatek, 
            koniec - poczatek + 2));
    }
    ++poczatek;
    ++koniec;
  }
  /* wyszukuje komentarze liniowe */
  poczatek = 0;
  while ((poczatek = kod_.find("//", poczatek)) != std::string::npos) {
    koniec = kod_.find('\n', poczatek);
    if (zagniezdzone.find(poczatek) == zagniezdzone.end()) {
      komentarze.push_back(Czysciciel::Komentarz(poczatek, koniec - poczatek + 1));
    }
    for (int i = poczatek + 1; i < koniec; ++i) {
      if (kod_.substr(i, 2) == "//") {
        zagniezdzone.insert(i);
      }
    }
    ++poczatek;
  }
  return komentarze;
}
