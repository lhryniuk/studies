#ifndef CZYSCICIEL_H_
#define CZYSCICIEL_H_

#include <string>
#include <vector>

/* klasa Czysciciel
 * Usuwa komentarze w stylu C++: // lub /\*  *\/ z podanego kodu. 
 * */
// //
class Czysciciel
{
  public:
    /* Wyszukuje komentarze za pomoca metody ZnajdzKomentarze,
     * a nastepnie, jesli nie pojawily sie zadne bledy, usuwa
     * je wszystkie. */
    void UsunKomentarze(void);
    /* Jesli podano plik_wejsciowy, wczytuje z niego kod, w przeciwnym
     * razie wczytuje kod z klawiatury do znaku konca pliku 
     * (ctrl-d na Linuksie, ctrl-z na Windowsie).
     * */
    void WczytajKod(const std::string& plik_wejsciowy=std::string());
    /* Wypisuje do pliku o nazwie plik_wyjsciowy. Jesli podano tylko
     * plik_wejsciowy (nazwa.{c,cpp,h}), to 
     * plik_wyjsciowy = nazwa_bez_kom.{c,cpp,h}
     * Jesli plik wyjsciowy juz istnieje lub ustawiono do_pliku = false,
     * wypisuje kod na ekran. */
    void WypiszKod(bool do_pliku=true, 
        const std::string& plik_wyjsciowy=std::string()) const;

  private:
    struct Komentarz
    {
      int poczatek_;
      int dlugosc_;
      Komentarz(int poczatek, int dlugosc)
        : poczatek_(poczatek), dlugosc_(dlugosc) { }
    };
    std::string WczytajPlik(const std::string& nazwa_pliku) const;
    /* Zglasza rozmaite bledy, w przypadku powazniejszych konczy
     * program, zwracajac kod bledu 
     * (domyslnie 1 ze wzgledu na komunikaty). */
    void SygnalizujBlad(const std::string& komunikat, 
        int kod = 1, 
        bool zakoncz=true) const;
    /* Odrzuca pliki rozne od *.{c,cpp,h} */
    void SprawdzNazwePliku(const std::string& nazwa) const;
    /* Przetwarza plik, wyszukuje komentarze i sprawdza ewentualne
     * nielegalne zagniezdzenia. */
    std::vector<Czysciciel::Komentarz> ZnajdzKomentarze(void);
    std::string kod_;
    std::string plik_wejsciowy_;
};

#endif
