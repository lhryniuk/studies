#include <iostream>
#include <sstream>
#include <cassert>

void in_out(std::istream& in, std::ostream& out)
{
  int nwierszy = 0;
  in >> nwierszy;
  assert(nwierszy >= 0);
  int** liczby = new int*[nwierszy];
  for (int i = 0; i < nwierszy; ++i) {
    int l;
    in >> l;
    assert(l >= 0);
    liczby[i] = new int[l + 1];
    liczby[i][0] = l;
    for (int j = 1; j <= l; ++j) {
      in >> liczby[i][j];
    }
  }
  for (int i = nwierszy - 1; i >= 0; --i) {
    int l = liczby[i][0];
    for (int j = l; j >= 1; --j) {
      out << liczby[i][j] << " ";
    }
    out << "\n";
  }
  for (int i = 0; i < nwierszy; ++i) {
    delete[] liczby[i];
  }
  delete[] liczby;
}

void test(void)
{
  std::istringstream in(
                        "4\n"
                        "5 4 9 1 4 2\n"
                        "2 -13 9\n"
                        "3 6 7 9\n"
                        "2 1 3\n"
                        );
  std::ostringstream out;
  in_out(in, out);
  assert(out.str() == 
                      "3 1 \n"
                      "9 7 6 \n"
                      "9 -13 \n"
                      "2 4 1 9 4 \n"
                      );
}

int main(void)
{
  test(); std::cout << "test ok!\n";
  return 0;
}

