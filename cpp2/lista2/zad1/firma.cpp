#include <algorithm>
#include "firma.h"

Firma::Firma(std::string nazwa, int rok, int miesiac, int dzien)
  : data_utworzenia_(rok, miesiac, dzien), nazwa_(nazwa)
{ }

int Firma::DodajPracownika(std::string imie,
                           std::string nazwisko,
                           int rok, int miesiac, int dzien)
{
  pracownicy_.push_back(Osoba(imie, nazwisko, rok, miesiac, dzien));
  return 0;
}

std::string Firma::PracownicyAlfabetycznie(void) const
{
  struct cmp_ {
    bool operator()(Osoba a, Osoba b) {
      return a.Nazwisko() < b.Nazwisko();
    }
  } cmp;
  std::vector<Osoba> pracownicy = pracownicy_;
  sort(std::begin(pracownicy), std::end(pracownicy), cmp);
  std::string lista_pracownikow;
  for (auto pracownik : pracownicy) {
    lista_pracownikow += pracownik.Imie() + " "
                       + pracownik.Nazwisko() + " "
                       + pracownik.DataUrodzenia().Str() + '\n';
  }
  return lista_pracownikow;
}

std::string Firma::PracownicyDataUrodzenia(void) const
{
  struct cmp_ {
    bool operator()(Osoba a, Osoba b) {
      return a.DataUrodzenia() < b.DataUrodzenia();
    }
  } cmp;
  std::vector<Osoba> pracownicy = pracownicy_;
  std::sort(std::begin(pracownicy), std::end(pracownicy), cmp);
  std::string lista_pracownikow;
  for (auto pracownik : pracownicy) {
    lista_pracownikow += pracownik.Imie() + " "
                       + pracownik.Nazwisko() + " "
                       + pracownik.DataUrodzenia().Str() + '\n';
  }
  return lista_pracownikow;
}

std::string Firma::Nazwa(void) const
{
  return nazwa_;
}
