#ifndef FIRMA_H_
#define FIRMA_H_

#include <vector>
#include <string>
#include "osoba.h"
#include "data.h"

class Firma
{
  public:
    Firma(std::string nazwa, int rok, int miesiac, int dzien);
    int DodajPracownika(std::string imie,
                        std::string nazwisko,
                        int rok, int miesiac, int dzien);
    /* sortowanie wg nazwisk */
    std::string PracownicyAlfabetycznie(void) const;
    std::string PracownicyDataUrodzenia(void) const;
    std::string Nazwa(void) const;

  private:
    Data data_utworzenia_;
    std::vector<Osoba> pracownicy_;
    std::string nazwa_;
};

#endif /* FIRMA_H_ */
