#include "osoba.h"

    
Osoba::Osoba(std::string imie, std::string nazwisko)
  : imie_(imie), nazwisko_(nazwisko)
{ }

Osoba::Osoba(std::string imie, std::string nazwisko, 
             int rok, int miesiac, int dzien)
  : data_urodzenia_(rok, miesiac, dzien), imie_(imie), nazwisko_(nazwisko)
{ }
    
std::string Osoba::ImieNazwisko(void) const
{
  return imie_ + " " + nazwisko_;
}
    
Data Osoba::DataUrodzenia(void) const
{
  return data_urodzenia_;
}

std::string Osoba::Imie(void) const
{
  return imie_;
}

std::string Osoba::Nazwisko(void) const
{
  return nazwisko_;
}
