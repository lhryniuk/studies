#include <iostream>
#include "firma.h"
#include "osoba.h"
#include "data.h"

void przyklad(void)
{
  Firma f1("PTY S.A.", 1989, 4, 15);
  Firma f2("STZ S.A.", 1989, 4, 15);
  f1.DodajPracownika("Jacek", "Szymański", 1972, 4, 11);
  f1.DodajPracownika("Krystian", "Szymański", 1951, 4, 25);
  f1.DodajPracownika("Julian", "Wiśniewski", 1966, 6, 27);
  f2.DodajPracownika("Żyraf", "Wiśniewski", 1977, 5, 13);
  f2.DodajPracownika("Wacek", "Nowak", 1985, 3, 11);
  f2.DodajPracownika("Jacek", "Wiśniewski", 1964, 4, 16);
  f2.DodajPracownika("Adam", "Nowak", 1957, 3, 11);
  std::cout << "Pracownicy firmy " << f1.Nazwa() << ":\n";
  std::cout << f1.PracownicyAlfabetycznie() << '\n';
  std::cout << "Pracownicy firmy " << f2.Nazwa() << ":\n";
  std::cout << f2.PracownicyDataUrodzenia() << '\n';

}

int main(void)
{
  przyklad();
  return 0;
}
