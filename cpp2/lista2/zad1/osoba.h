#ifndef OSOBA_H_
#define OSOBA_H_

#include <string>
#include "data.h"

class Osoba
{
  public:
    Osoba(std::string imie, std::string nazwisko);
    Osoba(std::string imie, std::string nazwisko, 
          int rok, int miesiac, int dzien);
    std::string ImieNazwisko(void) const;
    Data DataUrodzenia(void) const;
    std::string Imie(void) const;
    std::string Nazwisko(void) const;

  private:
    Data data_urodzenia_;
    std::string imie_;
    std::string nazwisko_;
};

#endif /* OSOBA_H_ */
