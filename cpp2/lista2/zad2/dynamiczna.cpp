#include <iostream>
#include <sstream>
#include <cassert>

void in_out(std::istream& in, std::ostream& out)
{
  int n = 0;
  in >> n;
  assert(n >= 0);
  int* liczby = new int[n];
  for (int i = 0; i < n; ++i) {
    in >> liczby[i];
  }
  for (int i = n - 1; i >= 0; --i) {
    out << liczby[i] << ' ';
  }
  delete[] liczby;
}

void test(void)
{
  std::istringstream in("5 4 9 1 4 2");
  std::ostringstream out;
  in_out(in, out);
  assert(out.str() == "2 4 1 9 4 ");
}

int main(void)
{
  test(); std::cout << "test ok!\n";
  return 0;
}

