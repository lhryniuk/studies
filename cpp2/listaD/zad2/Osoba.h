#ifndef OSOBA_H_
#define OSOBA_H_

#include <string>
#include "Data.h"

struct bledne_dane : public virtual std::exception { };
struct zly_pesel : public virtual std::exception { };

struct nieprawidlowa_plec : public bledne_dane { };
struct nieprawidlowe_imie : public bledne_dane { };
struct nieprawidlowe_nazwisko : public bledne_dane { };

struct niepoprawna_suma : public zly_pesel { };

struct konflikt_plec_pesel : public zly_pesel, public bledne_dane { };
struct konflikt_data_pesel : public zly_pesel, public bledne_dane { };

struct bledna_data : public konflikt_data_pesel { };
struct zly_rok : public bledna_data {};
struct zly_miesiac : public bledna_data {};
struct zly_dzien : public bledna_data {};

class Osoba
{
  public:
    Osoba(std::string imie="", std::string nazwisko="",
          char plec='x', std::string pesel="", Data data_urodzenia=Data(1900, 1, 1));
    virtual ~Osoba(void) = default;
    std::string Imie(void) const;
    std::string Nazwisko(void) const;
    char Plec(void) const;
    std::string Pesel(void) const;
    Data DataUrodzenia(void) const;

  private:
    std::string imie_;
    std::string nazwisko_;
    char plec_;
    std::string pesel_;
    Data data_urodzenia_;
};

#endif /* OSOBA_H_ */
