#include <sstream>
#include "Data.h"

/* inicjalizacja statycznych stalych */
const std::vector<int> 
  DniWMiesiacuN{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
const std::vector<int> 
  DniWMiesiacuP{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

Data::Data(void)
{
  rok_ = miesiac_ = dzien_ = 0;
}

Data::Data(int rok, int miesiac, int dzien)
{
  Sensowna(rok, miesiac, dzien);
  przestepny_ = Przestepny(rok);
  rok_ = rok;
  miesiac_ = miesiac;
  dzien_ = dzien;
}

auto Data::Rok(void) -> int const
{
  return rok_;
}

auto Data::Miesiac(void) -> int const
{
  return miesiac_;
}

auto Data::Dzien(void) -> int const
{
  return dzien_;
}
  
auto Data::Sensowna(int rok, int miesiac, int dzien) -> void
{
  assert(rok > 0);
  assert((1 <= miesiac && miesiac <= 12));
  assert(dzien > 0);
  std::vector<int> kLiczbaDni = (przestepny_ ? DniWMiesiacuP : DniWMiesiacuN);
  assert(dzien <= kLiczbaDni[miesiac]);
}

auto Data::Przestepny(int rok) -> bool
{
  return (!(rok % 400) || (!(rok % 4) && (rok % 100)));
}

auto Data::operator<(Data d) -> bool const
{
  if (rok_ != d.Rok())
    return rok_ < d.Rok();
  else if (miesiac_ != d.Miesiac())
    return miesiac_ < d.Miesiac();
  else
    return dzien_ < d.Dzien();
}
