#include "Osoba.h"
    
Osoba::Osoba(std::string imie, std::string nazwisko,
      char plec, std::string pesel, Data data_urodzenia)
  : imie_(imie), nazwisko_(nazwisko), plec_(plec), 
    pesel_(pesel), data_urodzenia_(data_urodzenia)
{ }

std::string Osoba::Imie(void) const
{
  return imie_;
}

std::string Osoba::Nazwisko(void) const
{
  return nazwisko_;
}
    
char Osoba::Plec(void) const
{
  return plec_;
}

std::string Osoba::Pesel(void) const
{
  return pesel_;
}

Data Osoba::DataUrodzenia(void) const
{
  return data_urodzenia_;
}
