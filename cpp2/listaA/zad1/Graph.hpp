#include <algorithm>
#include <vector>

template<typename T>
class Graph
{
  public:
// ##########################################
    struct Edge
    {
      public:
        Edge(T begin, T end)
          : begin_(begin), end_(end)
        { }
        Edge(std::initializer_list<T> init)
        { 
          auto it = std::begin(init);
          begin_ = *it;
          end_ = *(++it);
        }
        T begin_;
        T end_;
    };
// ##########################################

    Graph(std::initializer_list<std::pair<T, std::initializer_list<T>>> edges_list)
    {
      for (auto pair : edges_list) {
        T beg = pair.first;
        for (auto end : pair.second) {
          AddEdge({beg, end});
        }
      }
    }
    auto AddEdge(Edge f) -> void
    {
      E_.push_back(f);
    }
    auto HasEdge(Edge f) -> bool
    {
      // no == operator - 2163 linii komunikatu o bledach :)
      //return (std::find(E_.begin(), E_.end(), f) != std::end(E_));
      for (auto e : E_) {
        if ((e.begin_ == f.begin_) && (e.end_ == f.end_)) {
          return true;
        }
      }
      return false;
    }

  private:
    std::vector<Edge> E_;
};
