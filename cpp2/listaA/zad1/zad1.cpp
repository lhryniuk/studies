#include <cassert>
#include <iostream>

#include "Graph.hpp"

auto test_Graph(void) -> void
{
  enum Country {Poland,Ukraine,Germany,USA};
  typedef Graph<Country> GC;
  GC gc{{Poland,{Ukraine,Germany}},
    {Germany,{Poland,Ukraine,USA}},
    {USA,{Poland,Ukraine,USA}}};

  assert(gc.HasEdge({Poland,Ukraine}));
  assert(!gc.HasEdge({Poland,USA}));

  GC::Edge up(Ukraine,Poland);
  assert(!gc.HasEdge(up));
  gc.AddEdge({Ukraine,Poland});
  assert(gc.HasEdge(up));
}

int main(void)
{
  test_Graph(); std::cerr << "test_Graph ok\n";
  return 0;
}
