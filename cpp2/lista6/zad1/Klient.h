#ifndef KLIENT_H_
#define KLIENT_H_

#include <string>

class Klient
{
  public:
    explicit Klient(std::string kid);
    auto Kid(void) -> std::string const;
    /* bez wirtualnego destruktora segmentation fault
     * w g++ 4.7.3 i 4.8.1 */
    virtual ~Klient(void) = default;

  private:
    std::string kid_; /* identyfikator klienta (np. numer karty) */
};

#endif /* KLIENT_H_ */
