#include "Klient.h"

Klient::Klient(std::string kid)
  : kid_(kid)
{ }

auto Klient::Kid(void) -> std::string const
{
  return kid_;
}
