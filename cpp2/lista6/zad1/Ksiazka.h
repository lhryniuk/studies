#ifndef KSIAZKA_H_
#define KSIAZKA_H_

#include "Zasob.h"
#include "Osoba.h"

/*
 * Identyfikacja
 * Id -  umiejscowienie wewnatrz systemu ??
 * biz - identyfikator ksiazki (jak UKD)
 * (ISBN - taki sam dla identycznych ksiazek)
 * (UKD - Uniwersalna Klasyfikacja Dziesietna - takie same numery
 *        dla ksiazek o tym samym tytule)
 * (sygnatura - nr zalezny od umiejscowienia ksiazki w danej placowce)
 * (kod kreskowy - różny dla kazdych dwoch obiektow)
 */
class Ksiazka : public Zasob
{
  public:
    Ksiazka(std::string biz, 
            std::string tytul, 
            std::vector<Osoba> autorzy);

  private:
    std::vector<Osoba> autorzy_;
};

#endif /* KSIAZKA_H_ */
