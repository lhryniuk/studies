#ifndef OSOBAFIZYCZNA_H_
#define OSOBAFIZYCZNA_H_

#include "Osoba.h"
#include "Klient.h"

class OsobaFizyczna : public Osoba, public Klient
{
  public:
    OsobaFizyczna(std::string kid, std::string imie, std::string nazwisko);
};

#endif /* OSOBAFIZYCZNA_H_ */
