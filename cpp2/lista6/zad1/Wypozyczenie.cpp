#include "Wypozyczenie.h"

Wypozyczenie::Wypozyczenie(Data koniec, std::string biz, std::string kid)
  : koniec_(koniec), biz_(biz), kid_(kid)
{}

auto Wypozyczenie::TerminZwrotu(void) -> Data
{
  return koniec_;
}

auto Wypozyczenie::IdZasobu(void) -> std::string const
{
  return biz_;
}
auto Wypozyczenie::IdKlienta(void) -> std::string const
{
  return kid_;
}
