#include <algorithm>
#include <sstream>
#include "Biblioteka.h"
#include "Firma.h"
#include "OsobaFizyczna.h"
#include "Ksiazka.h"
#include "Dvd.h"

auto Biblioteka::DodajKlienta(std::string kid, 
                              std::string nazwa) -> void
{
  klienci_.push_back(std::unique_ptr<Klient>(new Firma(kid, nazwa)));   
}

auto Biblioteka::DodajKlienta(std::string kid, 
                              std::string imie, 
                              std::string nazwisko) -> void
{
  klienci_.push_back(std::unique_ptr<Klient>(new OsobaFizyczna(kid, imie, nazwisko)));   
}

auto Biblioteka::DodajKsiazke(std::string biz, 
                              std::string tytul, 
                              std::vector<Osoba> autorzy) -> void
{
  zasoby_.push_back(std::unique_ptr<Zasob>(new Ksiazka(biz, tytul, autorzy)));
}

auto Biblioteka::DodajDvd(std::string biz,
                          std::string tytul, 
                          Osoba rezyser, 
                          std::vector<Osoba> obsada) -> void
{
  zasoby_.push_back(std::unique_ptr<Zasob>(new Dvd(biz, tytul, rezyser, obsada)));
}

auto Biblioteka::WypozyczenieZasobu(std::string biz, std::string kid) -> void
{
  wypozyczenia_.push_back(std::unique_ptr<Wypozyczenie>(new Wypozyczenie(Data(2013, 12, 29), biz, kid)));
}

auto Biblioteka::Zwrot(std::string biz) -> void
{
  for (int i = 0; i < int(wypozyczenia_.size()); ++i) {
    if (wypozyczenia_[i] && wypozyczenia_[i]->IdZasobu() == biz) {
      wypozyczenia_[i].reset();
      break;
    }
  }
}

auto int2str(int a) -> std::string
{
  std::stringstream ss;
  ss << a;
  return ss.str();
}

auto Biblioteka::ListaWypozyczen(void) -> void
{
  std::string lista;
  auto zapisz = [&](const std::unique_ptr<Wypozyczenie>& w)
  {
    if (w) {
      lista += int2str(w->TerminZwrotu().Dzien()) + '/';
      lista += int2str(w->TerminZwrotu().Miesiac()) + '/';
      lista += int2str(w->TerminZwrotu().Rok()) + ' ';
      lista += w->IdZasobu() + ' ';
      lista += w->IdKlienta() + '\n';
    }
  };
  for_each(std::begin(wypozyczenia_), std::end(wypozyczenia_), zapisz);
  std::cout << lista;
}

auto Biblioteka::ListaWypozyczenKlienta(std::string kid) -> void const
{
  std::string lista;
  auto zapisz = [&](const std::unique_ptr<Wypozyczenie>& w)
  {
    if (w && w->IdKlienta() == kid) {
      lista += int2str(w->TerminZwrotu().Dzien()) + '/';
      lista += int2str(w->TerminZwrotu().Miesiac()) + '/';
      lista += int2str(w->TerminZwrotu().Rok()) + ' ';
      lista += w->IdZasobu() + '\n';
    }
  };
  for_each(std::begin(wypozyczenia_), std::end(wypozyczenia_), zapisz);
  std::cout << lista;
}
