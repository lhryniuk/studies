#ifndef FIRMA_H_
#define FIRMA_H_

#include <string>
#include "Klient.h"

class Firma : public Klient
{
  public:
    Firma(std::string kid, std::string nazwa);
    auto Nazwa(void) -> std::string const;

  private:
    std::string nazwa_;
};

#endif /* FIRMA_H_ */
