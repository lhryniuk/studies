#ifndef WYPOZYCZENIE_H_
#define WYPOZYCZENIE_H_

#include <vector>
#include "Data.h"

/* rodzaj zgloszenia, przy rozbudowie mozna dodac
 * klase nadrzedna Zgloszenie i inne jej rodzaje,
 * np. Przetrzymanie lub Zamownienie (zamowien moze byc
 * wiele, wypozyczenie jedno) */
class Wypozyczenie
{
  public:
    Wypozyczenie(Data koniec, std::string biz, std::string kid);
    auto TerminZwrotu(void) -> Data;
    auto IdZasobu(void) -> std::string const;
    auto IdKlienta(void) -> std::string const;

  private:
    Data koniec_;
    std::string biz_;
    std::string kid_;
};

#endif /* WYPOZYCZENIE_H_ */
