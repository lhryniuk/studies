#ifndef BIBLIOTEKA_H_
#define BIBLIOTEKA_H_

#include <memory>
#include <string>
#include "Osoba.h"
#include "Klient.h"
#include "Zasob.h"
#include "Wypozyczenie.h"

class Biblioteka
{
  public:
    auto DodajKlienta(std::string kid, 
                      std::string nazwa) -> void;
    auto DodajKlienta(std::string kid, 
                      std::string imie, 
                      std::string nazwisko) -> void;
    auto DodajKsiazke(std::string biz, 
                      std::string tytul, 
                      std::vector<Osoba> autorzy) -> void;
    auto DodajDvd(std::string biz,
                  std::string tytul, 
                  Osoba rezyser=Osoba(), 
                  std::vector<Osoba> obsada=std::vector<Osoba>()) -> void;
    auto WypozyczenieZasobu(std::string biz, std::string kid) -> void;
    auto Zwrot(std::string biz) -> void;
    auto ListaWypozyczen(void) -> void;
    auto ListaWypozyczenKlienta(std::string kid) -> void const;

  private:
    std::vector<std::unique_ptr<Zasob> > zasoby_;
    std::vector<std::unique_ptr<Klient> > klienci_;
    std::vector<std::unique_ptr<Wypozyczenie> > wypozyczenia_;
};

#endif /* BIBLIOTEKA_H_ */
