#include "Dvd.h"

Dvd::Dvd(std::string biz, 
    std::string tytul, 
    Osoba rezyser,
    std::vector<Osoba> obsada)
  : Zasob(biz, tytul), rezyser_(rezyser), obsada_(obsada)
{ }

auto Dvd::Rezyser(void) -> Osoba const
{
  return rezyser_;
}

auto Dvd::Obsada(void) -> std::vector<Osoba> const
{
  return obsada_;
}
