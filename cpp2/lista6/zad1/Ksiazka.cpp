#include "Ksiazka.h"

Ksiazka::Ksiazka(std::string biz, 
                 std::string tytul, 
                 std::vector<Osoba> autorzy)
  : Zasob(biz, tytul), autorzy_(autorzy)
{ }
