#ifndef DVD_H_
#define DVD_H_

#include "Zasob.h"
#include "Osoba.h"

class Dvd : public Zasob
{
  public:
    Dvd(std::string biz, 
        std::string tytul, 
        Osoba rezyser=Osoba(), 
        std::vector<Osoba> obsada=std::vector<Osoba>());
    auto Rezyser(void) -> Osoba const;
    auto Obsada(void) -> std::vector<Osoba> const;

  private:
    Osoba rezyser_;
    std::vector<Osoba> obsada_;
};

#endif /* DVD_H_ */
