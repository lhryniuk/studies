#ifndef PRODUKT_H_
#define PRODUKT_H_

#include <string>
#include <vector>

class Zasob
{
  public:
    Zasob(std::string biz, std::string tytul);
    virtual ~Zasob(void) = default;
    auto Biz(void) -> std::string const;

  private:
    std::string biz_; /* biblioteczny identyfikator zasobu */
    std::string tytul_;
};

#endif /* PRODUKT_H_ */
