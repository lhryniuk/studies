#ifndef OSOBA_H_
#define OSOBA_H_

#include <string>

class Osoba
{
  public:
    Osoba(std::string imie="", std::string nazwisko="");
    virtual ~Osoba(void) = default;
    std::string Imie(void) const;
    std::string Nazwisko(void) const;

  private:
    std::string imie_;
    std::string nazwisko_;
};

#endif /* OSOBA_H_ */
