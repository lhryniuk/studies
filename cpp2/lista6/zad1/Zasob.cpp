#include "Zasob.h"

Zasob::Zasob(std::string biz, std::string tytul)
  : biz_(biz), tytul_(tytul)
{ }

auto Zasob::Biz(void) -> std::string const
{
  return biz_;
}
