#include "Biblioteka.h"

auto test_Biblioteka(void) -> void
{
  Biblioteka bib1;

  Osoba rez1(std::string("Jan"), std::string("Nowak"));
  Osoba rez2(std::string("Pan"), std::string("Kowalski"));
  Osoba aut1(std::string("Tan"), std::string("Ploc"));
  Osoba aut2(std::string("San"), std::string("Tetra"));
  Osoba akt1(std::string("Wan"), std::string("Sierpinski"));
  Osoba akt2(std::string("Man"), std::string("Sowinski"));

  bib1.DodajKlienta(std::string("414"), std::string("Dan"), std::string("Way"));
  bib1.DodajKlienta(std::string("515"), std::string("Can"), std::string("May"));
  bib1.DodajKlienta(std::string("313"), std::string("eBay"));

  bib1.DodajKsiazke(std::string("444"), std::string("Brimbrum"), {aut1});
  bib1.DodajKsiazke(std::string("111"), std::string("Brambrim"), {aut1});
  bib1.DodajKsiazke(std::string("222"), std::string("Bimbom"), {aut2});
  bib1.DodajKsiazke(std::string("555"), std::string("Bymtrym"), {aut2});
  bib1.DodajKsiazke(std::string("666"), std::string("Bambam"), {aut2});
  bib1.DodajDvd(std::string("123"), std::string("JakWak"), rez1, {akt1, akt2});
  bib1.DodajDvd(std::string("456"), std::string("PakMak"), rez2, {akt1});

  bib1.WypozyczenieZasobu(std::string("444"), std::string("414"));
  bib1.WypozyczenieZasobu(std::string("111"), std::string("414"));
  bib1.WypozyczenieZasobu(std::string("222"), std::string("515"));
  bib1.WypozyczenieZasobu(std::string("666"), std::string("414"));
  bib1.WypozyczenieZasobu(std::string("123"), std::string("313"));
  bib1.WypozyczenieZasobu(std::string("456"), std::string("313"));

  std::cerr << "Lista wypozyczen:\n";
  bib1.ListaWypozyczen();
  std::cerr << "Lista wypozyczen klienta Dan Way: \n";
  bib1.ListaWypozyczenKlienta("414");
  std::cerr << "Lista wypozyczen klienta eBay: \n";
  bib1.ListaWypozyczenKlienta("313");

  bib1.Zwrot("444");
  bib1.Zwrot("456");

  std::cerr << "Lista wypozyczen:\n";
  bib1.ListaWypozyczen();
  std::cerr << "Lista wypozyczen klienta eBay: \n";
  bib1.ListaWypozyczenKlienta("313");
  /* segmentation fault (free()) bez wirtualnych destruktorow! */
}

int main(void)
{
  test_Biblioteka(); std::cerr << "test_Biblioteka ok!\n";
  return 0;
}
