#include "Osoba.h"
    
Osoba::Osoba(std::string imie, std::string nazwisko)
  : imie_(imie), nazwisko_(nazwisko)
{ }

std::string Osoba::Imie(void) const
{
  return imie_;
}

std::string Osoba::Nazwisko(void) const
{
  return nazwisko_;
}
