#ifndef ID_H_
#define ID_H_

/* mogloby byc wykorzystywane do indeksowania wewnatrz systemu
 * przy np. wektorze do przechowywania danych */
struct Id
{
  Id(int a, int b)
    : a_(a), b_(b)
  int a_;
  int b_;
};

#endif /* ID_H_ */
