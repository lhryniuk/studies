#ifndef DATA_H_
#define DATA_H_

#include <cassert>
#include <iostream>
#include <vector>

class Data
{
 public:
  Data(void);
  Data(int rok, int miesiac, int dzien);
  auto Przestepny(int rok) -> bool;
  auto Rok(void) -> int const;
  auto Miesiac(void) -> int const;
  auto Dzien(void) -> int const;
  auto operator<(Data a) -> bool const;

 private:
  /* do liczenia liczby dni przy konwersji na ulamek
   * oraz sprawdzania sensownosci daty (by uniknac ifow) */
  auto Sensowna(int rok, int miesiac, int dzien) -> void;
  int rok_;
  int miesiac_;
  int dzien_;
  bool przestepny_;  
};

#endif
