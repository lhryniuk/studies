#include "Firma.h"

Firma::Firma(std::string kid, std::string nazwa)
  : Klient(kid), nazwa_(nazwa)
{ }

auto Firma::Nazwa(void) -> std::string const
{
  return nazwa_;
}
