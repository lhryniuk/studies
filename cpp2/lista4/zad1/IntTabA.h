#ifndef INTTAB_A_H_
#define INTTAB_A_H_

#include <algorithm>
#include "IntTab.h"

class IntTabA : public IntTab
{
  public:
    explicit IntTabA(int size=0);
    IntTabA(const IntTabA& a);
    IntTabA(IntTabA&& a) noexcept;
    IntTabA operator=(IntTabA&& a) noexcept;

  private:
    int size_;
    int* tab_ptr_;
};

#endif /* INITAB_A_H_ */
