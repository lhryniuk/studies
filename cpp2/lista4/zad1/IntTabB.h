#ifndef INTTAB_B_H_
#define INTTAB_B_H_

#include <algorithm>

class IntTabB
{
  public:
    explicit IntTabB(int size=0);
    IntTabB(const IntTabB& a);
    IntTabB(IntTabB&& a);
    ~IntTabB(void);
    IntTabB& operator=(const IntTabB& a);
    IntTabB& operator=(IntTabB&& a);
    void swap(IntTabB& a) noexcept;
    int size(void) const;
    int& operator[](int a) const;
    int& operator[](int a);

  private:
    int size_;
    int* tab_ptr_;
};

void swap(IntTabB& a, IntTabB& b) noexcept;

#endif /* INITAB_B_H_ */
