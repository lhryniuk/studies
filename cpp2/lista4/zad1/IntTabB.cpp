#include <algorithm>
#include <cassert>
#include "IntTabB.h"

IntTabB::IntTabB(int size)
  : size_(size), tab_ptr_(nullptr)
{
  /* moze cos lepszego? */
  if (size > 0)
    tab_ptr_ = new int[size];
}

IntTabB::IntTabB(const IntTabB& a)
  : size_(a.size_)
{
  tab_ptr_ = new int[a.size_];
  std::copy(a.tab_ptr_, a.tab_ptr_ + a.size_, tab_ptr_);
}

IntTabB::IntTabB(IntTabB&& a)
  : size_(0), tab_ptr_(nullptr)
{
  swap(a);
}

IntTabB& IntTabB::operator=(const IntTabB& a)
{
  tab_ptr_ = new int[a.size_];
  size_ = a.size_;
  std::copy(a.tab_ptr_, a.tab_ptr_ + a.size_, tab_ptr_);
  return *this;
}
    
IntTabB& IntTabB::operator=(IntTabB&& a)
{
  swap(a);
  return *this;
}

IntTabB::~IntTabB(void)
{
  delete[] tab_ptr_;
}

void IntTabB::swap(IntTabB& a) noexcept
{
  using std::swap;
  swap(tab_ptr_, a.tab_ptr_);
  swap(size_, a.size_);
}

int IntTabB::size(void) const
{
  return size_;
}

int& IntTabB::operator[](int a) const
{
  assert(0 <= a && a < size_);
  return tab_ptr_[a];
}

int& IntTabB::operator[](int a)
{
  assert(0 <= a && a < size_);
  return tab_ptr_[a];
}

void swap(IntTabB& a, IntTabB& b) noexcept
{
  a.swap(b);
}
