#include <cassert>
#include <chrono>
#include <iostream>
#include "IntTab.h"
#include "IntTabA.h"
#include "IntTabB.h"

const int g_Iter = 1;

template<typename Tab>
void test_IntTab(void)
{
  std::vector<Tab> v1;
  for (int i = 1; i <= g_Iter; ++i) {
    v1.push_back(Tab(i));
  }
  std::vector<Tab> v2;
  v2 = std::move(v1);
}

template<typename Tab>
double IntTab_test_time(void)
{
  std::chrono::time_point<std::chrono::system_clock> start;
  std::chrono::time_point<std::chrono::system_clock> end;
  start = std::chrono::system_clock::now();
  test_IntTab<Tab>();
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed = end - start;
  return elapsed.count();
}

int main(void)
{
  /* ### wyniki ###
   * dla g_Iter =  5000
   * 0.072451s
   * 0.045925s
   * 0.012972s
   * 
   * dla g_Iter = 10000
   * 0.256626s
   * 0.153875s
   * 0.026909s
   *
   * dla g_Iter = 15000
   * 0.431963s
   * 0.160502s
   * 0.040545s
   *
   * dla g_Iter = 20000
   * 0.99258s
   * 0.57661s
   * 0.05846s
   *
   * dla g_Iter = 25000
   * 1.28681s
   * 0.58899s
   * 0.07437s
   *
   * z optymalizacja -O2
   * dla g_Iter =  5000
   * 0.062962s
   * 0.038379s
   * 0.010643s
   * 
   * dla g_Iter = 10000
   * 0.235632s
   * 0.136614s
   * 0.022754s
   *
   * dla g_Iter = 15000
   * 0.416813s
   * 0.153532s
   * 0.037118s
   *
   * dla g_Iter = 20000
   * 0.96486s
   * 0.54305s
   * 0.05239s
   *
   * dla g_Iter = 25000
   * 1.24910s
   * 0.57296s
   * 0.06691s
   *
   * */
  std::cout << "Test dla IntTab bez &&: " << IntTab_test_time<IntTab>() << "s\n";
  std::cout << "Test dla IntTab z && i except: " << IntTab_test_time<IntTabB>() << "s\n";
  std::cout << "Test dla IntTab z && i noexcept: " << IntTab_test_time<IntTabA>() << "s\n";
  return 0;
}
