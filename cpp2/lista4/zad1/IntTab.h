#ifndef INTTAB_H_
#define INTTAB_H_

#include <algorithm>

class IntTab
{
  public:
    explicit IntTab(int size=0);
    IntTab(const IntTab& a);
    virtual ~IntTab(void);
    virtual IntTab operator=(const IntTab& a);
    virtual void swap(IntTab& a);
    virtual int size(void) const;
    virtual int& operator[](int a) const;
    virtual int& operator[](int a);

  private:
    int size_;
    int* tab_ptr_;
};

void swap(IntTab& a, IntTab& b);

#endif /* INITAB_H_ */
