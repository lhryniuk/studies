#include <algorithm>
#include <cassert>
#include <iostream>
#include "IntTabA.h"

IntTabA::IntTabA(int size)
  : IntTab(size)
{ }

IntTabA::IntTabA(const IntTabA& a)
  : IntTab(a)
{ }

IntTabA::IntTabA(IntTabA&& a) noexcept
  : size_(0), tab_ptr_(nullptr)
{
  swap(a);
}

IntTabA IntTabA::operator=(IntTabA&& a) noexcept
{
  swap(a);
  return *this;
}
