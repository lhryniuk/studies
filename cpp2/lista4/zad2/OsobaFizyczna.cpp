#include "OsobaFizyczna.h"

OsobaFizyczna::OsobaFizyczna(std::string imie, 
                             std::string nazwisko, 
                             std::string pesel,
                             int rok,
                             int miesiac,
                             int dzien)
  : imie_(imie), nazwisko_(nazwisko), pesel_(pesel), 
    data_urodzenia_(Data(rok, miesiac, dzien))
{ }

std::string OsobaFizyczna::Dane(void)
{
  return "<osoba " + imie_ + " " + nazwisko_ + " "  + pesel_ + " "
       + int2str(data_urodzenia_.Rok()) + " "
       + int2str(data_urodzenia_.Miesiac()) + " "
       + int2str(data_urodzenia_.Dzien()) + ">";
}
