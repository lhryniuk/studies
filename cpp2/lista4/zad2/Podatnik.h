#ifndef PODATNIK_H_
#define PODATNIK_H_

#include <string>
#include "conversion.h"

class Podatnik
{
  public:
    virtual std::string Dane(void) = 0;
    /* to czy to:
     * virtual Podatnik(void) = default;
     * virtual Podatnik(void) = 0;
     * ...
     * Podatnik::~Podatnik(void)
     * { } ? */
    virtual ~Podatnik(void) = default;
};

#endif /* PODATNIK_H_ */
