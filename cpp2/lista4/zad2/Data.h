#ifndef DATA_H_
#define DATA_H_

#include <cassert>
#include <iostream>
#include <vector>

class Data
{
 public:
  Data(void);
  Data(int rok, int miesiac, int dzien);
  bool Przestepny(int rok);
  int Rok(void) const;
  int Miesiac(void) const;
  int Dzien(void) const;
  std::string Str(void) const;
  bool operator<(Data a) const;

 private:
  /* do liczenia liczby dni przy konwersji na ulamek
   * oraz sprawdzania sensownosci daty (by uniknac ifow) */
  void Sensowna(int rok, int miesiac, int dzien);
  int rok_;
  int miesiac_;
  int dzien_;
  bool przestepny_;  
};

#endif
