#ifndef HEAP_OPTIONAL_H_
#define HEAP_OPTIONAL_H_

template<typename T>
class HeapOptional
{
  public:
    HeapOptional(T* ptr=nullptr)
      : ptr_(ptr)
    { }
    ~HeapOptional(void)
    {
      delete ptr_;
    }
    HeapOptional(HeapOptional&& a) noexcept
      : ptr_(nullptr)
    {
      swap(a);
    }
    HeapOptional& operator=(HeapOptional&& a) noexcept
    {
      swap(a);
      return *this;
    }
    T& operator*(void)
    {
      return *ptr_;
    }
    T* operator->(void)
    {
      return &(operator*());
    }
    operator bool(void) const
    {
      return (ptr_ != nullptr);
    }
    void swap(HeapOptional& a) noexcept
    {
      using std::swap;
      swap(ptr_, a.ptr_);
    }

  private:
    T* ptr_;
};

#endif /* HEAP_OPTIONAL_H_ */
