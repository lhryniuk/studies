#include "Fundacja.h"
#include "Data.h"

Fundacja::Fundacja(std::string nazwa, 
                   std::string krs,
                   bool pub)
  : nazwa_(nazwa), krs_(krs), pub_(pub)
{ }

std::string Fundacja::Dane(void)
{
  return "<fundacja " + nazwa_ + " " + krs_ + " " + int2str(pub_) + ">";
}
