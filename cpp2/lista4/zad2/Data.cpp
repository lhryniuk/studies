#include <sstream>
#include "Data.h"
#include "conversion.h"

/* inicjalizacja statycznych stalych */
const std::vector<int> 
  DniWMiesiacuN{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
const std::vector<int> 
  DniWMiesiacuP{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

Data::Data(void)
{
  rok_ = miesiac_ = dzien_ = 0;
}

Data::Data(int rok, int miesiac, int dzien)
{
  Sensowna(rok, miesiac, dzien);
  przestepny_ = Przestepny(rok);
  rok_ = rok;
  miesiac_ = miesiac;
  dzien_ = dzien;
}

int Data::Rok(void) const
{
  return rok_;
}

int Data::Miesiac(void) const
{
  return miesiac_;
}

int Data::Dzien(void) const
{
  return dzien_;
}

std::string Data::Str(void) const
{
  return int2str(dzien_) + '/' + int2str(miesiac_) + '/' + int2str(rok_); 
}
  
void Data::Sensowna(int rok, int miesiac, int dzien)
{
  assert(rok > 0);
  assert((1 <= miesiac && miesiac <= 12));
  assert(dzien > 0);
  std::vector<int> kLiczbaDni = (przestepny_ ? DniWMiesiacuP : DniWMiesiacuN);
  assert(dzien <= kLiczbaDni[miesiac]);
}

bool Data::Przestepny(int rok)
{
  return (!(rok % 400) || (!(rok % 4) && (rok % 100)));
}

bool Data::operator<(Data d) const
{
  if (rok_ != d.Rok())
    return rok_ < d.Rok();
  else if (miesiac_ != d.Miesiac())
    return miesiac_ < d.Miesiac();
  else
    return dzien_ < d.Dzien();
}
