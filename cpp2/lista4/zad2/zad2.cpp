#include <iostream>
#include <cassert>
#include <sstream>
#include <vector>
#include "OsobaFizyczna.h"
#include "Firma.h"
#include "Fundacja.h"
#include "HeapOptional.h"

HeapOptional<Podatnik> wczytaj_podatnika(std::istream& in)
{
  std::string dana;
  in >> dana;
  if (in) {
    if (dana ==  "<osoba") {
      std::string imie;
      std::string nazwisko;
      std::string pesel;
      int rok;
      int miesiac;
      int dzien;
      in >> imie >> nazwisko >> pesel >> rok >> miesiac >> dzien;
      in.get();
      if (in) {
        HeapOptional<Podatnik> a = new OsobaFizyczna(imie, nazwisko, pesel,
                                                      rok, miesiac, dzien);
        return a;
      }
    } else if (dana =="<firma") {
      std::string nazwa;
      std::string nip;
      in >> nazwa;
      in >> std::ws;
      while (in.peek() != '>') {
        nip += in.get();
      }
      in.get();
      if (in) {
        HeapOptional<Podatnik> a = new Firma(nazwa, nip);
        return a;
      }
    } else if (dana == "<fundacja") {
      std::string nazwa;
      std::string krs;
      bool pub;
      in >> nazwa >> krs >> pub;
      in.get();
      if (in) {
        HeapOptional<Podatnik> a = new Fundacja(nazwa, krs, pub);
        return a;
      }
    }
  }
  in.setstate(std::ios::failbit);
  return HeapOptional<Podatnik>();
}

std::vector<HeapOptional<Podatnik> > wczytaj_podatnikow(std::istream& in)
{
  std::vector<HeapOptional<Podatnik> > v;
  while (in) {
    HeapOptional<Podatnik> hp = wczytaj_podatnika(in);
    if (hp)
      /* use of deleted function ... konstruktor kopiujacy 
       * lub internal compiler error bez move */
      v.push_back(std::move(hp));
  }
  return v;
}

void test(void)
{
  std::istringstream is(
                        "<osoba Kazimierz Nowak 99444402313 99 04 04>\n"
                        "<firma Impc 104-50-03-271>\n"
                        "<fundacja Oimpc 4010583286 0>\n"
                        "<osoba Marian Nowak 75080402313 75 08 04>\n"
                        "<osoba Lech Sowinski 81022302313 81 02 23>\n"
                        "<firma Limpc 114-50-03-271>\n"
                        "<firma Wimpc 124-40-12-931>\n"
                        "<fundacja Wimpc 4010682286 1>\n"
                        "<osoba Jan Kowalski 92051702313 92 05 17>\n"
                        "<firma Dimpc 143-55-34-561>\n"
                        "<fundacja Dimpc 4280144286 0>\n"
                        "<firma Pimpc 941-30-73-153>\n"
                        );
  std::vector<HeapOptional<Podatnik> > v = wczytaj_podatnikow(is);
  /* for (auto i : v) nie dziala!
   * use of deleted function ... konstruktor kopiujacy */
  for (auto i : v) {
    std::cout << (*i)->Dane() << "\n";
    //for (int i = 0; i < int(v.size()); ++i) {
    //std::cerr << "wypisuje " << i << "-tego podatnika\n";
    //std::cout << v[i]->Dane() << "\n";
  }
}

int main(void)
{
  test();
  return 0;
}
