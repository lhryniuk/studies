#ifndef OSOBA_FIZYCZNA_H_
#define OSOBA_FIZYCZNA_H_

#include <string>
#include "Data.h"
#include "Podatnik.h"

class OsobaFizyczna : public Podatnik
{
  public:
    OsobaFizyczna(std::string imie, std::string nazwisko, std::string pesel,
                  int rok, int miesiac, int dzien);
    std::string Dane(void) override final;
  private:
    std::string imie_;
    std::string nazwisko_;
    std::string pesel_;
    Data data_urodzenia_;
};

#endif /* OSOBA_FIZYCZNA_H_ */
