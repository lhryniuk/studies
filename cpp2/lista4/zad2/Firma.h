#ifndef FIRMA_H_
#define FIRMA_H_

#include <string>
#include "Podatnik.h"

class Firma : public Podatnik
{
  public:
    Firma(std::string nazwa, std::string nip);
    std::string Dane(void) override final;
    
  private:
    std::string nazwa_;
    std::string nip_;
};

#endif /* FIRMA_H_ */
