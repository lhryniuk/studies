#ifndef FUNDACJA_H_
#define FUNDACJA_H_

#include <string>
#include "Podatnik.h"

class Fundacja : public Podatnik
{
  public:
    Fundacja(std::string nazwa, std::string krs, bool pub);
    std::string Dane(void) override final;
  private:
    std::string nazwa_;
    std::string krs_;
    bool pub_;
};

#endif /* FUNDACJA_H_ */
