#!/bin/bash

all_input=$1
input=$(basename ${all_input})
filename="${input%%.*}"
echo "input: ${filename}"
asm_file="${filename}.s"
echo "asm_file: ${asm_file}"

mkdir -p tmp
make clean && make
./komp < ${all_input} 2> ${filename}_komp.log 1> tmp/${asm_file} &&\
gcc -o a.out tmp/${asm_file}
