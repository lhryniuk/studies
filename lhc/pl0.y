%{
#include <stdio.h>
#include <stdlib.h>

#include "pl0_aux.h"

sem_val parse_tree;

extern int yylex(void);

void yyerror(const char * arg)
{
	printf("%s",arg);
	exit(1);
}

%}
%start program

%token SYM_SEMI SYM_COMMA SYM_LPAR SYM_RPAR
%token SYM_EQ SYM_NEQ SYM_LT SYM_GT SYM_LE SYM_GE SYM_ASSIGN
%token SYM_PLUS SYM_MINUS SYM_STAR SYM_SLASH
%token SYM_IDENT SYM_NUMBER SYM_RETURN
%token SYM_BEGIN SYM_CONST SYM_END SYM_ELIF SYM_ELSE
%token SYM_TRUE SYM_FALSE SYM_CALL SYM_PRINT SYM_PRINTLN
%token SYM_IF SYM_PROC SYM_VAR SYM_WHILE

%%
program: block
          { parse_tree = $1; }
	;

block: constdecl blockv
          { $$ = make2 (block, $1, $2); }
	| blockv
          { $$ = make2 (block,  0, $1); }
	;

blockv: blockp
          { $$ = make2 (blockv, 0, $1); }
	;

blockp: procdecl
          { $$ = make2 (blockp, $1, 0); }
	| instr
          { $$ = make2 (blockp, 0, $1); }
	;

constdecl: SYM_CONST constdecl_items SYM_SEMI
          { $$ = $2; }
	;

constdecl_items: constdecl_item SYM_COMMA constdecl_items
          { $$ = make2 (constdecl_items, $1, $3); }
	| constdecl_item
	;

constdecl_item: SYM_IDENT SYM_ASSIGN SYM_NUMBER
          { $$ = make2 (constdecl_item, $1, $3); }
	;

vardef_items: vardef_item SYM_COMMA vardef_items
          { $$ = make2 (vardef_items, $1, $3); }
	| vardef_item
	;

vardef_item: SYM_IDENT SYM_ASSIGN expression
          { $$ = make3 (vardef_item, $2, $1, $3); }
    | SYM_IDENT SYM_ASSIGN function_call
          { $$ = make3 (vardef_item, $2, $1, $3); }
	;

ident_list: SYM_IDENT
          { $$ = make2 (ident_list, 0, $1); }
    | SYM_IDENT ident_list
          { $$ = make2 (ident_list, $1, $2); }

procdecl: procdecl_item procdecl
          { $$ = make2 (procdecl, $1, $2); }
	| procdecl_item
	;

procdecl_item: SYM_PROC SYM_IDENT ident_list SYM_BEGIN instrs SYM_END
          { $$ = make3 (procdecl_item, $2, $3, $5); }
    | SYM_PROC SYM_IDENT SYM_BEGIN instrs SYM_END
          { $$ = make2 (procdecl_item, $2, $4); }
	;

return: SYM_RETURN expression SYM_SEMI
          { $$ = make2 (return, 0, $2); }
      ;

function_args: factor function_args
          { $$ = make2 (function_args, $1, $2); }
    | SYM_LPAR function_call_in SYM_RPAR function_args
          { $$ = make2 (function_args, $2, $4); }
    | factor
          { $$ = make2 (function_args, 0, $1); }
    | SYM_LPAR function_call_in SYM_RPAR
          { $$ = make2 (function_args, 0, $2); }
    ;

function_call_in: SYM_CALL SYM_IDENT function_args
          { $$ = make2 (function_call_in, $1, $2); }
    ;

function_call: SYM_CALL SYM_IDENT function_args
          { $$ = make2 (function_call, $1, $2); }
    ;

if_instr: SYM_IF SYM_LPAR condition SYM_RPAR bracked_instr
    ;

elif_instr: SYM_ELIF SYM_LPAR condition SYM_RPAR bracked_instr
    | SYM_ELIF SYM_LPAR condition SYM_RPAR bracked_instr elif_instr
    ;

else_instr: SYM_ELSE bracked_instr
    ;

full_if_instr: if_instr
    | if_instr elif_instr
    | if_instr else_instr
    | if_instr elif_instr else_instr
    ;

bracked_instr: SYM_BEGIN instrs SYM_END

instr: SYM_IDENT SYM_ASSIGN expression SYM_SEMI
          { $$ = make3 (instr, $2, $1, $3); }
	| SYM_BEGIN instrs SYM_END
          { $$ = make2 (instr, $1, $2); }
    | full_if_instr
	| SYM_WHILE SYM_LPAR condition SYM_RPAR instr
          { $$ = make3 (instr, $1, $3, $5); }
    | return
    | function_call SYM_SEMI
    | SYM_VAR vardef_items SYM_SEMI
          { $$ = make2 (instr, $1, $2); }
    | SYM_PRINT value SYM_SEMI
          { $$ = make2 (instr, $1, $2); }
    | SYM_PRINTLN value SYM_SEMI
          { $$ = make2 (instr, $1, $2); }
	;

instrs: instr
	| instr instrs
          { $$ = make2 (instrs, $1, $2); }
	;

condition: expression cond_op expression
          { $$ = make3 (condition, $2, $1, $3); }
	;

cond_op: SYM_EQ
    | SYM_NEQ
    | SYM_LT
    | SYM_GT
    | SYM_LE
    | SYM_GE
	;

expression: sign aexpression
          { $$ = make2 (expression, $1, $2); }
	| aexpression
	;

aexpression: term
          { $$ = $1; }
	| aexpression sign term
          { $$ = make3 (term, $2, $1, $3); }
	;

sign: SYM_PLUS
    | SYM_MINUS
	;

term: factor
	| term mul_op factor
          { $$ = make3 (term, $2, $1, $3); }
	;

mul_op: SYM_STAR
      | SYM_SLASH
	;

value: SYM_IDENT
    | SYM_NUMBER
    | SYM_TRUE
    | SYM_FALSE

factor: SYM_IDENT
	| SYM_NUMBER
    | SYM_TRUE
    | SYM_FALSE
	| SYM_LPAR expression SYM_RPAR
          { $$ = $2; }
	;
%%

int main(void)
{
	yydebug = 1;
	yyparse();
    //print_nodes(0, parse_tree);
    generuj_program(tablica_symboli, parse_tree);
	return 0;
}
