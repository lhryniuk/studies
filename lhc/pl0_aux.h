#include "symtab.h"

struct parse_node {
  int rodzaj;
  struct parse_node * wartosc1;
  struct parse_node * nastepny;
  int wartosc_int;
  struct ident * id;
  const char * nazwa;
};

typedef struct parse_node * sem_val;

extern void generuj_program(sym_tab st, sem_val y);

#define YYSTYPE sem_val

extern char * strdup(const char * str);

extern void print_nodes(int indent, sem_val y);

extern sem_val new_node(int rodzaj);

extern sem_val make_1_node (int rodzaj, const char * nazwa,
                              sem_val y1);
extern sem_val make_2_node (int rodzaj, const char * nazwa,
                              sem_val y1, sem_val y2);
extern sem_val make_3_node (int rodzaj, const char * nazwa,
                              sem_val y1, sem_val y2, sem_val y3);
extern sem_val make_4_node (int rodzaj, const char * nazwa,
                              sem_val y1, sem_val y2, sem_val y3, sem_val y4);
extern sem_val make_5_node (int rodzaj, const char * nazwa, sem_val y1,
                              sem_val y2, sem_val y3, sem_val y4, sem_val y5);


#define make1(x, y1)                 make_1_node (sym_ ## x, # x, y1)
#define make2(x, y1, y2)             make_2_node (sym_ ## x, # x, y1, y2)
#define make3(x, y1, y2, y3)         make_3_node (sym_ ## x, # x, y1, y2, y3)
#define make4(x, y1, y2, y3, y4)     make_4_node (sym_ ## x, # x, y1, y2, y3, y4)
#define make5(x, y1, y2, y3, y4, y5) make_5_node (sym_ ## x, # x, y1, y2, y3, y4, y5)

#define list_node 900
#define sym_assign 901
#define sym_call 902
#define sym_begin 903
#define sym_if 904
#define sym_while 905

/* definicje symboli, otrzymane przez:

grep ':' pl0.y | sed 's,:.*,,;s,^,sym_,' | perl -e'$i=1000; while(<>) { chop; printf("#define %s %d\n", $_, $i++);}'

*/

#define sym_program 1000
#define sym_block 1001
#define sym_blockv 1002
#define sym_blockp 1003
#define sym_constdecl 1004
#define sym_constdecl_items 1005
#define sym_constdecl_item 1006
#define sym_vardef_items 1007
#define sym_vardef_item 1008
#define sym_ident_list 1009
#define sym_procdecl 1010
#define sym_procdecl_item 1011
#define sym_return 1012
#define sym_function_args 1013
#define sym_function_call_in 1014
#define sym_function_call 1015
#define sym_if_instr 1016
#define sym_elif_instr 1017
#define sym_else_instr 1018
#define sym_full_if_instr 1019
#define sym_bracked_instr 1020
#define sym_instr 1021
#define sym_instrs 1022
#define sym_condition 1023
#define sym_cond_op 1024
#define sym_expression 1025
#define sym_aexpression 1026
#define sym_sign 1027
#define sym_term 1028
#define sym_mul_op 1029
#define sym_factor 1030
