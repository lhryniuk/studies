#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pl0_aux.h"

sem_val
new_node(int rodzaj)
{
    sem_val res = malloc(sizeof(*res));
    if (!res) {
        fprintf(stderr, "Brak pamięci\n");
        exit(1);
    }
    res->rodzaj = rodzaj;
    res->nastepny = 0;
    return res;
}

char *
strdup(const char * str)
{
    char * res = malloc(strlen(str)+1);
    if (!res) {
        fprintf(stderr, "Brak pamięci\n");
        exit(1);
    }
    strcpy(res, str);
    return res;
}

void
print_nodes(int indent, sem_val y)
{
    int i;
    for (i=0; i<indent; i++) {
        putchar(' ');
    }
    printf("%s\n", y->nazwa);
    if (y->rodzaj > list_node) {
        y = y->nastepny;
        while (y) {
            sem_val w1 = y->wartosc1;
            if (w1) {
                print_nodes(indent+4, w1);
            }
            print_nodes(indent+4, y->wartosc1);
        }
        y = y->nastepny;
    }
}


sem_val
chain (sem_val y1, sem_val y2)
{
    fprintf(stderr, "chain | ");
    if (y1 != NULL) {
        fprintf(stderr, "y1->rodzaj = %4d | ", y1->rodzaj);
    } else {
        fprintf(stderr, "y1->rodzaj = NULL | ");
    }
    if (y2 != NULL) {
        fprintf(stderr, "y2->rodzaj = %4d", y2->rodzaj);
    } else {
        fprintf(stderr, "y2->rodzaj = NULL");
    }
    fprintf(stderr, "\n");
    sem_val res = new_node(list_node);
    res->wartosc1 = y1;
    res->nastepny = y2;
    return res;
}

sem_val
make_n_node (int rodzaj, const char * nazwa, sem_val y)
{
    fprintf(stderr, "make_n_node | rodzaj = %d | nazwa = %s\n", rodzaj, nazwa);
    sem_val res = new_node(rodzaj);
    res->nastepny = y;
    res->nazwa = nazwa;
    return res;
}

sem_val
make_1_list (sem_val y1)
{
    return chain (y1, 0);
}

sem_val
make_2_list (sem_val y1, sem_val y2)
{
    return chain (y1, make_1_list (y2));
}

sem_val
make_3_list (sem_val y1, sem_val y2, sem_val y3)
{
    return chain (y1, make_2_list (y2, y3));
}

sem_val
make_4_list (sem_val y1, sem_val y2, sem_val y3, sem_val y4)
{
    return chain (y1, make_3_list (y2, y3, y4));
}

sem_val
make_5_list (sem_val y1, sem_val y2,
                       sem_val y3, sem_val y4, sem_val y5)
{
    return chain (y1, make_4_list (y2, y3, y4, y5));
}

sem_val
make_1_node (int rodzaj, const char * nazwa, sem_val y1)
{
    return make_n_node (rodzaj, nazwa, make_1_list (y1));
}

sem_val
make_2_node (int rodzaj, const char * nazwa, sem_val y1, sem_val y2)
{
    return make_n_node (rodzaj, nazwa, make_2_list (y1, y2));
}

sem_val
make_3_node (int rodzaj, const char * nazwa, sem_val y1, sem_val y2, sem_val y3)
{
    return make_n_node (rodzaj, nazwa, make_3_list (y1, y2, y3));
}

sem_val
make_4_node (int rodzaj, const char * nazwa, sem_val y1, sem_val y2,
                       sem_val y3, sem_val y4)
{
    return make_n_node (rodzaj, nazwa, make_4_list (y1, y2, y3, y4));
}

sem_val
make_5_node (int rodzaj, const char * nazwa, sem_val y1, sem_val y2,
                       sem_val y3, sem_val y4, sem_val y5)
{
    return make_n_node (rodzaj, nazwa, make_5_list (y1, y2, y3, y4, y5));
}

