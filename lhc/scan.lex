%{
#include "pl0_aux.h"
#include "pl0.tab.h"
#define RET(x) do {\
     yylval = new_node(x);\
     yylval->rodzaj = x;\
     yylval->nazwa = strdup (yytext); \
     return x;\
  } while (0)
%}
DIGIT    [0-9]
ALPHA    [a-zA-Z]
ALNUM    [a-zA-Z0-9]
WHITE    [ \t\n]
AP       [']
%option noyywrap
%x COMMENT
%%
{WHITE}*                {;}
"!"                     { RET (SYM_CALL);}
";"                     { RET (SYM_SEMI);}
","                     { RET (SYM_COMMA);}
"("                     { RET (SYM_LPAR);}
")"                     { RET (SYM_RPAR);}
"=="                    { RET (SYM_EQ);}
"<>"                    { RET (SYM_NEQ);}
"<"                     { RET (SYM_LT);}
"<="                    { RET (SYM_LE);}
">"                     { RET (SYM_GT);}
">="                    { RET (SYM_GE);}
"="                     { RET (SYM_ASSIGN);}
"+"                     { RET (SYM_PLUS);}
"-"                     { RET (SYM_MINUS);}
"*"                     { RET (SYM_STAR);}
"/"                     { RET (SYM_SLASH);}
"{"                     { RET (SYM_BEGIN);}
"const"                 { RET (SYM_CONST);}
"}"                     { RET (SYM_END);}
"if"                    { RET (SYM_IF);}
"elif"                  { RET (SYM_ELIF);}
"else"                  { RET (SYM_ELSE);}
"\\"                    { RET (SYM_PROC);}
"var"                   { RET (SYM_VAR);}
"print"                 { RET (SYM_PRINT);}
"println"               { RET (SYM_PRINTLN);}
"while"                 { RET (SYM_WHILE);}
"ret"                   { RET (SYM_RETURN);}
"true"                  { RET (SYM_TRUE); }
"false"                 { RET (SYM_FALSE); }
"#"                     { BEGIN (COMMENT);}
<COMMENT>"\n"           { BEGIN (INITIAL);}
<COMMENT>.              { }
{ALPHA}{ALNUM}*{AP}?    {
                            yylval = new_node(SYM_IDENT);
                            yylval->nazwa = strdup (yytext);
                            return SYM_IDENT;
                        }
{DIGIT}+                {
                            yylval = new_node(SYM_NUMBER);
                            yylval->wartosc_int = atol(yytext);
                            yylval->nazwa = strdup (yytext);
                            return SYM_NUMBER;
                        }
%%
