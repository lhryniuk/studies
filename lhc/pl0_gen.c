#include "pl0_aux.h"
#include "pl0.tab.h"
#include "symtab.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>


/* Obsluga deklacji */

/* obj_intst jest potrzebne w deklaracjach */
typedef struct obj_instr_item * obj_instr;

typedef enum {type_int, type_proc,} type_info;
enum {decl_const, decl_var, decl_proc,};

typedef struct decl_entry * decl_ref;

struct decl_entry {
   decl_ref nastepny;
   int rodzaj;
   int level;
   symtab_ref s_ref;
   union {
       long const_val;
       type_info type_val;
       obj_instr var_instr;
   };
};

#define MAX_DECL_LEVEL 30
int decl_level = 0;
decl_ref lista_odtwarzania = 0;
decl_ref stara_lista_odtwarzania[MAX_DECL_LEVEL];

void
pushdecls(void)
{
    if(decl_level == MAX_DECL_LEVEL) {
        fprintf(stderr, "Too much nesting of procedures (max %d)\n",
                MAX_DECL_LEVEL);
        exit(1);
    }
    stara_lista_odtwarzania[decl_level] = lista_odtwarzania;
    decl_level++;
}

void
popdecls(void)
{
    decl_level--;
    decl_ref pom = lista_odtwarzania;
    while(pom) {
        set_symtab_value(pom->s_ref, pom);
        decl_ref pp = pom;
        pom = pom->nastepny;
        pp->nastepny = 0;
    }
    lista_odtwarzania = stara_lista_odtwarzania[decl_level];
    stara_lista_odtwarzania[decl_level] = 0;
}

decl_ref
get_new_declaration(symtab_ref s_ref)
{
    void * old_val = get_symtab_value(s_ref);
    if (old_val) {
        decl_ref old_decl = (decl_ref)old_val;
        if (old_decl->level == decl_level) {
            fprintf(stderr, "Duplicate declaration %s\n",
                    get_symtab_name(s_ref));
                    exit(1);
        }
        old_decl->nastepny = lista_odtwarzania;
        lista_odtwarzania = old_decl;
    }
    decl_ref new_decl = malloc(sizeof(*new_decl));
    if (!new_decl) {
        fprintf(stderr, "Out of memory\n");
        exit(1);
    }
    new_decl->nastepny = 0;
    new_decl->s_ref = s_ref;
    new_decl->level = decl_level;
    set_symtab_value(s_ref, new_decl);
    return new_decl;
}

/* Warstwa 'gen':  generacja kodu maszynowego z reprezentacji jako
   lista instruckcji */

struct obj_instr_item {
    obj_instr nastepny;
    int rodzaj;
    union {
        int label_num;
        int tmp_var_num;
        struct {const char * name; int level;} var;
        struct {long val; obj_instr dst;} dst_val;
        struct {long val; int type;} print_val;
        struct {obj_instr dst; obj_instr arg1; obj_instr arg2;} dst_args;
    };
};

enum obj_node_rodzaj {obj_label, obj_tmp_var, obj_var_ref, obj_const,
     obj_move, obj_plus, obj_minus, obj_times, obj_divide,
     obj_eq, obj_neq, obj_le, obj_lt, obj_unary_minus,
     obj_jump, obj_cond_jump, obj_call, obj_stack_var, obj_print_var, obj_print_val};

obj_instr
make_obj_node(int rodzaj)
{
    obj_instr res = malloc(sizeof(*res));
    if (!res) {
        fprintf(stderr, "Out of memory\n");
        exit(1);
    }
    res->rodzaj = rodzaj;
    res->nastepny = 0;
    return res;
}

int label_counter = 0;
obj_instr
make_label(void)
{
    obj_instr res = make_obj_node(obj_label);
    res->label_num = label_counter;
    label_counter++;
    return res;
}

int tmp_var_counter = 0;
obj_instr
make_tmp_var(void)
{
    obj_instr res = make_obj_node(obj_tmp_var);
    res->label_num = tmp_var_counter;
    tmp_var_counter++;
    return res;
}

obj_instr
make_var_ref(const char * name, int level)
{
    obj_instr res = make_obj_node(obj_var_ref);
    res->var.name = name;
    res->var.level = level;
    return res;
}

void
gen_to_rax(const char * op_name, obj_instr src)
{
    const char * src_name;
    char src_buff[18];
    if (src->rodzaj == obj_tmp_var) {
        sprintf(src_buff, "t%d", src->tmp_var_num);
        src_name = src_buff;
    } else if (src->rodzaj == obj_var_ref) {
        src_name = src->var.name;
    } else if (src->rodzaj == obj_stack_var) {
        sprintf(src_buff, "%d(%%rsp)", src->tmp_var_num);
        src_name = src_buff;
    }
    assert(src_name);
    printf("   %s %s, %%rax\n", op_name, src_name);
}

void
gen_from_prefix(const char * prefix, obj_instr dst)
{
    const char * dst_name;
    char dst_buff[18];
    if (dst->rodzaj == obj_tmp_var) {
        sprintf(dst_buff, "t%d", dst->tmp_var_num);
        dst_name = dst_buff;
    } else if (dst->rodzaj == obj_var_ref) {
        dst_name = dst->var.name;
    } else if (dst->rodzaj == obj_stack_var) {
        sprintf(dst_buff, "%d(%%rsp)", dst->tmp_var_num);
        dst_name = dst_buff;
    }
    assert(dst_name);
    printf("   %s %s\n", prefix, dst_name);
}

void
gen_from_rax(obj_instr dst)
{
    gen_from_prefix("movq %rax,",  dst);
}

void
gen_move(obj_instr dst, obj_instr src)
{
    gen_to_rax("movq", src);
    gen_from_rax(dst);
}

void
gen_const(obj_instr dst, long val)
{
    printf("   movq $%ld, %%rax\n", val);
    gen_from_rax(dst);
}

void
gen_regop(const char * op_name, obj_instr dst,
           obj_instr arg1, obj_instr arg2)
{
    gen_to_rax("movq", arg1);
    gen_to_rax(op_name, arg2);
    gen_from_rax(dst);
}

void
gen_cmp(const char * name, obj_instr dst, obj_instr arg1, obj_instr arg2)
{
    gen_to_rax("movq", arg1);
    gen_to_rax("cmpq", arg2);
    printf("   %s  %%al\n", name);
    printf("   movzbl  %%al, %%eax\n");
    gen_from_rax(dst);
}

void
gen_unary_minus(obj_instr dst, obj_instr arg1)
{
    gen_to_rax("movq", arg1);
    printf("   negq %%rax\n");
    gen_from_rax(dst);
}

void
gen_label(obj_instr lab)
{
    printf(".lab%d:\n", lab->label_num);
};

void
gen_cond_jump(obj_instr lab, obj_instr var)
{
    gen_from_prefix("cmp $0,", var);
    printf("   jz .lab%d\n", lab->label_num);
}

void
gen_jump(obj_instr lab)
{
    printf("   jmp .lab%d\n", lab->label_num);
}

void
gen_call(obj_instr vlab)
{
    printf("   call %s\n", vlab->var.name);
}

void
gen_print_call(const char* var_name, int n)
{
    printf("   movq %s, %%rsi\n", var_name);
    if (n) {
        printf("   movq $formatn, %%rdi\n");
    } else {
        printf("   movq $format, %%rdi\n");
    }
    printf("   movq $0, %%rax\n");
    printf("   call printf\n");
}

void
gen_print_call(int val, int n)
{
    printf("   movq $%d, %%rsi\n", l);
    if (n) {
        printf("   movq $formatn, %%rdi\n");
    } else {
        printf("   movq $format, %%rdi\n");
    }
    printf("   movq $0, %%rax\n");
    printf("   call printf\n");
}

void
gen_header(const char * name, int var_size)
{
   printf("   .text\n");
   printf(".globl %s\n", name);
   printf("   .type   %s, @function\n", name);
   printf("%s:\n", name);
   printf("   subq $%d, %%rsp\n", var_size);
}

void
gen_ret(int var_size)
{
   printf("   addq $%d, %%rsp\n", var_size);
   printf("   ret\n");
}

void
gen_instr(obj_instr instr)
{
    switch(instr->rodzaj) {
        case obj_label:
            gen_label(instr);
            break;
        case obj_jump:
            gen_jump(instr->dst_args.dst);
            break;
        case obj_call:
            gen_call(instr->dst_args.dst);
            break;
        case obj_cond_jump:
            gen_cond_jump(instr->dst_args.dst, instr->dst_args.arg1);
            break;
        case obj_const:
            gen_const(instr->dst_val.dst, instr->dst_val.val);
            break;
        case obj_unary_minus:
            gen_unary_minus(instr->dst_val.dst, instr->dst_args.arg1);
            break;
        case obj_move:
            gen_move(instr->dst_args.dst, instr->dst_args.arg1);
            break;
        case obj_plus:
            gen_regop("addq", instr->dst_args.dst, instr->dst_args.arg1,
                      instr->dst_args.arg2);
            break;
        case obj_minus:
            gen_regop("subq", instr->dst_args.dst, instr->dst_args.arg1,
                      instr->dst_args.arg2);
            break;
        case obj_times:
            gen_regop("imulq", instr->dst_args.dst, instr->dst_args.arg1,
                      instr->dst_args.arg2);
            break;
        case obj_divide:
            printf("   movq $0, %%rdx\n");
            gen_regop("idivq", instr->dst_args.dst, instr->dst_args.arg1,
                      instr->dst_args.arg2);
            break;
        case obj_eq:
            gen_cmp("sete", instr->dst_args.dst, instr->dst_args.arg1,
                      instr->dst_args.arg2);
            break;
        case obj_neq:
            gen_cmp("setne", instr->dst_args.dst, instr->dst_args.arg1,
                      instr->dst_args.arg2);
            break;
        case obj_le:
            gen_cmp("setle", instr->dst_args.dst, instr->dst_args.arg1,
                      instr->dst_args.arg2);
            break;
        case obj_lt:
            gen_cmp("setl", instr->dst_args.dst, instr->dst_args.arg1,
                      instr->dst_args.arg2);
            break;
        case obj_print_var:
            gen_print_call(instr->var.name, instr->var.level);
            break;
        case obj_print_val:
            gen_print_call(instr->print_val.val, instr->print_val.type);
            break;
        default:
            assert(0);
            break;
    }
}

obj_instr
nreverse(obj_instr ilst)
{
    obj_instr res = ilst;
    if (!res) {
        return res;
    }
    ilst = res->nastepny;
    res->nastepny = 0;
    while(ilst) {
        obj_instr pom = ilst;
        ilst = pom->nastepny;
        pom->nastepny = res;
        res = pom;
    }
    return res;
}

obj_instr var_list;

void
enlist_var(obj_instr var)
{
    if (var->nastepny) {
        return;
    }
    var->nastepny = var_list;
    var_list = var;
}

int
generate_variables(obj_instr ilist)
{
    struct obj_instr_item dummy;
    var_list = &dummy;
    for(;ilist; ilist = ilist->nastepny) {
        switch(ilist->rodzaj) {
            /* Nie zawieraja zmiennych */
            case obj_label:
            case obj_jump:
            case obj_call:
            case obj_print:
               break;
            case obj_cond_jump:
               enlist_var(ilist->dst_args.arg1);
               break;
            case obj_const:
               enlist_var(ilist->dst_val.dst);
               break;
            case obj_unary_minus:
            case obj_move:
               break;
            case obj_plus:
            case obj_minus:
            case obj_times:
            case obj_divide:
            case obj_eq:
            case obj_neq:
            case obj_le:
            case obj_lt:
               enlist_var(ilist->dst_args.dst);
               enlist_var(ilist->dst_args.arg1);
               enlist_var(ilist->dst_args.arg2);
               break;
            default:
                assert(0);
                break;
        }
    }
    obj_instr pom;
    int var_cnt = 0;
    for(pom = var_list; pom != &dummy; pom = pom->nastepny) {
        if (pom->rodzaj == obj_tmp_var) {
            var_cnt++;
            pom->rodzaj = obj_stack_var;
        } else if (pom->rodzaj == obj_var_ref) {
            if (decl_level > 0 && pom->var.level == decl_level) {
                var_cnt++;
                pom->rodzaj = obj_stack_var;
            } else if (pom->var.level != 0) {
                fprintf(stderr, "can not access nested variable\n");
                exit(1);
            }
        } else {
            fprintf(stderr, "Internal error: unexpected king of variable\n");
        }
    }
    int i = 0;
    for(pom = var_list; pom != &dummy ; pom = pom->nastepny) {
        if (pom->rodzaj == obj_stack_var) {
            pom->tmp_var_num = 8*i;
            i++;
        }
    }
    return 8*i;
}

obj_instr zmienne_globalne = 0;

void
generuj_zmienne_globalne(void)
{
    obj_instr pom;
    printf("   .data\n");
    for(pom = zmienne_globalne; pom; pom = pom->nastepny) {
        const char * name = pom->var.name;
        printf("%s:\n", name);
        printf(" .quad 0\n");
    }
}

/* Warstwa 'emit':  generacje elementow listy instukcji (czyli
   struktur obj_instr) z parametrow.  Uzywana do przetwarzania
   drzewa rozbioru na liste instukcji.
*/

obj_instr instr_lst;

void
emit_binop(int rodzaj, obj_instr dst, obj_instr arg1, obj_instr arg2)
{
    obj_instr res = make_obj_node(rodzaj);
    res->dst_args.dst = dst;
    res->dst_args.arg1 = arg1;
    res->dst_args.arg2 = arg2;
    res->nastepny = instr_lst;
    instr_lst = res;
}

void
emit_unop(int rodzaj, obj_instr dst, obj_instr arg1)
{
    obj_instr res = make_obj_node(rodzaj);
    res->dst_args.dst = dst;
    res->dst_args.arg1 = arg1;
    res->nastepny = instr_lst;
    instr_lst = res;
}

void
emit_const(obj_instr dst, long val)
{
    obj_instr res = make_obj_node(obj_const);
    res->dst_val.val = val;
    res->dst_val.dst = dst;
    res->nastepny = instr_lst;
    instr_lst = res;
}

void
emit_label(obj_instr label)
{
    assert(label->nastepny == 0);
    label->nastepny = instr_lst;
    instr_lst = label;
}

void
emit_jump(obj_instr label)
{
    emit_unop(obj_jump, label, 0);
}

void
emit_cond_jump(obj_instr val, obj_instr label)
{
    emit_unop(obj_cond_jump, label, val);
}

void
emit_call(obj_instr vlab)
{
    emit_unop(obj_call, vlab, 0);
}

void
emit_print(const char* var_name, int new_line)
{
    obj_instr res = make_obj_node(obj_print);
    res->var.name = var_name;
    res->var.level = new_line;
    res->nastepny = instr_lst;
    instr_lst = res;
}

void
emit_print(int val, int new_line)
{
    obj_instr res = make_obj_node(obj_print);
    res->var.name = var_name;
    res->var.level = new_line;
    res->nastepny = instr_lst;
    instr_lst = res;
}

/* Warstwa 'generuj': przechodzenie drzewa rozbioru i
   generowanie instrucji.  Uzywa wartwe 'emit' i
   'make_tmp_var' z warstwy 'gen' */

obj_instr
get_var_ref(const char * name, decl_ref v_decl)
{
    assert(v_decl->rodzaj == decl_var);
    obj_instr res = v_decl->var_instr;
    if (res) {
        return res;
    }
    res = make_var_ref(name, v_decl->level);
    v_decl->var_instr = res;
    /* Oddzielnie zapamietujemy zmienne, alokujemy wezel obj_instr zeby
       miec list. */
    if (v_decl->level == 0) {
        obj_instr pom = make_var_ref(name, v_decl->level);
        pom->nastepny = zmienne_globalne;
        zmienne_globalne = pom;
    }
    return res;
}

void generuj_expr(sym_tab st, obj_instr var, sem_val expr);

void generuj_expr(sym_tab st, obj_instr dst, sem_val expr)
{
    assert(expr != NULL);
    fprintf(stderr, "generuj_expr\n");
    fprintf(stderr, "expr->nazwa = %s | expr->rodzaj = %d\n",
                    expr->nazwa, expr->rodzaj);
    sem_val arg1, arg2;
    int rodzaj;
    if (expr->rodzaj == SYM_IDENT) {
        symtab_ref s_ref = lookup(st, expr->nazwa, no_alloc);
        void * decl_val = get_symtab_value(s_ref);
        if (!decl_val) {
            fprintf(stderr, "Undeclared constant or variable %s\n",
                    expr->nazwa);
            exit(1);
        }
        decl_ref v_decl = (decl_ref)decl_val;
        if (v_decl->rodzaj == decl_var) {
            obj_instr var2 = get_var_ref(expr->nazwa, v_decl);
            emit_unop(obj_move, dst, var2);
        } else if (v_decl->rodzaj == decl_const) {
            emit_const(dst, v_decl->const_val);
        } else {
            fprintf(stderr, "%s can not appear in expression\n",
                    expr->nazwa);
            exit(1);
        }
    }
    if (expr->rodzaj == SYM_NUMBER) {
        emit_const(dst, expr->wartosc_int);
    }
    if (expr->rodzaj == sym_expression) {
        expr = expr->nastepny;
        rodzaj = expr->wartosc1->rodzaj;
        expr = expr->nastepny;
        arg1 = expr->wartosc1;
        if (rodzaj == SYM_PLUS) {
            generuj_expr(st, dst, arg1);
            return;
        }
        obj_instr var1 = make_tmp_var();
        generuj_expr(st, var1, arg1);
        if (rodzaj == SYM_MINUS) {
            emit_unop(obj_unary_minus, dst, var1);
            return;
        }
        assert(0);
    }
    if (expr->rodzaj == sym_condition || expr->rodzaj == sym_term) {
        expr = expr->nastepny;
        rodzaj = expr->wartosc1->rodzaj;
        expr = expr->nastepny;
        arg1 = expr->wartosc1;
        obj_instr var1 = make_tmp_var();
        generuj_expr(st, var1, arg1);
        arg2 = expr->nastepny->wartosc1;
        obj_instr var2 = make_tmp_var();
        generuj_expr(st, var2, arg2);
        switch(rodzaj) {
            case SYM_PLUS:
                emit_binop(obj_plus, dst, var1,  var2);
                break;
            case SYM_MINUS:
                emit_binop(obj_minus, dst, var1,  var2);
                break;
            case SYM_STAR:
                emit_binop(obj_times, dst, var1,  var2);
                break;
            case SYM_SLASH:
                emit_binop(obj_divide, dst, var1,  var2);
                break;
            case SYM_EQ:
                emit_binop(obj_eq, dst, var1,  var2);
                break;
            case SYM_NEQ:
                emit_binop(obj_neq, dst, var1,  var2);
                break;
            case SYM_LT:
                emit_binop(obj_lt, dst, var1,  var2);
                break;
            case SYM_GT:
                emit_binop(obj_lt, dst, var2, var1);
                break;
            case SYM_LE:
                emit_binop(obj_le, dst, var1, var2);
                break;
            case SYM_GE:
                emit_binop(obj_le, dst, var2, var1);
                break;
            default:
                assert(0);
                break;
        }
    }
}

void generuj_block(sym_tab st, sem_val y);
void generuj_instr(sym_tab st, sem_val y);

void
generuj_instrs(sym_tab st, sem_val y)
{
    fprintf(stderr, "generuj_instrs\n");
    fprintf(stderr, "y->rodzaj == %d\n", y->rodzaj);
    fprintf(stderr, "sym_instrs == %d\n", sym_instrs);
    while(y->rodzaj == sym_instrs) {
        y = y->nastepny;
        generuj_instr(st, y->wartosc1);
        y = y->nastepny->wartosc1;
    }
    generuj_instr(st, y);   
}

void generuj_assign(sym_tab st, sem_val id, sem_val expr)
{
    assert(id != NULL);
    assert(expr != NULL);
    fprintf(stderr, "generuj assign | id->nazwa = %s\n", id->nazwa);
    assert(id->rodzaj == SYM_IDENT);
    symtab_ref v_ref = lookup(st, id->nazwa, no_alloc);
    void * decl_val = get_symtab_value(v_ref);
    if (!decl_val) {
        fprintf(stderr, "Undeclared variable %s\n",
                get_symtab_name(v_ref));
        exit(1);
    }
    decl_ref v_decl = (decl_ref)decl_val;
    if (v_decl->rodzaj != decl_var) {
        fprintf(stderr, "Can not assign to %s\n",
                       get_symtab_name(v_ref));
        exit(1);
    }
    obj_instr var = get_var_ref(id->nazwa, v_decl);
    generuj_expr(st, var, expr);
}

void
generuj_call(sym_tab st, sem_val id)
{
    assert(id->rodzaj == SYM_IDENT);
    symtab_ref p_ref = lookup(st, id->nazwa, no_alloc);
    void * decl_val = get_symtab_value(p_ref);
    if (!decl_val) {
        fprintf(stderr, "Undeclared procedure %s\n",
                get_symtab_name(p_ref));
        exit(1);
    }
    decl_ref p_decl = (decl_ref)decl_val;
    if (p_decl->rodzaj != decl_proc) {
        fprintf(stderr, "Can not call %s\n",
                get_symtab_name(p_ref));
        exit(1);
    }
    obj_instr vlab = make_var_ref(id->nazwa, -1);
    emit_call(vlab);
}

void
generuj_if(sym_tab st, sem_val cond, sem_val instr)
{
    obj_instr l_end = make_label();
    obj_instr cond_var = make_tmp_var();
    generuj_expr(st, cond_var, cond);
    emit_cond_jump(cond_var, l_end);
    generuj_instr(st, instr);
    emit_label(l_end);
}

void
generuj_while(sym_tab st, sem_val cond, sem_val instr)
{
    obj_instr l_end = make_label();
    obj_instr l_start = make_label();
    obj_instr cond_var = make_tmp_var();
    emit_label(l_start);
    generuj_expr(st, cond_var, cond);
    emit_cond_jump(cond_var, l_end);
    generuj_instr(st, instr);
    emit_jump(l_start);
    emit_label(l_end);
}

void generuj_print_var(sym_tab st, sem_val var, int new_line)
{
    emit_print(var->nazwa, new_line);
}

void generuj_print_val(sym_tab st, sem_val val, int new_line)
{
    emit_print(val->wartosc_int, new_line);
}

void handle_vardef(sym_tab st, sem_val y);

void generuj_instr(sym_tab st, sem_val y)
{
    fprintf(stderr, "generuj_instr\n");
    fprintf(stderr, "y->rodzaj == %d\n", y->rodzaj);
    fprintf(stderr, "sym_instr == %d\n", sym_instr);
    assert(y->rodzaj == sym_instr);
    y = y->nastepny;
    sem_val ys = y->wartosc1;
    if (!ys) {
        return;
    }
    sem_val y1 = y->nastepny;
    sem_val w1 = y1->wartosc1;
    sem_val y2 = y1->nastepny;
    fprintf(stderr, "switch | ys->rodzaj = %d\n", ys->rodzaj);
    switch(ys->rodzaj) {
        case SYM_BEGIN:
            generuj_instrs(st, w1);
            break;
        case SYM_ASSIGN:
            generuj_assign(st, w1, y2->wartosc1);
            break;
        case SYM_CALL:
            generuj_call(st, w1);
            break;
        case SYM_IF:
            generuj_if(st, w1, y2->wartosc1);
            break;
        case SYM_WHILE:
            generuj_while(st, w1, y2->wartosc1);
            break;
        case SYM_VAR:
        {
            fprintf(stderr, "case SYM_VAR\n");
            handle_vardef(st, w1);
            break;
        }
        case SYM_PRINT:
        {
            fprintf(stderr, "case SYM_PRINT\n");
            generuj_print(st, w1, 0);
            break;
        }
        case SYM_PRINTLN:
        {
            fprintf(stderr, "case SYM_PRINT\n");
            generuj_print(st, w1, 1);
            break;
        }
    }
}

void handle_constdecl_item(sym_tab st, sem_val y)
{
    fprintf(stderr, "handle_constdecl_item\n");
    fprintf(stderr, "%d\n", y->rodzaj);
    assert(y->rodzaj == sym_constdecl_item);
    y = y->nastepny;
    sem_val y1 = y->wartosc1;
    assert(y1->rodzaj == SYM_IDENT);
    symtab_ref c_ref = lookup(st, y1->nazwa, alloc);
    y = y->nastepny->wartosc1;
    assert(y->rodzaj == SYM_NUMBER);
    decl_ref d_ref = get_new_declaration(c_ref);
    d_ref->rodzaj = decl_const;
    d_ref->const_val = y->wartosc_int;
}

void handle_constdecl(sym_tab st, sem_val y)
{
    fprintf(stderr, "handle_constdecl\n");
    if (!y) {
        return;
    }
    while(y->rodzaj == sym_constdecl_items) {
        y = y->nastepny;
        handle_constdecl_item(st, y->wartosc1);
        y = y->nastepny->wartosc1;
    }
    handle_constdecl_item(st, y);
}

/* y - vardef_item
 * y->nastepny - SYM_ASSIGN
 * y->nastepny->nastepny - SYM_IDENT
 * y->nastepny->nastepny->nastepny - expression
 * */
void handle_vardef_item(sym_tab st, sem_val y)
{
    fprintf(stderr, "handle_vardef_item\n");
    sem_val id = y->nastepny->nastepny;
	fprintf(stderr, "id->rodzaj = %d\n", id->wartosc1->rodzaj);
	fprintf(stderr, "SYM_IDENT = %d\n", SYM_IDENT);
    fprintf(stderr, "id->nazwa = %s\n", id->wartosc1->nazwa);
    assert(id->wartosc1->rodzaj == SYM_IDENT);
    symtab_ref v_ref = lookup(st, id->wartosc1->nazwa, alloc);
    decl_ref d_ref = get_new_declaration(v_ref);
    d_ref->rodzaj = decl_var;
    d_ref->type_val = type_int;
    generuj_assign(st, id->wartosc1, id->nastepny->wartosc1);
    /* void generuj_assign(sym_tab st, sem_val id, sem_val expr) */
}

void handle_vardef(sym_tab st, sem_val y)
{
    fprintf(stderr, "handle_vardef\n");
    if (!y) {
        return;
    }
    while(y->rodzaj == sym_vardef_items) {
        y = y->nastepny;
        handle_vardef_item(st, y->wartosc1);
        y = y->nastepny->wartosc1;
    }
    handle_vardef_item(st, y);
}

void handle_procdecl(sym_tab st, sem_val y);

void start_proc(sym_tab st, sem_val y)
{
    assert(y->rodzaj == SYM_IDENT);
    const char * name = y->nazwa;
    fprintf(stderr, "starting proc %s\n", name);
    symtab_ref p_ref = lookup(st, name, alloc);
    decl_ref d_ref = get_new_declaration(p_ref);
    d_ref->rodzaj = decl_proc;
    d_ref->type_val = type_proc;
    pushdecls();
}

void finish_proc2(sym_tab st, const char * name)
{    
    fprintf(stderr, "finish_proc2\n");
    instr_lst = nreverse(instr_lst);
    int var_size = generate_variables(instr_lst);
    gen_header(name, var_size);
    while(instr_lst) {
        gen_instr(instr_lst);
        instr_lst = instr_lst->nastepny;
    }
    gen_ret(var_size);
    popdecls();
}

void finish_proc(sym_tab st, sem_val y)
{
    fprintf(stderr, "finish_proc\n");
    assert(y->rodzaj == SYM_IDENT);
    const char * name = y->nazwa;
    fprintf(stderr, "finishing proc %s\n", name);
    finish_proc2(st, name);
}

void
handle_procdecl_item(sym_tab st, sem_val y)
{
    fprintf(stderr, "handle_procdecl_item\n");
    assert(y->rodzaj == sym_procdecl_item);
    y = y->nastepny;
    start_proc(st, y->wartosc1);
    generuj_block(st, y->nastepny->wartosc1);
    finish_proc(st, y->wartosc1);
}

void
handle_procdecl(sym_tab st, sem_val y)
{
    fprintf(stderr, "handle_procdecl\n");
    if (!y) {
        return;
    }
    while(y->rodzaj == sym_procdecl) {
        y = y->nastepny;
        handle_procdecl_item(st, y->wartosc1);
        y = y->nastepny->wartosc1;
    }
    handle_procdecl_item(st, y);
}

void
generuj_block(sym_tab st, sem_val y)
{
    fprintf(stderr, "generuj_block\n");
    fprintf(stderr, "generuj_block | y->rodzaj = %d\n", y->rodzaj);
    assert(y->rodzaj == sym_block);
    y = y->nastepny;
    fprintf(stderr, "generuj_block | y->rodzaj = %d\n", y->rodzaj);
    handle_constdecl(st, y->wartosc1);
    y = y->nastepny->wartosc1;
    fprintf(stderr, "generuj_block | y->rodzaj = %d\n", y->rodzaj);
    assert(y->rodzaj == sym_blockv);
    y = y->nastepny;
    fprintf(stderr, "generuj_block | y->rodzaj = %d\n", y->rodzaj);
    handle_vardef(st, y->wartosc1);
    y = y->nastepny->wartosc1;
    fprintf(stderr, "generuj_block | y->rodzaj = %d\n", y->rodzaj);
    assert(y->rodzaj == sym_blockp);
    y = y->nastepny;
    fprintf(stderr, "generuj_block | y->rodzaj = %d\n", y->rodzaj);
    handle_procdecl(st, y->wartosc1);
    y = y->nastepny->wartosc1;
    fprintf(stderr, "generuj_block | y->rodzaj = %d\n", y->rodzaj);
    generuj_instr(st,  y);
}

void generuj_format_printf()
{
    printf("%s\n", "format:\n   .string \"%d\"");
    printf("%s\n", "formatn:\n   .string \"%d\\n\"");
}

void
generuj_program(sym_tab st, sem_val y)
{
    generuj_block(st, y);
    finish_proc2(st, "main");
    generuj_zmienne_globalne();
    generuj_format_printf();
}
