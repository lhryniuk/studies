#ifndef SYMTAB_H
#define SYMTAB_H 0
/* Wskaznik wewnatarz struktury aby uzyskac kontrole typow */
typedef struct {void * tab;} sym_tab;
typedef struct {void * ref;} symtab_ref;

typedef enum {no_alloc, alloc,} alloc_type;

extern symtab_ref
lookup(sym_tab st, const char * name, alloc_type at);

extern void *
get_symtab_value(symtab_ref s_ref);

extern const char *
get_symtab_name(symtab_ref s_ref);

extern void
set_symtab_value(symtab_ref s_ref, void * new_val);

extern sym_tab tablica_symboli;
#endif
