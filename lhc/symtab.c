#include "symtab.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NAME_LENGTH 30
#define MAX_NAMES 500
typedef struct {char name[MAX_NAME_LENGTH]; void * val;} symtab_pos;

/* Na razie nie uzywana */
sym_tab tablica_symboli = {0};

symtab_pos symtab[MAX_NAMES];
int symtab_used = 0;

void *
get_symtab_value(symtab_ref s_ref)
{
    symtab_pos * pos = (symtab_pos *)(s_ref.ref);
    return pos->val;
}

const char *
get_symtab_name(symtab_ref s_ref)
{
    symtab_pos * pos = (symtab_pos *)(s_ref.ref);
    return pos->name;
}

void
set_symtab_value(symtab_ref s_ref, void * new_val)
{
    symtab_pos * pos = (symtab_pos *)(s_ref.ref);
    pos->val = new_val;
}

symtab_ref
lookup(sym_tab st, const char * name, alloc_type at)
{
    if (strlen(name)>= MAX_NAME_LENGTH) {
        fprintf(stderr, "name too long: %s\n", name);
        exit(1);
    }
    int i;
    for(i = 0; i< symtab_used; i++) {
        if(!strcmp(symtab[i].name, name)) {
            symtab_ref res = {&(symtab[i])};
            return res;
        }
    }
    if (at == no_alloc) {
        symtab_ref res = {0};
        return res;
    }
    if (symtab_used == MAX_NAMES) {
        fprintf(stderr, "too many names (max %d)\n", MAX_NAMES);
        exit(1);
    }
    strcpy(symtab[symtab_used].name, name);
    symtab[symtab_used].val = 0;
    symtab_ref res = {&(symtab[symtab_used])};
    symtab_used++;
    return res;
}
